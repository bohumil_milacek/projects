package com.company;

public abstract class Node {

    protected Node leftNode;
    protected Node rightNode;

    public void setLeftNode(Node n){
        leftNode = n;
    }

    public void setRightNode(Node n){
        rightNode = n;
    }

    public abstract int evaulate();
}

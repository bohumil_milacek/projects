package com.company;

public class FunctionNode extends Node {
    private FunctionType func;

    public FunctionNode(FunctionType func){
        this.func = func;
    }

    @Override
    public int evaulate(){
        int leftEval = 0;
        int rightEval = 0;

        if(this.leftNode != null)
            leftEval = this.leftNode.evaulate();
        if(this.rightNode != null)
            rightEval = this.rightNode.evaulate();


        int result = 0;

        switch(func){
            case ADD:
                result = leftEval + rightEval;
                break;
            case MULT:
                System.out.println("xxx");
                result = leftEval * rightEval;
                break;
        }
        return result;
    }

}

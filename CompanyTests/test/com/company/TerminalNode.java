package com.company;

public class TerminalNode extends Node {

    int value;

    public TerminalNode(int val){
        value = val;
    }

    public void setValue(int val){
        value = val;
    }

    @Override
    public int evaulate(){
        return value;
    }


}

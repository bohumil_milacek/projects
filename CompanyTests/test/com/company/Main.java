package com.company;

public class Main {

    public static void main(String[] args) {
        Tree t = new Tree();

        Node n = new FunctionNode(FunctionType.MULT);
        n.setLeftNode(new TerminalNode(2));
        n.setRightNode(new TerminalNode(3));

        System.out.println(n.evaulate());

    }
}

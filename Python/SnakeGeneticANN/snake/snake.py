import random, pygame, math, numpy as np, numpy.linalg as la
from enum import Enum
from ai.neural_network import NeuralNetwork
import config


class Direction(Enum):
    UP = 0
    RIGHT = 1
    DOWN = 2
    LEFT = 3

    @staticmethod
    def random():
        return random.choice([Direction.UP, Direction.RIGHT, Direction.DOWN, Direction.LEFT])


def map_output(output):
    min_index = output.argmax(axis=0)
    return (Direction.UP, Direction.DOWN, Direction.LEFT, Direction.RIGHT)[min_index]


def distance(v1, v2):
    return math.sqrt((v1[0] - v2[0]) ** 2 + (v1[1] - v2[1]) ** 2)


class Snake:
    size = 10

    EAT_SCORE = 20
    GOOD_DIRECTION_SCORE = 1
    BAD_DIRECTION_SCORE = 1  # -1.5
    MOVEMENTS_PER_FOOD = 100

    def __init__(self, ann=None):
        self.score = 0
        self.movements_left = config.SNAKE_MOVEMENTS
        self.direction = Direction.random()
        x = int(config.WIDTH / 2)
        y = int(config.HEIGHT / 2)
        self.body = [(x, y)]
        self.ai = ann
        if ann is None:
            self.ai = NeuralNetwork(24, 4, 1)
        self.can_change_direction = True

        self.steps = 0
        self.food_count = 1

    @staticmethod
    def random_food():
        x = random.randint(0, config.WIDTH - Snake.size)
        y = random.randint(0, config.HEIGHT - Snake.size)
        return x, y

    def crossover(self, parent):
        ann = self.ai.crossover(parent.ai)
        return Snake(ann)

    def reset(self):
        x = int(config.WIDTH / 2)
        y = int(config.HEIGHT / 2)
        self.body = [(x, y)]
        self.direction = Direction.random()
        self.can_change_direction = True
        self.movements_left = config.SNAKE_MOVEMENTS
        self.score = 0

        self.steps = 0
        self.food_count = 1

    def move(self, food):
        self.movements_left -= 1
        dist_before = distance(self.body[0], food)

        ann_in = self.ann_input(food)
        ann_out = self.ai.feedforward(ann_in)
        direction = map_output(ann_out)
        self.set_direction(direction)

        for i in range(len(self.body) - 1, 0, -1):
            self.body[i] = self.body[i - 1]
        x, y = self.body[0]
        if self.direction == Direction.UP:
            self.body[0] = (x, y - 1)
        elif self.direction == Direction.RIGHT:
            self.body[0] = (x + 1, y)
        elif self.direction == Direction.LEFT:
            self.body[0] = (x - 1, y)
        else:
            self.body[0] = (x, y + 1)
        self.can_change_direction = True

        dist_after = distance(self.body[0], food)
        if dist_after < dist_before:
            self.steps += 1
        else:
            self.steps += -1.5

    def calculate_fitness(self):
        self.score = (self.steps ** 2) * (2 ** self.food_count)

    def eat(self):
        self.food_count += 1
        self.movements_left += Snake.MOVEMENTS_PER_FOOD
        x, y = self.body[-1]
        if self.direction == Direction.UP:
            self.body.append((x, y + Snake.size))
        elif self.direction == Direction.RIGHT:
            self.body.append((x - Snake.size, y))
        elif self.direction == Direction.LEFT:
            self.body.append((x + Snake.size, y))
        else:
            self.body.append((x, y - Snake.size))

    def increase_score(self, score):
        self.score += score

    def draw(self, draw, block_size, screen, color, space):
        for body_part in self.body:
            x, y = body_part
            draw.rect(screen, color, pygame.Rect(x * block_size + x * space, y * block_size + y * space, Snake.size, Snake.size))

    def draw_ann(self, draw, screen):
        self.ai.draw(draw, screen)

    def set_direction(self, direction):
        self.can_change_direction = False
        if ((direction == Direction.UP and self.direction != Direction.DOWN)
                or (direction == Direction.DOWN and self.direction != Direction.UP)
                or (direction == Direction.RIGHT and self.direction != Direction.LEFT)
                or (direction == Direction.LEFT and self.direction != Direction.RIGHT)):
            self.direction = direction

    def get_head_x(self):
        return self.body[0][0]

    def get_head_y(self):
        return self.body[0][1]

    def eats_its_body(self):
        x, y = self.body[0]
        if (x, y) in self.body[1:]:
            return True
        return False

    def look_in_direction(self, vx, vy, food):
        x, y = self.body[0]
        actual_distance = 0
        body_distance = 0

        body_found = False
        food_found = False
        x += vx
        y += vy
        while 0 <= x < config.WIDTH and 0 <= y < config.HEIGHT:
            if not body_found and (x, y) in self.body:
                body_found = True
                body_distance = actual_distance
            if (x, y) == food:
                food_found = True
            x += vx
            y += vy
            actual_distance += 1

        if body_distance == 0 and actual_distance == 0:
            return 0, 0, int(food_found)
        elif body_distance == 0:
            return 1 / actual_distance, 0, int(food_found)
        elif actual_distance == 0:
            return 0, 1 / body_distance, int(food_found)
        return 1 / actual_distance, 1 / body_distance, int(food_found)

    def contains_obstacle(self, x, y, width, height):
        if x < 0 or x >= width or y < 0 or y >= height:
            return int(True)
        for (bx, by) in self.body:
            if bx == x and by == y:
                return int(True)
        return int(False)

    def get_food_angle(self, food):
        hx, hy = self.body[0]
        fx, fy = food
        fy = config.HEIGHT - fy
        hy = config.HEIGHT - hy
        delta_x = fx - hx
        delta_y = fy - hy

        # snake direction vector
        v_food = [delta_x, delta_y]
        if self.direction == Direction.LEFT:
            v_snake = [-1, 0]
        elif self.direction == Direction.RIGHT:
            v_snake = [1, 0]
        elif self.direction == Direction.DOWN:
            v_snake = [0, -1]
        else:
            v_snake = [0, 1]

        # check if oposite direction
        if ((v_food[0] == 0 and np.sign(v_food[1]) == -np.sign(v_snake[1])) or
                (v_food[1] == 0 and np.sign(v_food[0]) == -np.sign(v_snake[0]))):
            return 180

        # get the angle in degrees (-180, 180) (left, right)
        sign = np.sign(la.det([v_food, v_snake]))
        cosang = np.dot(v_food, v_snake)
        sinang = la.norm(np.cross(v_food, v_snake))
        return ((np.arctan2(sinang, cosang) * 180) / math.pi) * sign

    def ann_input(self, food):
        # 0 - left obstacle, 1 - top obstacle, 2 - right obstace, 4 - angle to food
        values = []
        values.extend(self.look_in_direction(0, -1, food))
        values.extend(self.look_in_direction(1, -1, food))
        values.extend(self.look_in_direction(1, 0, food))
        values.extend(self.look_in_direction(1, 1, food))
        values.extend(self.look_in_direction(0, 1, food))
        values.extend(self.look_in_direction(-1, 1, food))
        values.extend(self.look_in_direction(-1, 0, food))
        values.extend(self.look_in_direction(-1, -1, food))
        # values.append(self.get_food_angle(food) / 180)
        return values

    def __eq__(self, other):
        return (self.score == other.score and
                self.movements_left == other.movements_left and
                self.direction == other.direction and
                self.body == other.body and
                self.ai == other.ai and
                self.can_change_direction == other.can_change_direction and
                self.steps == other.steps and
                self.food_count == other.food_count)

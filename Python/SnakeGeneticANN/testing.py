from ai.neural_network import NeuralNetwork
from snake.snake import Snake, Direction
import main
import config
import pickle
from game.game import Game
import random
import numpy as np


def test_ann_same_output_on_input():
    """
    Tests weather ann is deterministic on same input
    """
    ann = NeuralNetwork(24, 4, 1)
    input = [random.uniform(0, 1)] * 24
    out1 = ann.feedforward(input)
    out2 = ann.feedforward(input)
    for x, y in zip(out1, out2):
        if x != y:
            assert False
    assert True


def test_picke_correct_save_load():
    """
    Test whether pickle saves and loads data and ann networks behave the same
    """
    population = [Snake() for _ in range(config.POPULATION_SIZE)]
    main.save_generation(population)
    loaded_population = main.load_generation()

    input = [random.uniform(0, 1)] * 24
    for snake1, snake2 in zip(population, loaded_population):
        if not np.array_equal(snake1.ai.feedforward(input), snake2.ai.feedforward(input)):
            assert False
    assert True


def test_picke_correct_ann_save_load():
    """
    Test whether pickle saves and loads same data
    """
    population = [Snake() for _ in range(config.POPULATION_SIZE)]
    main.save_generation(population)
    loaded_population = main.load_generation()
    assert pickle.dumps(population) == pickle.dumps(loaded_population)


def test_game_walls():
    """
    Test whether game has correct wall collisions
    """
    snake1 = Snake()
    game1 = Game(snake1)
    snake1.body[0] = (-1, snake1.get_head_y())
    game1.check_walls()

    snake2 = Snake()
    game2 = Game(snake2)
    snake2.body[0] = (config.WIDTH + 1, snake2.get_head_y())
    game2.check_walls()

    snake3 = Snake()
    game3 = Game(snake3)
    snake3.body[0] = (snake3.get_head_x(), -1)
    game3.check_walls()

    snake4 = Snake()
    game4 = Game(snake4)
    snake4.body[0] = (snake3.get_head_x(), config.HEIGHT + 1)
    game4.check_walls()

    assert not game1.game_running and not game2.game_running and not game3.game_running and not game4.game_running


def test_snake_eats():
    """
    Test whether game adds food when snake eats and on correct position
    """
    snake = Snake()
    food_count = 50
    last_body = snake.body[0]
    for _ in range(food_count):
        snake.eat()
        new_body = snake.body[-1]
        if snake.direction == Direction.UP:
            if last_body[0] != new_body[0] or last_body[1] + Snake.size != new_body[1]:
                assert False
        elif snake.direction == Direction.DOWN:
            if last_body[0] != new_body[0] or last_body[1] - Snake.size != new_body[1]:
                assert False
        elif snake.direction == Direction.RIGHT:
            if last_body[0] - Snake.size != new_body[0] or last_body[1] != new_body[1]:
                assert False
        elif snake.direction == Direction.LEFT:
            if last_body[0] + Snake.size != new_body[0] or last_body[1] != new_body[1]:
                assert False
        last_body = new_body
        snake.direction = Direction.random()
    assert True

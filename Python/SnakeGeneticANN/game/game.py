import config
import pygame
import random
from snake.snake import Direction


def generate_food():
    return random.randrange(0, config.WIDTH), random.randrange(0, config.HEIGHT)


class Game:

    def __init__(self, snake):
        self.game_running = True
        self.snake = snake
        self.food = generate_food()
        self.screen = None
        self.snake_screen = None
        self.ann_screen = None
        self.snake_camera = None
        self.ann_camera = None

    def start_with_gui(self):
        self.start_window()
        clock = pygame.time.Clock()
        while self.game_running:
            self.window_events()
            self.window_update()
            self.check_walls()
            self.check_body()
            self.check_food()
            self.move()
            clock.tick(config.FPS)
        pygame.quit()

    def start(self):
        while self.game_running:
            self.check_walls()
            self.check_body()
            self.check_food()
            self.move()
        self.snake.calculate_fitness()

    def start_window(self):
        pygame.init()

        snake_res = config.SNAKE_RES
        screen_res = config.SCREEN_RES

        self.screen = pygame.display.set_mode(screen_res)
        canvas = pygame.Surface(screen_res)

        self.snake_camera = pygame.Rect(config.BORDER, config.BORDER, snake_res[0], snake_res[1])
        self.ann_camera = pygame.Rect(screen_res[0] - config.ANN_WIDTH, 0, config.ANN_WIDTH, screen_res[1])
        self.snake_screen = canvas.subsurface(self.snake_camera)
        self.ann_screen = canvas.subsurface(self.ann_camera)

        canvas.fill((255, 255, 255))
        self.snake_screen.fill(config.BG)
        self.ann_screen.fill(config.ANN_BG)

        self.screen.blit(canvas, (0, 0))
        self.__update_screen()

    def window_update(self):
        self.snake_screen.fill(config.BG)
        self.ann_screen.fill(config.ANN_BG)

        self.snake.draw(pygame.draw, config.BLOCK_SIZE, self.snake_screen, config.ORANGE, config.SPACE)
        pygame.draw.rect(self.snake_screen, config.PINK, self.__food_rect())
        self.snake.draw_ann(pygame.draw, self.ann_screen)
        self.__update_screen()

    def check_walls(self):
        if self.snake.get_head_x() < 0 or self.snake.get_head_x() >= config.WIDTH or self.snake.get_head_y() < 0 or self.snake.get_head_y() >= config.HEIGHT:
            self.game_running = False

    def check_body(self):
        if self.snake.eats_its_body():
            self.game_running = False

    def check_food(self):
        if self.snake.get_head_x() == self.food[0] and self.snake.get_head_y() == self.food[1]:
            self.snake.eat()
            self.food = generate_food()

    def move(self):
        if self.snake.movements_left == 0:
            self.game_running = False
        else:
            self.snake.move(self.food)

    def window_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.game_running = False

        pressed = pygame.key.get_pressed()
        if pressed[pygame.K_UP]:
            self.snake.set_direction(Direction.UP)
        if pressed[pygame.K_DOWN]:
            self.snake.set_direction(Direction.DOWN)
        if pressed[pygame.K_LEFT]:
            self.snake.set_direction(Direction.LEFT)
        if pressed[pygame.K_RIGHT]:
            self.snake.set_direction(Direction.RIGHT)

    def __food_rect(self):
        return pygame.Rect(self.food[0] * config.BLOCK_SIZE + self.food[0] * config.SPACE, self.food[1] * config.BLOCK_SIZE + self.food[1] * config.SPACE, config.BLOCK_SIZE, config.BLOCK_SIZE)

    def __update_screen(self):
        self.screen.blit(self.snake_screen, self.snake_camera)
        self.screen.blit(self.ann_screen, self.ann_camera)
        pygame.display.update()

import random
import numpy as np
import math
import config
import pygame


def sigmoid(x):
    return 1 / (1 + math.exp(-x))


def relu(x):
    return x * (x > 0)


class NeuralNetwork:

    def __init__(self, input_count, output_count, hidden_layers, biases=None, weights=None):
        self.input_count = input_count
        self.output_count = output_count
        self.hidden_layers = hidden_layers
        self.neuron_count = int((2 / 3) * input_count) + output_count
        self.synapse_layer_count = hidden_layers + 1
        # biases for each synapse layer
        neuron_count = int((2 / 3) * input_count) + output_count
        self.biases = biases
        self.weights = weights
        if biases is None:
            self.biases = [np.random.rand(neuron_count) for _ in range(hidden_layers)] + [np.random.rand(output_count)]
        if weights is None:
            self.weights = [np.random.uniform(- 0.2, 0.2, (neuron_count, input_count))]
            for i in range(hidden_layers - 1):
                self.weights.append(np.random.uniform(- 0.2, 0.2, (neuron_count, neuron_count)))
            self.weights.append(np.random.uniform(- 0.2, 0.2, (output_count, neuron_count)))
        self.signals = []

    def feedforward(self, input_data):
        tmp = input_data
        self.signals = []
        self.signals.append(tmp)
        for weights, bias in zip(self.weights[:-1], self.biases[:-1]):
            tmp = relu(np.dot(weights, tmp) + bias)
            self.signals.append(tmp)
        output = np.array([sigmoid(x) for x in np.dot(self.weights[-1], tmp) + self.biases[-1]])
        self.signals.append(output)
        return output

    def crossover(self, ann):
        return self.uniform_crossover(ann)

    def single_point_crossover(self, ann):
        biases = []
        for layer_bias1, layer_bias2 in zip(self.biases, ann.biases):
            border = random.randrange(0, len(layer_bias1))
            bias = []
            # crossover k-point
            for i, (bias1, bias2) in enumerate(zip(layer_bias1, layer_bias2)):
                if i < border:
                    bias.append(bias1)
                else:
                    bias.append(bias2)

                # mutation
                for _ in range(int(math.ceil(len(bias) * config.MUTATION_RATE))):
                    bias[random.randrange(len(bias))] = random.uniform(-1, 1)

            biases.append(np.array(bias))

        weights = []
        for mat1, mat2 in zip(self.weights, ann.weights):
            matrix = []
            row_b = random.randrange(0, len(mat1))
            col_b = random.randrange(0, len(mat1[0]))

            for row, (row1, row2) in enumerate(zip(mat1, mat2)):
                matrix_row = []
                # crossover k-point
                for col, (weight1, weight2) in enumerate(zip(row1, row2)):
                    if row < row_b or (row == row_b and col < col_b):
                        matrix_row.append(weight1)
                    else:
                        matrix_row.append(weight2)

                    for _ in range(random.randint(0, int(math.ceil(len(matrix_row) * config.MUTATION_RATE)))):
                        index = random.randrange(len(matrix_row))
                        matrix_row[index] = random.uniform(-1, 1)

                matrix.append(np.array(matrix_row))
            weights.append(np.array(matrix))
        return NeuralNetwork(self.input_count, self.output_count, self.hidden_layers, biases, weights)

    def uniform_crossover(self, ann):
        biases = []
        for layer_bias1, layer_bias2 in zip(self.biases, ann.biases):
            bias = []
            for i, (bias1, bias2) in enumerate(zip(layer_bias1, layer_bias2)):
                if random.uniform(0, 1) < 0.5:
                    bias.append(bias1)
                else:
                    bias.append(bias2)
                for _ in range(int(math.ceil(len(bias) * config.MUTATION_RATE))):
                    bias[random.randrange(len(bias))] = random.uniform(-1, 1)
            biases.append(np.array(bias))

        weights = []
        for mat1, mat2 in zip(self.weights, ann.weights):
            matrix = []
            for row, (row1, row2) in enumerate(zip(mat1, mat2)):
                matrix_row = []
                for col, (weight1, weight2) in enumerate(zip(row1, row2)):
                    if random.uniform(0, 1) < 0.5:
                        matrix_row.append(weight1)
                    else:
                        matrix_row.append(weight2)

                    for _ in range(random.randint(0, int(math.ceil(len(matrix_row) * config.MUTATION_RATE)))):
                        index = random.randrange(len(matrix_row))
                        matrix_row[index] = random.uniform(-1, 1)

                matrix.append(np.array(matrix_row))
            weights.append(np.array(matrix))
        return NeuralNetwork(self.input_count, self.output_count, self.hidden_layers, biases, weights)

    def arithmetic_crossover(self, ann):
        biases = []
        for layer_bias1, layer_bias2 in zip(self.biases, ann.biases):
            bias = []
            for i, (bias1, bias2) in enumerate(zip(layer_bias1, layer_bias2)):
                bias.append((bias1 + bias2) / 2)
            for _ in range(int(math.ceil(len(bias) * config.MUTATION_RATE))):
                bias[random.randrange(len(bias))] = random.uniform(-1, 1)
            biases.append(np.array(bias))

        weights = []
        for mat1, mat2 in zip(self.weights, ann.weights):
            matrix = []
            for row, (row1, row2) in enumerate(zip(mat1, mat2)):
                matrix_row = []
                for col, (weight1, weight2) in enumerate(zip(row1, row2)):
                    matrix_row.append((weight1 + weight2) / 2)
                for _ in range(random.randint(0, int(math.ceil(len(matrix_row) * config.MUTATION_RATE)))):
                    index = random.randrange(len(matrix_row))
                    matrix_row[index] = random.uniform(-1, 1)

                matrix.append(np.array(matrix_row))
            weights.append(np.array(matrix))
        return NeuralNetwork(self.input_count, self.output_count, self.hidden_layers, biases, weights)

    @staticmethod
    def generate(input_count, output_count, hidden_layers):
        # biases for each synapse layer
        neuron_count = int((2 / 3) * input_count) + output_count
        synapse_layer_biases = []
        for i in range(hidden_layers):
            synapse_layer_biases.append(np.random.rand(neuron_count))
        synapse_layer_biases.append(np.random.rand(output_count))

        # weights for each synapse layer
        synapse_layer_weights = [np.random.uniform(-1, 1, (neuron_count, input_count))]
        for i in range(hidden_layers - 1):
            synapse_layer_weights.append(np.random.uniform(-1, 1, (neuron_count, neuron_count)))
        synapse_layer_weights.append(np.random.uniform(-1, 1, (output_count, neuron_count)))
        return NeuralNetwork(input_count, output_count, hidden_layers, synapse_layer_biases, synapse_layer_weights)

    def print(self):
        print("_______________________________________________________________________________________________________")
        print("BIASES:______")
        for x in self.biases:
            print(np.array_str(x, precision=2, suppress_small=True))
        print("WEIGHTS:______")
        for matrix in self.weights:
            print(np.array_str(matrix, precision=2, suppress_small=True))
        print("_______________________________________________________________________________________________________")

    def draw(self, draw, screen):
        pygame.font.init()
        max_signal = (max(map(max, self.signals)))
        for neurons_layer_number, signals in enumerate(self.signals):
            layer_center = (config.SCREEN_RES[1] - len(signals) * (config.NEURON_SIZE * 2 + config.NEURON_GAP)) / 2
            layer_x = int(config.ANN_LEFT_GAP + neurons_layer_number * (config.ANN_WIDTH - config.ANN_LEFT_GAP) / len(self.signals))
            if neurons_layer_number == 0:
                self.__draw_ann_input_description(screen, len(signals), layer_center)
            elif neurons_layer_number == len(self.signals) - 1:
                self.__draw_ann_output_description(screen, layer_center, layer_x)
            # DRAWING NEURONS__________________________________________________
            for neuron_layer_position, signal in enumerate(signals):
                neuron_center = (layer_x,
                                 int(layer_center + (config.NEURON_SIZE * 2 + config.NEURON_GAP) * neuron_layer_position))
                neuron_color = (255 * signal / max_signal, 255 * signal / max_signal, 255 * signal / max_signal)
                # DRAWING NEURON SYNAPSES__________________________________________________
                if neurons_layer_number != len(self.weights):
                    for next_layer_neuron_position, weights in enumerate(self.weights[neurons_layer_number]):
                        next_layer_center = (config.SCREEN_RES[1] - len(self.weights[neurons_layer_number]) * (config.NEURON_SIZE * 2 + config.NEURON_GAP)) / 2
                        next_neuron_center = (int(config.ANN_LEFT_GAP + (neurons_layer_number + 1) * (config.ANN_WIDTH - config.ANN_LEFT_GAP) / len(self.signals)),
                                              int(next_layer_center + (config.NEURON_SIZE * 2 + config.NEURON_GAP) * next_layer_neuron_position))
                        draw.line(screen, config.ANN_SYNAPSES, neuron_center, next_neuron_center)
                draw.circle(screen, neuron_color, neuron_center, config.NEURON_SIZE)

    def __draw_ann_input_description(self, screen, neurons_count, layer_center):
        input_type = ["wall distance", "body distance", "food found"]
        input_direction = ["0, -1", "1, -1", "1, 0", "1, 1", "0, 1", "-1, 1", "-1, 0", "-1, -1"]

        for neuron_position in range(neurons_count):
            font = pygame.font.Font(pygame.font.get_default_font(), 10)
            text_surface = font.render(f"{input_type[neuron_position % 3]}({input_direction[neuron_position % 8]})", False, config.ANN_SYNAPSES)
            neuron_center = (config.INPUT_LEFT_GAP,
                             int((layer_center + (config.NEURON_SIZE * 2 + config.NEURON_GAP) * neuron_position) - text_surface.get_height() / 2))
            screen.blit(text_surface, dest=neuron_center)

    def __draw_ann_output_description(self, screen, layer_center, x):
        output = ["UP", "DOWN", "LEFT", "RIGHT"]
        for neuron_position in range(4):
            font = pygame.font.Font(pygame.font.get_default_font(), 10)
            text_surface = font.render(output[neuron_position], False, config.ANN_SYNAPSES)
            neuron_center = (x + config.NEURON_SIZE * 2,
                             int((layer_center + (config.NEURON_SIZE * 2 + config.NEURON_GAP) * neuron_position) - text_surface.get_height() / 2))
            screen.blit(text_surface, dest=neuron_center)

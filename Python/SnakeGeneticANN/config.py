WIDTH = 50
HEIGHT = 50
# _______________________________________________________
CONF_PATH = 'conf/config.dat'
# _______________________________________________________

POPULATION_SIZE = 100
SURVIVE_RATE = 0.10  # top 10% survive
CROSSOVER_RATE = 0.15  # top 40% will crossover
MUTATION_RATE = 0.05
SNAKE_MOVEMENTS = 4 * (WIDTH + HEIGHT) / 2
FPS = 15
# _______________________________________________________
ANN_WIDTH = 400
ANN_LEFT_GAP = 120

NEURON_SIZE = 10
NEURON_GAP = 2

INPUT_LEFT_GAP = 10

# _______________________________________________________
BLOCK_SIZE = 10
SPACE = 2
BORDER = 1

SNAKE_RES = (WIDTH * (BLOCK_SIZE + SPACE) - SPACE, HEIGHT * (BLOCK_SIZE + SPACE) - SPACE)
SCREEN_RES = (SNAKE_RES[0] + 2 * BORDER + ANN_WIDTH, SNAKE_RES[1] + 2 * BORDER)
# _______________________________________________________
BG = (42, 57, 80)
VIOLET = (151, 64, 99)
PINK = (245, 71, 104)
ORANGE = (255, 150, 120)
BLACK = (0, 0, 0)
ANN_BG = (82, 97, 120)
ANN_SYNAPSES = (142, 157, 180)

# Snake AI

### Popis programu
* V první fázi se program zapne (metoda main v souboru "main.py")
* Následuje otevření GUI s výberem 1 nebo 3 možnosti
    * Start         - Program se spustí
    * Load Config   - Program načte konfiguraci posledního běhu programu a spustí se
    * Run Config    - Program načte poslední konfiguraci a odreprezentuje ji
* V další fázi se začnou odehrávat na pozadí hry "Had", vytvoří se vždy několik generací a ty hry odehraji
* Poté se provádí genetický algoritmus
* Každých 10 generací se odprezentuje tím, že se spustí GUI kde je vizualizace hada a neuronky

### Závislosti
- Veškeré potřebné závislosti a balíčky s verzemi, které byly využity při tvorbě jsou v souboru "requirements.txt"


- Numpy - reprezentace ANN
- Pygame - vykreslování hry
- Pickle - ukládání a načítání konfigurací hry 

### Spuštění programu
1. vytvoření virtuálního prostředí  
`$ virtualenv venv`  
1. zapnout virtuální prostředí  
`$ source venv/bin/activate`
1. nainstalovat závislosti  
`(venv)$ pip3 install -r requirements.txt`
1. spustit program  
`(venv)$ python3 main.py` 

### Spuštění testů
1. Předpokládáme, že máme virtuální prostředí vytvořené a zapnuté  
`(venv)$ pytest testing.py`  
 
 
PS: doporučuji používat `python3` a `pip3`, mohlo by dojít se zámněnou Python2  

### Soubory a složiky
- složka `conf` obsahuje uloženou konfiguraci s generacemi hadů
- soubor `config.py` obsahuje konfiguraci pro progam, lze zde měnit různé parametry, například ladit Genetický algoritmus

 

import os.path
import pickle
import sys
import threading

from pygame.locals import *

from game.game import *
from snake.snake import Snake


# returns True to load config or False to start new
def start_window():
    config_exits = os.path.exists(config.CONF_PATH)
    pygame.init()
    screen_res = (400, 400)
    screen = pygame.display.set_mode(screen_res)
    screen.fill((255, 255, 255))

    font = pygame.font.Font(None, 25)
    start_text = font.render("Start", True, (200, 200, 200))
    start_text_rect = start_text.get_rect(center=(200, 125))
    if config_exits:
        load_text = font.render("Load Config", True, (200, 200, 200))
        load_text_rect = load_text.get_rect(center=(200, 185))
        pygame.draw.rect(screen, config.BG, [120, 160, 160, 50])
        screen.blit(load_text, load_text_rect)

        run_text = font.render("Run Config", True, (200, 200, 200))
        run_text_rect = run_text.get_rect(center=(200, 245))
        pygame.draw.rect(screen, config.BG, [120, 220, 160, 50])
        screen.blit(run_text, run_text_rect)

    pygame.draw.rect(screen, config.BG, [120, 100, 160, 50])

    screen.blit(start_text, start_text_rect)
    pygame.display.flip()
    pygame.event.clear()

    while True:
        event = pygame.event.wait()
        mouse = pygame.mouse.get_pos()
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
        elif event.type == pygame.MOUSEBUTTONDOWN:
            if 120 <= mouse[0] <= 280 and 100 <= mouse[1] <= 150:
                pygame.quit()
                return 0
            elif 120 <= mouse[0] <= 280 and 160 <= mouse[1] <= 210:
                pygame.quit()
                return 1
            elif 120 <= mouse[0] <= 280 and 220 <= mouse[1] <= 270:
                pygame.quit()
                return 2


def main():
    load_config = start_window()
    generation = 0
    if load_config >= 1:
        population = load_generation()
        if load_config == 2:
            s = population[0]
            s.reset()
            g = Game(s)
            g.start_with_gui()
            exit(0)
    else:
        population = [Snake() for _ in range(config.POPULATION_SIZE)]
    print("Initialization")
    while True:
        threads = []
        for s in population:
            s.reset()
            threads = []
            g = Game(s)
            thread = threading.Thread(target=(lambda: g.start()))
            threads.append(thread)
            thread.start()
        for t in threads:
            t.join()

        population.sort(key=lambda x: x.score, reverse=True)
        print([ann.score for ann in population])

        if generation % 10 == 0:
            population[0].reset()
            g = Game(population[0])
            g.start_with_gui()
            save_generation(population)

        print("Crossover")
        survive_count = int(len(population) * config.SURVIVE_RATE)  # survive
        new_population = population[:survive_count]
        for _ in range(len(population) - survive_count):  # generate new
            s1 = weighted_random_choice(population)
            s2 = weighted_random_choice(population)
            new_population.append(s1.crossover(s2))
        population = new_population

        print(f"New Generation: {generation}")
        generation += 1
    # g = Game(WIDTH, HEIGHT)
    # g.start_with_gui()


def weighted_random_choice(snakes):
    score_max = sum([snake.score for snake in snakes])
    pick = random.uniform(0, score_max)
    current = 0
    for snake in snakes:
        current += snake.score
        if current > pick:
            return snake


def save_generation(data):
    with open(config.CONF_PATH, "wb") as f:
        pickle.dump(data, f)


def load_generation():
    with open(config.CONF_PATH, "rb") as f:
        return pickle.load(f)


if __name__ == "__main__":
    main()

import numpy as np


def apply_filter(image: np.array, kernel: np.array) -> np.array:
    # A given image has to have either 2 (grayscale) or 3 (RGB) dimensions
    assert image.ndim in [2, 3]
    # A given filter has to be 2 dimensional and square
    assert kernel.ndim == 2
    assert kernel.shape[0] == kernel.shape[1]

    # TODO
    kernel_size = kernel.shape[0]
    padding = kernel_size // 2
    # Add zero padding to the input image
    if image.ndim == 3:
        image_padded = np.zeros((image.shape[0] + padding * 2, image.shape[1] + padding * 2, 3))
    else:
        image_padded = np.zeros((image.shape[0] + padding * 2, image.shape[1] + padding * 2))
    image_padded[padding:-padding, padding:-padding] = image
    output = np.zeros_like(image)
    if image.ndim == 3:
        for img_i, image_row in enumerate(image_padded[padding:-padding, padding:-padding], start=padding):
            for img_j, image_pixel in enumerate(image_row, start=padding):
                acc_r = 0
                acc_g = 0
                acc_b = 0
                for ker_i, kernel_row in enumerate(kernel, start=-padding):
                    for ker_j, kernel_element in enumerate(kernel_row, start=-padding):
                        cor_i = img_i + ker_i
                        cor_j = img_j + ker_j
                        acc_r += image_padded[cor_i][cor_j][0] * kernel_element
                        acc_g += image_padded[cor_i][cor_j][1] * kernel_element
                        acc_b += image_padded[cor_i][cor_j][2] * kernel_element
                output[img_i-padding][img_j-padding][0] = min(255, max(0, acc_r))
                output[img_i-padding][img_j-padding][1] = min(255, max(0, acc_g))
                output[img_i-padding][img_j-padding][2] = min(255, max(0, acc_b))
    else:
        for img_i, image_row in enumerate(image_padded[padding:-padding, padding:-padding], start=padding):
            for img_j, image_pixel in enumerate(image_row, start=padding):
                acc = 0
                for ker_i, kernel_row in enumerate(kernel, start=-padding):
                    for ker_j, kernel_element in enumerate(kernel_row, start=-padding):
                        cor_i = img_i + ker_i
                        cor_j = img_j + ker_j
                        acc += image_padded[cor_i][cor_j] * kernel_element
                output[img_i-padding][img_j-padding] = min(255, max(0, acc))
    return output

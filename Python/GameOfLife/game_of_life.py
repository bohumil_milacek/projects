"""
Homework 01 - Game of life.

Your task is to implement a kind of cellular automaton called "Game of life".
The automaton is a 2D simulation where each cell on the grid is either dead
or alive.

The state of each cell is updated in every iteration based state of neighbouring cells.
Cell neighbours are cells that are horizontally, vertically, or diagonally adjacent.

Rules for the update are as follows:

1. Any live cell with fewer than two live neighbours dies, as if by underpopulation.
2. Any live cell with two or three live neighbours lives on to the next generation.
3. Any live cell with more than three live neighbours dies, as if by overpopulation.
4. Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.


Our implementation uses the coordinate system with grid coordinates starting
from (0, 0) - upper left corner. The first coordinate is a row, and the second
is a column.

Do not use wrap-around (toroid) when reaching the edge of the board.

For more details about Game of Life, see Wikipedia:
https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life
"""


def update(alive: set, size: (int, int), iter_n: int) -> set:
    """
    Perform iter_n iterations.
    Args
    ----
        alive (set):
            A set of cell coordinates marked as alive, can be empty.
        size (int, int):
            The size of simulation grid as a tuple of two ints.
        iter_n (int):
            A number of iterations to perform.
    Returns
    -------
        _  (set):
            A set of coordinates of alive cells after iter_n iterations.
    """
    # TODO: Implement update rules.
    size_y = size[0]
    size_x = size[1]

    for _ in range(iter_n):
        area = [[1 if (y, x) in alive else 0 for x in range(size_x)] for y in range(size_y)]
        new_alive = set()
        for i in range(size_y):
            for j in range(size_x):
                neighbours_count = 0
                for (n_y, n_x) in [(i - 1, j), (i + 1, j), (i, j - 1), (i, j + 1), (i - 1, j - 1), (i - 1, j + 1), (i + 1, j - 1), (i + 1, j + 1)]:
                    if n_x < 0 or n_x >= size_x or n_y < 0 or n_y >= size_y:
                        continue
                    if area[n_y][n_x]:
                        neighbours_count += 1
                if area[i][j]:  # ziva bunka
                    if neighbours_count == 2 or neighbours_count == 3:
                        new_alive.add((i, j))
                else:  # mrtva bunka
                    if neighbours_count == 3:
                        new_alive.add((i, j))
        alive = new_alive
    return alive


def my_draw(field):
    print("+" + ("-" * len(field[0])) + "+")
    for line in field:
        print("|", end="")
        for x in line:
            if x:
                print("X", end="")
            else:
                print(" ", end="")
        print("|")
    print("+" + ("-" * len(field[0])) + "+")


def my_draw_num(field):
    print("+" + ("-" * len(field[0])) + "+")
    for line in field:
        print("|", end="")
        for x in line:
            print(x, end="")
        print("|")
    print("+" + ("-" * len(field[0])) + "+")


def draw(alive: set, size: (int, int)) -> str:
    """
    Draw a game board.

    Args
    ----
        alive (set):
            A set of cell coordinates marked as alive, can be empty.
        size (int, int):
            The size of simulation grid as a tuple of two ints.

    Returns
    -------
        _  (string):
           A string showing the board state with alive cells marked with X.
    """
    # TODO: implement board drawing logic and return it as output
    # Don't call print in this method, just return board string as output.
    # Example of 3x3 board with 1 alive cell at coordinates (0, 2):
    # +---+
    # |  X|
    # |   |
    # |   |
    # +---+
    size_y = size[0]
    size_x = size[1]

    field = "+" + ("-" * size_x) + "+\n"
    for i in range(size_y):
        field += "|"
        for j in range(size_x):
            field += ("X" if (i, j) in alive else " ")
        field += "|\n"
    field += "+" + ("-" * size_x) + "+"
    return field

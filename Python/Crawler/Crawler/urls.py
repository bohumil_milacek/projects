from django.urls import path
from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("query/", views.get_requested_sites, name="get_requested_sites"),
    path("admin_console/", views.admin_console, name="admin_console"),
    path("crawl/", views.crawl, name="crawl"),
    path("crawl/info/", views.crawl_info, name="crawl"),
    path("test/", views.test, name="test")
]
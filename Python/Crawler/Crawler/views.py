from django.shortcuts import render, HttpResponse
from django.http import HttpResponseBadRequest
from .crawler import Crawler
from .page_ranker import Ranker
from django.http import JsonResponse
from .models import Document
import requests

crawler = Crawler()
ranker = Ranker()

def index(request):
    return render(request, "index.html")

def get_requested_sites(request):
    global ranker
    if Document.count() == 0:
        return HttpResponse("No sites were found")
    if request.method == "GET":
        query = request.GET["query"]
        sites = ranker.query(query)
        return render(request, "websites.html", dict(sites=sites))

def admin_console(request):
    return render(request, "admin_console.html")

def crawl(request):
    global crawler
    if crawler.is_crawling:
        return HttpResponseBadRequest(u"Crawler is running")
    start_page = request.GET["start_page"]
    thread_count = int(request.GET["thread_count"])
    depth = int(request.GET["depth"])

    try:
        response = requests.get(start_page)
    except:
        return HttpResponseBadRequest(u"Something went wrong")
    crawler.crawl(depth, start_page, thread_count)
    return HttpResponse("Crawling done")

def crawl_info(request):
    global crawler
    if not crawler.is_crawling:
        return HttpResponseBadRequest(u"Crawler is not running")
    info = crawler.get_info()
    return JsonResponse(info)

def test(request):
    r = Ranker()
    return HttpResponse()

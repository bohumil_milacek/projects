import rank_bm25
import requests
import numpy as np
from .models import Document, Link


class Ranker:
    def __init__(self):
        self.sites = list(Document.all_ordered())
        self.np_indexes = {}
        index = 0
        for site in self.sites:
            self.np_indexes[site.id] = index
            index += 1
        self.page_ranks = self.__calculate_page_ranks().flatten()
        corpus = [site.text for site in self.sites]
        self.bm = rank_bm25.BM25Okapi([doc.split(" ") for doc in corpus])

    def __calculate_page_ranks(self, num_iterations=100, d=0.85):
        M = np.zeros((len(self.sites), len(self.sites)))
        for i in range(len(self.sites)):
            self.sites[i].get_outlinks()
            number_of_outlinks = self.sites[i].get_outlinks_count()
            if number_of_outlinks > 0:
                for link in self.sites[i].get_outlinks():
                    if link.get_link() not in self.np_indexes:
                        continue
                    index = self.np_indexes[link.get_link()]
                    M[i][index] = 1/number_of_outlinks
            else:
                val = 1/len(self.sites)
                for j in range(len(self.sites)):
                    M[i][j] = val
        N = M.shape[1]
        v = np.random.rand(N, 1)
        v = v / np.linalg.norm(v, 1)
        M_hat = (d * M + (1 - d) / N)
        for i in range(num_iterations):
            v = M_hat @ v
        return v

    def query(self, text):
        tokenized_query = text.split(" ")
        content_scores = np.array(self.bm.get_scores(tokenized_query))
        final_scores = zip(self.sites, ((self.page_ranks + content_scores) / 2).tolist())
        final_scores = sorted(final_scores, key=lambda x: x[1], reverse=True)
        return [tup[0] for tup in final_scores]
 
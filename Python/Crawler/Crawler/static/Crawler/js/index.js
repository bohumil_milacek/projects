$(document).ready(function () {
    if (jQuery)
        pageInit();
    else
        alert("JQuery not loaded");
});

function pageInit() {
    $("#query").keypress(function (e) {
        if (e.which === 13)
            onQuerySubmit($(this).val());
    });

    function onQuerySubmit(query) {
        $.ajax({
            type: "GET",
            url: "http://localhost:8000/query/",
            async: true,
            cache: false,
            data: {
                "query": query
            },
            success: function (data) {
                $pages_container = $("#pages-container");
                $($pages_container).css("display", "flex");
                $($pages_container).html(data);
                $([document.documentElement, document.body]).animate({
                    scrollTop: $($pages_container).offset().top
                }, 300);
            },
            fail: function (data) {
                alert("Fail:" + data);
            }
        });
    }
}

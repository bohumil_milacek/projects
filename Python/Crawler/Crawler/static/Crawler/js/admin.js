




function crawl() {
    startPage = $("#start_page").val();
    threadCount = $("#thread_count").val();
    depth = $("#depth").val();

    var updateInterval = setInterval(updateCrawlerInfo, 2000);

    $.ajax({
        type: "GET",
        url: "http://localhost:8000/crawl/",
        async: true,
        cache: false,
        data: {
            "start_page": startPage,
            "thread_count": threadCount,
            "depth": depth
        },
        success: function (data) {
            showSuccess(data);
            clearInterval(updateInterval);
        },
        error: function (data) {
            showError(data.responseText);
            clearInterval(updateInterval);
        }
    });
}


function updateCrawlerInfo() {
    $.ajax({
        type: "GET",
        url: "http://localhost:8000/crawl/info/",
        async: true,
        cache: false,
        success: function (data) {
            $("#info").fadeIn(100);
            showInfo(data);
        }
    });
}


function showError(text) {
    $errorAlert = $("#error");

    $($errorAlert).html(text);
    $($errorAlert).fadeIn(300).delay(3000).fadeOut(300);
}

function showSuccess(text) {
    $successAlert = $("#success");
    $infoAlert = $("#info");

    $($infoAlert).hide();
    $($successAlert).html(text);
    $($successAlert).fadeIn(300).delay(3000).fadeOut(300);
}

function showInfo(info) {
    $("#cell_crawledPages").html(info.crawledPages);
    $("#cell_threadsDone").html(info.threadsDone);
}
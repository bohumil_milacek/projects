from django.db import models
from django.db.models import CharField, Value
from django.db.models.expressions import RawSQL
from django.db import connection

class Document(models.Model):
    text = models.TextField()
    id = models.TextField(primary_key=True)

    def get_outlinks(self):
        return list(self.link_set.all())

    def get_outlinks_count(self):
        return self.link_set.all().count()

    def get_inlinks_count(self):
        return Link.objects.filter(doc=self.id).count()
    
    def get_title(self):
        p = re.compile("^(http|https)://(.*)+\..*$")
        return p.search(s)

    def taste_text(self):
        return text[:100]

    @staticmethod
    def all_ordered():
        return Document.objects.order_by('id')

    @staticmethod
    def count():
        return Document.objects.count()

    @staticmethod
    def delete_unused_links():
        Document.objects.raw("DELETE FROM Crawler_link WHERE Crawler_link.link NOT IN(SELECT Crawler_document.id FROM Crawler_document)")
    


class Link(models.Model):
    link = models.TextField()
    doc = models.ForeignKey(Document, on_delete=models.CASCADE)

    def __str__(self):
        return "Link: {0}".format(self.link)
    
    def get_link(self):
        return self.link

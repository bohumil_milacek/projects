from Crawler.models import Document
from threading import Lock, Thread, Condition
import requests
import re
import html2text
import time


class Crawler:
    visited = set()
    opened = []
    cond = Condition()
    db_lock = Lock()

    def __init__(self):
        self.is_crawling = False
        self.depth = 0
        self.threads_done_count = 0
        self.print_depth = 0
    
    # crawls pages, and than deletes all links pointing to invalid documents not in database
    def crawl(self, depth, start_page, thread_count):
        self.is_crawling = True

        Crawler.opened.append((start_page,0))

        threads = []
        for i in range(thread_count):
            print(f"starting thread {i}")
            t = Thread(target=self.bfs, args=(depth, i))
            t.start()
            threads.append(t)

        for t in threads:
            t.join()
        Document.delete_unused_links()

        self.is_crawling = False
        self.depth = 0
        self.print_depth = 0
        self.threads_done_count = 0

        Crawler.visited = set()
        Crawler.opened = []
        Crawler.cond = Condition()
        Crawler.db_lock = Lock()


    def bfs(self, depth, tID):
        actual_depth = 0
        while True:
            with Crawler.cond:
                while len(Crawler.opened)==0:
                    Crawler.cond.wait()
                page_link, actual_depth = Crawler.opened.pop(0)
                if actual_depth > self.print_depth and actual_depth< self.depth:
                    self.print_depth = actual_depth
                if page_link in Crawler.visited:
                    continue
                if actual_depth > depth:
                    Crawler.cond.notify()
                    break
                Crawler.visited.add(page_link)
            print("{0}[{1}]:: {2}".format(tID, actual_depth, page_link))
            page = self.download_page(page_link)
            page_links = self.get_page_links(page)
            page_text = self.remove_tags(page)
            doc = Document(text=page_text, id=page_link)
            with Crawler.db_lock:
                doc.save()
            for _link in page_links:
                with Crawler.cond:
                    with Crawler.db_lock:
                        doc.link_set.create(link=_link)
                    if _link not in Crawler.visited:
                        Crawler.opened.append((_link, actual_depth+1))
                    Crawler.cond.notify()
        print(f"{tID} DONE")
        self.threads_done_count += 1

    def download_page(self, url):
        page_downloaded = False
        while not page_downloaded:
            try:
                r = requests.get(url)
                page_downloaded = True
            except requests.exceptions.Timeout:
                print(f"Connection Exception, downloading {url} again")
                
        return r.content.decode('utf-8')

    def get_page_links(self, page):
        return [x.group() for x in re.finditer(pattern='(?<=<a href=")http.+?(?=")', string=page)]

    def remove_tags(self, page):
        cleaner = html2text.HTML2Text()
        cleaner.ignore_links = True
        return cleaner.handle(page)
    
    def get_info(self):
        data = {
            'crawledPages': len(Crawler.visited),
            'actualDepth': self.print_depth,
            'threadsDone': self.threads_done_count
        }
        return data
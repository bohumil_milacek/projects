# VWM

### INSTRUKCE
1. Instalace
    
    ./bi-vwm$ `sudo apt-get install python3`

    ./bi-vwm$ `sudo apt-get install python3-venv`
    
    ./bi-vwm$ `python3 -m venv venv`
    
    ./bi-vwm$ `. venv/bin/activate`
    
    ./bi-vwm$ `pip install -r requirements.txt`

2. Migrace DB - vytvoření/update DB

     ./bi-vwm$ `python3 manage.py makemigrations`
    
    ./bi-vwm$ `python3 manage.py migrate Crawler`
    
    ./bi-vwm$ `python3 manage.py migrate`

3. Spuštění serveru

    ./bi-vwm$ `python3 manage.py runserver`
    
    spustit na adrese `localhost:8000`
    
    
    

### INFO
**DB Superuser**
* username: admin
* password: admin


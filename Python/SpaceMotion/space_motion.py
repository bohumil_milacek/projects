import time  # measuring time
import math
from collections import namedtuple

# Define universal gravitation constant
G = 6.67408e-11  # N-m2/kg2
SpaceObject = namedtuple('SpaceObject', 'name mass x y vx vy color')
Force = namedtuple('Force', 'fx fy')


def logging(unit='ms', functions={}):  # , functions={}, unit=None
    def decorator_logging(func):
        def decorator_wrapper(*args, **kwargs):
            func_name = func.__name__
            if func_name not in functions:
                functions[func_name] = {
                    "count_calls": 0,
                    "count_time": 0
                }
            start = time.time()
            val_to_return = func(*args, **kwargs)
            functions[func_name]["count_calls"] += 1
            functions[func_name]["count_time"] += time.time() - start
            time_to_print = functions[func_name]["count_time"]
            if unit == 'ns':
                time_to_print *= 1.0e9
            elif unit == 'us':
                time_to_print *= 1.0e6
            elif unit == 'ms':
                time_to_print *= 1.0e3
            elif unit == 'min':
                time_to_print /= 60
            elif unit == 'h':
                time_to_print /= 3600
            elif unit == 'days':
                time_to_print /= 86400

            print(f"{func_name} - {functions[func_name]['count_calls']} - {time_to_print:.3f} {unit}")
            return val_to_return

        return decorator_wrapper

    return decorator_logging


@logging(unit='ms')
def calculate_force(space_object, *other_objects):
    # input: one of the space objects (indexed as i in below formulas), other space objects (indexed as j, may be any number of them)
    # returns named tuple (see above) that represents x and y components of the gravitational force
    # calculate force (vector) for each pair (space_object, other_space_object):
    # |F_ij| = G*m_i*m_j/distance^2
    # F_x = |F_ij| * (other_object.x-space_object.x)/distance
    # analogous for F_y
    # for each coordinate (x, y) it sums force from all other space objects
    forces = [0, 0]
    for other_object in other_objects:
        distance = math.sqrt((getattr(space_object, "x") - getattr(other_object, "x")) ** 2 + (getattr(space_object, "y") - getattr(other_object, "y")) ** 2)
        f_ij = abs((G * getattr(space_object, "mass") * getattr(other_object, "mass")) / (distance ** 2))
        f_x = f_ij * (getattr(other_object, "x") - getattr(space_object, "x")) / distance
        f_y = f_ij * (getattr(other_object, "y") - getattr(space_object, "y")) / distance
        forces[0] += f_x
        forces[1] += f_y
    return Force(fx=forces[0], fy=forces[1])


@logging(unit='s')
def update_space_object(space_object, force, timestep):
    # here we update coordinates and speed of the object based on the force that acts on it
    # input: space_object we want to update (evolve in time), force (from all other objects) that acts on it, size of timestep
    # returns: named tuple (see above) that contains updated coordinates and speed for given space_object
    # hint:
    # acceleration_x = force_x / mass
    # same for y
    # speed_change_x = acceleration_x * timestep
    # same for y
    # speed_new_x = speed_old_x + speed_change_x
    # same for y
    # x_final = x_old + speed_new_x * timestep

    acceleration_x = getattr(force, "fx") / getattr(space_object, "mass")
    acceleration_y = getattr(force, "fy") / getattr(space_object, "mass")
    speed_change_x = acceleration_x * timestep
    speed_change_y = acceleration_y * timestep
    speed_new_x = getattr(space_object, "vx") + speed_change_x
    speed_new_y = getattr(space_object, "vy") + speed_change_y
    x_final = getattr(space_object, "x") + speed_new_x * timestep
    y_final = getattr(space_object, "y") + speed_new_y * timestep
    return SpaceObject(name=getattr(space_object, "name"), mass=getattr(space_object, "mass"), x=x_final, y=y_final, vx=speed_new_x, vy=speed_new_y, color=getattr(space_object, "color"))


@logging(unit='ms')
def update_motion(timestep, *objects):
    # input: timestep and space objects we want to simulate (as named tuples above)
    # returns: list or tuple with updated objects
    # hint:
    # iterate over space objects, for given space object calculate_force with function above, update
    updated_space_objects = []
    for space_object in objects:
        other_objects = list(objects)
        other_objects.remove(space_object)
        updated_space_objects.append(update_space_object(space_object, calculate_force(space_object, *other_objects), timestep))
    return updated_space_objects  # (named tuple with x and y)


@logging()
def simulate_motion(timestep, timesteps_count, *objects):
    # generator that in every iteration yields dictionary with the name of the objects as a key and tuple of coordinates (x first, y second) as values
    # input size of the timestep, number of timesteps (integer), space objects (any number of them)
    for _ in range(timesteps_count):
        objects = update_motion(timestep, *objects)
        yield {getattr(space_object, "name"): (getattr(space_object, "x"), getattr(space_object, "y")) for space_object in objects}
    pass

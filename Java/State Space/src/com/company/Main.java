package com.company;

import com.company.GUI.SelectScreen;

public class Main {

    public static void main(String[] args) {
        printInstructions();
        new SelectScreen();
    }


    private static void printInstructions() {
        System.out.println("---------------INSTRUCTIONS---------------");
        System.out.println("ALL LABYRINTH FILES MUST BE IN \"resources.labirinths\" DIRECTORY!!!");
        System.out.println("[RIGHT BUTTON] - LOOK AROUND ALL GRAPHS");
        System.out.println("[LEFT BUTTON] - LOOK AROUND ACTUAL GRAPH");
        System.out.println("[MOUSE WHEEL] - ZOOM IN/OUT");
        System.out.println("[R] - RESET VIEW");
        System.out.println("[P] - START ALL ALGORITHMS");
        System.out.println("[SPACE] - STEP IN ALGORITHMS");
        System.out.println("[C] - COMPLETE ALGORITHMS");
        System.out.println("OUTPUT MAY BE IN DIFFERENT ORDER, BECAUSE OF THREADS");
        System.out.println("YOU CAN CONFIGURE COLORS AND ALG. SPEED IN \"resources.Config\" class");
        System.out.println("----------------ALGORITHMS----------------");
        System.out.println("[DFS]\t[BFS]\t\t[Random]");
        System.out.println("[A*]\t[Dijkstra]\t[Greedy]");
        System.out.println("-----------------RESULTS------------------");
    }
}

package com.company.Util;

public class Position{

    //cost for A* algorithm
    protected int fCost = Integer.MAX_VALUE;

    //cost for greedy
    protected int greedyHCost = 0;

    //cost for Dijkstra
    protected int cost = 0;

    private int x;
    private int y;

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Position(int x, int y, int fCost) {
        this.x = x;
        this.y = y;
        this.fCost = fCost;
    }


    public int getX() {
        return x;
    }


    public int getY() {
        return y;
    }

    public int getfCost() {
        return fCost;
    }


    public int getGreedyHCost() {
        return greedyHCost;
    }

    public void setGreedyHCost(int greedyHCost) {
        this.greedyHCost = greedyHCost;
    }

    // comparable for A*

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

}

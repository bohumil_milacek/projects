package com.company.Util;

public class Util {

    public static int getDiagonalDistance(Position pos1, Position pos2) {
        int xDist = Math.abs(pos1.getX() - pos2.getX());
        int yDist = Math.abs(pos1.getY() - pos2.getY());
        return (int) (Math.sqrt(xDist * xDist + yDist * yDist) * 10);
    }

    public static int getManhattanDistance(Position pos1, Position pos2) {
        int xDist = Math.abs(pos1.getX() - pos2.getX());
        int yDist = Math.abs(pos1.getY() - pos2.getY());
        return xDist + yDist;
    }

}

package com.company.Alg;

import com.company.Model.End;
import com.company.Model.Labirinth;
import com.company.Model.Place;
import com.company.Model.Wall;
import com.company.Util.Position;
import com.company.Util.Util;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

/// Greedy is using Diagonal distance to count the h cost
public class Greedy extends Algorithm {

    Queue<Position> priorityQueue = new PriorityQueue<>(Comparator.comparingInt(Position::getGreedyHCost));

    @Override
    public void solve(Labirinth labirinth) {
        new Thread(() -> {
            Greedy(labirinth);
            constructPath(labirinth);
        }).start();
    }


    private void Greedy(Labirinth labirinth) {
        structure = labirinth.getStructure();

        Position actualPos;
        Place actualPlace;

        priorityQueue.add(labirinth.getStartPos());

        while (!priorityQueue.isEmpty()) {
            actualPos = priorityQueue.poll();

            int x = actualPos.getX();
            int y = actualPos.getY();
            actualPlace = structure[y][x];
            actualPlace.setActual(true);
            actualPlace.setVisited(true);

            stepHandler.handleStep();
            pauseHandle();

            if (actualPlace instanceof End) {
                solutionFound = true;
                return;
            }

            if (y - 1 >= 0 &&
                    !((top = structure[y - 1][x]) instanceof Wall) &&
                    !top.isVisited()) {
                top.setVisited(true);
                top.setPredcessor(actualPlace);
                Position topPos = new Position(x, y - 1);
                topPos.setGreedyHCost(top.getGreedyH());
                priorityQueue.add(topPos);
                ++nodesExpanded;
            }
            if (x + 1 < labirinth.getWidth() &&
                    !((right = structure[y][x + 1]) instanceof Wall) &&
                    !right.isVisited()) {
                right.setVisited(true);
                right.setPredcessor(actualPlace);
                Position rightPos = new Position(x + 1, y);
                rightPos.setGreedyHCost(right.getGreedyH());
                priorityQueue.add(rightPos);
                ++nodesExpanded;
            }
            if (y + 1 < labirinth.getHeight() &&
                    !((bottom = structure[y + 1][x]) instanceof Wall) &&
                    !bottom.isVisited()) {
                bottom.setVisited(true);
                bottom.setPredcessor(actualPlace);
                Position bottomPos = new Position(x, y + 1);
                bottomPos.setGreedyHCost(bottom.getGreedyH());
                priorityQueue.add(bottomPos);
                ++nodesExpanded;
            }
            if (x - 1 >= 0 &&
                    !((left = structure[y][x - 1]) instanceof Wall) &&
                    !left.isVisited()) {
                left.setVisited(true);
                left.setPredcessor(actualPlace);
                Position leftPos = new Position(x - 1, y);
                leftPos.setGreedyHCost(left.getGreedyH());
                priorityQueue.add(leftPos);
                ++nodesExpanded;
            }

            actualPlace.setActual(false);
            actualPlace.setClosed(true);
            stepHandler.handleStep();
            pauseHandle();
        }
    }


    @Override
    protected void print(String s) {
        System.out.println("[Greedy]\t" + s);
    }
}

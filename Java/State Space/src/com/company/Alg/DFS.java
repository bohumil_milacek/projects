package com.company.Alg;

import com.company.Model.End;
import com.company.Model.Labirinth;
import com.company.Model.Place;
import com.company.Model.Wall;
import com.company.Util.Position;

import java.util.Stack;

public class DFS extends Algorithm {

    Stack<Position> positionStack = new Stack<>();

    @Override
    public void solve(Labirinth labirinth) {
        new Thread(() -> {
            DFS(labirinth);
            constructPath(labirinth);
        }).start();
    }


    private void DFS(Labirinth labirinth) {
        structure = labirinth.getStructure();
        positionStack.push(labirinth.getStartPos());
        Position actual;


        while (!positionStack.empty()) {
            actual = positionStack.pop();
            int x = actual.getX();
            int y = actual.getY();
            Place actualPlace = structure[y][x];
            actualPlace.setActual(true);
            actualPlace.setVisited(true);

            stepHandler.handleStep();
            pauseHandle();

            if (actualPlace instanceof End) {
                solutionFound = true;
                return;
            }

            if (y - 1 >= 0 &&
                    !((top = structure[y - 1][x]) instanceof Wall) &&
                    !top.isVisited()) {
                top.setVisited(true);
                top.setPredcessor(actualPlace);
                actualPlace.setActual(false);
                ++nodesExpanded;
                positionStack.push(new Position(x, y - 1));
            }
            if (x + 1 < labirinth.getWidth() &&
                    !((right = structure[y][x + 1]) instanceof Wall) &&
                    !right.isVisited()) {
                right.setVisited(true);
                right.setPredcessor(actualPlace);
                actualPlace.setActual(false);
                ++nodesExpanded;
                positionStack.push(new Position(x + 1, y));
            }
            if (y + 1 < labirinth.getHeight() &&
                    !((bottom = structure[y + 1][x]) instanceof Wall) &&
                    !bottom.isVisited()) {
                bottom.setVisited(true);
                bottom.setPredcessor(actualPlace);
                actualPlace.setActual(false);
                ++nodesExpanded;
                positionStack.push(new Position(x, y + 1));
            }
            if (x - 1 >= 0 &&
                    !((left = structure[y][x - 1]) instanceof Wall) &&
                    !left.isVisited()) {
                left.setVisited(true);
                left.setPredcessor(actualPlace);
                actualPlace.setActual(false);
                ++nodesExpanded;
                positionStack.push(new Position(x - 1, y));
            }

            pauseHandle();
            actualPlace.setClosed(true);
            stepHandler.handleStep();
        }

    }


    @Override
    protected void print(String s) {
        System.out.println("[DFS]\t" + s);
    }
}

package com.company.Alg;

import com.company.Model.End;
import com.company.Model.Labirinth;
import com.company.Model.Place;
import com.company.Model.Wall;
import com.company.Util.Position;
import com.company.Util.Util;

import java.util.LinkedList;
import java.util.Queue;

public class BFS extends Algorithm {

    Queue<Position> unprocessed = new LinkedList<>();

    @Override
    public void solve(Labirinth labirinth) {
        new Thread(() -> {
            BFS(labirinth);
            constructPath(labirinth);
        }).start();
    }


    private void BFS(Labirinth labirinth) {
        structure = labirinth.getStructure();
        unprocessed.add(labirinth.getStartPos());
        structure[labirinth.getStartPos().getY()][labirinth.getStartPos().getX()].setVisited(true);

        while (!unprocessed.isEmpty()) {
            Position actualPos = unprocessed.poll();
            int x = actualPos.getX();
            int y = actualPos.getY();
            Place actualPlace = structure[y][x];
            actualPlace.setActual(true);

            stepHandler.handleStep();
            pauseHandle();

            if (actualPlace instanceof End) {
                solutionFound = true;
                return;
            }


            if (y - 1 >= 0 &&
                    !((top = structure[y - 1][x]) instanceof Wall) &&
                    !top.isVisited()) {
                top.setVisited(true);
                top.setPredcessor(actualPlace);
                unprocessed.add(new Position(x, y - 1));
                ++nodesExpanded;
            }
            if (x + 1 < labirinth.getWidth() &&
                    !((right = structure[y][x + 1]) instanceof Wall) &&
                    !right.isVisited()) {
                right.setVisited(true);
                right.setPredcessor(actualPlace);
                unprocessed.add(new Position(x + 1, y));

                ++nodesExpanded;
            }
            if (y + 1 < labirinth.getHeight() &&
                    !((bottom = structure[y + 1][x]) instanceof Wall) &&
                    !bottom.isVisited()) {
                bottom.setVisited(true);
                bottom.setPredcessor(actualPlace);
                unprocessed.add(new Position(x, y + 1));
                ++nodesExpanded;
            }
            if (x - 1 >= 0 &&
                    !((left = structure[y][x - 1]) instanceof Wall) &&
                    !left.isVisited()) {
                left.setVisited(true);
                left.setPredcessor(actualPlace);
                unprocessed.add(new Position(x - 1, y));
                ++nodesExpanded;
            }

            actualPlace.setClosed(true);
            actualPlace.setActual(false);
            stepHandler.handleStep();
            pauseHandle();

        }
    }

    @Override
    protected void print(String s) {
            System.out.println("[BFS]\t" + s);
    }
}

package com.company.Alg;

import com.company.GUI.AlgorithmStepHandler;
import com.company.Model.Labirinth;
import com.company.Model.Place;
import com.company.Util.Util;
import resources.Config;

public abstract class Algorithm {

    protected static boolean stepByStep = true;
    protected AlgorithmStepHandler stepHandler;
    protected int nodesExpanded = 1;
    protected int pathLenght = 0;
    protected static int delayMillis = Config.algDelayMilis;
    public static Object binLock = new Object();
    protected boolean solutionFound = false;

    Place[][] structure;
    Place left;
    Place right;
    Place top;
    Place bottom;


    public abstract void solve(Labirinth labirinth);

    public void setStepHandler(AlgorithmStepHandler algorithmStepHandler) {
        stepHandler = algorithmStepHandler;
    }

    public static void setStepByStep(boolean stepByStep) {
        Algorithm.stepByStep = stepByStep;
    }

    protected void pauseHandle() {
        try {
            if (stepByStep)
                synchronized (binLock) {
                    binLock.wait();
                }
            else
                sleep(delayMillis);
        } catch (InterruptedException e) {
            System.out.println("Vlakno ma problem s uspanim nebo synchronizaci:" + e.getMessage());
        }

    }

    protected void constructPath(Labirinth labirinth) {
        if (!solutionFound)
            print("Solution not found");
        Place actual = labirinth.getStructure()[labirinth.getEndPos().getY()][labirinth.getEndPos().getX()];
        while (actual.getPredcessor() != null) {
            ++pathLenght;
            actual.setPath(true);
            stepHandler.handleStep();
            pauseHandle();
            actual = actual.getPredcessor();
        }
        print("Path Lenght:\t" + pathLenght);
        print("Nodes Expaned:\t" + nodesExpanded+"\n");
    }

    private void sleep(int millis) throws InterruptedException {
        Thread.sleep(millis);
    }


    abstract protected void print(String s);
}

package com.company.Alg;

import com.company.Model.End;
import com.company.Model.Labirinth;
import com.company.Model.Place;
import com.company.Model.Wall;
import com.company.Util.Position;
import com.company.Util.Util;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

/// A* is using Manhattan distance to count the g, h and f cost
public class AStar extends Algorithm {

    Queue<Place> priorityQueue = new PriorityQueue<>(Comparator.comparingInt(Place::getCostF));

    @Override
    public void solve(Labirinth labirinth) {
        new Thread(() -> {
            AStar(labirinth);
            constructPath(labirinth);
        }).start();
    }


    private void AStar(Labirinth labirinth) {
        structure = labirinth.getStructure();
        labirinth.getStartPlace().setCostG(0);
        priorityQueue.add(labirinth.getStartPlace());

        while (!priorityQueue.isEmpty()) {
            Place actualPlace = priorityQueue.poll();
            if (actualPlace.isClosed())
                continue;
            int x = actualPlace.getX();
            int y = actualPlace.getY();

            actualPlace.setActual(true);
            actualPlace.setVisited(true);

            stepHandler.handleStep();
            pauseHandle();

            if (actualPlace instanceof End) {
                solutionFound = true;
                return;
            }

            Place topPlace = structure[y - 1][x];
            int gCost = actualPlace.getCostG() + 10;
            if (y - 1 >= 0 &&
                    !(topPlace instanceof Wall) &&
                    !topPlace.isClosed() &&
                    gCost < topPlace.getCostG()
            ) {
                topPlace.setVisited(true);
                topPlace.setPredcessor(actualPlace);
                topPlace.setCostG(gCost);
                if (!priorityQueue.contains(topPlace))
                    priorityQueue.add(topPlace);
                ++nodesExpanded;
            }

            Place rightPlace = structure[y][x + 1];
            gCost = actualPlace.getCostG() + 10;
            if (x + 1 < labirinth.getWidth() &&
                    !(rightPlace instanceof Wall) &&
                    !rightPlace.isClosed() &&
                    gCost < rightPlace.getCostG()
            ) {
                rightPlace.setVisited(true);
                rightPlace.setPredcessor(actualPlace);
                rightPlace.setCostG(gCost);
                if (!priorityQueue.contains(rightPlace))
                    priorityQueue.add(rightPlace);
                ++nodesExpanded;
            }


            Place bottomPlace = structure[y + 1][x];
            gCost = actualPlace.getCostG() + 10;

            if (y + 1 < labirinth.getHeight() &&
                    !(bottomPlace instanceof Wall) &&
                    !bottomPlace.isClosed() &&
                    gCost < bottomPlace.getCostG()
            ) {
                bottomPlace.setVisited(true);
                bottomPlace.setPredcessor(actualPlace);
                bottomPlace.setCostG(gCost);
                if (!priorityQueue.contains(bottomPlace))
                    priorityQueue.add(bottomPlace);
                ++nodesExpanded;
            }


            Place leftPlace = structure[y][x - 1];
            gCost = actualPlace.getCostG() + 10;
            if (x - 1 >= 0 &&
                    !(leftPlace instanceof Wall) &&
                    !leftPlace.isClosed() &&
                    gCost < leftPlace.getCostG()
            ) {
                leftPlace.setVisited(true);
                leftPlace.setPredcessor(actualPlace);
                leftPlace.setCostG(gCost);
                if (!priorityQueue.contains(leftPlace))
                    priorityQueue.add(leftPlace);
                ++nodesExpanded;
            }

            actualPlace.setActual(false);
            actualPlace.setClosed(true);
            stepHandler.handleStep();
            pauseHandle();
        }
    }

    @Override
    public void print(String s) {
        System.out.println("[A*]\t" + s);
    }
}

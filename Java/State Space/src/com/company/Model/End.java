package com.company.Model;

import resources.Config;

import java.awt.*;

public class End extends Place {

    private static Color color = Config.endBG;

    public End() {
    }

    public End(int x, int y) {
        super(x, y);
    }

    public End(Place clone) {
        super(clone);
    }

    @Override
    public void paint(Graphics g, int x, int y, int size) {
        g.setColor(color);
        g.fillRect(x, y, size, size);
    }


    @Override
    protected Place clone() {
        return new End(this);
    }
}

package com.company.Model;

import com.company.Util.Position;

public class Labirinth {

    private Place[][] structure;
    private Position startPos;
    private Position endPos;
    private Place startPlace;
    private Place endPlace;
    private int width;
    private int height;


    public Labirinth(Place[][] structure, Position startPos, Position endPos, Place startPlace, Place endPlace, int width, int height) {
        this.structure = structure;
        this.startPos = startPos;
        this.endPos = endPos;
        this.startPlace = startPlace;
        this.endPlace = endPlace;
        this.width = width;
        this.height = height;
    }

    public Place[][] getStructure() {
        return structure;
    }

    public Position getStartPos() {
        return startPos;
    }

    public Position getEndPos() {
        return endPos;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public Place getStartPlace() {
        return startPlace;
    }

    public Place getEndPlace() {
        return endPlace;
    }

    @Override
    public Labirinth clone() {
        Place[][] cloneStructure = new Place[height][width];
        for (int i = 0; i < height; i++)
            for (int j = 0; j < structure[i].length; j++)
            cloneStructure[i][j] = structure[i][j].clone();
        return new Labirinth( cloneStructure,  startPos,  endPos,  cloneStructure[startPlace.getY()][startPlace.getX()],  cloneStructure[endPlace.getY()][endPlace.getX()],  width,  height);
    }
}

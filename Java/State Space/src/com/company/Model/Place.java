package com.company.Model;

import resources.Config;

import java.awt.*;


public class Place {

    protected static Color color = Config.tileBG;
    protected static Color visitedColor = Config.openBG;
    protected static Color closedColor = Config.closedBG;
    protected static Color actualColor = Config.actualBG;
    protected static Color pathColor = Config.pathBG;
    protected boolean visited = false;
    protected boolean closed = false;
    protected boolean actual = false;
    protected boolean path = false;
    protected int greedyH;
    int x;
    int y;
    // for A*
    protected int costH = Integer.MAX_VALUE;
    protected int costG = Integer.MAX_VALUE;

    protected Place predcessor;




    public Place() {
    }


    public Place(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Place(Place p) {
        this.visited = p.visited;
        this.closed = p.closed;
        this.actual = p.actual;
        this.path = p.path;
        this.greedyH = p.greedyH;
        this.x = p.x;
        this.y = p.y;
        this.costH = p.costH;
        this.costG = p.costG;
        this.predcessor = p.predcessor;
    }

    public void paint(Graphics g, int x, int y, int size) {
        g.setColor(color);
        if (visited) {
            g.setColor(visitedColor);
            if (actual)
                g.setColor(actualColor);
            if (closed)
                g.setColor(closedColor);
            if (path)
                g.setColor(pathColor);
        }
        g.fillRect(x, y, size, size);
    }


    final public boolean isVisited() {
        return visited;
    }

    final public void setVisited(boolean visited) {
        this.visited = visited;
    }

    final public boolean isClosed() {
        return closed;
    }

    final public void setClosed(boolean closed) {
        this.closed = closed;
    }

    final public Place getPredcessor() {
        return predcessor;
    }

    final public void setPredcessor(Place predcessor) {
        this.predcessor = predcessor;
    }

    final public boolean isActual() {
        return actual;
    }

    final public void setActual(boolean actual) {
        this.actual = actual;
    }

    final public boolean isPath() {
        return path;
    }

    final public void setPath(boolean path) {
        this.path = path;
    }

    final public int getGreedyH() {
        return greedyH;
    }

    final public void setGreedyH(int greedyH) {
        this.greedyH = greedyH;
    }

    public int getCostH() {
        return costH;
    }

    public void setCostH(int costH) {
        this.costH = costH;
    }

    public int getCostG() {
        return costG;
    }

    public void setCostG(int costG) {
        this.costG = costG;
    }

    public int getCostF(){
        return costG+costH;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    protected Place clone() {
        return new Place(this);
    }


}

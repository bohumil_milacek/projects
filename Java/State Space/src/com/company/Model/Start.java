package com.company.Model;

import resources.Config;

import java.awt.*;

public class Start extends Place {

    private static Color color = Config.startBG;

    public Start() {
    }

    public Start(int x, int y) {
        super(x, y);
    }

    public Start(Place clone) {
        super(clone);
    }

    @Override
    public void paint(Graphics g, int x, int y, int size) {
        g.setColor(color);
        g.fillRect(x, y, size, size);
    }

    @Override
    protected Place clone() {
        return new Start(this);
    }
}

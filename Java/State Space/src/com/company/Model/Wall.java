package com.company.Model;

import resources.Config;

import java.awt.*;

public class Wall extends Place {
    private static Color color = Config.wallBG;

    public Wall() {
    }

    public Wall(Place clone) {
        super(clone);
    }

    @Override
    public void paint(Graphics g, int x, int y, int size) {
        g.setColor(color);
        g.fillRect(x, y, size, size);
    }

    @Override
    public Place clone() {
        return new Wall(this);
    }
}

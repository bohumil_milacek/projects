package com.company.GUI;

import com.company.Alg.Algorithm;
import com.company.Model.Labirinth;
import com.company.Model.Place;
import resources.Config;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;

public class GraphComponent extends JPanel {

    private Labirinth labirinth;
    private Camera camera = new Camera();
    private boolean mouseDrag = false;
    private Algorithm algorithm;

    private int labTileSize = 30;
    private int labGap = 3;

    public GraphComponent(Labirinth labirinth, Algorithm algorithm) {
        this.labirinth = labirinth;
        this.algorithm = algorithm;
        setBackground(Config.componentsBG);
        setDoubleBuffered(true);
        init();
    }

    private void init() {
        setDoubleBuffered(true);
        addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                super.mousePressed(e);
                if (e.getButton() == MouseEvent.BUTTON3) {
                    ((Screen) getParent().getParent().getParent().getParent()).mousePressed(e);
                    return;
                }
                moveGraph();
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                super.mouseReleased(e);
                if (e.getButton() == MouseEvent.BUTTON3) {
                    ((Screen) getParent().getParent().getParent().getParent()).mouseReleased(e);
                    return;
                }
                stopMovingGraph();
            }


        });

    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        g.setColor(Color.BLACK);
        for (int i = 0; i < labirinth.getStructure().length; i++) {
            Place[] labRow = labirinth.getStructure()[i];
            for (int j = 0; j < labRow.length; j++) {
                Place labTile = labRow[j];
                labTile.paint(g, getTileXPos(j), getTileYPos(i), (int) (camera.getMagnification() * labTileSize));
            }
        }
    }

    private int getTileXPos(int j) {
        return (int) (camera.getMagnification() * (j * (labTileSize + labGap))) + camera.getxCenter();
    }

    private int getTileYPos(int i) {
        return (int) (camera.getMagnification() * (i * (labTileSize + labGap))) + camera.getyCenter();
    }


    public void startAlgorithm() {
        algorithm.setStepHandler(() -> repaint());
        algorithm.solve(labirinth);
    }

    public void moveGraph() {
        new Thread(() -> {
            mouseDrag = true;
            while (mouseDrag) {
                int xStart = (int) MouseInfo.getPointerInfo().getLocation().getX();
                int yStart = (int) MouseInfo.getPointerInfo().getLocation().getY();
                try {
                    Thread.sleep(15);
                } catch (InterruptedException ex) {
                    System.out.println("Chyba pri uspani vlakna:" + ex.getMessage());
                }
                int xEnd = (int) MouseInfo.getPointerInfo().getLocation().getX();
                int yEnd = (int) MouseInfo.getPointerInfo().getLocation().getY();
                int x = (xEnd - xStart);
                int y = (yEnd - yStart);
                camera.addX(x);
                camera.addY(y);
                repaint();
            }
        }).start();
    }

    public void stopMovingGraph() {
        mouseDrag = false;
    }

    public void zoomIn() {
        if (camera.getMagnification() < 2.1) {
            camera.increaseMagnification();
            repaint();
        }
    }

    public void zoomOut() {
        if (camera.getMagnification() > 0.1) {
            camera.decreaseMagnification();
            repaint();
        }
    }

    public void centerCamera() {
        camera.setxCenter(0);
        camera.setyCenter(0);
        repaint();
    }

}

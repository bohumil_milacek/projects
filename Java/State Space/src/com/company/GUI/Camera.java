package com.company.GUI;

public class Camera {

    private int xCenter = 0;
    private int yCenter = 0;
    private double magnification = 1.0;

    public int getxCenter() {
        return xCenter;
    }

    public int getyCenter() {
        return yCenter;
    }

    public void addX(int x) {
        xCenter += x;
    }

    public void addY(int y) {
        yCenter += y;
    }

    public double getMagnification() {
        return magnification;
    }

    public void increaseMagnification() {
        magnification += 0.02;
    }

    public void decreaseMagnification() {
        magnification -= 0.02;
    }

    public void setxCenter(int xCenter) {
        this.xCenter = xCenter;
    }

    public void setyCenter(int yCenter) {
        this.yCenter = yCenter;
    }
}

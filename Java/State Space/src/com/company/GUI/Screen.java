package com.company.GUI;

import com.company.Alg.*;
import com.company.Loader.LabirinthLoader;
import com.company.Model.Labirinth;
import com.company.Util.Util;
import resources.Config;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Screen extends JFrame implements KeyListener, MouseWheelListener, MouseListener {

    GraphComponent dfsComponent;
    GraphComponent bfsComponent;
    GraphComponent starComponent;
    GraphComponent randComponent;
    GraphComponent dijkComponent;
    GraphComponent greedyComponent;

    private boolean started = false;

    public Screen(String file) {
        super();
        init(file);
    }

    private void init(String file) {
        Labirinth l = null;
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Future<Labirinth> future = executor.submit(() -> LabirinthLoader.loadLabirinth(file));


        Dimension DimMax = Toolkit.getDefaultToolkit().getScreenSize();
        setMaximumSize(DimMax);
        setVisible(true);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        GridLayout gl = new GridLayout(2, 3);
        gl.setVgap(5);
        gl.setHgap(5);
        setLayout(gl);
        setBackground(Config.screenBG);
        addKeyListener(this);
        addMouseListener(this);
        addMouseWheelListener(this);

        try {
            l = future.get();
        } catch (InterruptedException e) {
            System.out.println("chyba pri parsovani souboru:" + e);
        } catch (ExecutionException e) {
            System.out.println("chyba pri parsovani souboru:" + e);
        }

        dfsComponent = new GraphComponent(l.clone(), new DFS());
        bfsComponent = new GraphComponent(l.clone(), new BFS());
        randComponent = new GraphComponent(l.clone(), new RNDSearch());
        starComponent = new GraphComponent(l.clone(), new AStar());
        dijkComponent = new GraphComponent(l.clone(), new Dijkstra());
        greedyComponent = new GraphComponent(l, new Greedy());


        add(dfsComponent);
        add(bfsComponent);
        add(randComponent);
        add(starComponent);
        add(dijkComponent);
        add(greedyComponent);


        revalidate();
    }


    public void start() {
        if (started)
            return;
        started = true;
        dfsComponent.startAlgorithm();
        bfsComponent.startAlgorithm();
        randComponent.startAlgorithm();
        starComponent.startAlgorithm();
        dijkComponent.startAlgorithm();
        greedyComponent.startAlgorithm();

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_SPACE:
                synchronized (Algorithm.binLock) {
                    Algorithm.binLock.notifyAll();
                }
                break;
            case KeyEvent.VK_C:
                Algorithm.setStepByStep(false);
                synchronized (Algorithm.binLock) {
                    Algorithm.binLock.notifyAll();
                }
                break;
            case KeyEvent.VK_R:
                dfsComponent.centerCamera();
                bfsComponent.centerCamera();
                starComponent.centerCamera();
                randComponent.centerCamera();
                dijkComponent.centerCamera();
                greedyComponent.centerCamera();
                break;
            case KeyEvent.VK_P:
                start();
                break;
            default:
                break;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (e.getButton() == MouseEvent.BUTTON1)
            return;
        dfsComponent.moveGraph();
        bfsComponent.moveGraph();
        starComponent.moveGraph();
        randComponent.moveGraph();
        dijkComponent.moveGraph();
        greedyComponent.moveGraph();

    }

    @Override
    public void mouseReleased(MouseEvent e) {
        dfsComponent.stopMovingGraph();
        bfsComponent.stopMovingGraph();
        starComponent.stopMovingGraph();
        randComponent.stopMovingGraph();
        dijkComponent.stopMovingGraph();
        greedyComponent.stopMovingGraph();
    }


    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }


    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        if (e.getWheelRotation() < 0)
        {
            dfsComponent.zoomIn();
            bfsComponent.zoomIn();
            starComponent.zoomIn();
            randComponent.zoomIn();
            dijkComponent.zoomIn();
            greedyComponent.zoomIn();
        }else{
            dfsComponent.zoomOut();
            bfsComponent.zoomOut();
            starComponent.zoomOut();
            randComponent.zoomOut();
            dijkComponent.zoomOut();
            greedyComponent.zoomOut();
        }
    }
}

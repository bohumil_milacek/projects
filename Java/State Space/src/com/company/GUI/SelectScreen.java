package com.company.GUI;

import com.company.Loader.LabirinthLoader;
import com.company.Util.Util;
import resources.Config;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;
import java.util.concurrent.*;


public class SelectScreen extends JFrame {

    JScrollPane scrollPane = new JScrollPane();
    JList jList = new JList();

    public SelectScreen() {
        init();
    }

    private void init() {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Future<List<String>> future = executor.submit(() -> {
            List<String> loadLabirinths = LabirinthLoader.loadLabirinths();
            return loadLabirinths;
        });

        setSize(400, 600);
        setLocationRelativeTo(null);
        setVisible(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        GridLayout gl = new GridLayout(2, 3);
        gl.setVgap(5);
        gl.setHgap(5);
        setLayout(gl);
        setBackground(Config.screenBG);

        DefaultListModel<String> listModel = new DefaultListModel<>();
        try {
            listModel.addAll(future.get());
        } catch (InterruptedException e) {
            System.out.println("chyba pri nacitani souborů:" + e);
        } catch (ExecutionException e) {
            System.out.println("chyba pri nacitani souborů:" + e);
        }

        DefaultListCellRenderer renderer = (DefaultListCellRenderer) jList.getCellRenderer();
        renderer.setHorizontalAlignment(JLabel.CENTER);

        jList.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() != KeyEvent.VK_ENTER)
                    return;
                    new Screen(jList.getSelectedValue().toString());
                setVisible(false); //you can't see me!
                dispose();
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });

        jList.setModel(listModel);
        scrollPane.setViewportView(jList);
        setContentPane(scrollPane);
        revalidate();
    }
}

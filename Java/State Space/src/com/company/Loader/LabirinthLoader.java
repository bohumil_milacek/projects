package com.company.Loader;

import com.company.Model.*;
import com.company.Util.Position;
import com.company.Util.Util;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class LabirinthLoader {

    private static String labDirectory = "src/resources/labirinths";
    private static String filePath = "src/resources/labirinths/";

    public static Labirinth loadLabirinth(String file) throws Exception {
        FileReader fr = new FileReader(new File(filePath+file));
        BufferedReader br = new BufferedReader(fr);
        String line = br.readLine();
        ArrayList<Place[]> labirinthTmp = new ArrayList<>();
        Position startPos = null;
        Position endPos = null;
        int labWidth = line.length();
        while (line != null) {
            Place[] labRow = new Place[labWidth];
            if (line.contains("start")) {
                startPos = getPositions(line);
            } else if (line.contains("end")) {
                endPos = getPositions(line);
                break;
            } else {
                for (int i = 0; i < line.length(); i++) {
                    char labChar = line.charAt(i);
                    switch (labChar) {
                        case 'X':
                            labRow[i] = new Wall();
                            break;
                        case ' ':
                            labRow[i] = new Place(i, labirinthTmp.size());
                            break;
                        default:
                            System.out.println("'" + labChar + "'");
                            throw new Exception("Incorrect labirinth file structure");
                    }
                }
                labirinthTmp.add(labRow);
            }
            line = br.readLine();
        }
        fr.close();    //closes the stream and release the resources

        Place[][] labirinth = new Place[labirinthTmp.size()][];
        for (int i = 0; i < labirinthTmp.size(); i++) {
            labirinth[i] = labirinthTmp.get(i);
            for (int j = 0; j < labirinth[i].length; j++) {
                Position actualPos = new Position(j, i);
                int distanceFromEnd = Util.getDiagonalDistance(actualPos, endPos);
                int distanceFromStart = Util.getDiagonalDistance(actualPos, startPos);
                labirinth[i][j].setGreedyH(distanceFromEnd + distanceFromStart);
                labirinth[i][j].setCostH(distanceFromEnd);
            }
        }
        labirinth[startPos.getY()][startPos.getX()] = new Start(startPos.getX(),startPos.getY());
        labirinth[endPos.getY()][endPos.getX()] = new End(endPos.getX(),endPos.getY());

        return new Labirinth(labirinth, startPos, endPos,  labirinth[startPos.getY()][startPos.getX()],  labirinth[endPos.getY()][endPos.getX()], labWidth, labirinthTmp.size());
    }


    public static List<String> loadLabirinths() {
        List<String> files = new ArrayList<>();
        File labFolder = new File(labDirectory);
        for (final File fileEntry : labFolder.listFiles()) {
            if (fileEntry.isDirectory())
                continue;
            files.add(fileEntry.getName());
        }
        return files.stream().sorted().collect(Collectors.toList());
    }


    private static Position getPositions(String line) {
        line = line.replaceAll("[^0-9]+", " ");
        int[] arr = Arrays.asList(line.trim().split(" ")).stream().mapToInt(Integer::valueOf).toArray();
        return new Position(arr[0], arr[1]);
    }

}

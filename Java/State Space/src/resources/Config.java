package resources;

import java.awt.*;

public class Config {
    public static Color componentsBG = new Color(107, 110, 112);
    public static Color screenBG = new Color(26, 26, 29);
    public static Color wallBG = new Color(49, 68, 85);
    public static Color tileBG = new Color(203, 204, 205);
    public static Color startBG = new Color(245, 66, 66);
    public static Color endBG = new Color(224, 20, 221);
    public static Color actualBG = new Color(168, 36, 36);
    public static Color openBG = new Color(50, 233, 150);
    public static Color closedBG = new Color(36, 168, 36);
    public static Color pathBG = new Color(22, 105, 22);
    public static int algDelayMilis = 1;


}

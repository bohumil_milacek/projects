package trojan.thedrake;

public enum GameResult {
	VICTORY, DRAW, IN_PLAY;
}

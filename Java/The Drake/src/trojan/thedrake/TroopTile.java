package trojan.thedrake;

import java.util.ArrayList;
import java.util.List;

public class TroopTile implements Tile {

    private final Troop troop;
    private final PlayingSide side;
    private final TroopFace face;

    public TroopTile(Troop troop, PlayingSide side, TroopFace face) {
        this.troop = troop;
        this.side = side;
        this.face = face;
    }

    public Troop troop() {
        return troop;
    }

    public PlayingSide side() {
        return side;
    }

    public TroopFace face() {
        return face;
    }

    @Override
    public boolean canStepOn() {
        return this.troop == null;
    }

    @Override
    public boolean hasTroop() {
        return this.troop != null;
    }

    public TroopTile flipped() {
        if (this.face == TroopFace.AVERS) {
            return new TroopTile(troop(), side(), TroopFace.REVERS);
        }

        return new TroopTile(troop(), side(), TroopFace.AVERS);
    }

    @Override
    public List<Move> movesFrom(BoardPos pos, GameState state) {
        List<TroopAction> troopActions = troop.actions(face);
        List<Move> moves = new ArrayList<>();
        for (TroopAction troopAction : troopActions) {
            moves.addAll(troopAction.movesFrom(pos, side, state));
        }
        return moves;
    }


}

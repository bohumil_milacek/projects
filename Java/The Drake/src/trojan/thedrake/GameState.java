package trojan.thedrake;

import java.io.PrintWriter;
import java.util.Iterator;
import java.util.Optional;

public class GameState implements JSONSerializable {
    private final Board board;
    private final PlayingSide sideOnTurn;
    private final Army blueArmy;
    private final Army orangeArmy;
    private final GameResult result;

    public GameState(
            Board board,
            Army blueArmy,
            Army orangeArmy) {
        this(board, blueArmy, orangeArmy, PlayingSide.BLUE, GameResult.IN_PLAY);
    }

    public GameState(
            Board board,
            Army blueArmy,
            Army orangeArmy,
            PlayingSide sideOnTurn,
            GameResult result) {
        this.board = board;
        this.sideOnTurn = sideOnTurn;
        this.blueArmy = blueArmy;
        this.orangeArmy = orangeArmy;
        this.result = result;
    }

    public Board board() {
        return board;
    }

    public PlayingSide sideOnTurn() {
        return sideOnTurn;
    }

    public GameResult result() {
        return result;
    }

    public Army army(PlayingSide side) {
        if (side == PlayingSide.BLUE) {
            return blueArmy;
        }

        return orangeArmy;
    }

    public Army armyOnTurn() {
        return army(sideOnTurn);
    }

    public Army armyNotOnTurn() {
        if (sideOnTurn == PlayingSide.BLUE)
            return orangeArmy;

        return blueArmy;
    }

    public Tile tileAt(BoardPos pos) {
        //Checking if any army troop is on position "pos"
        for (BoardPos troopPosition : blueArmy.boardTroops().troopPositions()) {
            if (pos.equals(troopPosition)) {
                return blueArmy.boardTroops().at(pos).get();
            }
        }
        for (BoardPos troopPosition : orangeArmy.boardTroops().troopPositions()) {
            if (pos.equals(troopPosition)) {
                return orangeArmy.boardTroops().at(pos).get();
            }
        }

        return board.at(pos);
    }

    private boolean canStepFrom(TilePos origin) {
        // Game start, can't make any step - therefore returning false
        if (!armyOnTurn().boardTroops().isLeaderPlaced() || armyOnTurn().boardTroops().isPlacingGuards()) {
            return false;
        }
        // Cant make step while game is not being played or if position is out of board
        if (result != GameResult.IN_PLAY || origin == TilePos.OFF_BOARD) {
            return false;
        }
        //Can't make step if there is not armyOnTurn troop on the position(there is nothing or enemy)
        return armyOnTurn().boardTroops().at(origin).isPresent();
    }

    private boolean canStepTo(TilePos target) {
        // Cant make step while game is not being played or if target is out of board
        if (result != GameResult.IN_PLAY || target == TilePos.OFF_BOARD) {
            return false;
        }
        BoardPos boardTargetPosition = board.positionFactory().pos(target.i(), target.j());
        Tile tileToStepOn = tileAt(boardTargetPosition);
        return tileToStepOn == null ? false : tileToStepOn.canStepOn();
    }

    private boolean canCaptureOn(TilePos target) {
        // Cant make step while game is not being played or if target is out of board
        if (result != GameResult.IN_PLAY || target == TilePos.OFF_BOARD) {
            return false;
        }
        return armyNotOnTurn().boardTroops().at(target).isPresent();
    }

    public boolean canStep(TilePos origin, TilePos target) {
        return canStepFrom(origin) && canStepTo(target);
    }

    public boolean canCapture(TilePos origin, TilePos target) {
        return canStepFrom(origin) && canCaptureOn(target);
    }

    public boolean canPlaceFromStack(TilePos target) {
        // Cant make step while game is not being played or if target is out of board or if can't step to target(MOUNTANIN or so)
        if (result != GameResult.IN_PLAY || target == TilePos.OFF_BOARD || !canStepTo(target)) {
            return false;
        }

        boolean isNextToAnyTroop = false;
        for (BoardPos troopPosition : armyOnTurn().boardTroops().troopPositions()) {
            if (target.isNextTo(troopPosition)) {
                isNextToAnyTroop = true;
            }
        }
        if (!isNextToAnyTroop && armyOnTurn().boardTroops().isLeaderPlaced())
            return false;

        // Game start, checking if placing leaders on correct positions
        if (!armyOnTurn().boardTroops().isLeaderPlaced() &&
                ((sideOnTurn == PlayingSide.BLUE && target.j() != 0) ||
                        (sideOnTurn == PlayingSide.ORANGE && target.j() != board.dimension() - 1))) {
            return false;
        }

        // Game start, checking if placing guards on correct position,if guards are being placed next to leader
        if (armyOnTurn().boardTroops().isPlacingGuards() &&
                !armyOnTurn().boardTroops().leaderPosition().isNextTo(target)) {
            return false;
        }


        //Can't place troop, if there is not any in stack
        if (armyOnTurn().stack().isEmpty()) {
            return false;
        }
        return true;
    }

    public GameState stepOnly(BoardPos origin, BoardPos target) {
        if (canStep(origin, target))
            return createNewGameState(
                    armyNotOnTurn(),
                    armyOnTurn().troopStep(origin, target), GameResult.IN_PLAY);

        throw new IllegalArgumentException();
    }

    public GameState stepAndCapture(BoardPos origin, BoardPos target) {
        if (canCapture(origin, target)) {
            Troop captured = armyNotOnTurn().boardTroops().at(target).get().troop();
            GameResult newResult = GameResult.IN_PLAY;

            if (armyNotOnTurn().boardTroops().leaderPosition().equals(target))
                newResult = GameResult.VICTORY;

            return createNewGameState(
                    armyNotOnTurn().removeTroop(target),
                    armyOnTurn().troopStep(origin, target).capture(captured), newResult);
        }

        throw new IllegalArgumentException();
    }

    public GameState captureOnly(BoardPos origin, BoardPos target) {
        if (canCapture(origin, target)) {
            Troop captured = armyNotOnTurn().boardTroops().at(target).get().troop();
            GameResult newResult = GameResult.IN_PLAY;

            if (armyNotOnTurn().boardTroops().leaderPosition().equals(target))
                newResult = GameResult.VICTORY;

            return createNewGameState(
                    armyNotOnTurn().removeTroop(target),
                    armyOnTurn().troopFlip(origin).capture(captured), newResult);
        }

        throw new IllegalArgumentException();
    }

    public GameState placeFromStack(BoardPos target) {
        if (canPlaceFromStack(target)) {
            return createNewGameState(
                    armyNotOnTurn(),
                    armyOnTurn().placeFromStack(target),
                    GameResult.IN_PLAY);
        }
        throw new IllegalArgumentException();
    }

    public GameState resign() {
        return createNewGameState(
                armyNotOnTurn(),
                armyOnTurn(),
                GameResult.VICTORY);
    }

    public GameState draw() {
        return createNewGameState(
                armyOnTurn(),
                armyNotOnTurn(),
                GameResult.DRAW);
    }

    private GameState createNewGameState(Army armyOnTurn, Army armyNotOnTurn, GameResult result) {
        if (armyOnTurn.side() == PlayingSide.BLUE) {
            return new GameState(board, armyOnTurn, armyNotOnTurn, PlayingSide.BLUE, result);
        }

        return new GameState(board, armyNotOnTurn, armyOnTurn, PlayingSide.ORANGE, result);
    }

    @Override
    public void toJSON(PrintWriter writer) {
        String json = "{";
        json += "\"result\":";
        json += "\"" + result + "\",";
        json += "\"board\":";
        json += "{\"dimension\":" + board.dimension() + ",";
        json += "\"tiles\":[";

        for (int i = 0; i < board.dimension(); i++) {
            for (int j = 0; j < board.dimension(); j++) {
                json += "\"" + board.at(board.positionFactory().pos(j, i)) + "\"";

                if (i != board.dimension() - 1 || j != board.dimension() - 1) {
                    json += ",";
                }
            }
        }

        json += "]},\"blueArmy\":{\"boardTroops\":{\"side\":";
        json += "\"BLUE\",\"leaderPosition\":\"" + blueArmy.boardTroops().leaderPosition() + "\",";
        json += "\"guards\":" + blueArmy.boardTroops().guards() + ",";
        json += "\"troopMap\":{";


        for (int i = 0; i < board.dimension(); i++) {
            for (int j = 0; j < board.dimension(); j++) {
                if (blueArmy.boardTroops().at(board.positionFactory().pos(i, j)).isPresent()) {
                    json += "\"" + board.positionFactory().pos(i, j) + "\":{";
                    json += "\"troop\":\"";
                    json += blueArmy.boardTroops().at(board.positionFactory().pos(i, j)).get().troop().name();
                    json += "\",\"side\":\"BLUE\",\"face\":\"";
                    json += blueArmy.boardTroops().at(board.positionFactory().pos(i, j)).get().face() + "\"},";

                }
            }
        }
        //if ()

        int strLen = json.length();
        if (json.substring(strLen - 1, strLen).equals(",")) {
            json = json.substring(0, strLen - 1);
        }

        json += "}},\"stack\":[";

        for (Troop troop : blueArmy.stack()) {
            json += "\"" + troop.name() + "\",";
        }

        strLen = json.length();
        if (json.substring(strLen - 1, strLen).equals(",")) {
            json = json.substring(0, strLen - 1);
        }

        json += "],\"captured\":[";

        for (Troop troop : blueArmy.captured()) {
            json += "\"" + troop.name() + "\",";
        }

        strLen = json.length();
        if (json.substring(strLen - 1, strLen).equals(",")) {
            json = json.substring(0, strLen - 1);
        }

        json += "]},";


        json += "\"orangeArmy\":{\"boardTroops\":{\"side\":";
        json += "\"ORANGE\",\"leaderPosition\":\"" + orangeArmy.boardTroops().leaderPosition() + "\",";
        json += "\"guards\":" + orangeArmy.boardTroops().guards() + ",";
        json += "\"troopMap\":{";


        for (int i = 0; i < board.dimension(); i++) {
            for (int j = 0; j < board.dimension(); j++) {
                if (orangeArmy.boardTroops().at(board.positionFactory().pos(i, j)).isPresent()) {
                    json += "\"" + board.positionFactory().pos(i, j) + "\":{";
                    json += "\"troop\":\"";
                    json += orangeArmy.boardTroops().at(board.positionFactory().pos(i, j)).get().troop().name();
                    json += "\",\"side\":\"ORANGE\",\"face\":\"";
                    json += orangeArmy.boardTroops().at(board.positionFactory().pos(i, j)).get().face() + "\"},";

                }
            }
        }
        //if ()

        strLen = json.length();
        if (json.substring(strLen - 1, strLen).equals(",")) {
            json = json.substring(0, strLen - 1);
        }

        json += "}},\"stack\":[";

        for (Troop troop : orangeArmy.stack()) {
            json += "\"" + troop.name() + "\",";
        }

        strLen = json.length();
        if (json.substring(strLen - 1, strLen).equals(",")) {
            json = json.substring(0, strLen - 1);
        }

        json += "],\"captured\":[";

        for (Troop troop : orangeArmy.captured()) {
            json += "\"" + troop.name() + "\",";
        }

        strLen = json.length();
        if (json.substring(strLen - 1, strLen).equals(",")) {
            json = json.substring(0, strLen - 1);
        }

        json += "]}";


        json += "}";
        writer.write(json);
    }
}

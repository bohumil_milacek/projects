package trojan.thedrake;

public class Board {

	private final PositionFactory positionFactory;

	private final BoardTile[][] boardTile;

	public Board(int dimension) {

		this.positionFactory = new PositionFactory(dimension);
		this.boardTile = new BoardTile[dimension][dimension];

		for(int i = 0; i < dimension; i++)
		{
			for (int j = 0; j < dimension; j++)
			{
				boardTile[i][j] = BoardTile.EMPTY;
			}
		}
	}

	public int dimension() {
		return positionFactory.dimension();
	} 
	
	public BoardTile at(BoardPos pos) {

		return boardTile[pos.j()][pos.i()];
	}
		
	public Board withTiles(TileAt ...ats) {

		Board newBoard = new Board(dimension());

		for(int i = 0; i < positionFactory.dimension(); i++)
		{
			newBoard.boardTile[i] = this.boardTile[i].clone();
		}

		for (int i = 0; i < ats.length; i++) {

			newBoard.boardTile[ats[i].pos.j()][ats[i].pos.i()] = ats[i].tile;
		}

		return newBoard;
	}
	
	public PositionFactory positionFactory() {
		return new PositionFactory(positionFactory.dimension());
	}
	
	public static class TileAt {
		public final BoardPos pos;
		public final BoardTile tile;
		
		public TileAt(BoardPos pos, BoardTile tile) {
			this.pos = pos;
			this.tile = tile;
		}
	}
}


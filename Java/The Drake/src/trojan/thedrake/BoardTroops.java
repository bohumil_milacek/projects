package trojan.thedrake;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public class BoardTroops {
    private final PlayingSide playingSide;
    private final Map<BoardPos, TroopTile> troopMap;
    private final TilePos leaderPosition;
    private final int guards;

    public BoardTroops(PlayingSide playingSide) {
        this.playingSide = playingSide;
        troopMap = Collections.emptyMap();
        leaderPosition = TilePos.OFF_BOARD;
        guards = 0;
    }

    public BoardTroops(
            PlayingSide playingSide,
            Map<BoardPos, TroopTile> troopMap,
            TilePos leaderPosition,
            int guards) {
        this.playingSide = playingSide;
        this.troopMap = troopMap;
        this.leaderPosition = leaderPosition;
        this.guards = guards;
    }

    public Optional<TroopTile> at(TilePos pos) {

        if (troopMap.containsKey(pos)) {
            Optional<TroopTile> t = Optional.of(troopMap.get(pos));
            return t;
        }
        return Optional.empty();
    }

    public PlayingSide playingSide() {
        return playingSide;
    }

    public TilePos leaderPosition() {
        return leaderPosition;
    }


    // returns always 0 || 1 || 2
    public int guards() {
        return guards;
    }

    public boolean isLeaderPlaced() {
        return leaderPosition != TilePos.OFF_BOARD;
    }

    public boolean isPlacingGuards() {
       // return (troopMap.size() >= 1 && troopMap.size() <= 2);
        return  isLeaderPlaced() && guards <2;
    }

    public Set<BoardPos> troopPositions() {
        return troopMap.keySet();
    }

    public BoardTroops placeTroop(Troop troop, BoardPos target) {
        if (troopMap.containsKey(target)) {
            throw new IllegalArgumentException(
                    "Target is already taken");
        }

        TroopTile newTroopTile = new TroopTile(troop, playingSide, TroopFace.AVERS);
        Map<BoardPos, TroopTile> newTroops = new HashMap<>(troopMap);
        newTroops.put(target, newTroopTile);
        int newGuards = guards;
        //Leader is being placed, let's set his position
        if (newTroops.size() == 1) {
            return new BoardTroops(playingSide, newTroops, target, guards);
        }
        if (isLeaderPlaced() && isPlacingGuards()) {
            newGuards++;
        }
        return new BoardTroops(playingSide, newTroops, leaderPosition, newGuards);

    }

    public BoardTroops troopStep(BoardPos origin, BoardPos target) {
        if (!isLeaderPlaced()) {
            throw new IllegalStateException(
                    "Cannot move troops before the leader is placed.");
        }

        if (isPlacingGuards()) {
            throw new IllegalStateException(
                    "Cannot move troops before guards are placed.");
        }
        if (!troopMap.containsKey(origin) || troopMap.containsKey(target)) {
            throw new IllegalArgumentException(
                    "Origin is empty or target isn't empty");

        }

        Map<BoardPos, TroopTile> newTroops = new HashMap<>(troopMap);
        TroopTile tile = newTroops.remove(origin);
        newTroops.put(target, tile.flipped());

        if (leaderPosition.equalsTo(origin.i(), origin.j())) {
            return new BoardTroops(playingSide, newTroops, target, guards);
        }

        return new BoardTroops(playingSide, newTroops, leaderPosition, guards);
    }

    public BoardTroops troopFlip(BoardPos origin) {
        if (!isLeaderPlaced()) {
            throw new IllegalStateException(
                    "Cannot move troops before the leader is placed.");
        }

        if (isPlacingGuards()) {
            throw new IllegalStateException(
                    "Cannot move troops before guards are placed.");
        }

        if (!at(origin).isPresent())
            throw new IllegalArgumentException();

        Map<BoardPos, TroopTile> newTroops = new HashMap<>(troopMap);
        TroopTile tile = newTroops.remove(origin);
        newTroops.put(origin, tile.flipped());

        return new BoardTroops(playingSide(), newTroops, leaderPosition, guards);
    }

    public BoardTroops removeTroop(BoardPos target) {
        if (!isLeaderPlaced()) {
            throw new IllegalStateException(
                    "Cannot move troops before the leader is placed.");
        }

        if (isPlacingGuards()) {
            throw new IllegalStateException(
                    "Cannot move troops before guards are placed.");
        }

        if (!troopMap.containsKey(target)) {
            throw new IllegalArgumentException(
                    "Troop is not present");
        }
        Map<BoardPos, TroopTile> newTroops = new HashMap<>(troopMap);
        newTroops.remove(target);
        if (leaderPosition.equalsTo(target.i(), target.j())) {
            return new BoardTroops(playingSide, newTroops, TilePos.OFF_BOARD, guards);
        }
        return new BoardTroops(playingSide, newTroops, leaderPosition, guards);
    }
}

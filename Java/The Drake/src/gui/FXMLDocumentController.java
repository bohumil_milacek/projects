package gui;

import gui.ui.BoardView;
import gui.ui.UnitStack;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.*;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import trojan.thedrake.*;

import javax.swing.*;
import java.io.IOException;
import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;

public class FXMLDocumentController {


    @FXML
    Button btnTwoPlayers;


    @FXML
    public void exitMenu() {
        System.exit(0);
    }


    @FXML
    public void twoPlayersGamePressed(ActionEvent event) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("Game.fxml"));
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        Scene rootScene = new Scene(root, 900, 600);
        rootScene.getStylesheets().add(getClass().getResource("game_css.css").toExternalForm());

        window.setScene(rootScene);
        window.setTitle("PvP");
        window.setFullScreen(true);
        GameState gs = createSampleGameState();

        UnitStack blueStack = new UnitStack(gs.army(PlayingSide.BLUE).stack(), PlayingSide.BLUE );
        UnitStack orangeStack = new UnitStack(gs.army(PlayingSide.BLUE).stack(), PlayingSide.ORANGE );
        UnitStack blueCaptured = new UnitStack( PlayingSide.BLUE );
        UnitStack orangeCaptured = new UnitStack( PlayingSide.ORANGE );

        HBox blueStackLayout = (HBox) rootScene.lookup("#blueStackLayout");
        HBox orangeStackLayout = (HBox) rootScene.lookup("#orangeStackLayout");
        Pane playerIndicator=(Pane) rootScene.lookup("#playerIndicator");
        Button btBackToMenu=(Button) rootScene.lookup("#btBackToMenu");
        btBackToMenu.setOnAction(clickEvent -> {
            try {
                goBackToMenu(clickEvent);
            } catch (IOException e) {
                System.out.println("chyba:" + e);
            }
        });
        blueStackLayout.getChildren().add(orangeCaptured);
        blueStackLayout.getChildren().add(blueStack);
        orangeStackLayout.getChildren().add(orangeStack);
        orangeStackLayout.getChildren().add(blueCaptured);

        BoardView boardView = new BoardView(gs, blueStack, orangeStack, blueCaptured, orangeCaptured,playerIndicator,btBackToMenu);
        BorderPane rootPane = (BorderPane) rootScene.getRoot();
        ((BorderPane)rootPane.lookup("#gridViewBorderPane")).setCenter(boardView);




    }

    private static GameState createSampleGameState() {
        Random random = new Random();
        Board board = new Board(4);
        PositionFactory positionFactory = board.positionFactory();
        board = board.withTiles(new Board.TileAt(positionFactory.pos(random.nextInt(4), random.nextInt(2) + 1), BoardTile.MOUNTAIN));
        return new StandardDrakeSetup().startState(board);
    }

    private void goBackToMenu(Event event) throws IOException {
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent root = FXMLLoader.load(getClass().getResource("main_menu.fxml"));
        window.setTitle("Main menu");
        Scene menuScene = new Scene(root, 700, 380);
        menuScene.getStylesheets().add(getClass().getResource("menu_css.css").toExternalForm());
        Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();
        window.setX((screenBounds.getWidth() - 700) / 2);
        window.setY((screenBounds.getHeight() - 380) / 2);
        window.setScene(menuScene);
    }
}

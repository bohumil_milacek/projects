package gui.ui;

import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import trojan.thedrake.TroopTile;

public class StackTroopTileView extends Pane {

    TroopTile troopTile;
    private TileBackgrounds backgrounds = new TileBackgrounds();
    private Border selectBorder = new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(3)));

    public StackTroopTileView(TroopTile troopTile) {
        this.troopTile = troopTile;
        setBorder(new Border(new BorderStroke(Color.BLACK,
                BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
        setBackground(backgrounds.get(troopTile));
        setPrefSize(100, 100);
    }
}

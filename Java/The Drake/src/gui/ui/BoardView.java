package gui.ui;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import trojan.thedrake.*;

import java.util.List;

public class BoardView extends GridPane implements TileViewContext {

    private GameState gameState;

    private ValidMoves validMoves;

    private TileView selected;

    private UnitStack blueStack;
    private UnitStack orangeStack;
    private UnitStack blueCaptured;
    private UnitStack orangeCaptured;
    private Pane playerIndicator;
    private Button btBackToMenu;

    public BoardView(GameState gameState, UnitStack blueStack, UnitStack orangeStack, UnitStack blueCaptured, UnitStack orangeCaptured, Pane playerIndicator, Button btBackToMenu) {
        this.gameState = gameState;
        this.validMoves = new ValidMoves(gameState);
        this.playerIndicator = playerIndicator;
        this.btBackToMenu = btBackToMenu;


        PositionFactory positionFactory = gameState.board().positionFactory();


        this.blueStack = blueStack;
        this.orangeStack = orangeStack;
        this.blueCaptured = blueCaptured;
        this.orangeCaptured = orangeCaptured;

        blueStack.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (BoardView.this.gameState.sideOnTurn() == PlayingSide.BLUE && selected != null) {
                    selected.unselect();
                    selected = null;
                    clearMoves();
                    showMoves(validMoves.movesFromStack());
                }
            }
        });

        orangeStack.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (BoardView.this.gameState.sideOnTurn() == PlayingSide.ORANGE && selected != null) {
                    selected.unselect();
                    selected = null;
                    clearMoves();
                    showMoves(validMoves.movesFromStack());
                }
            }
        });

        for (int y = 0; y < 4; y++) {
            for (int x = 0; x < 4; x++) {
                int i = x;
                int j = 3 - y;
                BoardPos boardPos = positionFactory.pos(i, j);
                TileView tileView = new TileView(boardPos, gameState.tileAt(boardPos), this);

                tileView.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        if (BoardView.this.gameState.canPlaceFromStack(tileView.position()) && selected == null) {
                            clearMoves();
                            placeTroop(tileView);
                            showMoves(validMoves.movesFromStack());
                        } else {
                            tileView.onClick();
                            if (BoardView.this.gameState.sideOnTurn() == PlayingSide.BLUE) {
                                blueCaptured.setTroops(BoardView.this.gameState.armyNotOnTurn().captured());
                            } else {
                                orangeCaptured.setTroops(BoardView.this.gameState.armyNotOnTurn().captured());
                            }

                            if (BoardView.this.gameState.result() == GameResult.VICTORY) {
                                String playerWon = "Blue";
                                if (BoardView.this.gameState.sideOnTurn() == PlayingSide.BLUE)
                                    playerWon = "Orange";
                                Alert alert = new Alert(Alert.AlertType.INFORMATION, playerWon + " player won the game", ButtonType.YES);
                                alert.showAndWait();
                                if (alert.getResult() == ButtonType.YES) {
                                    btBackToMenu.fire();
                                }
                            }
                        }
                        setPlayerIndicator();

                    }
                });
                add(tileView, x, y);
            }
        }

        setHgap(5);
        setVgap(5);
        setPadding(new Insets(15));
        setAlignment(Pos.CENTER);
        showMoves(validMoves.movesFromStack());
        setPlayerIndicator();

    }

    private void clearMoves() {
        for (Node node : getChildren()) {
            TileView tileView = (TileView) node;
            tileView.clearMove();
        }
    }

    private void showMoves(List<Move> moveList) {
        for (Move move : moveList) {
            tileViewAt(move.target()).setMove(move);
        }
    }

    private TileView tileViewAt(BoardPos target) {
        int index = (3 - target.j()) * 4 + target.i();
        return (TileView) getChildren().get(index);

    }

    @Override
    public void tileViewSelected(TileView tileView) {
        if (selected != null && selected != tileView) {
            selected.unselect();
        }

        selected = tileView;
        clearMoves();
        showMoves(validMoves.boardMoves(tileView.position()));
    }

    @Override
    public void executeMove(Move move) {
        selected.unselect();
        selected = null;
        clearMoves();
        gameState = move.execute(gameState);
        validMoves = new ValidMoves(gameState);
        updateTiles();
    }

    private void updateTiles() {
        for (Node node : getChildren()) {
            TileView tileView = (TileView) node;
            tileView.setTile(gameState.tileAt(tileView.position()));
            tileView.update();
        }
    }

    private void placeTroop(TileView tileView) {
        try {

            BoardView.this.gameState = BoardView.this.gameState.placeFromStack(tileView.position());
            this.validMoves = new ValidMoves(gameState);

            if (BoardView.this.gameState.sideOnTurn() != PlayingSide.BLUE) {
                blueStack.removeTroop();
                blueStack.updateTroops();
            } else {
                orangeStack.removeTroop();
                orangeStack.updateTroops();
            }

        } catch (IllegalArgumentException e) {
        }

        updateTiles();
    }


    private void setPlayerIndicator() {
        if (gameState.sideOnTurn() == PlayingSide.BLUE)
            playerIndicator.setBackground(new Background(new BackgroundFill(new Color(0, 0.75, 1, 1), CornerRadii.EMPTY, Insets.EMPTY)));
        else
            playerIndicator.setBackground(new Background(new BackgroundFill(new Color(1, 0.5, 0.31, 1), CornerRadii.EMPTY, Insets.EMPTY)));
    }

}

package gui.ui;

import javafx.scene.layout.VBox;
import trojan.thedrake.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class UnitStack extends VBox {
    List<TroopTile> troops = new ArrayList<>();
    PlayingSide playingSide;


    public UnitStack(PlayingSide playingSide ) {
        this.playingSide = playingSide;
        setPrefSize(100, 1000);
        setSpacing(2);

    }

    public UnitStack(List<Troop> troops, PlayingSide playingSide ) {
        //this.troops = troops;
        this(playingSide);
        for (Troop troop : troops) {
            TroopTile troopTile = new TroopTile(troop, playingSide, TroopFace.AVERS);
            this.troops.add(troopTile);
            getChildren().add(new StackTroopTileView(troopTile));
        }

    }


    public void removeTroop() {
        this.troops.remove(0);
    }

    public void updateTroops() {
        getChildren().clear();
        for (TroopTile troopTile : troops) {
            getChildren().add(new StackTroopTileView(troopTile));
        }
    }

    public void setTroops(List<Troop> captured) {
        troops.clear();
        for (Troop troop : captured) {
            TroopTile troopTile = new TroopTile(troop, playingSide, TroopFace.AVERS);
            this.troops.add(troopTile);
            getChildren().add(new StackTroopTileView(troopTile));
        }
        updateTroops();
    }
}

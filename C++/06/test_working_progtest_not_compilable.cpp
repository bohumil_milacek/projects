#ifndef __PROGTEST__

#include <cassert>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <list>
#include <algorithm>
#include <memory>
#include <functional>

using namespace std;
#endif /* __PROGTEST__ */


class CComponent {
public:
    static string LINE_START;

    virtual void print(ostream &out) const {
        out << "Component" << endl;
    };

    virtual CComponent *clone() const {
        return new CComponent(*this);
    }

    friend ostream &operator<<(ostream &out, const CComponent &component);
};

ostream &operator<<(ostream &out, const CComponent &component) {
    component.print(out);
    return out;
}

class CComputer {
    const string host;
    vector <string> addresses;
    vector <shared_ptr<const CComponent>> components;

public:
    static string LINE_START;

    CComputer(string host) : host(host) {}

    /*  CComputer(CComputer &computer) {}

      ~CComputer() {}*/

    CComputer &AddComponent(const CComponent &component) {
        //    cout << component << endl;
        const CComponent *cPtr = component.clone();
        shared_ptr<const CComponent> componentPtr(cPtr);
        components.push_back(componentPtr);
        return *this;
    }

    CComputer &AddAddress(string address) {
        addresses.push_back(address);
        return *this;
    }

    friend ostream &operator<<(ostream &out, const CComputer &computer);

    const string &Host() const {
        return host;
    }

};

ostream &operator<<(ostream &out, const CComputer &computer) {
    out << "Host: " << computer.host << endl;
    string componentPrefix = "+-";

    for (const auto &address : computer.addresses)
        out << CComputer::LINE_START << "+-" << address << endl;

    for (unsigned i = 0; i < computer.components.size(); i++) {
        if (i != computer.components.size() - 1) {
            CComponent::LINE_START = "| ";
        } else {
            CComponent::LINE_START = "  ";
            componentPrefix = "\\-";
        }
        out << CComputer::LINE_START << componentPrefix << *computer.components[i];
        CComponent::LINE_START = "";

    }
    return out;
}

class CNetwork {
     string name;
    vector <CComputer> computers;

public:
    CNetwork(string name) : name(name) {
    }


   /* CNetwork(CNetwork &network) : name(network.name),computers(network.computers){
    }*/

 //   ~CNetwork() {}



    CNetwork &AddComputer(CComputer computer) {
        computers.push_back(computer);
        return *this;
    }

    CComputer *FindComputer(string address) {
        for (unsigned i = 0; i < computers.size(); i++)
            if (computers[i].Host() == address)
                return &computers[i];

        return nullptr;
    }

    friend ostream &operator<<(ostream &out, const CNetwork &network);
};

ostream &operator<<(ostream &out, const CNetwork &network) {
    out << "Network: " << network.name << endl;
    string computerPrefix = "+-";
    for (unsigned i = 0; i < network.computers.size(); i++) {
        if (i != network.computers.size() - 1) {
            CComputer::LINE_START = "| ";
        } else {
            CComputer::LINE_START = "  ";
            computerPrefix = "\\-";
        }
        out << computerPrefix << network.computers[i];
    }
    CComputer::LINE_START = "";

    return out;
}

class CCPU : public CComponent {
    int numberOfCores;
    int frequency;
public:
    CCPU(int numberOfCores, int frequency) : numberOfCores(numberOfCores), frequency(frequency) {

    }

    CCPU(const CCPU &cpu) : numberOfCores(cpu.numberOfCores), frequency(cpu.frequency) {

    }

    virtual CCPU *clone() const {
        //cout << "Cloning CPU!" << endl;
        return new CCPU(*this);
    };

    virtual void print(ostream &out) const {
        out << "CPU, " << numberOfCores << " cores @ " << frequency << "MHz" << endl;
    }

};

class CMemory : public CComponent {
    int capacity;
public:
    CMemory(int capacity) : capacity(capacity) {}

    virtual CMemory *clone() const { return new CMemory(*this); };


    virtual void print(ostream &out) const {
        out << "Memory, " << capacity << " MiB" << endl;

    }

};

class CDisk : public CComponent {
    short TYPE;
    int capacity;
    vector <pair<string, int>> partitions;
public:
    static const short SSD = 0;
    static const short MAGNETIC = 1;


    CDisk(short TYPE, int capacity) : TYPE(TYPE), capacity(capacity) {}

    virtual CDisk *clone() const { return new CDisk(*this); };


    CDisk &AddPartition(int partitionCapacity, string partitionName) {
        partitions.push_back(make_pair(partitionName, partitionCapacity));
        return *this;
    }

    virtual void print(ostream &out) const {
        out << (TYPE == 0 ? "SSD" : "HDD") << ", " << capacity << " GiB" << endl;
        // "| | +-[0]: 50 GiB, /\n"
        string partitionPrefix = "+-";
        for (unsigned i = 0; i < partitions.size(); i++) {
            if (i != partitions.size() - 1) // last element
                partitionPrefix = "+-";
            else
                partitionPrefix = "\\-";

            out << CComputer::LINE_START << CComponent::LINE_START << partitionPrefix << "[" << i << "]: "
                << partitions[i].second << " GiB, "
                << partitions[i].first
                << endl;
        }


    }


private:

};

string CComputer::LINE_START = "";
string CComponent::LINE_START = "";

#ifndef __PROGTEST__

template<typename _T>
string toString(const _T &x) {
    ostringstream oss;
    oss << x;
    return oss.str();
}


void progtestTest() {
    CNetwork n("FIT network");
    n.AddComputer(
            CComputer("progtest.fit.cvut.cz").
                    AddAddress("147.32.232.142").
                    AddComponent(CCPU(8, 2400)).
                    AddComponent(CCPU(8, 1200)).
                    AddComponent(CDisk(CDisk::MAGNETIC, 1500).
                    AddPartition(50, "/").
                    AddPartition(5, "/boot").
                    AddPartition(1000, "/var")).
                    AddComponent(CDisk(CDisk::SSD, 60).
                    AddPartition(60, "/data")).
                    AddComponent(CMemory(2000)).
                    AddComponent(CMemory(2000))).
            AddComputer(
            CComputer("edux.fit.cvut.cz").
                    AddAddress("147.32.232.158").
                    AddComponent(CCPU(4, 1600)).
                    AddComponent(CMemory(4000)).
                    AddComponent(CDisk(CDisk::MAGNETIC, 2000).
                    AddPartition(100, "/").
                    AddPartition(1900, "/data"))).
            AddComputer(
            CComputer("imap.fit.cvut.cz").
                    AddAddress("147.32.232.238").
                    AddComponent(CCPU(4, 2500)).
                    AddAddress("2001:718:2:2901::238").
                    AddComponent(CMemory(8000)));
    assert(toString(n) ==
           "Network: FIT network\n"
           "+-Host: progtest.fit.cvut.cz\n"
           "| +-147.32.232.142\n"
           "| +-CPU, 8 cores @ 2400MHz\n"
           "| +-CPU, 8 cores @ 1200MHz\n"
           "| +-HDD, 1500 GiB\n"
           "| | +-[0]: 50 GiB, /\n"
           "| | +-[1]: 5 GiB, /boot\n"
           "| | \\-[2]: 1000 GiB, /var\n"
           "| +-SSD, 60 GiB\n"
           "| | \\-[0]: 60 GiB, /data\n"
           "| +-Memory, 2000 MiB\n"
           "| \\-Memory, 2000 MiB\n"
           "+-Host: edux.fit.cvut.cz\n"
           "| +-147.32.232.158\n"
           "| +-CPU, 4 cores @ 1600MHz\n"
           "| +-Memory, 4000 MiB\n"
           "| \\-HDD, 2000 GiB\n"
           "|   +-[0]: 100 GiB, /\n"
           "|   \\-[1]: 1900 GiB, /data\n"
           "\\-Host: imap.fit.cvut.cz\n"
           "  +-147.32.232.238\n"
           "  +-2001:718:2:2901::238\n"
           "  +-CPU, 4 cores @ 2500MHz\n"
           "  \\-Memory, 8000 MiB\n");
    CNetwork x = n;
    auto c = x.FindComputer("imap.fit.cvut.cz");
    assert(toString(*c) ==
           "Host: imap.fit.cvut.cz\n"
           "+-147.32.232.238\n"
           "+-2001:718:2:2901::238\n"
           "+-CPU, 4 cores @ 2500MHz\n"
           "\\-Memory, 8000 MiB\n");
    c->AddComponent(CDisk(CDisk::MAGNETIC, 1000).
            AddPartition(100, "system").
            AddPartition(200, "WWW").
            AddPartition(700, "mail"));
    assert(toString(x) ==
           "Network: FIT network\n"
           "+-Host: progtest.fit.cvut.cz\n"
           "| +-147.32.232.142\n"
           "| +-CPU, 8 cores @ 2400MHz\n"
           "| +-CPU, 8 cores @ 1200MHz\n"
           "| +-HDD, 1500 GiB\n"
           "| | +-[0]: 50 GiB, /\n"
           "| | +-[1]: 5 GiB, /boot\n"
           "| | \\-[2]: 1000 GiB, /var\n"
           "| +-SSD, 60 GiB\n"
           "| | \\-[0]: 60 GiB, /data\n"
           "| +-Memory, 2000 MiB\n"
           "| \\-Memory, 2000 MiB\n"
           "+-Host: edux.fit.cvut.cz\n"
           "| +-147.32.232.158\n"
           "| +-CPU, 4 cores @ 1600MHz\n"
           "| +-Memory, 4000 MiB\n"
           "| \\-HDD, 2000 GiB\n"
           "|   +-[0]: 100 GiB, /\n"
           "|   \\-[1]: 1900 GiB, /data\n"
           "\\-Host: imap.fit.cvut.cz\n"
           "  +-147.32.232.238\n"
           "  +-2001:718:2:2901::238\n"
           "  +-CPU, 4 cores @ 2500MHz\n"
           "  +-Memory, 8000 MiB\n"
           "  \\-HDD, 1000 GiB\n"
           "    +-[0]: 100 GiB, system\n"
           "    +-[1]: 200 GiB, WWW\n"
           "    \\-[2]: 700 GiB, mail\n");
    assert(toString(n) ==
           "Network: FIT network\n"
           "+-Host: progtest.fit.cvut.cz\n"
           "| +-147.32.232.142\n"
           "| +-CPU, 8 cores @ 2400MHz\n"
           "| +-CPU, 8 cores @ 1200MHz\n"
           "| +-HDD, 1500 GiB\n"
           "| | +-[0]: 50 GiB, /\n"
           "| | +-[1]: 5 GiB, /boot\n"
           "| | \\-[2]: 1000 GiB, /var\n"
           "| +-SSD, 60 GiB\n"
           "| | \\-[0]: 60 GiB, /data\n"
           "| +-Memory, 2000 MiB\n"
           "| \\-Memory, 2000 MiB\n"
           "+-Host: edux.fit.cvut.cz\n"
           "| +-147.32.232.158\n"
           "| +-CPU, 4 cores @ 1600MHz\n"
           "| +-Memory, 4000 MiB\n"
           "| \\-HDD, 2000 GiB\n"
           "|   +-[0]: 100 GiB, /\n"
           "|   \\-[1]: 1900 GiB, /data\n"
           "\\-Host: imap.fit.cvut.cz\n"
           "  +-147.32.232.238\n"
           "  +-2001:718:2:2901::238\n"
           "  +-CPU, 4 cores @ 2500MHz\n"
           "  \\-Memory, 8000 MiB\n");
}

void myTest() {
    CNetwork n("FIT network");
    n.AddComputer(
            CComputer("progtest.fit.cvut.cz").
                    AddAddress("147.32.232.142").
                    AddComponent(CCPU(8, 2400)).
                    AddComponent(CCPU(8, 1200)).
                    AddComponent(CDisk(CDisk::MAGNETIC, 1500).
                    AddPartition(50, "/").
                    AddPartition(5, "/boot").
                    AddPartition(1000, "/var")).
                    AddComponent(CDisk(CDisk::SSD, 60).
                    AddPartition(60, "/data")).
                    AddComponent(CMemory(2000)).
                    AddComponent(CMemory(2000))).
            AddComputer(
            CComputer("edux.fit.cvut.cz").
                    AddAddress("147.32.232.158").
                    AddComponent(CCPU(4, 1600)).
                    AddComponent(CMemory(4000)).
                    AddComponent(CDisk(CDisk::MAGNETIC, 2000).
                    AddPartition(100, "/").
                    AddPartition(1900, "/data"))).
            AddComputer(
            CComputer("imap.fit.cvut.cz").
                    AddAddress("147.32.232.238").
                    AddComponent(CCPU(4, 2500)).
                    AddAddress("2001:718:2:2901::238").
                    AddComponent(CMemory(8000)));

    /* cout << endl << endl << endl;
     cout << toString(n);
     cout << endl << endl << endl;*/
    assert(toString(n) ==
           "Network: FIT network\n"
           "+-Host: progtest.fit.cvut.cz\n"
           "| +-147.32.232.142\n"
           "| +-CPU, 8 cores @ 2400MHz\n"
           "| +-CPU, 8 cores @ 1200MHz\n"
           "| +-HDD, 1500 GiB\n"
           "| | +-[0]: 50 GiB, /\n"
           "| | +-[1]: 5 GiB, /boot\n"
           "| | \\-[2]: 1000 GiB, /var\n"
           "| +-SSD, 60 GiB\n"
           "| | \\-[0]: 60 GiB, /data\n"
           "| +-Memory, 2000 MiB\n"
           "| \\-Memory, 2000 MiB\n"
           "+-Host: edux.fit.cvut.cz\n"
           "| +-147.32.232.158\n"
           "| +-CPU, 4 cores @ 1600MHz\n"
           "| +-Memory, 4000 MiB\n"
           "| \\-HDD, 2000 GiB\n"
           "|   +-[0]: 100 GiB, /\n"
           "|   \\-[1]: 1900 GiB, /data\n"
           "\\-Host: imap.fit.cvut.cz\n"
           "  +-147.32.232.238\n"
           "  +-2001:718:2:2901::238\n"
           "  +-CPU, 4 cores @ 2500MHz\n"
           "  \\-Memory, 8000 MiB\n");
    CNetwork x = n;
    auto c = x.FindComputer("imap.fit.cvut.cz");

    //   cout << toString(*c) << endl;
    assert(toString(*c) ==
           "Host: imap.fit.cvut.cz\n"
           "+-147.32.232.238\n"
           "+-2001:718:2:2901::238\n"
           "+-CPU, 4 cores @ 2500MHz\n"
           "\\-Memory, 8000 MiB\n");
    c->AddComponent(CDisk(CDisk::MAGNETIC, 1000).
            AddPartition(100, "system").
            AddPartition(200, "WWW").
            AddPartition(700, "mail"));
    assert(toString(x) ==
           "Network: FIT network\n"
           "+-Host: progtest.fit.cvut.cz\n"
           "| +-147.32.232.142\n"
           "| +-CPU, 8 cores @ 2400MHz\n"
           "| +-CPU, 8 cores @ 1200MHz\n"
           "| +-HDD, 1500 GiB\n"
           "| | +-[0]: 50 GiB, /\n"
           "| | +-[1]: 5 GiB, /boot\n"
           "| | \\-[2]: 1000 GiB, /var\n"
           "| +-SSD, 60 GiB\n"
           "| | \\-[0]: 60 GiB, /data\n"
           "| +-Memory, 2000 MiB\n"
           "| \\-Memory, 2000 MiB\n"
           "+-Host: edux.fit.cvut.cz\n"
           "| +-147.32.232.158\n"
           "| +-CPU, 4 cores @ 1600MHz\n"
           "| +-Memory, 4000 MiB\n"
           "| \\-HDD, 2000 GiB\n"
           "|   +-[0]: 100 GiB, /\n"
           "|   \\-[1]: 1900 GiB, /data\n"
           "\\-Host: imap.fit.cvut.cz\n"
           "  +-147.32.232.238\n"
           "  +-2001:718:2:2901::238\n"
           "  +-CPU, 4 cores @ 2500MHz\n"
           "  +-Memory, 8000 MiB\n"
           "  \\-HDD, 1000 GiB\n"
           "    +-[0]: 100 GiB, system\n"
           "    +-[1]: 200 GiB, WWW\n"
           "    \\-[2]: 700 GiB, mail\n");
    assert(toString(n) ==
           "Network: FIT network\n"
           "+-Host: progtest.fit.cvut.cz\n"
           "| +-147.32.232.142\n"
           "| +-CPU, 8 cores @ 2400MHz\n"
           "| +-CPU, 8 cores @ 1200MHz\n"
           "| +-HDD, 1500 GiB\n"
           "| | +-[0]: 50 GiB, /\n"
           "| | +-[1]: 5 GiB, /boot\n"
           "| | \\-[2]: 1000 GiB, /var\n"
           "| +-SSD, 60 GiB\n"
           "| | \\-[0]: 60 GiB, /data\n"
           "| +-Memory, 2000 MiB\n"
           "| \\-Memory, 2000 MiB\n"
           "+-Host: edux.fit.cvut.cz\n"
           "| +-147.32.232.158\n"
           "| +-CPU, 4 cores @ 1600MHz\n"
           "| +-Memory, 4000 MiB\n"
           "| \\-HDD, 2000 GiB\n"
           "|   +-[0]: 100 GiB, /\n"
           "|   \\-[1]: 1900 GiB, /data\n"
           "\\-Host: imap.fit.cvut.cz\n"
           "  +-147.32.232.238\n"
           "  +-2001:718:2:2901::238\n"
           "  +-CPU, 4 cores @ 2500MHz\n"
           "  \\-Memory, 8000 MiB\n");
}

int main(void) {
    myTest();

    progtestTest();
    return 0;
}

#endif /* __PROGTEST__ */

#ifndef __PROGTEST__

#include <cassert>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <list>
#include <algorithm>
#include <memory>
#include <functional>

using namespace std;
#endif /* __PROGTEST__ */


// parent of CMemory, CDisk, CCPU
class CComponent {
public:
    // used for output
    static string LINE_START;

    virtual ~CComponent() = default;

    // method implemented individualy by each component
    virtual void print(ostream &out) const {
        out << "Component" << endl;
    };

    virtual CComponent *clone() const {
        return new CComponent(*this);
    }

    friend ostream &operator<<(ostream &out, const CComponent &component);
};

ostream &operator<<(ostream &out, const CComponent &component) {
    component.print(out);
    return out;
}


class CComputer {
    string host;
    vector <string> addresses;
    vector <shared_ptr<CComponent>> components;

public:
    // used for output
    static string LINE_START;

    CComputer(string host) : host(host) {}

    //Creates copy of component, adds it to computer
    CComputer &AddComponent(const CComponent &component) {
        shared_ptr <CComponent> componentPtr(component.clone());
        components.push_back(componentPtr);
        return *this;
    }

    // adds addres to computer
    CComputer &AddAddress(string address) {
        addresses.push_back(address);
        return *this;
    }

    friend ostream &operator<<(ostream &out, const CComputer &computer);

    const string &Host() const {
        return host;
    }

};

ostream &operator<<(ostream &out, const CComputer &computer) {
    out << "Host: " << computer.host << endl;
    string componentPrefix = "+-";

    //print all addresses
    for (const auto &address : computer.addresses)
        out << CComputer::LINE_START << "+-" << address << endl;

    // print all components, if component is las, print \-  before it
    for (unsigned i = 0; i < computer.components.size(); i++) {
        if (i != computer.components.size() - 1) {  // component is not last
            CComponent::LINE_START = "| ";
        } else {                                    // component being printed is last
            CComponent::LINE_START = "  ";
            componentPrefix = "\\-";
        }
        out << CComputer::LINE_START << componentPrefix << *computer.components[i];
        CComponent::LINE_START = "";
    }
    return out;
}

class CNetwork {
    string name;
    vector <CComputer> computers;

public:
    CNetwork(string name) : name(name) {
    }

    // Adds computer to network
    CNetwork &AddComputer(CComputer computer) {
        computers.push_back(computer);
        return *this;
    }

    //Finds computer in vector<CComputer> computers by its host address
    CComputer *FindComputer(string address) {
        for (unsigned i = 0; i < computers.size(); i++)
            if (computers[i].Host() == address)
                return &computers[i];
        return nullptr;
    }

    friend ostream &operator<<(ostream &out, const CNetwork &network);
};

// prints network in specific tree format
ostream &operator<<(ostream &out, const CNetwork &network) {
    out << "Network: " << network.name << endl;
    string computerPrefix = "+-";
    for (unsigned i = 0; i < network.computers.size(); i++) {
        if (i != network.computers.size() - 1)  // pc is not last
            CComputer::LINE_START = "| ";
        else {                                 // pc being printed is last
            CComputer::LINE_START = "  ";
            computerPrefix = "\\-";
        }
        out << computerPrefix << network.computers[i];
    }
    //Reset line prefix
    CComputer::LINE_START = "";

    return out;
}

class CCPU : public CComponent {
    int numberOfCores;
    int frequency;
public:
    CCPU(int numberOfCores, int frequency) : numberOfCores(numberOfCores), frequency(frequency) {}

    virtual CCPU *clone() const {
        return new CCPU(*this);
    };

    virtual void print(ostream &out) const {
        out << "CPU, " << numberOfCores << " cores @ " << frequency << "MHz" << endl;
    }

};

class CMemory : public CComponent {
    int capacity;
public:
    CMemory(int capacity) : capacity(capacity) {}

    virtual CMemory *clone() const { return new CMemory(*this); };

    virtual void print(ostream &out) const {
        out << "Memory, " << capacity << " MiB" << endl;
    }
};

class CDisk : public CComponent {
    short TYPE;
    int capacity;
    vector <pair<string, int>> partitions;
public:
    static const short SSD = 0;
    static const short MAGNETIC = 1;

    CDisk(short TYPE, int capacity) : TYPE(TYPE), capacity(capacity) {}

    virtual CDisk *clone() const { return new CDisk(*this); };

    CDisk &AddPartition(int partitionCapacity, string partitionName) {
        partitions.push_back(make_pair(partitionName, partitionCapacity));
        return *this;
    }

    // prints disk with all partions as tree
    virtual void print(ostream &out) const {
        out << (TYPE == 0 ? "SSD" : "HDD") << ", " << capacity << " GiB" << endl;
        string partitionPrefix = "+-";
        for (unsigned i = 0; i < partitions.size(); i++) {
            if (i != partitions.size() - 1) // last element
                partitionPrefix = "+-";
            else
                partitionPrefix = "\\-";

            out << CComputer::LINE_START << CComponent::LINE_START << partitionPrefix << "[" << i << "]: "
                << partitions[i].second << " GiB, "
                << partitions[i].first
                << endl;
        }

    }

private:

};

string CComputer::LINE_START = "";
string CComponent::LINE_START = "";

// Created by milacboh on 5. 5. 2019.

#pragma once

#include "Question.h"
#include "Answer.h"

using namespace std;

class MultipleChoiceQustion : public Question {
    list<Answer *> answers;

public:
    MultipleChoiceQustion(string &text, int points, list<Answer *> &answers);

    void displayQuestion(ostream &out) const override;

    int checkAnswer(string answer);

    string toXML() const;
};


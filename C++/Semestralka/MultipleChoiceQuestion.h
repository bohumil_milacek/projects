/**
* @file         TextQuestion.h
* @author       milacboh
*/
#pragma once

#include "Question.h"
#include "Answer.h"
#include <vector>


using namespace std;
/**
* @class    MultipleChoiceQuestion
* @brief    MultipleChoiceQuestion represents question which can have multiple correct answers
* @details  MultipleChoiceQuestion holds question and correctAnswers. The user will be given the interface to select the answers.
*/
class MultipleChoiceQuestion : public Question {
    vector<Answer *> answers;       ///< All answers to this question

public:
    /**
     * @param text Text of the question
     * @param points Sum of points for the correct answers
     * @param answers All answers to this question correct and incorrect*/
    MultipleChoiceQuestion(string &text, int points, vector<Answer *> &answers);
    /// Deallocates vector of answers
    ~MultipleChoiceQuestion();

    /**@fn toXML
     * @brief parses MultipleChoiceQuestion to XML
     * @returns MultipleChoiceQuestion represented in XML*/
    xmlNodePtr toXML() const override;

    /** @fn displayQuestionWithAnswers
      * @param questionWindow window where question will be displayed
      * @param answersWindow window to provide interface for the user to give an answer
      * @param hintWindow window where will be printed hint if needed
      * @brief prints the question, hint and gives the user the interface to type in answer
      * @returns the result of question due to his answer
      */
    AnswerStruct displayQuestionWithAnswers(WINDOW *questionWindow, WINDOW *answersWindow,WINDOW* hintWindow) const;

private:
    /** @param answersWindow Window where to provide interface for answering
     * @brief Gives the user the interface in answersWindow to select from all the answers, after confirmation it checks the answers
     * @returns Sum of points for selected answers*/
    int runAnswersMenu(WINDOW *answersWindow) const;
};

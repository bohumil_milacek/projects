/**
* @file         Engine.h
* @brief        Quiz Engine
* @details      This documentation covers the Quiz Engine ands it's functionality
* @author       milacboh
*/
#pragma once

#include <iostream>
#include <ncurses.h>
#include "XMLParser.h"
#include "Menu.h"
#include "QuizMaker.h"
#include "QuizRunner.h"

using namespace std;

/**
* @class    Engine
* @brief    Engine is program running core
* @details   Engine is core which starts the whole progam, gives the user the choice to
* @details   choose what to do, checks if program has enough space to run etc..
*/
class Engine{

public:
    /// initialized on the start of the program, refers to terminal height during program startup
    static int screenHeight;
    /// initialized on the start of the program, refers to terminal width during program startup
    static int screenWidth;

    /**
    * @fn      run
    * @brief   Starts %Quiz %Engine
    */
    void run();

private:
    /**
    * @fn      initializeWindow
    * @brief   Starts ncurses and initializes #screenHeight and #screenWidth
    */
    void initializeWindow();
    /**
    * @fn      goToMenu
    * @brief   Starts the menu where user chooses what to do
    */
    void goToMenu();
    /**
    * @fn      makeAQuiz
    * @brief   Starts a screen where user creates his quiz
    */
    void makeAQuiz();
    /**
    * @fn      displayQuizes
    * @brief   Starts a screen where user selects which quiz will user run
    */
    void displayQuizes();
    /**
    * @fn      checkWindowSize
    * @brief   Checks if screen is big enough, if not informs user
    */
    void checkWindowSize() const;

    /**
    * @fn      exitProgram
    * @brief   Ends ncourses and exits program
    */
    void exitProgram();

};

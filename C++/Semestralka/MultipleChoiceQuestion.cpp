#include "MultipleChoiceQuestion.h"


MultipleChoiceQuestion::MultipleChoiceQuestion(string &text, int points, vector<Answer *> &answers)
        : Question(text, points), answers(answers) {

}

MultipleChoiceQuestion::~MultipleChoiceQuestion() {
    for (Answer * answer : answers)
        delete answer;
}



xmlNodePtr MultipleChoiceQuestion::toXML() const {
    string pointsStr = to_string(points);
    xmlNodePtr questionNode = xmlNewNode(NULL, BAD_CAST"MultipleChoiceQuestion");
    xmlNewProp(questionNode, BAD_CAST"text", BAD_CAST text.c_str());
    xmlNewProp(questionNode, BAD_CAST"points", BAD_CAST pointsStr.c_str());
    cout << "size of answers" << answers.size();
    for (auto &answer : answers) {
        xmlNodePtr answerNode = answer->toXML();
        xmlAddChild(questionNode, answerNode);
    }
    return questionNode;
}

Question::AnswerStruct MultipleChoiceQuestion::displayQuestionWithAnswers(WINDOW *questionWindow, WINDOW *answersWindow,WINDOW* hintWindow) const {
    displayQuestion(questionWindow);
    wclear(hintWindow);
    box(hintWindow,0,0);
    string hint="By SPACEBAR you select answers, by ENTER you confirm them";
    mvwprintw(hintWindow, 0, (60 -4) / 2,"HINT");
    mvwprintw(hintWindow, 3/2, (60 - hint.length()) / 2,hint.c_str());
    wrefresh(hintWindow);
    refresh();

    int earnedPoints=runAnswersMenu(answersWindow);

    wclear(hintWindow);
    wrefresh(hintWindow);
    refresh();

    if(earnedPoints==points)
        return AnswerStruct(earnedPoints,AnswerStatus::CORRECT);
    else if( earnedPoints>0 && earnedPoints< points)
        return AnswerStruct(earnedPoints,AnswerStatus::PARTIALLY_CORRECT);
    else
        return AnswerStruct(earnedPoints,AnswerStatus::INCORRECT);
}


int MultipleChoiceQuestion::runAnswersMenu(WINDOW *answersWindow) const {
    bool isRunningChoices=true;
    int sumOfPoints=0;
    unsigned hilighted = 0;
    bool selected[6] = {false};

    keypad(answersWindow, true);
    wclear(answersWindow);
    refresh();

    int choice;
    while (isRunningChoices) {
        for (unsigned i = 0; i < answers.size(); i++) {
            if (i == hilighted || selected[i])
                wattron(answersWindow, A_REVERSE);
            mvwprintw(answersWindow, i, (60 - answers[i]->getText().length()) / 2, answers[i]->getText().c_str());
            wattroff(answersWindow, A_REVERSE);
        }
        choice = wgetch(answersWindow);

        switch (choice) {
            case KEY_UP:
                hilighted--;
                if (hilighted == UINT_MAX)
                    hilighted = answers.size() - 1;
                break;
            case KEY_DOWN:
                hilighted++;
                if (hilighted == answers.size())
                    hilighted = 0;
                break;
            case ' ':    //representing space bar
                selected[hilighted] = !selected[hilighted];
                break;
            case 10:
                for (unsigned i = 0; i < answers.size(); ++i){
                    if (selected[i])
                        sumOfPoints+=answers[i]->getPoints();
                }
                    isRunningChoices=false;
                break;
            default:
                break;
        }
    }

    return sumOfPoints;
}




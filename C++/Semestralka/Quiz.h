/**
* @file         TextQuestion.h
* @author       milacboh
*/
#pragma once

#include <iostream>
#include <vector>
#include "Section.h"
#include "IXMLConvertible.h"

using namespace std;

/**
* @class    Quiz
* @brief    Quiz represents a structure of questions in specific area.
* @details  Quiz represents thw whole structure. Holds set of Sections.
* @details  Is represented by XML files which are then being stored and loaded from local repository.
*/
class Quiz{

public:
    /**
    * @struct    QuizRating
    * @brief    Represents rating of a quiz with grade and percentage for the grade
    */
    struct QuizRating : public IXMLConvertible{
        char grade;     ///< Represents mark for a specific percentage
        int percentage; ///< Represents percentage for a specific mark
        /**
         * @param grade Grade that represents the rating
         * @param percentage Percentage for a specific grade*/
        QuizRating(char grade,int percentage) : grade(grade), percentage(percentage){

        }
        /** @brief Transforms Rating to a XML structure
         * @returns XML structure of Rating*/
    xmlNodePtr toXML() const override{
            string gradeStr(1,grade);
            string percentageStr=to_string(percentage);
            xmlNodePtr sectionNode = xmlNewNode(NULL,BAD_CAST "Rating");
            xmlNewProp(sectionNode,BAD_CAST"grade",BAD_CAST gradeStr.c_str());
            xmlNewProp(sectionNode,BAD_CAST"percentage",BAD_CAST percentageStr.c_str());
            return sectionNode;
        }
    };
private:

    string name;                  ///< Name of the quiz
    vector<Section *> sections;   ///< Sections of the quiz
    vector<QuizRating *> ratings; ///< Rating for the quiz

public:

    virtual ~Quiz();    ///< Deallocates the Sections

    const string &getName() const;      ///< @returns Name of the quiz
    /**
     * @param name Name to be set to the quiz
     * @brief sets The name of the quiz*/
    void setName(const string &name);
    const vector<Section *> & getSections() const;          ///< @returns Vector of sections within the Quiz
    /**
     * @param sections Sections to be set to the quiz
     * @brief Sets section of the Quiz*/
    void setSections(const vector<Section *> &sections);
    /**
     * @param section Section to be added to Quiz
     * @brief Adds section to the quiz*/
    void addSection(Section & section);

    const vector<QuizRating *> &getRatings() const;     ///< @returns Vector of quiz ratings

    /**
     * @param ratings Ratings to be set to the quiz
     * @brief Sets the Ratings of the quiz*/
    void setRatings(const vector<QuizRating *> &ratings);

    /**
     * @brief Transforms the Quiz to a XML structure
     * @returns Transformed Quiz as XML structure*/
    xmlDocPtr toXML() const;

    /**
     * @param percentage Percentage for rating
     * @brief Takes a Percentage and looks for a Mark for that Percentage
     * @returns Mark for a percentage for the Quiz*/
    char getGrade(int percentage) const;

};

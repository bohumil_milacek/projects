/**
* @file         InvalidFormatException.h
* @author       milacboh
*/
#pragma once
#include "iostream"

using namespace std;
/**
* @class    InvalidFormatException
* @brief    InvalidFormatException represents exception which is thrown when some object doesn't have right structure
*/
struct InvalidFormatException : public exception
{
    string exceptionText;        ///< Text of the exception

    InvalidFormatException(const string &exceptionText) : exceptionText(exceptionText) {}

    const char * what () const throw ()
    {
        return "Invalid Format Exception";
    }
};



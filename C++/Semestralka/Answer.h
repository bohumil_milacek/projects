/**
* @file         Answer.h
* @author       milacboh
*/

#pragma once
#include "iostream"
#include "IXMLConvertible.h"
#include <libxml/parser.h>

using namespace std;
/**
* @class    Answer
* @brief    Represents structure which holds text of the answer and points for the answer
*/
class Answer : public IXMLConvertible{

protected:
    const string text;      ///< Text of the answer
    const int points;       ///< Points for the answer

public:
    /**
     * @param text Text of the answer
     * @param points Points for the answer*/
    Answer(const string &text, const int points) : text(text), points(points) {}

    virtual ~Answer() {
    }

    ///< @returns Text of the answer
    const string &getText() const {
        return text;
    }
    ///< @returns Points for the answer
    int getPoints() const {
        return points;
    }

    /**@fn toXML
     * @brief parses Answer to XML
     * @returns Answer represented in XML*/
    virtual xmlNodePtr toXML() const override{
        string pointsStr=to_string(points);
        xmlNodePtr answerNode = xmlNewNode(NULL,BAD_CAST "Answer");
        xmlNewProp(answerNode,BAD_CAST"text",BAD_CAST text.c_str());
        xmlNewProp(answerNode,BAD_CAST"points",BAD_CAST pointsStr.c_str());
        return answerNode;
    }

    /**
     * @param answer Text to be compared to answer
     * @brief Checks if given answer is equal not depending on the upper case to the Answer text
     * @returns points for the correct/bad answer*/
    int check(string &answer) const {
        return Utils::toLower(answer)==Utils::toLower(text) ? points : 0;
    }


};




/**
* @file         ChoiceQuestion.h
* @author       milacboh
*/
#pragma once

#include "Question.h"
#include "ChoiceAnswer.h"
#include <vector>

using namespace std;

/**
* @class    ChoiceQuestion
* @brief    ChoiceQuestion represents question from which user can jump in quiz. Also can be question with one correct answer and continue in quiz sequentially
*/
class ChoiceQuestion : public Question {
    vector<ChoiceAnswer *> answers;     ///< All the answers for the question to be provided user to select from

public:
    /**
     * @param text Text of the question
     * @param points Sum of points for the correct answers
     * @param answers All answers for the question*/
    ChoiceQuestion(string &text, int points,vector<ChoiceAnswer *> & answers);
    /// Deallocates vector of answers
    ~ChoiceQuestion();
    /**@fn toXML
     * @brief parses ChoiceQuestion to XML
     * @returns ChoiceQuestion represented in XML*/
    xmlNodePtr toXML() const override ;

    /** @fn displayQuestionWithAnswers
      * @param questionWindow   window where question will be displayed
      * @param answersWindow     window to& provide interface for the user to give an answer
      * @param hintWindow       window where will be printed hint if needed
      * @brief prints the question, hint and gives the user the interface to type in answer
      * @returns the result of question due to his answer
      */
    AnswerStruct displayQuestionWithAnswers(WINDOW * questionWindow,WINDOW *answersWindow,WINDOW* hintWindow) const;

private:
    /** @param answersWindow Window where to provide interface for answering
     * @brief Gives the user the interface in answersWindow to select a answer
     * @returns Structure of a answer that contains important info*/
    Question::AnswerStruct runAnswersMenu(WINDOW *answersWindow) const;
};


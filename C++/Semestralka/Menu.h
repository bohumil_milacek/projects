/**
* @file         Menu.h
* @author       milacboh
*/
#pragma once
#include "Engine.h"
#include <iostream>
#include <ncurses.h>

using namespace std;

/**
* @class    Menu
* @brief    Menu is screen with options
* @details  In menu user can choose what to do, if to make a quiz, run a quiz or exit the program
*/
class Menu {
    /// index pointing to selected item in menu
    int hilighted;
public:
    Menu() : hilighted(0){}
    /**
     * Indicator which Menu returns, to indicate what user has selected
     * */
    enum MenuSelection{
        /// Indicator to make a quiz
        MAKE_A_QUIZ,
        /// Indicator to start a quiz
        RUN_A_QUIZ,
        /// Indicator to exit the engine
        EXIT
    };
    /**
     * @fn run
     * Prints the menu and gives the user interface to control the menu
     * @returns user choice in menu
     * */
    MenuSelection run();
private:
    /**
     * @fn printMenuTitle
     * Prints the menu title
     * */
    void printMenuTitle() const;
    /**
     * @fn runMenuSelection
     * Prints the menu selection, runs in infinite loop to let the user choose
     * */
    void runMenuSelection();
};
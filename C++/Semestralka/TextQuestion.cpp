#include "TextQuestion.h"

TextQuestion::TextQuestion(string &text, int points, Answer &correctAnswer) : Question(text, points),
                                                                              correctAnswer(correctAnswer) {

}

TextQuestion::~TextQuestion() {
    delete &correctAnswer;

}

xmlNodePtr TextQuestion::toXML() const {
    string pointsStr=to_string(points);
    xmlNodePtr questionNode = xmlNewNode(NULL, BAD_CAST "TextQuestion");
    xmlNewProp(questionNode, BAD_CAST "text", BAD_CAST text.c_str());
    xmlNewProp(questionNode, BAD_CAST "points", BAD_CAST pointsStr.c_str());
    xmlNodePtr answerNode = correctAnswer.toXML();
    xmlAddChild(questionNode, answerNode);
    return questionNode;
}


Question::AnswerStruct TextQuestion::displayQuestionWithAnswers(WINDOW * questionWindow,WINDOW *answerWindow,WINDOW* hintWindow) const {
    displayQuestion(questionWindow);

    string answer=getAnswerInput(answerWindow);

    int earnedPoints=correctAnswer.check(answer);
    if(earnedPoints==points)
        return AnswerStruct(earnedPoints,AnswerStatus::CORRECT);
    return AnswerStruct(earnedPoints,AnswerStatus::INCORRECT);;
}



string TextQuestion::getAnswerInput(WINDOW * inputWindow) const {
    keypad(inputWindow, FALSE);
    echo();
    nocbreak();
    curs_set(1);                    //hide cursor

    wclear(inputWindow);
    string input;
    int ch = wgetch(inputWindow);
    while (ch != '\n') {
        input.push_back(ch);
        ch = wgetch(inputWindow);
    }
    noecho();
    cbreak();
    curs_set(0);
    wclear(inputWindow);
    wrefresh(inputWindow);
    refresh();
    return input;
}





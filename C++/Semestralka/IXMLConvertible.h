/**
* @file         IXMLConvertible.h
* @author       milacboh
*/
#pragma once
#include <libxml/parser.h>

/**
* @class    IXMLConvertible
* @brief    IXMLConvertible represents object that can be parsed into XML structure
* @details  IXMLConvertible provides the interface to convert object to XML structure by overriding its method toXML()
*/
class IXMLConvertible {

public:
    virtual ~IXMLConvertible() {}
    /**@fn toXML
     * @brief method to override to represent object as XML structure
     * @returns pointer to the XML structure of the parsed object*/
    virtual xmlNodePtr toXML() const = 0;
};



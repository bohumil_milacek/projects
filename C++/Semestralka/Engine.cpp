
#include "Engine.h"

int Engine::screenHeight, Engine::screenWidth;

int main() {
    Engine e;
    e.run();
    return 0;
}


void Engine::run(){
    initializeWindow();
    checkWindowSize();
    goToMenu();
}

void Engine::goToMenu() {
   Menu m;
   while (true){
   Menu::MenuSelection selectedItem=m.run();
   switch(selectedItem){
       case Menu::RUN_A_QUIZ:
           displayQuizes();
           break;
       case Menu::MAKE_A_QUIZ:
           makeAQuiz();
           break;
       case Menu::EXIT:
           exitProgram();
           break;
       default:
           break;
   }
   }

}


void Engine::initializeWindow() {
    /* NCURSES START*/
    initscr();
    noecho();
    cbreak();
    start_color();                  // called to be able to use colors
    curs_set(0);                    //hide cursor
    getmaxyx(stdscr, Engine::screenHeight, Engine::screenWidth);   //get screen size
}


void Engine::exitProgram() {
    endwin();
    exit(0);
}


void Engine::displayQuizes() {
    QuizRunner quizRunner;
    quizRunner.run();
}

void Engine::makeAQuiz() {
    QuizMaker quizMaker;
    quizMaker.run();
}


void Engine::checkWindowSize() const{
    if(screenWidth<70 || screenHeight<30){
        string alertText="Window size must me at least 70x30";
        mvprintw(screenHeight/2,(screenWidth-alertText.length())/2,alertText.c_str());
        getch();
        endwin();
        exit(-1);
    }
}
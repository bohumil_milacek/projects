#include "Quiz.h"


Quiz::~Quiz() {
    for (QuizRating * rating : ratings)
        delete rating;
    for (Section * section : sections)
        delete section;
}

const string &Quiz::getName() const {
    return name;
}

void Quiz::setName(const string &name) {
    Quiz::name = name;
}

const vector<Section *> &Quiz::getSections() const {
    return sections;
}

void Quiz::setSections(const vector<Section *> &sections) {
    Quiz::sections = sections;
}


void Quiz::addSection(Section &section) {
    sections.push_back(&section);

}


xmlDocPtr Quiz::toXML() const {
    xmlDocPtr doc = xmlNewDoc(BAD_CAST "1.0");
    xmlNodePtr rootnode = xmlNewDocNode(doc, NULL, BAD_CAST "Quiz", BAD_CAST "");
    xmlDocSetRootElement(doc, rootnode);

    xmlNodePtr ratingsNode = xmlNewNode(NULL,BAD_CAST "Ratings");
    xmlAddChild(rootnode, ratingsNode);

    for (auto rating :ratings) {
        xmlNodePtr ratingNode=rating->toXML();
        xmlAddChild(ratingsNode, ratingNode);
    }

    xmlNodePtr sectionsNode = xmlNewNode(NULL,BAD_CAST "Sections");
    xmlAddChild(rootnode, sectionsNode);


    for (auto section :sections) {
        xmlNodePtr sectionNode=section->toXML();
        xmlAddChild(sectionsNode, sectionNode);
    }
    return doc;
}

const vector<Quiz::QuizRating *> &Quiz::getRatings() const {
    return ratings;
}

void Quiz::setRatings(const vector<Quiz::QuizRating *> &ratings) {
    Quiz::ratings = ratings;
}


char Quiz::getGrade(int percentage) const {
    endwin();
    cout <<"percentage[%]:\t"<<percentage<<endl;
    for (const QuizRating * rating : ratings){
        cout <<"rating[%]:\t"<<rating->percentage<<endl;
        if(percentage>=rating->percentage)
            return rating->grade;
    }
return 'F';
}



#include "Menu.h"


Menu::MenuSelection Menu::run(){
     clear();
     printMenuTitle();
     runMenuSelection();
     clear();
     return hilighted==0 ? MAKE_A_QUIZ : hilighted==1 ? RUN_A_QUIZ : EXIT;
}


void Menu::printMenuTitle() const{
    int titleHeight=5;
    int titleLength=46;
    int startX=Engine::screenWidth/2-titleLength/2;
    int startY=5;

    string upperTitle=" _ _ _     _                      _____     \n"
                      "| | | |___| |___ ___ _____ ___   |_   _|___ \n"
                      "| | | | -_| |  _| . |     | -_|    | | | . |\n"
                      "|_____|___|_|___|___|_|_|_|___|    |_| |___|";
    string bottomTitle=" _____     _        _____         _         \n"
                       "|     |_ _|_|___   |   __|___ ___|_|___ ___ \n"
                       "|  |  | | | |- _|  |   __|   | . | |   | -_|\n"
                       "|__  _|___|_|___|  |_____|_|_|_  |_|_|_|___|\n"
                       "   |__|                      |___|        ";

    WINDOW *titleWindowUpper = newwin(titleHeight, titleLength, startY, startX);
    WINDOW *titleWindowBotom = newwin(titleHeight, titleLength, startY+titleHeight, startX);

    mvwprintw(titleWindowUpper,0, 0, "%s", upperTitle.c_str());
    mvwprintw(titleWindowBotom,0, 0, "%s", bottomTitle.c_str());
    refresh();
    wrefresh(titleWindowUpper);
    wrefresh(titleWindowBotom);
    delwin(titleWindowUpper);
    delwin(titleWindowBotom);

}

void Menu::runMenuSelection() {
    int menuWidth=Engine::screenWidth - 12;
    int menuHeight=6;
    WINDOW *menuWin = newwin(menuHeight,menuWidth, Engine::screenHeight - 8, 5);
    box(menuWin, 0, 0);
    refresh();
    wrefresh(menuWin);
    keypad(menuWin, true);

    string choices[3] = {"MAKE A QUIZ", "RUN A QUIZ", "EXIT"};
    int choice;

    while (true) {
        for (int i = 0; i < 3; i++) {
            if (i == hilighted)
                wattron(menuWin, A_REVERSE);
            mvwprintw(menuWin, i + 1, (menuWidth-choices[i].length())/2, choices[i].c_str());
            wattroff(menuWin, A_REVERSE);
        }
        choice = wgetch(menuWin);

        switch (choice) {
            case KEY_UP:
                hilighted--;
                if (hilighted == -1)
                    hilighted = 2;
                break;
            case KEY_DOWN:
                hilighted++;
                if (hilighted == 3)
                    hilighted = 0;
                break;
            default:
                break;
        }
        if (choice == 10) {
            break;
        }
    }
    delwin(menuWin);

}
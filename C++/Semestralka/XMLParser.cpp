// Created by milacboh on 5. 5. 2019.


#include "XMLParser.h"

// BAD_CAST is definition of "(const xmlChar*)"

// transfers quiz into XML structure and saves it
void XMLParser::quizToXML(Quiz &quiz) const {
    xmlInitParser();
    LIBXML_TEST_VERSION

    xmlDocPtr doc = quiz.toXML();

    //save the document
    int nRel = xmlSaveFile((quiz.getName() + ".xml").c_str(), doc);
    if (nRel != -1) {
        xmlChar *docstr;
        int len;
        xmlDocDumpMemory(doc, &docstr, &len);
    }

    //memory free
    xmlFreeDoc(doc);

}

// returns vector of quiz names found
vector <string> XMLParser::loadQuizNames() const {
    vector <string> xmlQuizNames;
    string path = ".";
    for (const auto &entry : fs::directory_iterator(path)) {
        if (entry.path().extension() == ".xml" && isQuizXmlFile(entry.path().filename())) {
            xmlQuizNames.push_back(entry.path().stem());
        }
    }

    return xmlQuizNames;
}

bool XMLParser::isQuizXmlFile(string xmlFileName) const {
    xmlDocPtr doc;
    xmlNodePtr cur;

    doc = xmlParseFile(xmlFileName.c_str());

    if (doc == nullptr) //Document not parsed successfully.
        return false;

    cur = xmlDocGetRootElement(doc);

    if (cur == nullptr) { //empty document
        xmlFreeDoc(doc);
        return false;
    }

    if (xmlStrcmp(cur->name, BAD_CAST "Quiz")) { //document of the wrong type, root node != Quiz
        xmlFreeDoc(doc);
        return false;
    }
    xmlFreeDoc(doc);
    return true;
}


// reads quiz by quizName, formats from XML to objects and returns
Quiz *XMLParser::loadQuizFromXML(const string &quizName) const {
    xmlDocPtr doc;
    xmlNodePtr cur;

    Quiz & quiz=*(new Quiz());

    doc = xmlParseFile(quizName.c_str());
    cur = xmlDocGetRootElement(doc);
    cur = cur->xmlChildrenNode;

    vector < Quiz::QuizRating * > ratings;

  //  std::cout.setstate(std::ios_base::failbit);
endwin();
    cout <<endl <<endl <<endl;
    while (cur != nullptr) {
        cout << "["<<cur->name <<"]" << endl;
        if ((!xmlStrcmp(cur->name, BAD_CAST"Ratings"))){
            ratings = parseRatings(doc, cur);
            if (ratings.size() != 5)
                throw InvalidFormatException("Invalid Ratings Format - not 5 records");
            quiz.setRatings(ratings);
        }
      if ((!xmlStrcmp(cur->name, BAD_CAST"Sections"))){
            vector < Section * > sections = parseSections(doc, cur);
            quiz.setSections(sections);
        }
        cur = cur->next;
    }
    cout <<endl <<endl <<endl;

    xmlFreeDoc(doc);
    return &quiz;
}


vector<Quiz::QuizRating *> XMLParser::parseRatings(xmlDocPtr doc, xmlNodePtr cur) const {

    vector < Quiz::QuizRating * > ratings;
    xmlChar *gradeChar;
    xmlChar *percentageChar;

    cur = cur->xmlChildrenNode;
    while (cur != nullptr) {
        if ((!xmlStrcmp(cur->name, BAD_CAST "Rating"))) {
            gradeChar = xmlGetProp(cur, BAD_CAST"grade");
            percentageChar = xmlGetProp(cur, BAD_CAST"percentage");
            char grade = getCharFromXML(gradeChar);
            int percentage = getIntFromXML(percentageChar);

            cout<< " -> " << grade << " - " <<percentage <<endl;

            ratings.push_back(new Quiz::QuizRating(grade, percentage));
            xmlFree(gradeChar);
            xmlFree(percentageChar);
        }
        cur = cur->next;
    }
    return ratings;
}

vector<Section *> XMLParser::parseSections(xmlDocPtr doc, xmlNodePtr cur) const {
    vector < Section * > sections;
    cur = cur->xmlChildrenNode;
    while (cur != nullptr) {

        if ((!xmlStrcmp(cur->name, BAD_CAST"Section"))) {
            xmlChar * sectionTitleChar = xmlGetProp(cur, BAD_CAST "title");
            string sectionTitle = getStringFromXML(sectionTitleChar);

            cout << "---"<< sectionTitle << "---"<<endl;

            list < Question * > questions = parseQuestions(doc, cur);
            sections.push_back(new Section(sectionTitle,questions));
            xmlFree(sectionTitleChar);
        }
        cur = cur->next;
    }
    return sections;
}

list<Question *> XMLParser::parseQuestions(xmlDocPtr doc, xmlNodePtr cur) const {
    list < Question * > questions;
    Question * question;
    cur = cur->xmlChildrenNode;
    while (cur != nullptr) {
        if ((!xmlStrcmp(cur->name, BAD_CAST"TextQuestion"))){

        question = parseTextQuestion(doc, cur);
            cout <<"STAGE 1.1"<<endl;
            questions.push_back(question);

        }
        else if ((!xmlStrcmp(cur->name, BAD_CAST"ChoiceQuestion"))){
            cout <<"STAGE 2"<<endl;
            question = parseChoiceQuestion(doc, cur);
            questions.push_back(question);

        }
        else if ((!xmlStrcmp(cur->name, BAD_CAST"MultipleChoiceQuestion"))){
            cout <<"STAGE 3"<<endl;
            question = parseMultipleChoiceQuestion(doc, cur);
            questions.push_back(question);

        }
      /*  else
        throw InvalidFormatException("Invalid Question Format");*/
        cur = cur->next;
    }
    return questions;
}

TextQuestion *XMLParser::parseTextQuestion(xmlDocPtr doc, xmlNodePtr cur) const {
    cout<< " -> TEXT_QUESTION"<<endl;
    Answer * answer;
    xmlChar * textChar = xmlGetProp(cur, BAD_CAST"text");
    xmlChar * pointsChar = xmlGetProp(cur, BAD_CAST"points");

    string text = getStringFromXML(textChar);
    int points = getIntFromXML(pointsChar);

    cur = cur->xmlChildrenNode;

    while (cur != nullptr) {
        if ((!xmlStrcmp(cur->name, BAD_CAST"Answer"))) {
            answer = parseAnswer(doc, cur);
        }
        cur = cur->next;
    }

    xmlFree(textChar);
    xmlFree(pointsChar);


    cout <<"    - "<<text<<"["<< points <<"]()"<<endl<<endl;
    return new TextQuestion(text, points, *answer);
}

MultipleChoiceQuestion *XMLParser::parseMultipleChoiceQuestion(xmlDocPtr doc, xmlNodePtr cur) const {
    cout<< " -> MULTIPLE_CHOICE_QUESTION"<<endl;
    vector < Answer * > answers;
    xmlChar * textChar = xmlGetProp(cur, BAD_CAST"text");
    xmlChar * pointsChar = xmlGetProp(cur, BAD_CAST"points");


    string text = getStringFromXML(textChar);
    int points = getIntFromXML(pointsChar);

    cout <<"    - "<<text<<"["<< points <<"]"<<endl<<endl;


    cur = cur->xmlChildrenNode;
    while (cur != nullptr) {
        if ((!xmlStrcmp(cur->name, BAD_CAST"Answer"))) {
            answers.push_back(parseAnswer(doc, cur));
        }
        cur = cur->next;
    }



    xmlFree(textChar);
    xmlFree(pointsChar);
    return new MultipleChoiceQuestion(text, points, answers);
}

ChoiceQuestion *XMLParser::parseChoiceQuestion(xmlDocPtr doc, xmlNodePtr cur) const {
    cout<< " -> CHOICE_QUESTION"<<endl;
    vector < ChoiceAnswer * > answers;
    xmlChar * textChar = xmlGetProp(cur, BAD_CAST"text");
    xmlChar * pointsChar = xmlGetProp(cur, BAD_CAST"points");

    string text = getStringFromXML(textChar);
    int points = getIntFromXML(pointsChar);


    cur = cur->xmlChildrenNode;
    while (cur != nullptr) {
        if ((!xmlStrcmp(cur->name, BAD_CAST"ChoiceAnswer")))
        answers.push_back(parseChoiceAnswer(doc, cur));
        cur = cur->next;
    }
    xmlFree(textChar);
    xmlFree(pointsChar);
    return new ChoiceQuestion(text, points, answers);
}

Answer *XMLParser::parseAnswer(xmlDocPtr doc, xmlNodePtr cur) const {
    cout <<"ANSWER"<<endl;
    xmlChar * answerTextChar = xmlGetProp(cur, BAD_CAST"text");
    xmlChar * answerPointsChar = xmlGetProp(cur, BAD_CAST"points");

    string answerText = getStringFromXML(answerTextChar);
    int answerPoints = getIntFromXML(answerPointsChar);

    xmlFree(answerTextChar);
    xmlFree(answerPointsChar);
    cout <<"(" <<answerText<<")(" << answerPoints<<")"<<endl;
    return new Answer(answerText,answerPoints);
}

ChoiceAnswer *XMLParser::parseChoiceAnswer(xmlDocPtr doc, xmlNodePtr cur) const {
    xmlChar *answerTextChar=xmlGetProp(cur, BAD_CAST"text");
    xmlChar *answerPointsChar=xmlGetProp(cur, BAD_CAST"points");
    xmlChar *answerSectionChar=xmlGetProp(cur, BAD_CAST"sectionIndex");

    string answerText=getStringFromXML(answerTextChar);
    int answerPoints=getIntFromXML(answerPointsChar);
    int answerSection=getIntFromXML(answerSectionChar);

    xmlFree(answerTextChar);
    xmlFree(answerPointsChar);
    xmlFree(answerSectionChar);
    return new ChoiceAnswer(answerText,answerSection,answerPoints);
}

string XMLParser::getStringFromXML(xmlChar *xmlCharText) const {
    return string(reinterpret_cast<char *>(xmlCharText));
}

int XMLParser::getIntFromXML(xmlChar *xmlCharNumber) const {
    string number=getStringFromXML(xmlCharNumber);
    cout <<number<<endl;
    int i= stoi(number);
    return i;
}

char XMLParser::getCharFromXML(xmlChar *xmlCharacter) const {
    return *xmlCharacter;
}



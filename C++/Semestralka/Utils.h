/**
* @file         Utils.h
* @author       milacboh
*/
#pragma once
#include <iostream>
#include <sstream>
#include <string>
#include <locale>

using namespace std;

/**
* @class    Utils
* @brief    Utils contains helper methods
*/
class Utils {

public:
    /**
    * @fn      wrap
    * @param text text to be wrapped
    * @param line_length maximum characters per line
    * @brief Word wrapping function which takes in text and line_length and wraps it
    * @returns wrapped text
    */
    static string wrap(const string & text, size_t line_length = 60)
    {
        istringstream words(text);
        ostringstream wrapped;
        string word;

        if (words >> word) {
            wrapped << word;
            size_t space_left = line_length - word.length();
            while (words >> word) {
                if (space_left < word.length() + 1) {
                    wrapped << '\n' << word;
                    space_left = line_length - word.length();
                } else {
                    wrapped << ' ' << word;
                    space_left -= word.length() + 1;
                }
            }
        }
        return wrapped.str();
    }


    /**
    * @fn      toLower
    * @param s text to be converted to lowercase
    * @brief Takes in text, converts it to lower case
    * @returns lowercase text
    */
    static string toLower(const string& s)
    {
        string result;

        locale loc;
        for (unsigned int i = 0; i < s.length(); ++i)
        {
            result += tolower(s.at(i), loc);
        }

        return result;
    }
};




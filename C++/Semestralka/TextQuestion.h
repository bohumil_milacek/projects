/**
* @file         TextQuestion.h
* @author       milacboh
*/
#pragma once

#include "Question.h"
#include "Answer.h"

/**
* @class    TextQuestion
* @brief    TextQuestion represents question which requires just text answer
* @details  TextQuestion holds question and correctAnswer. If user types in correct answer he gets points
* @details  for this question
*/
class TextQuestion : public Question {
    Answer & correctAnswer;     ///< contains the correct Answer fot question

public:
    /**
      * @brief Default constructor
      * @param text question
      * @param points points the user gets if his answer is correct
      * @param correctAnswer the correct answer to this question
      */
    TextQuestion(string &text, int points,Answer & correctAnswer);

    virtual ~TextQuestion(); ///<  @brief Deallocates correct answer

    /**@fn toXML
     * @brief parses TextQuestion to XML
     * @returns TextQuestion represented in XML*/
    xmlNodePtr toXML() const override ;

    /** @fn displayQuestionWithAnswers
      * @param questionWindow   window where question will be displayed
      * @param answerWindow     window to provide interface for the user to give an answer
      * @param hintWindow       window where will be printed hint if needed
      * @brief prints the question, hint and gives the user the interface to type in answer
      * @returns the result of question due to his answer
      */
    AnswerStruct displayQuestionWithAnswers(WINDOW * questionWindow,WINDOW *answerWindow,WINDOW* hintWindow) const override;

private:
    /** @fn getAnswerInput
      * @param inputWindow window from which the answer will be taken
      * @brief provides user the interface in inputWindow to type in answer
      * @returns answer as string taken from inputWindow
      */
    string getAnswerInput(WINDOW * inputWindow) const;
};


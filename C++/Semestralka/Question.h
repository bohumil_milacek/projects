/**
* @file         QuizMaker.h
* @author       milacboh
*/
#pragma once

#include "Utils.h"
#include "IXMLConvertible.h"
#include <iostream>
#include <list>
#include <ncurses.h>
#include <typeinfo>


using namespace std;

/**
* @class    Question
* @brief    Question is structure that holds text of the question and points for correct answer/s
*/
class Question : public IXMLConvertible {

public:
    /** @enum QuestionType
    *   @brief Represents the type of a question
    */
    enum QuestionType {
        CHOICE_QUESTION,
        MULTIPLE_CHOICE_QUESTION,
        TEXT_QUESTION
    };

    /** @enum AnswerStatus
    *   @brief Represents if the question was answered CORRECTLY, PARTIALLY_CORRECT or INCORRECT
    */
    enum AnswerStatus{
        CORRECT,
        PARTIALLY_CORRECT,
        INCORRECT
    };
    /** @struct AnswerStruct
    *   @brief Structure that is returned when question is answered, holds important data
    */
    struct AnswerStruct{
        int points;             ///< Points qiven for answer
        AnswerStatus status;    ///< Status of the answer
        int section;            ///< Section where to go from this question
        /**
         * @param points Points for the answer
         * @param status Status of the answer
         * @param section Section where to go to from this question
         * */
        AnswerStruct(int points, AnswerStatus status,int section=-1) : points(points), status(status),section(section) {}
    };

protected:
    string text;                ///< Represents the text of the question
    int points;                 ///< Points for the correct answer/s
public:
    /**
     * @param text Represents the text of the question
     * @param points Points for the correct answer/s
     * */
    Question(string &text, int points) : text(text), points(points) {}

    virtual ~Question() {
    }

    /**
     * @brief Transforms Question to XML structure
     * @returns Pointer to XML structure of the Question*/
    virtual xmlNodePtr toXML() const override =0;

    /** @fn displayQuestionWithAnswers
      * @param questionWindow   window where question will be displayed
      * @param answerWindow     window to provide interface for the user to give an answer
      * @param hintWindow       window where will be printed hint if needed
      * @brief prints the question, hint and gives the user the interface to type in answer
      * @returns the result of question due to his answer
      */
    virtual AnswerStruct displayQuestionWithAnswers(WINDOW * questionWindow,WINDOW *answerWindow,WINDOW* hintWindow) const = 0;

    /// @returns text of the question
    const string &getText() const {
        return text;
    }
    /// @returns points for the correct answer/s
    int getPoints() const {
        return points;
    }
protected:

    /**
     * @fn displayQuestion
     * @param windowToPrint To which window print
     * @brief Prints text of the question to the given window, if the question is too long the text is wrapped */
    void displayQuestion(WINDOW * windowToPrint) const{
        wclear(windowToPrint);

        int posX=text.length();
        string wrapedText=text;
        if(text.length()>60){
            wrapedText=Utils::wrap(text);
            posX=wrapedText.find('\n');
        }
        mvwprintw(windowToPrint, 1, (60-posX)/2, "%s", wrapedText.c_str());
        wrefresh(windowToPrint);
        refresh();
    }
};




var searchData=
[
  ['save_5fquiz',['SAVE_QUIZ',['../classQuizMaker.html#a27796c2df97b4cfcf4d466c3ad4fe2a9a04e539c3c8bb3950aa790088f9d1ff6e',1,'QuizMaker']]],
  ['savequiz',['saveQuiz',['../classQuizMaker.html#ad165ae3f425a8a45ca3536169b46df88',1,'QuizMaker']]],
  ['screenheight',['screenHeight',['../classEngine.html#aa9e227458b39b7fb4019f09843730f06',1,'Engine']]],
  ['screenwidth',['screenWidth',['../classEngine.html#a002314ea09218285a24e32eddfe8d960',1,'Engine']]],
  ['section',['Section',['../classSection.html',1,'Section'],['../structQuestion_1_1AnswerStruct.html#a50a390e0f1de990f717ee1f163ba57cb',1,'Question::AnswerStruct::section()'],['../classSection.html#aee0ee70ce7ab110cf2fdc0d6992bacec',1,'Section::Section(const string &amp;title)'],['../classSection.html#aeaf28705fb687a4e9e60f70bb561f437',1,'Section::Section(const string &amp;title, const list&lt; Question *&gt; &amp;questions)']]],
  ['section_2ecpp',['Section.cpp',['../Section_8cpp.html',1,'']]],
  ['section_2eh',['Section.h',['../Section_8h.html',1,'']]],
  ['sectionindex',['sectionIndex',['../classChoiceAnswer.html#a88a0cab1b8d04a51c33672ad8d11d166',1,'ChoiceAnswer']]],
  ['sections',['sections',['../classQuiz.html#a57d0324d2d88a33439e9bdfbaed5e02a',1,'Quiz']]],
  ['sectiontitlewindow',['sectionTitleWindow',['../classQuizRunner.html#a6d1ce8dd948209ff1331e7cc38252f77',1,'QuizRunner']]],
  ['selectionwindow',['selectionWindow',['../classQuizMaker.html#a305870fe35b221a829e822ff500e3d7e',1,'QuizMaker']]],
  ['selectionwindowheight',['selectionWindowHeight',['../classQuizMaker.html#aab04e472657aa2d36132435ecd1c37ac',1,'QuizMaker']]],
  ['selectionwindowwidth',['selectionWindowWidth',['../classQuizMaker.html#a1a9af1bbe661e536e38f310383836c13',1,'QuizMaker']]],
  ['setname',['setName',['../classQuiz.html#a34c259203eb4a06c0488e94867b80408',1,'Quiz']]],
  ['setquestions',['setQuestions',['../classSection.html#a3f86bd8120f43449be682d7402d2c60b',1,'Section']]],
  ['setratings',['setRatings',['../classQuiz.html#ae16c03e128dc32151fa1b7307e8f1afb',1,'Quiz']]],
  ['setsections',['setSections',['../classQuiz.html#a1f71a8866c8552c58e7211b2d63ee894',1,'Quiz']]],
  ['status',['status',['../structQuestion_1_1AnswerStruct.html#ae9c65f2213f2865e55d9e3f33686fb06',1,'Question::AnswerStruct']]]
];

var searchData=
[
  ['incorrect',['INCORRECT',['../classQuestion.html#ade9a5048e9cea6ad9fba096e18228c1aac5e969a85281a7aacf962c4338bfa96b',1,'Question']]],
  ['initializewindow',['initializeWindow',['../classEngine.html#a4e4f7e48062e410a0c44b88014762836',1,'Engine']]],
  ['instructionwindow',['instructionWindow',['../classQuizMaker.html#a0e5e3f27ec0f8a539c27c8f39bc186ab',1,'QuizMaker']]],
  ['instructionwindowheight',['instructionWindowHeight',['../classQuizMaker.html#a8494bfd89c21f5f30b8b57e9b6346c6f',1,'QuizMaker']]],
  ['instructionwindowwidth',['instructionWindowWidth',['../classQuizMaker.html#abd54275182d1f5d2043b045787d5de12',1,'QuizMaker']]],
  ['invalidformatexception',['InvalidFormatException',['../structInvalidFormatException.html',1,'InvalidFormatException'],['../structInvalidFormatException.html#a948bf732d8a7ec52738915103b5494df',1,'InvalidFormatException::InvalidFormatException()']]],
  ['invalidformatexception_2eh',['InvalidFormatException.h',['../InvalidFormatException_8h.html',1,'']]],
  ['isquizxmlfile',['isQuizXmlFile',['../classXMLParser.html#a45d6df805ff804a13a0141dded05a979',1,'XMLParser']]],
  ['ixmlconvertible',['IXMLConvertible',['../classIXMLConvertible.html',1,'']]],
  ['ixmlconvertible_2eh',['IXMLConvertible.h',['../IXMLConvertible_8h.html',1,'']]]
];

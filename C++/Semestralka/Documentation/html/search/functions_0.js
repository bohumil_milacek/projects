var searchData=
[
  ['addquestion',['addQuestion',['../classSection.html#ab4eff10d4f7a2e4c1556420ea087d07b',1,'Section']]],
  ['addsection',['addSection',['../classQuiz.html#a7017f732f7c07632fae3ec1c5beabb32',1,'Quiz']]],
  ['alertwindow',['alertWindow',['../classQuizMaker.html#a25844d7d04267b77b1f755b09d69a594',1,'QuizMaker']]],
  ['answer',['Answer',['../classAnswer.html#a13b8097e0b843d5bb111f1227a4a7df5',1,'Answer']]],
  ['answerstruct',['AnswerStruct',['../structQuestion_1_1AnswerStruct.html#a7fbd649a5454b2a00073dd8a693fba19',1,'Question::AnswerStruct']]],
  ['askfornumberofanswers',['askForNumberOfAnswers',['../classQuizMaker.html#a5547f9643680cae31a429840fb09ab28',1,'QuizMaker']]],
  ['askforpoints',['askForPoints',['../classQuizMaker.html#a94465bd55bbe750bb2084c1a2f4eb782',1,'QuizMaker']]]
];

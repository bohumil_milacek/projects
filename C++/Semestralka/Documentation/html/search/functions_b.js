var searchData=
[
  ['savequiz',['saveQuiz',['../classQuizMaker.html#ad165ae3f425a8a45ca3536169b46df88',1,'QuizMaker']]],
  ['section',['Section',['../classSection.html#aee0ee70ce7ab110cf2fdc0d6992bacec',1,'Section::Section(const string &amp;title)'],['../classSection.html#aeaf28705fb687a4e9e60f70bb561f437',1,'Section::Section(const string &amp;title, const list&lt; Question *&gt; &amp;questions)']]],
  ['setname',['setName',['../classQuiz.html#a34c259203eb4a06c0488e94867b80408',1,'Quiz']]],
  ['setquestions',['setQuestions',['../classSection.html#a3f86bd8120f43449be682d7402d2c60b',1,'Section']]],
  ['setratings',['setRatings',['../classQuiz.html#ae16c03e128dc32151fa1b7307e8f1afb',1,'Quiz']]],
  ['setsections',['setSections',['../classQuiz.html#a1f71a8866c8552c58e7211b2d63ee894',1,'Quiz']]]
];

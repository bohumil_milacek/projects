/**
* @file         QuizMaker.h
* @author       milacboh
*/

#pragma once
#include "Quiz.h"
#include "Question.h"
#include "ChoiceQuestion.h"
#include "XMLParser.h"
#include "Engine.h"
#include "Answer.h"
#include "ChoiceQuestion.h"
#include "TextQuestion.h"
#include "MultipleChoiceQuestion.h"
#include <ncurses.h>
#include <ctype.h>
#include <climits>


using namespace std;


/**
* @class    QuizMaker
* @brief    QuizMaker gives the user the interface to create a quiz
* @details  Provides interface to create a quiz with named sections,
* @details  questions in each section. There are 3 types of questions to make TextQuestion,
* @details  ChoiceQuestion - can jump in quiz and MultipleChoiceQuestion
*/
class QuizMaker {
private:
    enum QuizMakerChoice{     ///< Indicator for menu which item user selected
        CREATE_A_SECTION,     ///<Indicator to create section
        CREATE_A_QUESTION,    ///<Indicator to create question
        SAVE_QUIZ,            ///<Indicator to save the quiz
        EXIT,                 ///<Indicator to exit to menu
        CANCEL                ///<Indicator to cancel operation

    };

    Quiz & quiz;                ///< Holds the quiz which is being created
    WINDOW *instructionWindow;  ///< Used to print instructions to the user
    WINDOW *answerWindow;       ///< Used to take input from user
    WINDOW *selectionWindow;    ///< Used as interactive menu for user
    WINDOW *hintWindow;         ///< Used to print errors, hints and warnings to user

    const unsigned instructionWindowHeight; ///< Variable to make the layout easier to rearrange
    const unsigned instructionWindowWidth;  ///< Variable to make the layout easier to rearrange
    const unsigned answerWindowHeight;      ///< Variable to make the layout easier to rearrange
    const unsigned answerWindowWidth;       ///< Variable to make the layout easier to rearrange
    const unsigned selectionWindowHeight;   ///< Variable to make the layout easier to rearrange
    const unsigned selectionWindowWidth;    ///< Variable to make the layout easier to rearrange
    const unsigned hintWindowHeight;        ///< Variable to make the layout easier to rearrange
    const unsigned hintWindowWidth;         ///< Variable to make the layout easier to rearrange

    int numberOfSections;           ///< Helper variable to see how many sections are there in the quiz already
    int questionsInActualSection;   ///< Helper variable to see how many questions are there in actual section

    XMLParser xmlParser;            ///< Parser which transforms Quiz to XML and saves it


public:
    QuizMaker();                                 ///< Initializes the quiz and all layout variables
    ~QuizMaker();                                ///< Deletes the quiz and all windows
    void run();                                  ///< Starts the quiz interactive screen to make a quiz

private:
    void printTitle() const;                     ///< Prints the QuizMaker title to the screen
    void makeAQuiz();                            ///< The main loop of making the quiz

    /**
     * @fn createQuestion
     * @details Runs the menu and creation process of question
     * @return new created Question
     */
    Question * createQuestion();

    /**
     * @fn runQuestionMenu
     * @details Gives the user menu of question types
     * @return type of question user selected in menu
     */
    Question::QuestionType runQuestionMenu();
    /**
     * @fn runTextQuestionMenu
     * @details Gives user series of questions about the Text Question
     * @return new created TextQuestion
     */
    TextQuestion *runTextQuestionMenu();
    /**
     * @fn runChoiceQuestionMenu
     * @details Gives user series of questions about the Choice Question
     * @return new created ChoiceQuestion
     */
    ChoiceQuestion * runChoiceQuestionMenu();

    /**
     * @fn runMultipleChoiceQuestionMenu
     * @details Gives user series of questions about the Multiple Choice Question
     * @return new created MultipleChoiceQuestion
     */
    MultipleChoiceQuestion *runMultipleChoiceQuestionMenu();

    /**
     * @fn createSection
     * @details Gives user series of questions about the new section
     * @return new created section
     */
    Section * createSection();
    void runQuizRatingMenu() const;         ///< Asks user for rating of the quiz


    /**
     * @fn getAnswerInput
     * @details Takes raw text input from user
     * @return  user input as string
     */
    string getAnswerInput() const;
    /**
     * @fn getAnswer
     * @param maxLength - max length of the input to be accepted
     * @details Gets text input from user and checks if the input was empty or was longer than maxLength,
     * @details if yes the user must repeat input and warning is printed
     * @return  user input as string
     */
    string getAnswer(unsigned maxLength=1000) const;
    /**
     * @fn getAnswerAsInt
     * @throws invalid_argument
     * @details Takes number as input from user, if there is not a number the function throws invalid_argument exception
     * @return  number from user
     */
    int getAnswerAsInt() const;
    /**
     * @fn askForPoints
     * @throws invalid_argument
     * Prints instruction to user to input points, users getAnswerAsInt()
     * @return  points from user
     */
    int askForPoints() const;
    /**
     * @fn getAnswerNumber
     * Takes number from user until correct number is typed in. Checks if number is too long to be parsed or is not a number
     * @return  correct number
     */
    int getAnswerNumber() const;
    /**
     * @fn askForNumberOfAnswers
     * @details Asks how many answers user want's in question
     * @return number of answers
     */
    int askForNumberOfAnswers() const;

    /**
     * @fn runMenuSelection
     * @details Gives the user interactive menu
     * @return selected item from menu
     */
    QuizMakerChoice runMenuSelection();
    /**
     * @fn alertWindow
     * @details Gives the user interactive YES/NO
     * @return selected item from menu
     */
    QuizMakerChoice alertWindow() const;
    /**
     * @fn clearMenuSelection
     * @details Clears the interactive menu
     */
    void clearMenuSelection() const;

    /**
     * @fn printInstruction
     * @param instruction
     * @details Prints instruction to the instructionWindow
     */
    void printInstruction(const string & instruction) const;
    /**
     * @fn printHint
     * @param hint
     * @param line
     * @details Prints hint to the hintWindow on specific line
     */
    void printHint(const string & hint,int line) const;
    /**
     * @fn printError
     * @param error
     * @details Prints error to the hintWindow
     */
    void printError(const string & error) const;
    void clearErrorWindow() const;                  ///< Clears the error window
    void clearHintWindow() const;                   ///< Clears the hint window
    void clearInstructionWindow()const;             ///< Clears the instruction window
    void saveQuiz();                                ///< Saves the quiz as xml using XMLParser
};



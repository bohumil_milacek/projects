/**
* @file         QuizRunner.h
* @author       milacboh
*/
#pragma once

#include <ncurses.h>
#include <iostream>
#include "Engine.h"

using namespace std;

/**
* @class    QuizRunner
* @brief    QuizRunner gives the user the interface to run a quiz
* @details  Displays available quizzes and provides the user interface to run them and fill them.
* @details  At the end the quiz is rated.
*/
class QuizRunner {

private:

    Quiz * quiz;                ///< Pointer to the quiz which is running
    XMLParser xmlParser;        ///< Parser to help load the available quizzes and parse them from XML
    WINDOW *sectionTitleWindow; ///< Window which displays name of actual section of quiz
    WINDOW *questionWindow;     ///< Window which displays the question
    WINDOW *answersWindow;      ///< Window which gives the user interface to answer a question
    WINDOW *hintWindow;         ///< Window which prints the user a help text


public:
    QuizRunner();               ///< Initializes all the windows
    virtual ~QuizRunner();      ///< Deletes all the windows and the quiz
    void run();                 ///< Starts the quiz selection menu

private:

    void printTitle() const;            ///< Prints the title of the QuizMaker
    /**
     * @fn runQuizSelectionMenu
     * @details Prints a list of available quizzes and gives the user interface to select one of them
     * @return name of the quiz user selected with .xml extension
     */
    string runQuizSelectionMenu() const;

    /**
     * @fn runQuiz
     * @details Starts the selected quiz, at the end prints user score
     */
    void runQuiz();

    /**
     * @fn updateSectionTitleWindow
     * @param title Text to be printed
     * @details Prints title to the sectionTitleWindow
     */
    void updateSectionTitleWindow(const string & title) const;

    /**
     * @fn printHint
     * @param title text to be printed above the hint window
     * @param hint text to be printed in the hint window
     * @details Prints the hint and title of the hint to the hintWindow
     */
    void printHint(const string & hint, const string & title="HINT") const;
    void clearHint() const;         ///< clears the hintWindow

};



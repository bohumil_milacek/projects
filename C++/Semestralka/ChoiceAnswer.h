/**
* @file         Answer.h
* @author       milacboh
*/

#pragma once

#include <ostream>
#include "Answer.h"

/**
* @class    Answer
* @brief    Represents structure which holds text of the answer, points for the answer and index to which Section to go to from this Answer
*/
class ChoiceAnswer : public Answer{

private:
    const int sectionIndex;             ///< Index of the Section to which to go from this Answer

public:
    /**
     * @param text Text of the answer
     * @param points Points for the answer
     * @param sectionIndex Index of the Section to which to go from this Answer*/
    ChoiceAnswer(string text, int sectionIndex, int points)
    : Answer(text,points), sectionIndex(sectionIndex) {

    }


    /**@fn toXML
     * @brief parses ChoiceAnswer to XML
     * @returns ChoiceAnswer represented in XML*/
    xmlNodePtr toXML() const override {
        string pointsStr=to_string(points);
        string indexStr=to_string(sectionIndex);
        xmlNodePtr answerNode = xmlNewNode(NULL,BAD_CAST "ChoiceAnswer");
        xmlNewProp(answerNode,BAD_CAST"text",BAD_CAST text.c_str());
        xmlNewProp(answerNode,BAD_CAST"points",BAD_CAST pointsStr.c_str());
        xmlNewProp(answerNode,BAD_CAST"sectionIndex",BAD_CAST indexStr.c_str());
        return answerNode;
    }
    ///@returns Index of the Section to which to go from this Answer
    const int getSectionIndex() const {
        return sectionIndex;
    }
};



// Created by milacboh on 5. 5. 2019.

#include "ChoiceQuestion.h"
#include <string>
#include <sstream>
#include <algorithm>
#include <iterator>

#include <ncurses.h>




ChoiceQuestion::ChoiceQuestion(string &text, int points,vector<ChoiceAnswer *> & answers): Question(text,points), answers(answers){
}

ChoiceQuestion::~ChoiceQuestion() {
    for (Answer * answer : answers)
        delete answer;
}


xmlNodePtr ChoiceQuestion::toXML() const {
    string pointsStr=to_string(points);
    xmlNodePtr questionNode = xmlNewNode(NULL,BAD_CAST "ChoiceQuestion");
    xmlNewProp(questionNode,BAD_CAST"text",BAD_CAST text.c_str());
    xmlNewProp(questionNode,BAD_CAST"points",BAD_CAST pointsStr.c_str());
    for(auto answer : answers){
        xmlNodePtr answerNode=answer->toXML();
        xmlAddChild(questionNode,answerNode);
    }
    return questionNode;
}


Question::AnswerStruct ChoiceQuestion::displayQuestionWithAnswers(WINDOW * questionWindow,WINDOW *answersWindow,WINDOW* hintWindow) const {
    displayQuestion(questionWindow);
    AnswerStruct answerStruct=runAnswersMenu(answersWindow);
    return answerStruct;
}





Question::AnswerStruct ChoiceQuestion::runAnswersMenu(WINDOW *answersWindow) const {
    unsigned hilighted = 0;

    keypad(answersWindow, true);

    wclear(answersWindow);

    int choice;
    while (true) {
        for (unsigned i = 0; i < answers.size(); i++) {
            if (i == hilighted)
                wattron(answersWindow, A_REVERSE);
            mvwprintw(answersWindow, i, (60 - answers[i]->getText().length()) / 2, answers[i]->getText().c_str());
            wattroff(answersWindow, A_REVERSE);
        }
        choice = wgetch(answersWindow);

        switch (choice) {
            case KEY_UP:
                hilighted--;
                if (hilighted == UINT_MAX)
                    hilighted = answers.size() - 1;
                break;
            case KEY_DOWN:
                hilighted++;
                if (hilighted == answers.size())
                    hilighted = 0;
                break;
            case 10:
                return AnswerStruct(answers[hilighted]->getPoints(),PARTIALLY_CORRECT,answers[hilighted]->getSectionIndex());
            default:
                break;
        }
    }
}



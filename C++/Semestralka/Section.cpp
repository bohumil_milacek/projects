
#include "Section.h"


Section::Section(const string &title) : title(title) {}

Section::Section(const string &title, const list<Question *> &questions) : title(title), questions(questions) {}

Section::~Section() {
    for (Question * question : questions)
        delete question;
}


void Section::addQuestion(Question * question) {
    questions.push_back(question);
}

const string &Section::getTitle() const {
    return title;
}

const list<Question *> &Section::getQuestions() const {
    return questions;
}

xmlNodePtr Section::toXML() const{
    xmlNodePtr sectionNode = xmlNewNode(NULL,BAD_CAST "Section");
    xmlNewProp(sectionNode,BAD_CAST"title",BAD_CAST title.c_str());
    for (auto question : questions) {
        xmlNodePtr questionNode = question->toXML();
        xmlAddChild(sectionNode,questionNode);
    }
    return sectionNode;
}


void Section::setQuestions(const list<Question *> &questions) {
    Section::questions = questions;
}


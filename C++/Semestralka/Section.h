/**
* @file         TextQuestion.h
* @author       milacboh
*/#pragma once

#include <iostream>
#include <list>
#include "Question.h"
#include "ChoiceQuestion.h"

using namespace std;

/**
* @class    Section
* @brief    Section represents a named part of a quiz which contains questions
* @details  Section can hold specific questions for example for same or similar topic.
* @details  ChoiceQuestion can even jump in quiz to different sections based on user answer.
*/

class Section : public IXMLConvertible{
    string title;               ///< Descriptive title of a section
    list<Question *> questions; ///< Questions contained in a section

public:
    /**
     * @param title Title of the Section
     * @brief Creates Section with specific title*/
    Section(const string &title);
    /**
     * @param title Title of the Section
     * @param questions Questions of the Section
     * @brief Creates Section with specific title and questions*/
    Section(const string &title, const list<Question *> &questions);


    virtual ~Section(); ///< Deallocates all questions

    /**
     * @param question Question to be added to Section
     * @brief Adds question to Section*/
    void addQuestion(Question *question);
    /** @returns Title of the Section*/
    const string &getTitle() const;
    /** @returns List of questions in Section*/
    const list<Question *> &getQuestions() const;
    /**
     * @brief Transforms Section to XML format
     * @returns Formatted section to XML*/
    xmlNodePtr toXML() const override;
    /**
     * @param questions to be set in Section
     * @brief sets the questions of the Section*/
    void setQuestions(const list<Question *> &questions);

};


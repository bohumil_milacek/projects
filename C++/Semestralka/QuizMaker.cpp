#include "QuizMaker.h"


QuizMaker::QuizMaker() : quiz(*new Quiz()), instructionWindowHeight(5), instructionWindowWidth(50),
                         answerWindowHeight(3), answerWindowWidth(60), selectionWindowHeight(6),
                         selectionWindowWidth(Engine::screenWidth - 12), hintWindowHeight(5),
                         hintWindowWidth(70), numberOfSections(0), questionsInActualSection(0) {
    instructionWindow = newwin(instructionWindowHeight, instructionWindowWidth, 8,
                               (Engine::screenWidth - instructionWindowWidth) / 2);
    hintWindow = newwin(hintWindowHeight, hintWindowWidth, 8 + instructionWindowHeight + 1,
                        (Engine::screenWidth - hintWindowWidth) / 2);
    answerWindow = newwin(answerWindowHeight, answerWindowWidth, 8 + instructionWindowHeight + hintWindowHeight + 2,
                          (Engine::screenWidth - answerWindowWidth) / 2);
    selectionWindow = newwin(selectionWindowHeight, selectionWindowWidth, Engine::screenHeight - 8, 5);
    echo();
    nocbreak();
    curs_set(1);                    //hide cursor
}

QuizMaker::~QuizMaker() {
    delete &quiz;
    delwin(instructionWindow);
    delwin(hintWindow);
    delwin(answerWindow);
    delwin(selectionWindow);

}

void QuizMaker::run() {
    printTitle();
    makeAQuiz();
}

void QuizMaker::printTitle() const {
    int titleHeight = 5;
    int titleLength = 44;

    string title = " _____     _        _____     _           \n"
                   "|     |_ _|_|___   |     |___| |_ ___ ___ \n"
                   "|  |  | | | |- _|  | | | | .'| '_| -_|  _|\n"
                   "|__  _|___|_|___|  |_|_|_|__,|_,_|___|_|  \n"
                   "   |__|                                   ";

    WINDOW *titleWindow = newwin(titleHeight + 2, Engine::screenWidth, 0, 0);
    WINDOW *titleCenterWindow = newwin(titleHeight, titleLength, 1, (Engine::screenWidth - titleLength) / 2);

    box(titleWindow, 0, 0);
    mvwprintw(titleCenterWindow, 0, 0, "%s", title.c_str());
    refresh();
    wrefresh(titleWindow);
    wrefresh(titleCenterWindow);

    delwin(titleWindow);
    delwin(titleCenterWindow);
}

void QuizMaker::makeAQuiz() {
    printInstruction("What is the name of the quiz?(Max 40 characters)");
    string quizName = getAnswer(40);
    quiz.setName(quizName);

    bool isMakingQuiz = true;
    Section *actualSection;
    while (isMakingQuiz) {
        QuizMakerChoice userChoice = runMenuSelection();
        switch (userChoice) {
            case CREATE_A_SECTION: {
                actualSection = createSection();
                numberOfSections++;
                questionsInActualSection = 0;
                quiz.addSection(*actualSection);
            }
                break;
            case CREATE_A_QUESTION: {
                questionsInActualSection++;
                Question *question = createQuestion();
                actualSection->addQuestion(question);
            }
                break;
            case SAVE_QUIZ:
                saveQuiz();
                isMakingQuiz=false;
                break;
            case EXIT: {
                QuizMakerChoice userExitChoice = alertWindow();
                switch (userExitChoice) {
                    case EXIT:
                        isMakingQuiz = false;
                        delete &quiz;
                        break;
                    default:
                        break;
                }
                break;
            }
            default:
                break;
        }
    }
}

string QuizMaker::getAnswerInput() const {
    keypad(answerWindow, TRUE);
    echo();
    nocbreak();
    curs_set(1);                    //hide cursor

    wclear(answerWindow);
    string input;
    int ch = wgetch(answerWindow);
    while (ch != '\n') {
        input.push_back(ch);
        ch = wgetch(answerWindow);
    }
    noecho();
    cbreak();
    curs_set(0);
    wclear(answerWindow);
    wrefresh(answerWindow);
    refresh();
    return input;
}

string QuizMaker::getAnswer(unsigned maxLength) const {
    string answer = getAnswerInput();
    while (answer.empty() || answer.length()>maxLength) {
        printError("Incorrect answer!!");
        answer = getAnswerInput();
    }
    clearErrorWindow();
    return answer;
}

int QuizMaker::getAnswerAsInt() const{
    echo();
    nocbreak();
    curs_set(1);                    //hide cursor
    wclear(answerWindow);
    string input;
    int ch = wgetch(answerWindow);
    while (ch != '\n') {
        if (!isdigit(ch)) {
            noecho();
            cbreak();
            curs_set(0);
            throw invalid_argument("Wrong input format");
        }
        input.push_back(ch);
        ch = wgetch(answerWindow);
    }
    noecho();
    cbreak();
    curs_set(0);
    wclear(answerWindow);
    wrefresh(answerWindow);
    refresh();
    return stoi(input);
}


int QuizMaker::askForPoints() const{
    printInstruction("Type points per this answer..");
    int answerPoints=getAnswerNumber();
    clearErrorWindow();
    return answerPoints;
}


int QuizMaker::getAnswerNumber() const{
    int number;
    while (true) {
        try {
            number = getAnswerAsInt();
        } catch (invalid_argument) {
            printError("Wrong input!!");
            continue;
        }catch (out_of_range) {
            printError("Wrong input!!");
            continue;
        }
        break;
    }
    return number;
}

Section *QuizMaker::createSection() {
    clearMenuSelection();
    printInstruction("Type title of the section?(Max 60 characters)");
    string sectionTitle = getAnswer(60);
    return new Section(sectionTitle);
}


ChoiceQuestion *QuizMaker::runChoiceQuestionMenu() {
    printInstruction("Type your questioon(Max 60 characters)");
    string questionText = getAnswer();
    int sumOfPoints=0;
    int numberOfAnswers = askForNumberOfAnswers();
    vector<ChoiceAnswer *> answers;
    int sectionIndex = -1;
    int answerPoints;
    for (int i = 0; i < numberOfAnswers; ++i) {
        printInstruction("Type your answer..");
        string answerText = getAnswer();
        printInstruction("Type index of section..");
        printHint("If you want to go to section type its index..", 1);
        printHint("If yout want to continue sequentially just skip..", 2);
        try {
            sectionIndex = getAnswerAsInt();
        } catch (invalid_argument) {
        }
        clearHintWindow();
        answerPoints = askForPoints();
        sumOfPoints += answerPoints;
        answers.push_back(new ChoiceAnswer(answerText, sectionIndex, answerPoints));
    }
    return new ChoiceQuestion(questionText, sumOfPoints, answers);
}


/*BEGIN_MENU_CONTROL*********************************************************************************/
QuizMaker::QuizMakerChoice QuizMaker::runMenuSelection() {
    clearInstructionWindow();
    wclear(selectionWindow);

    noecho();
    cbreak();
    curs_set(0);

    box(selectionWindow, 0, 0);
    refresh();
    wrefresh(selectionWindow);

    // makes it so we can use arrow keys
    keypad(selectionWindow, true);

    unsigned hilighted = 0;
    vector<string> choices;
    if (questionsInActualSection != 0 || numberOfSections == 0)
        choices.push_back("CREATE A SECTION");
    if (numberOfSections != 0)
        choices.push_back("CREATE A QUESTION");
    if (questionsInActualSection != 0)
        choices.push_back("SAVE");
    choices.push_back("EXIT");

    int choice;

    while (true) {
        for (unsigned i = 0; i < choices.size(); i++) {
            if (i == hilighted)
                wattron(selectionWindow, A_REVERSE);
            mvwprintw(selectionWindow, i + 1, (selectionWindowWidth - choices[i].length()) / 2, choices[i].c_str());
            wattroff(selectionWindow, A_REVERSE);
        }
        choice = wgetch(selectionWindow);

        switch (choice) {
            case KEY_UP:
                hilighted--;
                if (hilighted == UINT_MAX)
                    hilighted = choices.size() - 1;
                break;
            case KEY_DOWN:
                hilighted++;
                if (hilighted == choices.size())
                    hilighted = 0;
                break;
            default:
                break;
        }
        if (choice == 10) {
            if (choices[hilighted] == "CREATE A SECTION")
                return CREATE_A_SECTION;
            else if (choices[hilighted] == "CREATE A QUESTION")
                return CREATE_A_QUESTION;
            else if (choices[hilighted] == "SAVE")
                return SAVE_QUIZ;
            else if (choices[hilighted] == "EXIT")
                return EXIT;
        }
    }
}

QuizMaker::QuizMakerChoice QuizMaker::alertWindow() const{
    wclear(selectionWindow);

    int hilighted = 0;
    wclear(selectionWindow);
    box(selectionWindow, 0, 0);
    refresh();
    wrefresh(selectionWindow);

    // makes it so we can use arrow keys
    keypad(selectionWindow, true);

    string choices[2] = {"YES", "NO"};
    int numberOfChoices = end(choices) - begin(choices);

    int choice;

    while (true) {
        for (int i = 0; i < numberOfChoices; i++) {
            if (i == hilighted)
                wattron(selectionWindow, A_REVERSE);
            mvwprintw(selectionWindow, i + 1, (selectionWindowWidth - choices[i].length()) / 2, choices[i].c_str());
            wattroff(selectionWindow, A_REVERSE);
        }
        choice = wgetch(selectionWindow);

        switch (choice) {
            case KEY_UP:
                hilighted--;
                if (hilighted == -1)
                    hilighted = numberOfChoices - 1;
                break;
            case KEY_DOWN:
                hilighted++;
                if (hilighted == numberOfChoices)
                    hilighted = 0;
                break;
            default:
                break;
        }
        if (choice == 10) {
            switch (hilighted) {
                case 0:
                    return EXIT;
                case 1:
                    return CANCEL;
                default:
                    break;
            }
        }
    }
}

Question::QuestionType QuizMaker::runQuestionMenu() {
    clearInstructionWindow();
    clearMenuSelection();

    int hilighted = 0;
    wclear(selectionWindow);
    box(selectionWindow, 0, 0);
    refresh();
    wrefresh(selectionWindow);
    keypad(selectionWindow, true);

    string choices[3] = {"CHOICE QUESITON", "MULTIPLE CHOICE QUESITON", "TEXT QUESTION"};
    int numberOfChoices = end(choices) - begin(choices);
    int choice;
    while (true) {
        for (int i = 0; i < numberOfChoices; i++) {
            if (i == hilighted)
                wattron(selectionWindow, A_REVERSE);
            mvwprintw(selectionWindow, i + 1, (selectionWindowWidth - choices[i].length()) / 2, choices[i].c_str());
            wattroff(selectionWindow, A_REVERSE);
        }
        choice = wgetch(selectionWindow);

        switch (choice) {
            case KEY_UP:
                hilighted--;
                if (hilighted == -1)
                    hilighted = numberOfChoices - 1;
                break;
            case KEY_DOWN:
                hilighted++;
                if (hilighted == numberOfChoices)
                    hilighted = 0;
                break;
            default:
                break;
        }
        if (choice == 10) {
            switch (hilighted) {
                case 0:
                    return Question::CHOICE_QUESTION;
                case 1:
                    return Question::MULTIPLE_CHOICE_QUESTION;
                case 2:
                    return Question::TEXT_QUESTION;
                default:
                    break;
            }
        }
    }
}

void QuizMaker::clearMenuSelection() const {
    wclear(selectionWindow);
    refresh();
    wrefresh(selectionWindow);
}


/*BEGIN_WINDOWS_CONTROL*********************************************************************************/
void QuizMaker::printHint(const string & hint, int line = 3) const {
    box(hintWindow, 0, 0);
    mvwprintw(hintWindow, 0, (hintWindowWidth - 4) / 2, "%s", "HINT");
    mvwprintw(hintWindow, line, (hintWindowWidth - hint.length()) / 2, "%s", hint.c_str());
    wmove(answerWindow, 0, 0);
    refresh();
    wrefresh(hintWindow);
    wrefresh(answerWindow);
}

void QuizMaker::printInstruction(const string & instruction) const {
    wclear(instructionWindow);
    box(instructionWindow, 0, 0);
    mvwprintw(instructionWindow, instructionWindowHeight / 2, (instructionWindowWidth - instruction.length()) / 2, "%s",
              instruction.c_str());
    wmove(answerWindow, 0, 0);
    refresh();
    wrefresh(instructionWindow);
    wrefresh(answerWindow);
}

void QuizMaker::printError(const string & error) const {
    printHint(error,hintWindowHeight/2);
}

void QuizMaker::clearErrorWindow() const{
    clearHintWindow();
}

void QuizMaker::clearHintWindow() const{
    wclear(hintWindow);
    refresh();
    wrefresh(hintWindow);
}

Question *QuizMaker::createQuestion() {
    Question::QuestionType questionType = runQuestionMenu();
    clearMenuSelection();
    Question *question;
    switch (questionType) {
        case Question::CHOICE_QUESTION:
            question = runChoiceQuestionMenu();
            break;
        case Question::MULTIPLE_CHOICE_QUESTION:
            question = runMultipleChoiceQuestionMenu();
            break;
        case Question::TEXT_QUESTION:
            question = runTextQuestionMenu();
            break;
    }
    return question;
}

TextQuestion *QuizMaker::runTextQuestionMenu() {
    printInstruction("Type your question..");
    string questionText = getAnswer();
    printInstruction("Type your answer..");
    string answerText = getAnswer();
    int answerPoints = askForPoints();
    Answer & answer=*(new Answer(answerText,answerPoints));
    return new TextQuestion(questionText, answerPoints, answer);
}


int QuizMaker::askForNumberOfAnswers() const{
    int numberOfAnswers;
    while (true) {
        printInstruction("Type number of answers between 2 and 6");
        numberOfAnswers=getAnswerNumber();
        if (numberOfAnswers < 2 || numberOfAnswers > 6) {
            printError("Wrong input!!");
            continue;
        }
        break;
    }
    clearErrorWindow();
    return numberOfAnswers;
}

MultipleChoiceQuestion *QuizMaker::runMultipleChoiceQuestionMenu() {
    printInstruction("Type your question..");
    string questionText = getAnswer();
    int sumOfPoints=0;
    int numberOfAnswers = askForNumberOfAnswers();
    vector<Answer *> answers;
    int answerPoints;
    for (int i = 0; i < numberOfAnswers; ++i) {
        printInstruction("Type your answer(Max 60 characters)");
        string answerText = getAnswer(60);
        clearHintWindow();
        answerPoints = askForPoints();
        sumOfPoints += answerPoints;
        answers.push_back(new Answer(answerText, answerPoints));
    }
    return new MultipleChoiceQuestion(questionText, sumOfPoints, answers);
}

void QuizMaker::saveQuiz() {
    runQuizRatingMenu();
    xmlParser.quizToXML(quiz);
}

void QuizMaker::runQuizRatingMenu() const{
    vector<Quiz::QuizRating *> rating;
    char marks[]={'A','B','C','D','E'};
    clearErrorWindow();
    for(int i =0; i<5;i++){
        printInstruction("Type percentage for mark "+string(1,marks[i]));
        int percentage = getAnswerNumber();
        if(i!=0 && (rating[i-1]->percentage<=percentage ||percentage>100 || percentage<0 )){
            printError("Percentage higher than previous grade or invalid format");
            i--;
            continue;
        }
        rating.push_back(new Quiz::QuizRating(marks[i],percentage));
        clearErrorWindow();
    }
    quiz.setRatings(rating);

}


void QuizMaker::clearInstructionWindow() const {
    wclear(instructionWindow);
    wrefresh(instructionWindow);
    refresh();
}

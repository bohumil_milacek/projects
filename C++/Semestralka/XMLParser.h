/**
* @file         XMLParser.h
* @author       milacboh
*/
#pragma once

#include "Quiz.h"
#include <libxml/parser.h>
#include <ncurses.h>
#include <experimental/filesystem>
#include "TextQuestion.h"
#include "ChoiceQuestion.h"
#include "MultipleChoiceQuestion.h"
#include "InvalidFormatException.h"
#include "MultipleChoiceQuestion.h"
using namespace std;
namespace fs=std::experimental::filesystem;

/**
* @class    XMLParser
* @brief    XMLParser transforms quizzes to XML and loads them from .xml files
* @details  XMLParser can load quiz from XML format to program, it can
* @details  transform quiz from program to XML and save it to file
*/
class XMLParser {


public:

    /**
    * @fn      quizToXML
    * @param   quiz quiz to be transformed to xml
    * @brief   Takes in quiz, than transforms it to xml language and saves it as .xml file
    */
    void quizToXML(Quiz & quiz) const;

    /**
    * @fn      loadQuizNames
    * @brief   Loads all quiz names from local directory
    * @returns vector of all quiz names in local directory
    */
    vector<string> loadQuizNames() const;

    /**
    * @fn      loadQuizFromXML
    * @param   quizName quiz to be loaded from from local directory
    * @brief   Parses a quiz from local directory
    * @returns Pointer to parsed quiz
    */
    Quiz * loadQuizFromXML(const string & quizName) const;

private:
    /**
    * @fn      isQuizXmlFile
    * @param   xmlFileName quiz name to be checked
    * @brief   Checks if file is quiz, if it has xml structure of quiz
    * @returns true if file is quiz, or false if file is not quiz
    */
    bool isQuizXmlFile(string xmlFileName)const ;
    /**
    * @fn      parseRatings
    * @param   doc from which xmlDocument the ratings should be parsed
    * @param   cur points to the Ratings xml node
    * @brief   Parses Ratings from xml to program
    * @returns vector of pointers to parsed ratings
    */
    vector<Quiz::QuizRating *> parseRatings(xmlDocPtr doc, xmlNodePtr cur) const;
    /**
    * @fn      parseSections
    * @param   doc from which xmlDocument the sections should be parsed
    * @param   cur points to the Sections xml node
    * @brief   Parses Sections from xml to program
    * @returns vector of pointers to parsed sections
    */
    vector<Section *>  parseSections(xmlDocPtr doc, xmlNodePtr cur) const;
    /**
    * @fn      parseQuestions
    * @param   doc from which xmlDocument the questions should be parsed
    * @param   cur points to the Questions xml node
    * @brief   Parses Questions from xml to program
    * @returns list of pointers to parsed questions
    */
    list<Question *> parseQuestions(xmlDocPtr doc, xmlNodePtr cur) const;
    /**
    * @fn      parseTextQuestion
    * @param   doc from which xmlDocument the TextQuestion should be parsed
    * @param   cur points to the TextQuestion xml node
    * @brief   Parses TextQuestion from xml to program
    * @returns pointer to parsed TextQuestion
    */
    TextQuestion * parseTextQuestion(xmlDocPtr doc, xmlNodePtr cur) const;
    /**
    * @fn      parseChoiceQuestion
    * @param   doc from which xmlDocument the ChoiceQuestion should be parsed
    * @param   cur points to the ChoiceQuestion xml node
    * @brief   Parses ChoiceQuestion from xml to program
    * @returns pointer to parsed ChoiceQuestion
    */
    ChoiceQuestion * parseChoiceQuestion(xmlDocPtr doc, xmlNodePtr cur) const;
    /**
    * @fn      parseMultipleChoiceQuestion
    * @param   doc from which xmlDocument the MultipleChoiceQuestion should be parsed
    * @param   cur points to the MultipleChoiceQuestion xml node
    * @brief   Parses MultipleChoiceQuestion from xml to program
    * @returns pointer to parsed MultipleChoiceQuestion
    */
    MultipleChoiceQuestion * parseMultipleChoiceQuestion(xmlDocPtr doc, xmlNodePtr cur) const;
    /**
    * @fn      parseAnswer
    * @param   doc from which xmlDocument the Answer should be parsed
    * @param   cur points to the Answer xml node
    * @brief   Parses Answer from xml to program
    * @returns pointer to parsed Answer
    */
    Answer * parseAnswer(xmlDocPtr doc, xmlNodePtr cur) const;
    /**
    * @fn      parseChoiceAnswer
    * @param   doc from which xmlDocument the ChoiceAnswer should be parsed
    * @param   cur points to the ChoiceAnswer xml node
    * @brief   Parses ChoiceAnswer from xml to program
    * @returns pointer to parsed ChoiceAnswer
    */
    ChoiceAnswer * parseChoiceAnswer(xmlDocPtr doc, xmlNodePtr cur) const;

    /**
    * @fn      getStringFromXML
    * @param   xmlCharText pointer to xmlChar string to be transformed to c++ string
    * @brief   Transforms xmlChar to c++ string
    * @returns Transformed c++ string
    */
    string getStringFromXML(xmlChar * xmlCharText) const;
    /**
    * @fn      getIntFromXML
    * @param   xmlCharNumber pointer to xmlChar number to be transformed to c++ integer
    * @brief   Transforms xmlChar to c++ integer
    * @returns Transformed c++ integer
    */
    int getIntFromXML(xmlChar * xmlCharNumber) const;
    /**
    * @fn      getCharFromXML
    * @param   xmlCharacter pointer to xmlChar character to be transformed to c++ char
    * @brief   Transforms xmlChar to c++ char
    * @returns Transformed c++ char
    */
    char getCharFromXML(xmlChar * xmlCharacter) const;

};


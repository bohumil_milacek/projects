#include "QuizRunner.h"

QuizRunner::QuizRunner(){
    sectionTitleWindow = newwin(5,Engine::screenWidth,6,0);
    questionWindow = newwin(5,60,11,(Engine::screenWidth-60)/2);
    answersWindow = newwin(6,60,20,(Engine::screenWidth-60)/2);
    hintWindow = newwin(3,60,16,(Engine::screenWidth-60)/2);
}


QuizRunner::~QuizRunner() {
    delete quiz;
}

void QuizRunner::run() {
    printTitle();

    string quizName=runQuizSelectionMenu();

    quiz=xmlParser.loadQuizFromXML(quizName);
    runQuiz();
}

void QuizRunner::printTitle() const {
    int titleHeight = 5;
    int titleLength = 47;

    string title = " _____     _        _____                     \n"
                   "|     |_ _|_|___   | __  |_ _ ___ ___ ___ ___ \n"
                   "|  |  | | | |- _|  |    -| | |   |   | -_|  _|\n"
                   "|__  _|___|_|___|  |__|__|___|_|_|_|_|___|_|  \n"
                   "   |__|                                       ";

    WINDOW *titleWindow = newwin(titleHeight + 2, Engine::screenWidth, 0, 0);
    WINDOW *titleCenterWindow = newwin(titleHeight, titleLength, 1, (Engine::screenWidth - titleLength) / 2);

    box(titleWindow, 0, 0);
    mvwprintw(titleCenterWindow, 0, 0, "%s", title.c_str());
    refresh();
    wrefresh(titleWindow);
    wrefresh(titleCenterWindow);

}



string QuizRunner::runQuizSelectionMenu() const {
    unsigned quizNamesWindowHeight=15;
    unsigned quizNamesWindowWidth=50;

    WINDOW *quizNamesWindow = newwin(quizNamesWindowHeight, quizNamesWindowWidth, 8,
                             (Engine::screenWidth - quizNamesWindowWidth) / 2);

    vector <string> quizNames = xmlParser.loadQuizNames();

    keypad(quizNamesWindow, true);

    unsigned hilighted = 0;
    unsigned hilightedPosition = 0;

    int choice;
    int offset = 0;

    while (true) {
        wclear(quizNamesWindow);
        box(quizNamesWindow,0,0);
        for (unsigned i = 0; i < quizNamesWindowHeight; ++i) {
            unsigned index = i + offset;
            if (index < quizNames.size()) {
                if (index == hilighted)
                    wattron(quizNamesWindow, A_REVERSE);
                mvwprintw(quizNamesWindow, i, (quizNamesWindowWidth - quizNames[index].length()) / 2,
                          quizNames[index].c_str());
                wattroff(quizNamesWindow, A_REVERSE);
            }
        }
        choice = wgetch(quizNamesWindow);

        switch (choice) {
            case KEY_UP:
                if(hilighted>0)
                    hilighted--;
                else
                    break;

                if (hilightedPosition == 0)
                    offset--;
                else
                    hilightedPosition--;
                break;


            case KEY_DOWN:
                if(hilighted<quizNames.size()-1)
                    hilighted++;
                else
                    break;

                if (hilightedPosition == quizNamesWindowHeight-1)
                    offset++;
                else
                    hilightedPosition++;
                break;
            default:
                break;
        }
        if (choice == 10) {
            wclear(quizNamesWindow);
            wrefresh(quizNamesWindow);
            refresh();
            return quizNames[hilighted]+".xml";
        }
    }
}


void QuizRunner::runQuiz(){
    wrefresh(sectionTitleWindow);

    int sumOfPoints=0;
    int totalPointsPerQuiz=0;
    for(unsigned i=0;i<quiz->getSections().size();i++) {
        Section * section=quiz->getSections()[i];
        updateSectionTitleWindow(section->getTitle());
        for(const Question * question : section->getQuestions()){
            Question::AnswerStruct answerStructure=question->displayQuestionWithAnswers(questionWindow,answersWindow,hintWindow);
            totalPointsPerQuiz+=question->getPoints();
            sumOfPoints+=answerStructure.points;
            if(answerStructure.section!=-1 && (unsigned)answerStructure.section-1 <quiz->getSections().size()){
                i=answerStructure.section-2;
                break;
            }
        }
    }
    int percentage=0;
    if(totalPointsPerQuiz!=0)
        percentage=((double)sumOfPoints/totalPointsPerQuiz)*100;

    char grade=quiz->getGrade(percentage);
    string score="Score:"+to_string(sumOfPoints)+"/"+to_string(totalPointsPerQuiz)+" - "+grade;
    updateSectionTitleWindow(score);
    getch();
}

void QuizRunner::updateSectionTitleWindow(const string & title) const{
    wclear(sectionTitleWindow);
    box(sectionTitleWindow,0,0);
    mvwprintw(sectionTitleWindow, 5/2, (Engine::screenWidth - title.length()) / 2,title.c_str());
    wrefresh(sectionTitleWindow);
    refresh();
}





#ifndef __PROGTEST__

#include <cassert>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <list>
#include <algorithm>
#include <memory>
#include <functional>


using namespace std;

class CTimeStamp {
public:
    const int year, month, day, hour, minute;
    const double sec;

    CTimeStamp(int year, int month, int day, int hour, int minute, double sec) :
            year(year), month(month), day(day), hour(hour), minute(minute), sec(sec) {
    }

    /*
     * returns -1 if [this] Date is before [CTimeStamp x]
     * returns 0 if [this] Date is equal to [CTimeStamp x]
     * returns 1 if [this] Date is after than [CTimeStamp x]
     */
    int Compare(const CTimeStamp &x) const {
        if (year > x.year)
            return 1;
        else if (year < x.year)
            return -1;

        if (month > x.month)
            return 1;
        else if (month < x.month)
            return -1;

        if (day > x.day)
            return 1;
        else if (day < x.day)
            return -1;

        if (hour > x.hour)
            return 1;
        else if (hour < x.hour)
            return -1;

        if (minute > x.minute)
            return 1;
        else if (minute < x.minute)
            return -1;

        if (sec > x.sec)
            return 1;
        else if (sec < x.sec)
            return -1;

        return 0;
    }

    //outputs TimeStamp in format [YYYY-MM-DD HH24:MI:SS.UUU] to [ostream os]
    friend ostream &operator<<(ostream &os, const CTimeStamp &x);

private:
};


class CMail {
public:
    const CTimeStamp &timeStamp;
    const string &from;
    const string &to;
    const string &subject;

    CMail(const CTimeStamp &timeStamp, const string &from, const string &to, const string &subject) : timeStamp(
            timeStamp), from(from), to(to), subject(subject) {

    }

    int CompareByTime(const CTimeStamp &x) const {
        return timeStamp.Compare(x);
    }

    int CompareByTime(const CMail &x) const {
        return timeStamp.Compare(x.timeStamp);
    }

    const string &From(void) const {
        return from;
    }

    const string &To(void) const {
        return to;
    }

    const string &Subject(void) const {
        return subject;
    }

    const CTimeStamp &TimeStamp(void) const {
        return timeStamp;
    }

    friend ostream &operator<<(ostream &os, const CMail &x);

private:
};

ostream &operator<<(ostream &os, const CTimeStamp &x) {

    os << x.year << "-" << setw(2) << setfill('0') << x.month << "-" << setw(2) << setfill('0') << x.day << " "
       << x.hour << ":" << setw(2)
       << setfill('0') << x.minute << ":"
       << fixed << setprecision(3) << x.sec;
    return os;
}

ostream &operator<<(ostream &os, const CMail &x) {
    os << x.timeStamp << " " << x.from << " -> " << x.to << ", subject: " << x.subject;
    return os;
}


// your code will be compiled in a separate namespace
namespace MysteriousNamespace {
#endif /* __PROGTEST__ */

//----------------------------------------------------------------------------------------
    class CMailLog {
        //Constant map to convert month name to index
        const map<string, int> months
                {
                        {"Jan", 1},
                        {"Feb", 2},
                        {"Mar", 3},
                        {"Apr", 4},
                        {"May", 5},
                        {"Jun", 6},
                        {"Jul", 7},
                        {"Aug", 8},
                        {"Sep", 9},
                        {"Oct", 10},
                        {"Nov", 11},
                        {"Dec", 12}
                };

        map<string, CMail> mailsFrom;
        map<string, CMail> mailsSubject;
        multimap<string, CMail> mailsTo;

    public:
        //Takes each line of input [in] and passes it to parseLine()
        //Counts how many (delivered) emails with to=.. have been in log
        int ParseLog(istream &in) {
            string line;
            int numberOfDeliveredEmails = 0;
            while (getline(in, line)) {
                //   cout << line << endl;
                numberOfDeliveredEmails += parseLine(line);
            }
            //  printMails();
            cout << "PARSED LINES:" << numberOfDeliveredEmails;
            return numberOfDeliveredEmails;

        }


        /* Takes all delivered emails in time range [from, to] from mailTo map, puts them into list [listOfMails] and sorts them by delivered time,
         * If time is equal, they stay in same order in which they have been delivered, then returns sorted [listOfMails] */
        list<CMail> ListMail(const CTimeStamp &from,
                             const CTimeStamp &to) const {
            list<CMail> listOfMails;


            for (auto &mailToCopy : mailsTo) {
                if (mailToCopy.second.TimeStamp().Compare(from) >= 0 &&
                    mailToCopy.second.TimeStamp().Compare(to) <= 0) {
                    listOfMails.push_back(mailToCopy.second);
                }
            }
            listOfMails.sort([](CMail mail1, CMail mail2) {
                if (mail2.CompareByTime(mail1) > 0)
                    return true;
                return false;
            });
            return listOfMails;

        }

        // Takes all emails in time range [from, to] from mailTo and mailFrom map, takes email addres from them and puts addresses into set [activeUsers] and returns it
        set<string> ActiveUsers(const CTimeStamp &from,
                                const CTimeStamp &to) const {
            set<string> activeUsers;


            for (auto &mailToCopy : mailsTo) {
                if (mailToCopy.second.TimeStamp().Compare(from) >= 0 &&
                    mailToCopy.second.TimeStamp().Compare(to) <= 0) {

                    const auto mailFromIter = mailsFrom.find(mailToCopy.first);
                    if (mailFromIter != mailsFrom.end()) {
                        activeUsers.insert(mailFromIter->second.From());
                    }
                    activeUsers.insert(mailToCopy.second.To());


                }
            }
            return activeUsers;

        }

    private:
        /*
         * Method for parsing one line of log, if report is correct, it adds it to the log, if incorrect it skips the line
         * returns 1 if delivered mail found
         * return 0 if From, Subject found
         * if log line is in incorrecect format it's ignored and 0 is returned
         * */
        int parseLine(string &line) {
            cout << "PARSING _________________________" << endl;
            cout << ",," << line << "\"" << endl;
            string monthStr;
            int year, month, day, hour, minute;
            double sec;
            string dns, id;
            string &category = *(new string(""));
            string &from = *(new string(""));
            string &to = *(new string(""));
            string &subject = *(new string(""));

            char delimiter1, delimiter2;

            // parsing log line into specific variables
            istringstream iss(line);
            iss >> monthStr >> day >> year >> hour >> delimiter1 >> minute >> delimiter2 >> sec >> dns >> id;
            iss >> ws;
            getline(iss, category, '=');


            // fail or invalid log line format
            if (iss.fail() || delimiter1 != ':' || delimiter2 != ':') {
                cout << "FAIL PARSING LINE" << endl;
                return 0;
            }
            month = getMonthIndex(monthStr);

            CTimeStamp &mailTime = *(new CTimeStamp(year, month, day, hour, minute, sec));
            if (category == "from") {
                cout << endl << "FROM " << endl;
                getline(iss, from);
                CMail mailFrom(mailTime, from, to, subject);
                mailsFrom.insert(pair<string, CMail>(id, mailFrom));
            } else if (category == "subject") {
                cout << endl << "SUBJECT" << endl;
                getline(iss, subject);
                CMail mailSubject(mailTime, from, to, subject);
                cout << mailSubject << endl;
                mailsSubject.insert(make_pair(id, mailSubject));
            } else if (category == "to") {
                getline(iss, to);

                // find email from and email subject with same id as email delivered
                const auto mailFromIter = mailsFrom.find(id);
                const auto mailSubjectIter = mailsSubject.find(id);
                if (mailFromIter == mailsFrom.end()) {
                    cout << "Odesílatel neexistuje" << endl;
                    return 0;
                }
                from = mailFromIter->second.From();
                if (mailSubjectIter == mailsSubject.end()) {
                    cout << "Subject neexistuje" << endl;
                } else {
                    subject = mailSubjectIter->second.Subject();
                    cout << "SUBJECT:" << mailSubjectIter->second.Subject() << endl;
                }

                cout << "FROM:" << mailFromIter->second.From() << endl;


                CMail mailTo(mailTime, from, to, subject);
                mailsTo.insert(make_pair(id, mailTo));
                return 1;
            } else {
                cout << "Invalid line" << endl;
                return 0;
            }
            return 0;
        }

        //Returns index of the month by name name
        int getMonthIndex(string name) const {
            const auto iter = months.find(name);
            if (iter != months.cend())
                return iter->second;
            return -1;
        }
    };


//----------------------------------------------------------------------------------------
#ifndef __PROGTEST__
} // namespace
string printMail(const list<CMail> &all) {
    ostringstream oss;
    for (const auto &mail : all)
        oss << mail << endl;
    return oss.str();
}

string printUsers(const set<string> &all) {
    ostringstream oss;
    bool first = true;
    for (const auto &name : all) {
        if (!first)
            oss << ", ";
        else
            first = false;
        oss << name;
    }
    return oss.str();
}


void progtestTest() {
    MysteriousNamespace::CMailLog m;
    list<CMail> mailList;
    set<string> users;
    istringstream iss;

    iss.clear();
    iss.str(
            "Mar 29 2019 12:35:32.233 relay.fit.cvut.cz ADFger72343D: from=user1@fit.cvut.cz\n"
            "Mar 29 2019 12:37:16.234 relay.fit.cvut.cz JlMSRW4232Df: from=person3@fit.cvut.cz\n"
            "Mar 29 2019 12:55:13.023 relay.fit.cvut.cz JlMSRW4232Df: subject=New progtest homework!\n"
            "Mar 29 2019 13:38:45.043 relay.fit.cvut.cz Kbced342sdgA: from=office13@fit.cvut.cz\n"
            "Mar 29 2019 13:36:13.023 relay.fit.cvut.cz JlMSRW4232Df: to=user76@fit.cvut.cz\n"
            "Mar 29 2019 13:55:31.456 relay.fit.cvut.cz KhdfEjkl247D: from=PR-department@fit.cvut.cz\n"
            "Mar 29 2019 14:18:12.654 relay.fit.cvut.cz Kbced342sdgA: to=boss13@fit.cvut.cz\n"
            "Mar 29 2019 14:48:32.563 relay.fit.cvut.cz KhdfEjkl247D: subject=Business partner\n"
            "Mar 29 2019 14:58:32.000 relay.fit.cvut.cz KhdfEjkl247D: to=HR-department@fit.cvut.cz\n"
            "Mar 29 2019 14:25:23.233 relay.fit.cvut.cz ADFger72343D: mail undeliverable\n"
            "Mar 29 2019 15:02:34.231 relay.fit.cvut.cz KhdfEjkl247D: to=CIO@fit.cvut.cz\n"
            "Mar 29 2019 15:02:34.230 relay.fit.cvut.cz KhdfEjkl247D: to=CEO@fit.cvut.cz\n"
            "Mar 29 2019 15:02:34.230 relay.fit.cvut.cz KhdfEjkl247D: to=dean@fit.cvut.cz\n"
            "Mar 29 2019 15:02:34.230 relay.fit.cvut.cz KhdfEjkl247D: to=vice-dean@fit.cvut.cz\n"
            "Mar 29 2019 15:02:34.230 relay.fit.cvut.cz KhdfEjkl247D: to=archive@fit.cvut.cz\n");
    assert (m.ParseLog(iss) == 8);
    mailList = m.ListMail(CTimeStamp(2019, 3, 28, 0, 0, 0),
                          CTimeStamp(2019, 3, 29, 23, 59, 59));
    assert (printMail(mailList) ==
            "2019-03-29 13:36:13.023 person3@fit.cvut.cz -> user76@fit.cvut.cz, subject: New progtest homework!\n"
            "2019-03-29 14:18:12.654 office13@fit.cvut.cz -> boss13@fit.cvut.cz, subject: \n"
            "2019-03-29 14:58:32.000 PR-department@fit.cvut.cz -> HR-department@fit.cvut.cz, subject: Business partner\n"
            "2019-03-29 15:02:34.230 PR-department@fit.cvut.cz -> CEO@fit.cvut.cz, subject: Business partner\n"
            "2019-03-29 15:02:34.230 PR-department@fit.cvut.cz -> dean@fit.cvut.cz, subject: Business partner\n"
            "2019-03-29 15:02:34.230 PR-department@fit.cvut.cz -> vice-dean@fit.cvut.cz, subject: Business partner\n"
            "2019-03-29 15:02:34.230 PR-department@fit.cvut.cz -> archive@fit.cvut.cz, subject: Business partner\n"
            "2019-03-29 15:02:34.231 PR-department@fit.cvut.cz -> CIO@fit.cvut.cz, subject: Business partner\n");
    mailList = m.ListMail(CTimeStamp(2019, 3, 28, 0, 0, 0),
                          CTimeStamp(2019, 3, 29, 14, 58, 32));
    assert (printMail(mailList) ==
            "2019-03-29 13:36:13.023 person3@fit.cvut.cz -> user76@fit.cvut.cz, subject: New progtest homework!\n"
            "2019-03-29 14:18:12.654 office13@fit.cvut.cz -> boss13@fit.cvut.cz, subject: \n"
            "2019-03-29 14:58:32.000 PR-department@fit.cvut.cz -> HR-department@fit.cvut.cz, subject: Business partner\n");
    mailList = m.ListMail(CTimeStamp(2019, 3, 30, 0, 0, 0),
                          CTimeStamp(2019, 3, 30, 23, 59, 59));
    assert (printMail(mailList) == "");
    users = m.ActiveUsers(CTimeStamp(2019, 3, 28, 0, 0, 0),
                          CTimeStamp(2019, 3, 29, 23, 59, 59));


    cout << endl << endl << endl << endl;
    cout << printUsers(users) << endl;
    cout << endl << endl << endl << endl;


    assert (printUsers(users) ==
            "CEO@fit.cvut.cz, CIO@fit.cvut.cz, HR-department@fit.cvut.cz, PR-department@fit.cvut.cz, archive@fit.cvut.cz, boss13@fit.cvut.cz, dean@fit.cvut.cz, office13@fit.cvut.cz, person3@fit.cvut.cz, user76@fit.cvut.cz, vice-dean@fit.cvut.cz");
    users = m.ActiveUsers(CTimeStamp(2019, 3, 28, 0, 0, 0),
                          CTimeStamp(2019, 3, 29, 13, 59, 59));
    assert (printUsers(users) == "person3@fit.cvut.cz, user76@fit.cvut.cz");

}


void myTest() {
    MysteriousNamespace::CMailLog m;
    list<CMail> mailList;
    set<string> users;
    istringstream iss;

    iss.clear();
    iss.str(
            "Mar 9 2019 12:35:32.233 relay.fit.cvut.cz ADFger72343D: from=user1@fit.cvut.cz\n"
            "Mar 29 2019 12:37:16.234 relay.fit.cvut.cz JlMSRW4232Df: from=person3@fit.cvut.cz\n");
    m.ParseLog(iss);

}

int main(void) {
    myTest();
    //progtestTest();

    return 0;
}

#endif /* __PROGTEST__ */


#ifndef __PROGTEST__
#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <climits>
#include <cfloat>
#include <cassert>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <numeric>
#include <vector>
#include <set>
#include <list>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <queue>
#include <stack>
#include <deque>
#include <memory>
#include <functional>
#include <thread>
#include <mutex>
#include <atomic>
#include <chrono>
#include <condition_variable>
#include "progtest_solver.h"
#include "sample_tester.h"

using namespace std;
#endif /* __PROGTEST__ */

class CSentinelHacker
{
    vector<AReceiver> receivers;
    vector<ATransmitter> transmiters;
    vector<thread> processingThreads;
    vector<thread> receiveThreads;
    vector<thread> transmiterThreads;
    queue<pair<uint32_t, CBigInt>> toTransmit;
    queue<uint32_t> toProcess;
    map<uint32_t, vector<uint64_t>> messages;

    mutex receiveMutex;
    mutex transmitMutex;

    condition_variable cvReceive;
    condition_variable cvTransmit;

    atomic<bool> isProcessing;
    atomic<bool> isReceiving;

public:
    CSentinelHacker() : isProcessing(false), isReceiving(false)
    {
    }

    static bool SeqSolve(const vector<uint64_t> &fragments, CBigInt &res)
    {
        CBigInt minRes = 0;
        bool found = FindPermutations(fragments.data(), fragments.size(), [&minRes](const uint8_t *payload, size_t payloadLength) {
            const uint8_t *bitfield = payload + 4;
            size_t bitfieldLength = payloadLength - 32;
            CBigInt actualRes = CountExpressions(bitfield, bitfieldLength);
            if (minRes.CompareTo(actualRes) < 0)
                minRes = actualRes;
        });
        res = minRes;
        return found;
    }

    void Receive(const int i)
    {
        uint64_t fragment;
        while (receivers[i]->Recv(fragment))
            AddFragment(fragment);
    }

    void Process(const int i)
    {
        while (true)
        {
            unique_lock<mutex> locker(receiveMutex);
            cvReceive.wait(locker, [=]() { return !isReceiving || !toProcess.empty(); });
            if (!isReceiving && toProcess.empty())
                break;
            uint32_t messageID = toProcess.front();
            toProcess.pop();
            vector<uint64_t> messageToProcess(messages[messageID]);
            locker.unlock();
            cvReceive.notify_one();

            CBigInt res;
            if (SeqSolve(messageToProcess, res))
            {
                unique_lock<mutex> transmitLock(transmitMutex);
                toTransmit.push(make_pair(messageID, res));
                transmitLock.unlock();
                cvTransmit.notify_one();
                {
                    lock_guard<mutex> lock(receiveMutex);
                    messages.erase(messageID);
                }
            }
        }
    }

    void Transmit(const int i)
    {
        while (true)
        {
            unique_lock<mutex> locker(transmitMutex);
            cvTransmit.wait(locker, [=]() { return !isProcessing || !toTransmit.empty(); });
            if (!isProcessing && toTransmit.empty())
                break;
            transmiters[i]->Send(toTransmit.front().first, toTransmit.front().second);
            toTransmit.pop();
            locker.unlock();
            cvTransmit.notify_one();
        }

        for (auto &pair : messages)
        {
            unique_lock<mutex> lock(transmitMutex);
            uint32_t incompleteID = pair.first;
            lock.unlock();
            transmiters[i]->Incomplete(incompleteID);
        }
    }

    void AddTransmitter(ATransmitter x) { transmiters.push_back(x); }

    void AddReceiver(AReceiver x) { receivers.push_back(x); }

    void AddFragment(uint64_t x)
    {
        uint32_t messageID = GetMessageID(x);
        unique_lock<mutex> lock(receiveMutex);
        messages[messageID].push_back(x);
        toProcess.push(messageID);
        lock.unlock();
        cvReceive.notify_one();
    }

    void Start(unsigned thrCount)
    {
        thrCount = 1;
        isReceiving = true;
        isProcessing = true;
        for (size_t i = 0; i < receivers.size(); i++)
            receiveThreads.push_back(thread([=]() { Receive(i); }));
        for (size_t i = 0; i < thrCount; i++)
            processingThreads.push_back(thread([=]() { Process(i); }));
        for (size_t i = 0; i < transmiters.size(); i++)
            transmiterThreads.push_back(thread([=]() { Transmit(i); }));
    }

    void Stop(void)
    {
        for (size_t i = 0; i < receivers.size(); i++)
            receiveThreads[i].join();
        {
            lock_guard<mutex> lock(receiveMutex);
            isReceiving = false;
        }
        cvReceive.notify_all();
        for (size_t i = 0; i < processingThreads.size(); i++)
            processingThreads[i].join();
        {
            lock_guard<mutex> lock(transmitMutex);
            isProcessing = false;
        }
        cvTransmit.notify_all();
        for (size_t i = 0; i < transmiters.size(); i++)
            transmiterThreads[i].join();
    }

private:
    inline uint32_t GetMessageID(uint64_t fragment) const { return fragment >>= 37; }
};
// TODO: CSentinelHacker implementation goes here
//-------------------------------------------------------------------------------------------------
#ifndef __PROGTEST__

int main(void)
{
    using namespace placeholders;
    for (const auto &x : g_TestSets)
    {
        CBigInt res;
        assert(CSentinelHacker::SeqSolve(x.m_Fragments, res));
        assert(CBigInt(x.m_Result).CompareTo(res) == 0);
    }

    CSentinelHacker test;
    auto trans = make_shared<CExampleTransmitter>();
    AReceiver recv = make_shared<CExampleReceiver>(initializer_list<uint64_t>{0x02230000000c, 0x071e124dabef, 0x02360037680e, 0x071d2f8fe0a1,
                                                                              0x055500150755});

    test.AddTransmitter(trans);
    test.AddReceiver(recv);
    test.Start(3);

    static initializer_list<uint64_t> t1Data = {0x071f6b8342ab, 0x0738011f538d, 0x0732000129c3, 0x055e6ecfa0f9,
                                                0x02ffaa027451, 0x02280000010b, 0x02fb0b88bc3e};
    thread t1(FragmentSender, bind(&CSentinelHacker::AddFragment, &test, _1), t1Data);

    static initializer_list<uint64_t> t2Data = {0x073700609bbd, 0x055901d61e7b, 0x022a0000032b, 0x016f0000edfb};
    thread t2(FragmentSender, bind(&CSentinelHacker::AddFragment, &test, _1), t2Data);
    FragmentSender(bind(&CSentinelHacker::AddFragment, &test, _1),
                   initializer_list<uint64_t>{0x017f4cb42a68, 0x02260000000d, 0x072500000025});
    t1.join();
    t2.join();
    test.Stop();
    assert(trans->TotalSent() == 4);
    assert(trans->TotalIncomplete() == 2);
    return 0;
}

#endif /* __PROGTEST__ */

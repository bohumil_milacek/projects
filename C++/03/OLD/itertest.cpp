#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <ctime>
#include <climits>
#include <cmath>
#include <cassert>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>
#include <list>
#include <algorithm>
#include <functional>
#include <memory>

using namespace std;
int i = 0;


class InvalidRangeException {
};


class CRange {
    long long lo;
    long long hi;

public:
    //Constructor - If user creates Range with lover bound(lo) bigger than upper bound(hi), the constructor throws exeption
    CRange(long long lo, long long hi) : lo(lo), hi(hi) {
        if (lo > hi)
            throw InvalidRangeException();
    }

    //Getters and Setters
    long long lowValue() const { return lo; }

    long long highValue() const { return hi; }

    void setLowValue(long long lo) { this->lo = lo; }

    void setHighValue(long long hi) { this->hi = hi; }

    //compares two ranges based on their bounds
    bool operator==(const CRange &r) const { return lo == r.lo && hi == r.hi; }

    bool operator!=(const CRange &r) const { return !(*this == r); }

    //Prints out range in format <low..high>
    friend ostream &operator<<(ostream &out, const CRange &range);


private:
    // todo
};

ostream &operator<<(ostream &out, const CRange &range) {
    out << "<" << range.lowValue() << "," << range.highValue() <<
        ">" << endl;
}


struct A {
    A() {
        std::generate(&v[0], &v[10], [&i]() { return ++i; });
    }

    int *begin() {
        return &v[0];
    }

    int *end() {
        return &v[10];
    }

    int v[10];
};

class CRangeList {
    // operator <<
    friend ostream &operator<<(ostream &out, const CRangeList &list);

    vector <CRange> ranges;
public:
    vector<CRange>::iterator begin() {
        ranges.begin();
    }

    vector<CRange>::iterator end() {
        ranges.end();
    }


    const CRangeList &operator+=(const CRange &range) {
        ranges.push_back(range);

        return *this;
    }

    void lowerBound(const CRange &range) {
        auto low = std::lower_bound(ranges.begin(), ranges.end(), range.lowValue());
        cout << "Index:" << low - ranges.begin() << endl;
        //  cout << "Index to insert" << low << endl;
    }

    void upperBound(const CRange &range) {
        auto low = std::upper_bound(ranges.begin(), ranges.end(), range.highValue());
        cout << "Index:" << low - ranges.begin() << endl;
        //  cout << "Index to insert" << low << endl;
    }


};

bool operator<(const CRange &range, const long long val) {
    return range.lowValue() < val;
}

bool operator<(const long long val, const CRange &range) {
    return  val<=range.highValue();
}

ostream &operator<<(ostream &out, const CRangeList &list) {
    out << "{";
    int index = 0;
/*  for (CRange const &value: list) {
      if (value.lowValue() == value.highValue())
          out << value.lowValue();
      else
          out << "<" << value.lowValue() << ".." << value.highValue() << ">";
      if (index != list.size() - 1) {
          out << ",";
      }
      index++;
  }*/
    out << "}";
    return out;
}

int main() {
    CRangeList a;
    a += CRange(10, 20);
    a += CRange(30, 50);
    a += CRange(60, 100);
    a += CRange(200, 400);

    for (auto it : a) {
        std::cout << it;
    }

    a.lowerBound(CRange(9, 66));
    a.upperBound(CRange(9, 150));


}
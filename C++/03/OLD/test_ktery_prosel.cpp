#ifndef __PROGTEST__

#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <ctime>
#include <climits>
#include <cmath>
#include <cassert>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>
#include <list>
#include <algorithm>
#include <functional>
#include <memory>

using namespace std;

class InvalidRangeException {
};

#endif /* __PROGTEST__ */

// uncomment if your code implements initializer lists
// #define EXTENDED_SYNTAX

class CRange {
    long long lo;
    long long hi;

public:
    //Constructor - If user creates Range with lover bound(lo) bigger than upper bound(hi), the constructor throws exeption
    CRange(long long lo, long long hi) : lo(lo), hi(hi) {
        if (lo > hi)
            throw InvalidRangeException();
    }

    //Getters and Setters
    long long lowValue() const { return lo; }

    long long highValue() const { return hi; }

    void setLowValue(long long lo) { this->lo = lo; }

    void setHighValue(long long hi) { this->hi = hi; }

    //compares two ranges based on their bounds
    bool operator==(const CRange &r) const { return lo == r.lo && hi == r.hi; }

    bool operator!=(const CRange &r) const { return !(*this == r); }

    //Prints out range in format <low..high>
    friend ostream &operator<<(ostream &out, const CRange &range);


private:
    // todo
};

//Prints out range in format <low..high>
ostream &operator<<(ostream &out, const CRange &range) {
    out << "<" << range.lo << ".." << range.hi << ">";
    return out;
}


/*
 * Holds CRanges in 2D array
 * In first row are + CRanges
 * In second row are - CRanges
 * true - for +
 * false - for .
 * */
class CRangeContainer {
    vector <CRange> ranges;
    vector<bool> operators;

public:

    void addRange(const CRange &range, bool plus) {
        ranges.push_back(range);
        operators.push_back(plus);
    }

    const vector <CRange> &getRanges() const {
        return ranges;
    }

    const vector<bool> &getOperators() const {
        return operators;
    }
};


class CRangeList {
    // operator <<
    friend ostream &operator<<(ostream &out, const CRangeList &list);

    vector <CRange> ranges;

public:
    // constructor
    CRangeList() {}


    //Constructor for bonus
    // CRangeList(initializer_list <CRange> ranges) {}

    // += range / range list
    const CRangeList &operator+=(const CRange &range) {

        cout << "INSERTING:" << range << "______________________" << endl;
        int indexLowerBound = binarySeachByLo(range.lowValue());
        int indexUpperBound = binarySeachByHi(range.highValue());
        bool lowerBoundIntersects = false;
        bool upperBoundIntersects = false;


        cout << *this << endl;
        cout << "IndexLow:" << indexLowerBound << endl;
        cout << "IndexUpper:" << indexUpperBound << endl;

        //chekcing if Range is not being inserted on the 0 position, than it is the lowest bound, otherwise cheking if lower Range intersects
        lowerBoundIntersects = ((indexLowerBound > 0) &&
                                (ranges[indexLowerBound - 1].highValue() + 1 >= range.lowValue()));

        //chekcing if Range is not being inserted on the END position, than it is the biggest bound, otherwise cheking if upper Range intersects
        upperBoundIntersects = ((indexUpperBound < size()) &&
                                (ranges[indexUpperBound].lowValue() - 1 <= range.highValue()));


        // both upper and lower bound intersect with other ranges, need to erase some ranges
        if (indexLowerBound > indexUpperBound) {
            return *this;
        }
        if (lowerBoundIntersects && upperBoundIntersects) {
            cout << "Both boudns intersect" << endl;
            ranges[indexLowerBound - 1].setHighValue(ranges[indexUpperBound].highValue());
            ranges.erase(ranges.begin() + (indexLowerBound), ranges.begin() + (indexUpperBound + 1));
        } else {
            if (lowerBoundIntersects) {
                // lower range intersects, no exces inserting,
                cout << "Lower boudns intersect" << endl;
                ranges[indexLowerBound - 1].setHighValue(range.highValue());
                ranges.erase(ranges.begin() + indexLowerBound, ranges.begin() + indexUpperBound);
            } else if (upperBoundIntersects) {
                // upper range intersects, no exces inserting, just changing
                cout << "Upper boudns intersect" << endl;
                ranges[indexUpperBound].setLowValue(range.lowValue());
                ranges.erase(ranges.begin() + indexLowerBound, ranges.begin() + indexUpperBound);
            } else if (indexLowerBound == 0 && indexUpperBound == size() && size() > 0) {
                ranges.clear();
                ranges.push_back(range);
            } else {
                //Nothing intersects, can insert at position
                ranges.insert(ranges.begin() + indexLowerBound, range);
            };
        }

        cout << *this << endl;
        cout << "END_INSERTING______________________________" << endl << endl;
        return *this;
    }


    //Sum of two CRangeLists
    const CRangeList &operator+=(const CRangeList &list) {
        cout << "summ of two lists" << endl;

        for (const auto &item : list.ranges) {
            *this += item;
        }
        return *this;
    }

    // -= range / range list
    const CRangeList &operator-=(const CRange &range) {
        if (isEmpty()) {
            return *this;
        }
        int indexLowerBound = binarySeachByHi(range.lowValue());
        int indexUpperBound = binarySeachByHi(range.highValue() + 1);

        cout << *this << endl;
        cout << "REMOVING:" << range << "______________________.." << ranges[indexLowerBound - 1]
             << ranges[indexLowerBound] << ranges[indexUpperBound] << ".." << endl;

        cout << "IndexLow:" << indexLowerBound << endl;
        cout << "IndexUpper:" << indexUpperBound << endl;

        // exactly the same interval
        if (ranges[indexLowerBound] == range) {
            cout << "exactly the same interval" << endl;
            ranges.erase(ranges.begin() + indexLowerBound, ranges.begin() + indexUpperBound);
        } else if (ranges[indexLowerBound].lowValue() < range.lowValue() &&
                   range.highValue() < ranges[indexLowerBound].highValue()) {
            cout << "inside one interval" << endl;
            CRange *r = new CRange(range.highValue() + 1, ranges[indexLowerBound].highValue());
            ranges[indexLowerBound].setHighValue(range.lowValue() - 1);
            *this += *r;
            // interval is inside another interval, doesn't intersect with multiple..
        } else {
            // testing if intersects with multiple intervals
            if (indexLowerBound >= size()) {
                cout << "Out of range" << endl;
                return *this;
            }

            if (ranges[0].lowValue() >= range.lowValue() && ranges[size() - 1].highValue() <= range.highValue()) {
                ranges.clear();
            }
            if (indexLowerBound < indexUpperBound) {
                cout << "Affects upper bound" << endl;
                ranges[indexUpperBound].setLowValue(range.highValue() + 1);
            }

            if (range.lowValue() < ranges[indexLowerBound].lowValue()) {
                cout << "Low Value out of bounds" << endl;
                ranges[indexLowerBound].setLowValue(range.highValue() + 1);
            } else {
                cout << "Stays in lower bounds" << endl;
                ranges[indexLowerBound].setHighValue(range.lowValue() - 1);
            }
        }


        cout << *this << endl;
        cout << "END_REMOVING______________________________" << endl << endl << endl << endl;
        return *this;
    }

    void operator-=(const CRangeList &list) {
        for (const auto &item : list.ranges) {
            *this -= item;
        }
    }

    // = range / range list
    void operator=(const CRange &range) {
        *this += range;
    }

    // operator ==
    bool operator==(const CRangeList &list) const {
        if (list.size() != size()) {
            return false;
        }
        for (int i = 0; i < size(); ++i) {
            if (ranges[i] != list.ranges[i])
                return false;
        }
        return true;
    }

    // operator !=
    bool operator!=(const CRangeList &list) const {
        return !(*this == list);
    }

    // Includes long long / range
    bool Includes(long long number) const{
        cout << "Includes :" << number << endl;
        int index = binarySeachByHi(number);
        cout << index << endl;

        return (ranges[index].lowValue() <= number && number <= ranges[index].highValue());
    }

    bool Includes(const CRange &range) const{
        int index = binarySeachByHi(range.lowValue());

        cout << range << endl;
        cout << range.lowValue() << "L:" << index << endl << endl;
        return (ranges[index].lowValue() <= range.lowValue() && range.highValue() <= ranges[index].highValue());
    }

    //Returns number of Ranges in CRangeList
    int size() const { return ranges.size(); }


    void operator=(const CRangeContainer &container) {
        vector<bool> operators = container.getOperators();
        vector <CRange> ranges = container.getRanges();

        for (unsigned i = 0; i < ranges.size(); ++i) {
            if (operators[i])
                *this += ranges[i];
            else
                *this -= ranges[i];

        }

    }

    bool isEmpty() {
        return size() == 0;
    }

private:
    //Binary search in ranges, returns index where range should be by lower bound
    //Returns 0 if List is empty
    int binarySeachByLo(long long insertValue) const{
        if (size() == 0) {
            return 0;
        }
        int leftVal = 0;
        int rightVal = size() - 1;
        int index = size() - 1;

        while (true) {
            index = (rightVal + leftVal) / 2;
            if (leftVal == index) {
                if (ranges[index].lowValue() > insertValue) {
                    return index;
                }
            }
            if (ranges[index].lowValue() < insertValue) {
                leftVal = index + 1;          // its in the upper
                if (leftVal > rightVal) {
                    return index += 1;
                }
            } else if (leftVal > rightVal) {
                return index;
            } else {
                rightVal = index - 1;          // its in the lower
            }
        }
    }

    //Binary search in ranges, returns index where range should be by upper bound
    //Returns 0 if List is empty
    int binarySeachByHi(long long insertValue) const{
        if (size() == 0) {
            return 0;
        }
        int leftVal = 0;
        int rightVal = size() - 1;
        int index = size() - 1;

        while (true) {
            index = (rightVal + leftVal) / 2;
            if (leftVal == index) {
                if (ranges[index].highValue() > insertValue) {
                    return index;
                }
            }
            if (ranges[index].highValue() < insertValue) {
                leftVal = index + 1;          // its in the upper
                if (leftVal > rightVal) {
                    return index += 1;
                }
            } else if (leftVal > rightVal) {
                return index;
            } else {
                rightVal = index - 1;          // its in the lower
            }
        }
    }

};

/*BEGIN___Operators--------------------------------------------------------------------------------------------------*/

ostream &operator<<(ostream &out, const CRangeList &list) {
    out << "{";
    int index = 0;
    for (CRange const &value: list.ranges) {
        if (value.lowValue() == value.highValue())
            out << value.lowValue();
        else
            out << "<" << value.lowValue() << ".." << value.highValue() << ">";
        if (index != list.size() - 1) {
            out << ",";
        }
        index++;
    }
    out << "}";
    return out;
}


CRangeContainer &operator+(const CRange &range1, const CRange &range2) {
    CRangeContainer *container = new CRangeContainer();
    container->addRange(range1, true);
    container->addRange(range2, true);
    return *container;
}

CRangeContainer &operator-(const CRange &range1, const CRange &range2) {
    CRangeContainer *container = new CRangeContainer();
    container->addRange(range1, false);
    container->addRange(range2, false);
    return *container;
}


CRangeContainer &operator+(CRangeContainer &container, const CRange &range) {
    container.addRange(range, true);
    return container;
}

CRangeContainer &operator-(CRangeContainer &container, const CRange &range) {
    container.addRange(range, false);
    return container;
}


void operator+=(CRangeList &list, CRangeContainer &container) {

    vector<bool> operators = container.getOperators();
    vector <CRange> ranges = container.getRanges();

    for (unsigned i = 0; i < ranges.size(); ++i) {
        if (operators[i])
            list += ranges[i];
        else
            list -= ranges[i];

    }

}

void operator-=(CRangeList &list, CRangeContainer &container) {
    vector <CRange> ranges = container.getRanges();
    vector<bool> operators = container.getOperators();

    for (unsigned i = 0; i < ranges.size(); ++i) {
        if (i == 0)
            list -= ranges[i];
        else if (operators[i])
            list -= ranges[i];
        else
            list += ranges[i];

    }

}
/*END___Operators--------------------------------------------------------------------------------------------------*/


#ifndef __PROGTEST__


string toString(const CRangeList &x) {
    ostringstream oss;
    oss << x;
    return oss.str();
}


void test1() {
    CRangeList a, b;

    //   std::cout.setstate(std::ios_base::failbit);
    cout.setstate(std::ios_base::failbit);

    assert(sizeof(CRange) <= 2 * sizeof(long long));
    a = CRange(5, 10);
    a += CRange(25, 100);
    assert(toString(a) == "{<5..10>,<25..100>}");
    a += CRange(-5, 0);
    a += CRange(8, 50);
    assert(toString(a) == "{<-5..0>,<5..100>}");
    a += CRange(101, 105) + CRange(120, 150) + CRange(160, 180) + CRange(190, 210);
    assert(toString(a) == "{<-5..0>,<5..105>,<120..150>,<160..180>,<190..210>}");
    a += CRange(106, 119) + CRange(152, 158);
    assert(toString(a) == "{<-5..0>,<5..150>,<152..158>,<160..180>,<190..210>}");
    a += CRange(-3, 170);
    a += CRange(-30, 1000);
    assert(toString(a) == "{<-30..1000>}");
    b = CRange(-500, -300) + CRange(2000, 3000) + CRange(700, 1001);
    a += b;
    //cout << "A:" << toString(a) << endl;
    assert(toString(a) == "{<-500..-300>,<-30..1001>,<2000..3000>}");
    a -= CRange(-400, -400);
    assert(toString(a) == "{<-500..-401>,<-399..-300>,<-30..1001>,<2000..3000>}");
    a -= CRange(10, 20) + CRange(900, 2500) + CRange(30, 40) + CRange(10000, 20000);
    /*   cout << "________________________________________"<<endl;
       cout << "A:"<<toString(a)<<endl;
       cout << "________________________________________"<<endl;*/
    assert(toString(a) == "{<-500..-401>,<-399..-300>,<-30..9>,<21..29>,<41..899>,<2501..3000>}");
    try {
        a += CRange(15, 18) + CRange(10, 0) + CRange(35, 38);
        assert("Exception not thrown" == NULL);
    }
    catch (const InvalidRangeException &e) {
    }
    catch (...) {
        assert("Invalid exception thrown" == NULL);
    }
    assert(toString(a) == "{<-500..-401>,<-399..-300>,<-30..9>,<21..29>,<41..899>,<2501..3000>}");
    b = a;
    assert(a == b);
    assert(!(a != b));
    b += CRange(2600, 2700);
    assert(toString(b) == "{<-500..-401>,<-399..-300>,<-30..9>,<21..29>,<41..899>,<2501..3000>}");
    assert(a == b);
    assert(!(a != b));
    b += CRange(15, 15);
    assert(toString(b) == "{<-500..-401>,<-399..-300>,<-30..9>,15,<21..29>,<41..899>,<2501..3000>}");
    assert(!(a == b));
    assert(a != b);
    assert(b.Includes(15));
    assert(b.Includes(2900));
    assert(b.Includes(CRange(15, 15)));
    assert(b.Includes(CRange(-350, -350)));
    assert(b.Includes(CRange(100, 200)));
    assert(!b.Includes(CRange(800, 900)));
    assert(!b.Includes(CRange(-1000, -450)));
    assert(!b.Includes(CRange(0, 500)));
    a += CRange(-10000, 10000) + CRange(10000000, 1000000000);
    assert(toString(a) == "{<-10000..10000>,<10000000..1000000000>}");
    b += a;
    assert(toString(b) == "{<-10000..10000>,<10000000..1000000000>}");
    b -= a;

    cout << "-------------------------------------------------------------------------------------------------" << endl;
    std::cout.clear();
    cout << "B:" << toString(b) << endl;

    assert(toString(b) == "{}");
    b += CRange(0, 100) + CRange(200, 300) - CRange(150, 250) + CRange(160, 180) - CRange(170, 170);
    assert(toString(b) == "{<0..100>,<160..169>,<171..180>,<251..300>}");

    b -= CRange(10, 90) - CRange(20, 30) - CRange(40, 50) - CRange(60, 90) + CRange(70, 80);
    cout.clear();
    cout << "________________________________________" << endl;
    cout << "B:" << toString(b) << endl;
    cout << "________________________________________" << endl;
    assert(toString(b) == "{<0..9>,<20..30>,<40..50>,<60..69>,<81..100>,<160..169>,<171..180>,<251..300>}");
}

#ifdef EXTENDED_SYNTAX
void testExtra(){
     CRangeList x{{5,   20},
                  {150, 200},
                  {-9,  12},
                  {48,  93}};
      assert ( toString ( x ) == "{<-9..20>,<48..93>,<150..200>}" );
      ostringstream oss;
      oss << setfill ( '=' ) << hex << left;
      for ( const auto & v : x + CRange ( -100, -100 ) )
        oss << v << endl;
      oss << setw ( 10 ) << 1024;
      assert ( oss . str () == "-100\n<-9..20>\n<48..93>\n<150..200>\n400=======" );
}
#endif /* EXTENDED_SYNTAX */

void test2() {
    CRangeList a, b;
    a = b;
    cout << "A:" << toString(a) << endl;
}

int main(void) {
    //test1();
    test2();
    return 0;
}

#endif /* __PROGTEST__ */

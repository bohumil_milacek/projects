#ifndef __PROGTEST__

#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <ctime>
#include <climits>
#include <cmath>
#include <cassert>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>
#include <list>
#include <algorithm>
#include <functional>
#include <memory>

using namespace std;

class InvalidRangeException {
};

#endif /* __PROGTEST__ */

// uncomment if your code implements initializer lists
// #define EXTENDED_SYNTAX

class CRange {
    long long lo;
    long long hi;

public:
    //Constructor - If user creates Range with lover bound(lo) bigger than upper bound(hi), the constructor throws exeption
    CRange(long long lo, long long hi) : lo(lo), hi(hi) {
        if (lo > hi)
            throw InvalidRangeException();
    }

    //Getters and Setters
    long long lowValue() const { return lo; }

    long long highValue() const { return hi; }

    void setLowValue(long long lo) { this->lo = lo; }

    void setHighValue(long long hi) { this->hi = hi; }

    //compares two ranges based on their bounds
    bool operator==(const CRange &r) const { return lo == r.lo && hi == r.hi; }

    bool operator!=(const CRange &r) const { return !(*this == r); }

    //Prints out range in format <low..high>
    friend ostream &operator<<(ostream &out, const CRange &range);

    friend bool operator<(const CRange &range, const long long val);

    friend bool operator<(const long long val, const CRange &range);

private:
    // todo
};

//Prints out range in format <low..high>
ostream &operator<<(ostream &out, const CRange &range) {
    out << "<" << range.lo << ".." << range.hi << ">";
    return out;
}

// function for lower_bound from STL
bool operator<(const CRange &range, const long long val) {
    return range.lowValue() < val;
}

// function for upper_bound from STL
bool operator<(const long long val, const CRange &range) {
    return val <= range.highValue();
}


/*
 * Holds CRanges in 2D array
 * In first row are + CRanges
 * In second row are - CRanges
 * true - for +
 * false - for .
 * */
class CRangeContainer {
    vector <CRange> ranges;
    vector<bool> operators;

public:

    void addRange(const CRange &range, bool plus) {
        ranges.push_back(range);
        operators.push_back(plus);
    }

    const vector <CRange> &getRanges() const {
        return ranges;
    }

    const vector<bool> &getOperators() const {
        return operators;
    }
};


class CRangeList {
    // operator <<
    friend ostream &operator<<(ostream &out, const CRangeList &list);

    vector <CRange> ranges;

public:
    // constructor
    CRangeList() {}
    CRangeList(CRangeContainer * containerPtr) {
        *this+=containerPtr;
    }


    //iterator
    vector<CRange>::iterator begin() {
        return ranges.begin();
    }

    vector<CRange>::iterator end() {
        return ranges.end();
    }

    vector<CRange>::const_iterator begin() const {
        return ranges.begin();
    }

    vector<CRange>::const_iterator end() const {
        return ranges.end();
    }


    //Constructor for bonus
    // CRangeList(initializer_list <CRange> ranges) {}

    // += range / range list
    const CRangeList &operator+=(const CRange &range) {
        cout << "INSERTING__" << range << endl;
        if (isEmpty()) {
            cout << "List is empty" << endl;
            ranges.push_back(range);
            cout << endl;
            return *this;
        }

        int lowerIndex = lowerBound(range.lowValue() + 1);
        int upperIndex = upperBound(range.highValue());
        cout << "Lower Index:" << lowerIndex << endl;
        cout << "Upper Index:" << upperIndex << endl;

        /*
         * if lower bound is bigger, than it is in same interval as upper index
         * {<0..100>} + <90..95>
         * lower:1 because 90 is after 0
         * upper:0 because 95 is before 100*/
        if (lowerIndex > upperIndex) {
            // do nothing
            cout << "Exactly same interval" << endl << endl;
            return *this;
        }


        bool collidesDown = false;
        bool collidesUp = false;
        bool lowestBound = false;
        bool highestBound = false;
        // not at the beggining
        if (lowerIndex != 0) {
            if (range.lowValue() <= ranges[lowerIndex - 1].highValue() + 1) {
                cout << "Intersects with lower bound" << endl;
                cout << ranges[lowerIndex - 1] << ".." << range << endl;
                collidesDown = true;
            }
        } else if (range.lowValue() <= ranges[0].lowValue()) {
            lowestBound = true;
            cout << "Range has lowest bound" << endl;
        }


        // not at the end
        if (upperIndex != size()) {
            if (ranges[upperIndex].lowValue() - 1 <= range.highValue()) {
                cout << "Intersects with upper bound" << endl;
                cout << range << ".." << ranges[upperIndex] << endl;

                collidesUp = true;
            }
        } else {
            highestBound = true;
            cout << "Range has highest bound" << endl;
        }


        //collides with lower interval and with higher interval
        if (collidesDown && collidesUp) {
            ranges[lowerIndex - 1].setHighValue(ranges[upperIndex].highValue());
            ranges.erase(ranges.begin() + lowerIndex, ranges.begin() + upperIndex + 1);
            return *this;
        }
        // intersects only with upper bound
        if (collidesUp) {
            ranges[upperIndex].setLowValue(range.lowValue());
            ranges.erase(ranges.begin() + lowerIndex, ranges.begin() + upperIndex);
            return *this;
        }
        // intersects only with lower bound
        if (collidesDown) {
            ranges[lowerIndex - 1].setHighValue(range.highValue());
            ranges.erase(ranges.begin() + lowerIndex, ranges.begin() + upperIndex);
            return *this;

        }

        //Ranges iserted has lowes bound an biggest bound at the same time
        if (lowestBound && highestBound) {
            ranges.clear();
            ranges.push_back(range);
            return *this;
        }

        //Range which is being inserted is higher than any other
        if (highestBound && !collidesDown && lowerIndex == upperIndex) {
            ranges.push_back(range);
            return *this;
        }

        // just insert at position
        if (lowerIndex == upperIndex) {
            ranges.insert(ranges.begin() + lowerIndex, range);
            return *this;
        }

        //range has just the highest bound
        if (highestBound) {
            ranges[lowerIndex].setLowValue(range.lowValue());
            ranges[lowerIndex].setHighValue(range.highValue());
            ranges.erase(ranges.begin() + lowerIndex + 1, ranges.begin() + upperIndex);
            return *this;
        }

        if (lowestBound) {
            ranges[lowerIndex].setLowValue(range.lowValue());
            ranges[lowerIndex].setHighValue(range.highValue());
            ranges.erase(ranges.begin() + lowerIndex + 1, ranges.begin() + upperIndex);
            return *this;
        }

        cout << *this << endl << endl <<
             endl;
        return *this;
    }


//Sum of two CRangeLists
    const CRangeList &operator+=(const CRangeList &list) {
        cout << "summ of two lists" << endl;
        for (const auto &item : list.ranges) {
            *this += item;
        }
        return *this;
    }

// -= range / range list
    const CRangeList &operator-=(const CRange &range) {
        cout << endl << "REMOVING__" << range << endl;
        cout << *this << endl;
        if (isEmpty()) {
            cout << "List is empty" << endl;
            return *this;
        }

        int lowerIndex = lowerBound(range.lowValue()+1); // +1 inside
        int upperIndex = upperBound(range.highValue());
        cout << "Lower Index:" << lowerIndex << endl;
        cout << "Upper Index:" << upperIndex << endl;

        /*
         * if lower bound is bigger, than it is in same interval as upper index
         * {<0..100>} + <90..95>
         * lower:1 because 90 is after 0
         * upper:0 because 95 is before 100*/
        if (lowerIndex > upperIndex || size() == 1) {
            // do nothing
            cout << "Exactly same interval" << endl << endl;
            bool deleteLeftBound = ranges[upperIndex].lowValue() >= range.lowValue();
            bool deleterighBound = ranges[upperIndex].highValue() <= range.highValue();

            // interval is exactly the same, delete whole interval
            if (deleteLeftBound && deleterighBound) {
                ranges.erase(ranges.begin() + upperIndex);
            } else if (deleteLeftBound) { // only left bound is the same
                ranges[upperIndex].setLowValue(range.highValue() + 1);
            } else if (deleterighBound) { // only right bound is the same
                ranges[upperIndex].setHighValue(range.lowValue() - 1);
            } else {
                CRange *newRange = new CRange(range.highValue() + 1, ranges[upperIndex].highValue());
                ranges[upperIndex].setHighValue(range.lowValue() - 1);
                *this += *newRange;
            }
            cout << *this << endl;
            return *this;
        }


        bool collidesDown = false;
        bool collidesUp = false;
        // not at the beggining
        if (lowerIndex != 0) {
            if (range.lowValue() <= ranges[lowerIndex - 1].highValue()) {
                cout << "Intersects with lower bound" << endl;
                cout << ranges[lowerIndex - 1] << ".." << range << endl;
                collidesDown = true;
            }
        } else if (range.lowValue() <= ranges[0].lowValue()) {
            // lowestBound = true;
            cout << "Range has lowest bound" << endl;
        }


        // not at the end
        if (upperIndex != size()) {
            if (ranges[upperIndex].lowValue() <= range.highValue()) {
                cout << "Intersects with upper bound" << endl;
                cout << range << ".." << ranges[upperIndex] << endl;

                collidesUp = true;
            }
        }/* else {
            highestBound = true;
            cout << "Range has highest bound" << endl;
        }*/


        bool deleteRightRange = false;
        bool deleteLeftRange = false;
        // intersects with upper bound
        if (collidesUp) {
            // if right bounds are the same, we can delete left interval
            deleteRightRange = ranges[upperIndex].highValue() == range.highValue();
            if (deleteRightRange) {
                cout << "Deleting whole right interval which is intersecting" << endl;
            } else {
                ranges[upperIndex].setLowValue(range.highValue() + 1);
            }
        }

        // intersects with lower bound
        if (collidesDown) {
            // if left bounds are the same, we can delete left interval
            deleteLeftRange = ranges[lowerIndex - 1].lowValue() == range.lowValue();
            if (deleteLeftRange) {
                cout << "Deleting whole left interval which is intersecting" << endl;
            } else {
                ranges[lowerIndex - 1].setHighValue(range.lowValue() - 1);
            }
        }


        if (upperIndex - lowerIndex > 0) {

            ranges.erase(ranges.begin() + lowerIndex + (deleteLeftRange ? -1 : 0),
                         ranges.begin() + upperIndex + (deleteRightRange ? +1 : 0));
        }


        cout << endl << *this << endl << endl <<
             endl;
        return *this;
    }

    void operator-=(const CRangeList &list) {
        if (list == *this) {
            clear();
            return;
        }
        for (const auto &item : list) {
            *this -= item;
        }
    }

// = range
    void operator=(const CRange &range) {
        clear();
        *this += range;
    }


// =  range list
    /*  void operator=(const CRangeList &list) {
          if (list == *this)
              return;
          clear();
          for (const auto &item : list) {
              *this += item;
          }
      }*/

// operator ==
    bool operator==(const CRangeList &list) const {
        if (list.size() != size()) {
            return false;
        }
        for (int i = 0; i < size(); ++i) {
            if (ranges[i] != list.ranges[i])
                return false;
        }
        return true;
    }

// operator !=
    bool operator!=(const CRangeList &list) const {
        return !(*this == list);
    }

// Includes long long / range
    bool Includes(long long number) const {
        return Includes(CRange(number, number));
    }

    bool Includes(const CRange &range) const {
        if (isEmpty())
            return false;
        int lowerIndex = lowerBound(range.lowValue() + 1);
        int upperIndex = upperBound(range.highValue());

        cout << "________________________" << endl;
        cout << *this << endl;
        cout << range << endl;
        cout << "L:" << lowerIndex << endl;
        cout << "R:" << upperIndex << endl;
        cout << "________________________" << endl;

        return (ranges[upperIndex].lowValue() <= range.lowValue() &&
                range.highValue() <= ranges[upperIndex].highValue());
    }

//Returns number of Ranges in CRangeList
    int size() const { return ranges.size(); }


    void operator=(const CRangeContainer *containerPtr) {
        CRangeContainer container = *containerPtr;
        vector<bool> operators = container.getOperators();
        vector <CRange> ranges = container.getRanges();
        for (unsigned i = 0; i < ranges.size(); ++i) {
            if (operators[i])
                *this += ranges[i];
            else
                *this -= ranges[i];

        }
        delete containerPtr;

    }

    bool isEmpty() const {
        return size() == 0;
    }

    void clear() {
        ranges.clear();
    }




private:

    int lowerBound(const CRange &range) const {
        auto low = std::lower_bound(ranges.begin(), ranges.end(), range.lowValue());
        cout << "Lower Index:" << low - ranges.begin() << endl;
        return low - ranges.begin();
    }

    int upperBound(const CRange &range) const {
        auto high = std::upper_bound(ranges.begin(), ranges.end(), range.highValue());
        cout << "Upper Index:" << high - ranges.begin() << endl;
        return high - ranges.begin();
    }

    int lowerBound(long long value) const {
        auto low = std::lower_bound(ranges.begin(), ranges.end(), value);
        return low - ranges.begin();
    }

    int upperBound(long long value) const {
        auto high = std::upper_bound(ranges.begin(), ranges.end(), value);
        return high - ranges.begin();
    }


};

/*BEGIN___Operators--------------------------------------------------------------------------------------------------*/

ostream &operator<<(ostream &out, const CRangeList &list) {
    out << "{";
    int index = 0;
    for (CRange const &value: list) {
        if (value.lowValue() == value.highValue())
            out << value.lowValue();
        else
            out << "<" << value.lowValue() << ".." << value.highValue() << ">";
        if (index != list.size() - 1) {
            out << ",";
        }
        index++;
    }
    out << "}";
    return out;
}


CRangeContainer *operator+(const CRange &range1, const CRange &range2) {
    CRangeContainer *container = new CRangeContainer();
    container->addRange(range1, true);
    container->addRange(range2, true);
    return container;
}

CRangeContainer *operator-(const CRange &range1, const CRange &range2) {
    CRangeContainer *container = new CRangeContainer();
    container->addRange(range1, false);
    container->addRange(range2, false);
    return container;
}


CRangeContainer *operator+(CRangeContainer *container, const CRange &range) {
    container->addRange(range, true);
    return container;
}

CRangeContainer *operator-(CRangeContainer *container, const CRange &range) {
    container->addRange(range, false);
    return container;
}


void operator+=(CRangeList &list, CRangeContainer *containerPtr) {
    CRangeContainer container = *containerPtr;
    vector<bool> operators = container.getOperators();
    vector <CRange> ranges = container.getRanges();
    for (unsigned i = 0; i < ranges.size(); ++i) {
        if (operators[i])
            list += ranges[i];
        else
            list -= ranges[i];

    }
    delete containerPtr;

}

void operator-=(CRangeList &list, CRangeContainer *containerPtr) {
    CRangeContainer container = *containerPtr;
    vector <CRange> ranges = container.getRanges();
    vector<bool> operators = container.getOperators();

    for (unsigned i = 0; i < ranges.size(); ++i) {
        if (i == 0)
            list -= ranges[i];
        else if (operators[i])
            list -= ranges[i];
        else
            list += ranges[i];

    }

}

/*END___Operators--------------------------------------------------------------------------------------------------*/

#ifndef __PROGTEST__


string toString(const CRangeList &x) {
    ostringstream oss;
    oss << x;
    return oss.str();
}


#ifdef EXTENDED_SYNTAX
void testExtra(){
     CRangeList x{{5,   20},
                  {150, 200},
                  {-9,  12},
                  {48,  93}};
      assert ( toString ( x ) == "{<-9..20>,<48..93>,<150..200>}" );
      ostringstream oss;
      oss << setfill ( '=' ) << hex << left;
      for ( const auto & v : x + CRange ( -100, -100 ) )
        oss << v << endl;
      oss << setw ( 10 ) << 1024;
      assert ( oss . str () == "-100\n<-9..20>\n<48..93>\n<150..200>\n400=======" );
}
#endif /* EXTENDED_SYNTAX */

void test2() {
    CRangeList a, b;

    std::cout.setstate(std::ios_base::failbit);
    cout.clear();
    a = CRange(0, LLONG_MAX);
    /*    a += CRange(-500, -400);
      a += CRange(-399, -200);
      a += CRange(-100, -50);*/
    //  a -= CRange(-1, LLONG_MAX);
    cout << toString(a) << endl;
    /* a += CRange(200, 300);

     a += CRange(500, 600);
     a += CRange(700, 1000);
     a += CRange(2000, 10000);

     a -= CRange(-100000, 1000000);*/
    //   a += CRange(200, 300);
    //   a += CRange(320, 400);
//    a += CRange(100, 301);
    /*a += CRange(320, 400);
    a += CRange(320, 400);*/
    //a += CRange(-500, 500);
    /*a += CRange(-1, 11);
    a += CRange(-1, 11);
    a += CRange(-1, 11);*/
    //  a += CRange(-100, 5);
    // a += CRange(5, 100);
    //  a += CRange(20000, 20000000);
    //  a = b;
    //cout << "A:" << toString(a) << endl;
}

void test1() {
    CRangeList a, b;

    cout.setstate(std::ios_base::failbit);

    assert(sizeof(CRange) <= 2 * sizeof(long long));
    a = CRange(5, 10);
    a += CRange(25, 100);
    assert(toString(a) == "{<5..10>,<25..100>}");
    a += CRange(-5, 0);
    a += CRange(8, 50);
    assert(toString(a) == "{<-5..0>,<5..100>}");
    a += CRange(101, 105) + CRange(120, 150) + CRange(160, 180) + CRange(190, 210);
    assert(toString(a) == "{<-5..0>,<5..105>,<120..150>,<160..180>,<190..210>}");
    a += CRange(106, 119) + CRange(152, 158);
    assert(toString(a) == "{<-5..0>,<5..150>,<152..158>,<160..180>,<190..210>}");
    a += CRange(-3, 170);
    a += CRange(-30, 1000);
    assert(toString(a) == "{<-30..1000>}");
    b = CRange(-500, -300) + CRange(2000, 3000) + CRange(700, 1001);
    a += b;
    cout << "A:" << toString(a) << endl;
    assert(toString(a) == "{<-500..-300>,<-30..1001>,<2000..3000>}");
    a -= CRange(-400, -400);
    assert(toString(a) == "{<-500..-401>,<-399..-300>,<-30..1001>,<2000..3000>}");
    a -= CRange(10, 20) + CRange(900, 2500) + CRange(30, 40) + CRange(10000, 20000);
    /*   cout << "________________________________________"<<endl;
       cout << "A:"<<toString(a)<<endl;
       cout << "________________________________________"<<endl;*/
    assert(toString(a) == "{<-500..-401>,<-399..-300>,<-30..9>,<21..29>,<41..899>,<2501..3000>}");
    try {
        a += CRange(15, 18) + CRange(10, 0) + CRange(35, 38);
        assert("Exception not thrown" == NULL);
    }
    catch (const InvalidRangeException &e) {
    }
    catch (...) {
        assert("Invalid exception thrown" == NULL);
    }
    assert(toString(a) == "{<-500..-401>,<-399..-300>,<-30..9>,<21..29>,<41..899>,<2501..3000>}");
    b = a;
    assert(a == b);
    assert(!(a != b));
    b += CRange(2600, 2700);
    assert(toString(b) == "{<-500..-401>,<-399..-300>,<-30..9>,<21..29>,<41..899>,<2501..3000>}");
    assert(a == b);
    assert(!(a != b));
    b += CRange(15, 15);
    assert(toString(b) == "{<-500..-401>,<-399..-300>,<-30..9>,15,<21..29>,<41..899>,<2501..3000>}");
    assert(!(a == b));
    assert(a != b);
    std::cout.clear();
    assert(b.Includes(15));
    assert(b.Includes(2900));
    assert(b.Includes(CRange(15, 15)));
    assert(b.Includes(CRange(-350, -350)));
    assert(b.Includes(CRange(100, 200)));
    assert(!b.Includes(CRange(800, 900)));
    assert(!b.Includes(CRange(-1000, -450)));
    assert(!b.Includes(CRange(0, 500)));
    a += CRange(-10000, 10000) + CRange(10000000, 1000000000);
    assert(toString(a) == "{<-10000..10000>,<10000000..1000000000>}");
    b += a;
    assert(toString(b) == "{<-10000..10000>,<10000000..1000000000>}");
    b -= a;

    cout << "B:" << toString(b) << endl;

    assert(toString(b) == "{}");
    b += CRange(0, 100) + CRange(200, 300) - CRange(150, 250) + CRange(160, 180) - CRange(170, 170);
    assert(toString(b) == "{<0..100>,<160..169>,<171..180>,<251..300>}");

    b -= CRange(10, 90) - CRange(20, 30) - CRange(40, 50) - CRange(60, 90) + CRange(70, 80);
    cout.clear();
    cout << "B:" << toString(b) << endl;
    assert(toString(b) == "{<0..9>,<20..30>,<40..50>,<60..69>,<81..100>,<160..169>,<171..180>,<251..300>}");
    cout.clear();

    cout << endl << endl << endl << "NEW TEST___________________________" << endl << endl;
    cout << "B:" << toString(b) << endl;
    CRangeList f = b;
    f += CRange(LLONG_MIN, LLONG_MAX);
    cout << "F:" << toString(f) << endl;
    cout << "B:" << toString(b) << endl;
    b += b;
    cout << "B:" << toString(b) << endl;
    /* cout << "F:" << toString(f) << endl;
     // f-=CRange(250,400);
     f -= b;
     cout << "BEFORE ASSIGN" << endl << " F:" << toString(f) << endl;
     cout << "F:" << toString(f) << endl;
     cout << "B:" << toString(f) << endl;
     f += b;
     cout << "F:" << toString(f) << endl;*/

}


void test3() {
    cout.setstate(std::ios_base::failbit);

    CRangeList a, b;
    a -= CRange(5, 10);
    assert(!a.Includes(5));
    cout << "TEST 2.1 OK" << endl << endl << endl;
    assert(!a.Includes(CRange(5, 10)));
    cout << "TEST 2.2 OK" << endl << endl << endl;
    a += b;
    a = CRange(LLONG_MIN, LLONG_MAX);
    assert(a.Includes(5));
    assert(a.Includes(LLONG_MIN));
    assert(a.Includes(LLONG_MAX));
    cout << "TEST 2.5 OK" << endl << endl << endl;
    cout << a << endl;
    a -= CRange(LLONG_MIN, 0);
    cout << a << endl;
    assert(a.Includes(5));
    assert(!a.Includes(-5));
    cout << "TEST 2.7 OK" << endl << endl << endl;
    a = CRange(LLONG_MIN, LLONG_MAX);
    a += CRange(LLONG_MIN, LLONG_MAX);
    assert(a.Includes(LLONG_MIN));
    assert(a.Includes(LLONG_MAX));
    cout << "TEST 2.9 OK" << endl << endl << endl;
    cout << "odebrání LLONG_MIN" << endl;
    cout << a << endl;
    a -= CRange(LLONG_MIN, LLONG_MIN);
    cout << a << endl;
    assert(!a.Includes(LLONG_MIN));
    cout << "TEST 2.10 OK" << endl << endl << endl;
    a += CRange(LLONG_MIN, LLONG_MIN);
    cout << a << endl;
    assert(a.Includes(LLONG_MIN));
    cout.clear();
    cout << "TEST 2.11 OK" << endl << endl << endl;
    a -= CRange(LLONG_MAX, LLONG_MAX);
    cout << "A:" << a << endl;
    assert(!a.Includes(LLONG_MAX));
    cout << "TEST 2.12 OK" << endl << endl << endl;
    a += CRange(LLONG_MAX, LLONG_MAX);
    cout << a << endl;
    assert(a.Includes(LLONG_MAX));
    cout << "TEST 2.13 OK" << endl << endl << endl;
}

void test4() {
    CRangeList a = CRange(10, 20) + CRange(0, 9) + CRange(21, 30);
    cout << "a: " << a << endl;
    assert(toString(a) == "{<0..30>}");
    cout << "TEST 4.1 OK" << endl << endl << endl;
    a = CRange(10, 20) + CRange(0, 8) + CRange(22, 30);
    cout << "a: " << a << endl;
    assert(toString(a) == "{<0..8>,<10..20>,<22..30>}");
    cout << "TEST 4.2 OK" << endl << endl << endl;
    a = CRange(10, 20) + CRange(22, 30) + CRange(0, 50);
    cout << "a: " << a << endl;
    assert(toString(a) == "{<0..50>}");
    cout << "TEST 4.3 OK" << endl << endl << endl;
    a -= CRange(-5, 5) + CRange(25, 35) + CRange(45, 55);
    cout << "a: " << a << endl;
    assert(toString(a) == "{<6..24>,<36..44>}");
    cout << "TEST 4.4 OK" << endl << endl << endl;
    CRangeList b;
    b = CRange(0, 0);
    b -= CRange(0, 0);
    cout << "b: " << b << endl;
    assert(toString(b) == "{}");
    cout << "TEST 4.5 OK" << endl << endl << endl;
    b -= CRange(0, 10);
    cout << "b: " << b << endl;
    assert(toString(b) == "{}");
    cout << "TEST 4.6 OK" << endl << endl << endl;
    b += CRange(25, 100);
    cout << "b: " << b << endl;
    assert(toString(b) == "{<25..100>}");
    cout << "TEST 4.7 OK" << endl << endl << endl;
    b -= CRange(25, 25);
    cout << "b: " << b << endl;
    assert(toString(b) == "{<26..100>}");
    cout << "TEST 4.7 OK" << endl << endl << endl;
    b += CRange(1000, 1200);
    b -= CRange(1000, 1000);
    cout << "b: " << b << endl;
    assert(toString(b) == "{<26..100>,<1001..1200>}");
    cout << "TEST 4.8 OK" << endl << endl << endl;
    b -= CRange(1200, 1200);
    cout << "b: " << b << endl;
    assert(toString(b) == "{<26..100>,<1001..1199>}");
    cout << "TEST 4.9 OK" << endl << endl << endl;
    b += CRange(30, 1100);
    cout << "b: " << b << endl;
    assert(toString(b) == "{<26..1199>}");
    cout << "TEST 4.10 OK" << endl << endl << endl;
    b -= CRange(30, 1100);
    cout << "b: " << b << endl;
    assert(toString(b) == "{<26..29>,<1101..1199>}");
    cout << "TEST 4.11 OK" << endl << endl << endl;
}

int main(void) {
    cout.setstate(std::ios_base::failbit);
    test1();
    test2();
    test3();
    test4();

    cout.clear();

    return 0;
}

#endif /* __PROGTEST__ */

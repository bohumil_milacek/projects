
// uncomment if your code implements initializer lists
// #define EXTENDED_SYNTAX

class CRange {
    long long lo;
    long long hi;

public:
    //Constructor - If user creates Range with lover bound(lo) bigger than upper bound(hi), the constructor throws exeption
    CRange(long long lo, long long hi) : lo(lo), hi(hi) {
        if (lo > hi)
            throw InvalidRangeException();
    }

    //Getters and Setters
    long long lowValue() const { return lo; }

    long long highValue() const { return hi; }

    void setLowValue(long long lo) { this->lo = lo; }

    void setHighValue(long long hi) { this->hi = hi; }

    //compares two ranges based on their bounds
    bool operator==(const CRange &r) const { return lo == r.lo && hi == r.hi; }

    bool operator!=(const CRange &r) const { return !(*this == r); }

    //Prints out range in format <low..high>
    friend ostream &operator<<(ostream &out, const CRange &range);

    friend bool operator<(const CRange &range, const long long val);

    friend bool operator<(const long long val, const CRange &range);

private:
    // todo
};

//Prints out range in format <low..high>
ostream &operator<<(ostream &out, const CRange &range) {
    out << "<" << range.lo << ".." << range.hi << ">";
    return out;
}

// function for lower_bound from STL
bool operator<(const CRange &range, const long long val) {
    return range.lowValue() < val;
}

// function for upper_bound from STL
bool operator<(const long long val, const CRange &range) {
    return val <= range.highValue();
}


/*
 * Holds CRanges in 2D array
 * In first row are + CRanges
 * In second row are - CRanges
 * true - for +
 * false - for .
 * */

class CRangeList {
    // operator <<
    friend ostream &operator<<(ostream &out, const CRangeList &list);

    vector <CRange> ranges;

public:
    // constructor
    CRangeList() {}

    CRangeList(CRange range) {
        *this = range;
    }


    //iterator
    vector<CRange>::iterator begin() {
        return ranges.begin();
    }

    vector<CRange>::iterator end() {
        return ranges.end();
    }

    vector<CRange>::const_iterator begin() const {
        return ranges.begin();
    }

    vector<CRange>::const_iterator end() const {
        return ranges.end();
    }


    //Constructor for bonus
    // CRangeList(initializer_list <CRange> ranges) {}

    // += range / range list
    const CRangeList &operator+=(const CRange &range) {
        //cout  cout << "INSERTING__" << range << endl;
        if (isEmpty()) {
          //cout  cout << "List is empty" << endl;
            ranges.push_back(range);
            //cout cout << endl;
            return *this;
        }

        int lowerIndex = lowerBound(range.lowValue() + 1);
        int upperIndex = upperBound(range.highValue());

        //cout  cout << "Lower Index:" << lowerIndex << endl;
        //cout  cout << "Upper Index:" << upperIndex << endl;

        /*
         * if lower bound is bigger, than it is in same interval as upper index
         * {<0..100>} + <90..95>
         * lower:1 because 90 is after 0
         * upper:0 because 95 is before 100*/
        if (lowerIndex > upperIndex) {
            // do nothing
            //cout    cout << "Exactly same interval" << endl << endl;
            //cout  cout << *this << endl;
            return *this;
        }


        bool collidesDown = false;
        bool collidesUp = false;
        bool lowestBound = false;
        bool highestBound = false;
        // not at the beggining
        if (lowerIndex != 0) {
            if (range.lowValue() <= ranges[lowerIndex - 1].highValue() + 1) {
                //cout   cout << "Intersects with lower bound" << endl;
                //cout cout << ranges[lowerIndex - 1] << ".." << range << endl;
                collidesDown = true;
            }
        } else if (range.lowValue() <= ranges[0].lowValue()) {
            lowestBound = true;
            //cout  cout << "Range has lowest bound" << endl;
        }


        // not at the end
        if (upperIndex != size()) {
            if (ranges[upperIndex].lowValue() - 1 <= range.highValue()) {
                //cout   cout << "Intersects with upper bound" << endl;
                //cout   cout << range << ".." << ranges[upperIndex] << endl;

                collidesUp = true;
            }
        } else {
            highestBound = true;
            //cout    cout << "Range has highest bound" << endl;
        }


        //collides with lower interval and with higher interval
        if (collidesDown && collidesUp) {
            ranges[lowerIndex - 1].setHighValue(ranges[upperIndex].highValue());
            ranges.erase(ranges.begin() + lowerIndex, ranges.begin() + upperIndex + 1);
            //cout  cout << *this << endl;
            return *this;
        }
        // intersects only with upper bound
        if (collidesUp) {
            ranges[upperIndex].setLowValue(range.lowValue());
            ranges.erase(ranges.begin() + lowerIndex, ranges.begin() + upperIndex);
            //cout    cout << *this << endl;
            return *this;
        }
        // intersects only with lower bound
        if (collidesDown) {
            ranges[lowerIndex - 1].setHighValue(range.highValue());
            ranges.erase(ranges.begin() + lowerIndex, ranges.begin() + upperIndex);
            //cout  cout << *this << endl;
            return *this;

        }

        //Ranges iserted has lowes bound an biggest bound at the same time
        if (lowestBound && highestBound) {
            ranges.clear();
            ranges.push_back(range);
            //cout  cout << *this << endl;
            return *this;
        }

        //Range which is being inserted is higher than any other
        if (highestBound && !collidesDown && lowerIndex == upperIndex) {
            ranges.push_back(range);
            //cout    cout << *this << endl;
            return *this;
        }

        // just insert at position
        if (lowerIndex == upperIndex) {
            ranges.insert(ranges.begin() + lowerIndex, range);
            //cout  cout << *this << endl;
            return *this;
        }

        //range has just the highest bound
        if (highestBound) {
            ranges[lowerIndex].setLowValue(range.lowValue());
            ranges[lowerIndex].setHighValue(range.highValue());
            ranges.erase(ranges.begin() + lowerIndex + 1, ranges.begin() + upperIndex);
            //cout   cout << *this << endl;
            return *this;
        }

        if (lowestBound) {
            ranges[lowerIndex].setLowValue(range.lowValue());
            ranges[lowerIndex].setHighValue(range.highValue());
            ranges.erase(ranges.begin() + lowerIndex + 1, ranges.begin() + upperIndex);
            //cout   cout << *this << endl;
            return *this;
        }

        //cout  cout << *this << endl << endl << endl;
        return *this;
    }


//Sum of two CRangeLists
    const CRangeList &operator+=(const CRangeList &list) {
        //cout   cout << "summ of two lists" << endl;
        for (const auto &item : list.ranges) {
            *this += item;
        }
        return *this;
    }

// -= range / range list
    const CRangeList &operator-=(const CRange &range) {
        //cout  cout << endl << "REMOVING__" << range << endl;
        //cout     cout << *this << endl;
        if (isEmpty()) {
            //cout   cout << "List is empty" << endl;
            return *this;
        }

        int lowerIndex = upperBound(range.lowValue());
        int upperIndex = upperBound(range.highValue());
        //cout   cout << "Lower Index:" << lowerIndex << endl;
        //cout     cout << "Upper Index:" << upperIndex << endl;


        bool deleteLowerRange = false;
        bool deleteUpperRange = false;
        bool lowerRangeIntersects = false;
        bool upperRangeIntersects = false;

        int lowerIndexToDelete = 0;
        int upperIndexToDelete = size();


        if (lowerIndex != size()) {
            if (range.lowValue() <= ranges[lowerIndex].lowValue()) {
                //cout      cout << "Delete lower range" << endl;
                lowerIndexToDelete = lowerIndex;
                deleteLowerRange = true;
                if (lowerIndexToDelete == 0 && range.highValue() < ranges[lowerIndex].lowValue()) {
                    return *this;
                }
            } else {
                //cout   cout << "Lower range intersects" << endl;
                lowerIndexToDelete = lowerIndex + 1;
                lowerRangeIntersects = true;
            }
        }

        if (upperIndex != size() && ranges[upperIndex].lowValue() <= range.highValue()) {
            upperRangeIntersects = true;
            upperIndexToDelete = upperIndex;
            if (ranges[upperIndex].highValue() == range.highValue()) {
                upperRangeIntersects = false;
                deleteUpperRange = true;
                //cout    cout << "Delete upper range ==" << endl;
                upperIndexToDelete = upperIndex + 1;

            } else{
            //cout      cout << "Upper range intersects" << endl;
            }
        } else {
            deleteUpperRange = true;
            //cout   cout << "Delete upper range <" << endl;
            upperIndexToDelete = upperIndex;
        }

        if (lowerIndex == upperIndex) {
            if (lowerIndex != 0 && upperIndex != size() && range.lowValue() > ranges[lowerIndex - 1].highValue() &&
                range.highValue() < ranges[lowerIndex].lowValue()) {
                //cout       cout << "TIIIIIIIIIIIIIIIIIIIIIIIIME TO QUIIIIIIIIIIIIIIIIIIIT" << endl;
                return *this;
            }
            if ((lowerRangeIntersects && upperRangeIntersects)) {
                //cout    cout << "Same interval" << endl;
                CRange newRange(range.highValue() + 1, ranges[lowerIndex].highValue());
                ranges[lowerIndex].setHighValue(range.lowValue() - 1);
                //cout      cout << "Size" << size() << endl;
                *this += newRange;
                //cout      cout << "Size" << size() << endl;
                return *this;
            }
            if (deleteLowerRange && deleteUpperRange) {
                ranges.erase(ranges.begin() + lowerIndex);
            }

        }

        if (lowerRangeIntersects)
            ranges[lowerIndex].setHighValue(range.lowValue() - 1);
        if (upperRangeIntersects)
            ranges[upperIndex].setLowValue(range.highValue() + 1);


        if (upperIndex - lowerIndex > 0) {
            ranges.erase(ranges.begin() + lowerIndexToDelete,
                         ranges.begin() + upperIndexToDelete);
        }
        return *this;
    }

    void operator-=(const CRangeList &list) {
        if (list == *this) {
            clear();
            return;
        }
        for (const auto &item : list) {
            *this -= item;
        }
    }

// = range
    void operator=(const CRange &range) {
        clear();
        *this += range;
    }


// =  range list
    /*  void operator=(const CRangeList &list) {
          if (list == *this)
              return;
          clear();
          for (const auto &item : list) {
              *this += item;
          }
      }*/

// operator ==
    bool operator==(const CRangeList &list) const {
        if (this == &list)
            return true;
        if (list.size() != size())
            return false;

        for (int i = 0; i < size(); ++i)
            if (ranges[i] != list.ranges[i])
                return false;

        return true;
    }

// operator !=
    bool operator!=(const CRangeList &list) const {
        return !(*this == list);
    }

// Includes long long / range
    bool Includes(long long number) const{
        return Includes(CRange(number, number));
    }

    bool Includes(const CRange &range) const{
        int lowerIndex = lowerBound(range.lowValue() + 1);
        int upperIndex = upperBound(range.highValue());
        //cout    cout << "lower" << lowerIndex << endl;
        //cout    cout << "upper" << upperIndex << endl;

        if (isEmpty())
            return false;
        else if (lowerIndex > upperIndex)
            return true;
        else if (lowerIndex == upperIndex && ranges[lowerIndex].lowValue() <= range.lowValue() &&
                 range.highValue() <= ranges[lowerIndex].highValue())
            return true;
        return false;
    }


    CRangeList &operator+(const CRange &range) {
        *this += range;
        return *this;
    }

    CRangeList &operator-(const CRange &range) {
        *this -= range;
        return *this;
    }

//Returns number of Ranges in CRangeList
    int size() const { return ranges.size(); }


    bool isEmpty() const {
        return size() == 0;
    }

    void clear() {
        ranges.clear();
    }


private:

    int lowerBound(const CRange &range) const {
        auto low = std::lower_bound(ranges.begin(), ranges.end(), range.lowValue());
        //cout cout << "Lower Index:" << low - ranges.begin() << endl;
        return low - ranges.begin();
    }

    int upperBound(const CRange &range) const {
        auto high = std::upper_bound(ranges.begin(), ranges.end(), range.highValue());
        //cout    cout << "Upper Index:" << high - ranges.begin() << endl;
        return high - ranges.begin();
    }

    int lowerBound(long long value) const {
        auto low = std::lower_bound(ranges.begin(), ranges.end(), value);
        return low - ranges.begin();
    }

    int upperBound(long long value) const {
        auto high = std::upper_bound(ranges.begin(), ranges.end(), value);
        return high - ranges.begin();
    }


};

/*BEGIN___Operators--------------------------------------------------------------------------------------------------*/

ostream &operator<<(ostream &out, const CRangeList &list) {
    out << "{";
    int index = 0;
    for (CRange const &value: list) {
        if (value.lowValue() == value.highValue())
            out << value.lowValue();
        else
            out << "<" << value.lowValue() << ".." << value.highValue() << ">";
        if (index != list.size() - 1) {
            out << ",";
        }
        index++;
    }
    out << "}";
    return out;
}

CRangeList operator+(const CRange &range1, const CRange &range2) {
    CRangeList list = range1;
    list += range2;
    return list;
}

CRangeList operator-(const CRange &range1, const CRange &range2) {
    CRangeList list = range1;
    list -= range2;
    return list;
}




/*END___Operators--------------------------------------------------------------------------------------------------*/

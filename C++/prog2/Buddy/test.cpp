#ifndef __PROGTEST__
#include <cassert>
#include <cmath>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <cstring>

#include <iostream>
#include <vector>
using namespace std;
#endif /* __PROGTEST__ */

#define MAX_BLOCK 150
#define CONTROL 0xCBD5438E

struct Segment;
struct List;

short SegmentLevel(int size);
void *DataPointer(Segment *segment);
Segment *MergeBuddy(Segment *ptr);
void HeapInit(void *memPool, int memSize);
bool HeapFree(void *blk);
void HeapDone(int *pendingBlk);
inline int IndexInLevel(Segment *ptr);
inline Segment *GetBuddy(Segment *ptr);

struct Segment {
  bool free;
  unsigned control;
  Segment *prev;
  Segment *next;
  unsigned char level;

  int Size() const { return 1 << level; }
};

struct List {
  Segment *first;
  Segment *last;
  short level;

  List() {}
  List(short level) : first(nullptr), last(nullptr), level(level) {}

  void Push(Segment *seg) {
    seg->free = true;
    seg->control = CONTROL;
    seg->level = level;

    if (!first) {
      first = seg;
      last = seg;
      seg->prev = nullptr;
      seg->next = nullptr;
    } else {
      seg->prev = last;
      last->next = seg;
      last = seg;
    }
  }

  Segment *Pop() {
    if (!first)
      return nullptr;

    Segment *toReturn = first;
    first = first->next;
    if (toReturn->next)
      toReturn->next->prev = nullptr;
    if (toReturn == last) {
      first = nullptr;
      last = nullptr;
    }
    toReturn->free = false;
    return toReturn;
  }

  void Remove(Segment *seg) {

    if (seg == first && seg == last) {
      first = nullptr;
      last = nullptr;
    } else if (seg == first) {
      if (first->next)
        first->next->prev = nullptr;
      first = first->next;
    } else if (seg == last) {
      if (last->prev)
        last->prev->next = nullptr;
      last = last->prev;
    } else {
      seg->next->prev = seg->prev;
      seg->prev->next = seg->next;
    }
    seg->prev = nullptr;
    seg->next = nullptr;
    seg->free = false;
  }

  bool Empty() { return !first; }
};

static void *memoryStart;
static void *memoryEnd;

static int memorySize;
static int allocated;
List freeSegments[MAX_BLOCK];

short SegmentLevel(int size) { return ceil(log2(size + sizeof(Segment))); }
void *DataPointer(Segment *segment) { return (void *)(segment + 1); }
bool InMemoryRange(Segment *ptr) {
  return ptr >= memoryStart && ptr < memoryEnd;
}

inline int IndexInLevel(Segment *ptr) {
  int size = ptr->Size();
  return (((char *)ptr - (char *)memoryStart) / size) % 2;
}

inline Segment *GetBuddy(Segment *ptr) {
  int indexInLevel = IndexInLevel(ptr);
  Segment *buddy = nullptr;
  if (indexInLevel == 1)
    buddy = (Segment *)((char *)ptr - ptr->Size());
  else
    buddy = (Segment *)((char *)ptr + ptr->Size());
  return buddy;
}

void HeapInit(void *memPool, int memSize) {
  memoryStart = memPool;
  memoryEnd = (void *)(((char *)memoryStart) + memSize);
  memorySize = memSize;
  allocated = 0;
  int level = log2(memSize);
  for (short i = 0; i < MAX_BLOCK; ++i)
    freeSegments[i] = List(i);
  Segment *newSeg = (Segment *)memPool;
  freeSegments[level].Push(newSeg);
}

void *HeapAlloc(int size) { /* todo */
  if (size <= 0)
    return nullptr;
  short level = SegmentLevel(size);
  if (!freeSegments[level].Empty()) {
    ++allocated;
    return DataPointer(freeSegments[level].Pop());
  }

  short freeLevel = level + 1;
  while (freeSegments[freeLevel].Empty())
    if (++freeLevel == MAX_BLOCK)
      return nullptr;

  while (freeLevel != level) {
    Segment *seg1 = freeSegments[freeLevel].Pop();
    int segSize = 1 << seg1->level;
    Segment *seg2 = (Segment *)(((char *)seg1) + segSize / 2);
    --freeLevel;

    freeSegments[freeLevel].Push(seg2);
    freeSegments[freeLevel].Push(seg1);
  }
  ++allocated;
  return DataPointer(freeSegments[level].Pop());
}

bool HeapFree(void *blk) {
  Segment *toFree = (Segment *)blk;
  --toFree;
  if (!InMemoryRange(toFree) || toFree->control != CONTROL)
    return false;
  freeSegments[toFree->level].Push(toFree);
  --allocated;
  while (toFree)
    toFree = MergeBuddy(toFree);
  return true;
}

// tries to merge with next block
Segment *MergeBuddy(Segment *ptr) {
  Segment *buddy = GetBuddy(ptr);

  if (!buddy->free || buddy->level != ptr->level)
    return nullptr;

  if (ptr > buddy) {
    Segment *tmp = ptr;
    ptr = buddy;
    buddy = tmp;
  }
  buddy->control = 0;
  freeSegments[ptr->level].Remove(ptr);
  freeSegments[ptr->level].Remove(buddy);
  freeSegments[ptr->level + 1].Push(ptr);
  return ptr;
}

void HeapDone(int *pendingBlk) { *pendingBlk = allocated; }

void PrintList() {
  cout << "FREE SEGMENTS" << endl;
  for (int i = 0; i < MAX_BLOCK; ++i) {
    if (freeSegments[i].Empty())
      continue;
    for (Segment *tmp = freeSegments[i].first; tmp; tmp = tmp->next)
      cout << "[" << i << "] " << tmp->Size() << endl;
  }
}

#ifndef __PROGTEST__
int main(void) {


  uint8_t *p0, *p1, *p2, *p3, *p4;
  int pendingBlk;
  static uint8_t memPool[3 * 1048576];

  HeapInit(memPool, 2097152);
  assert((p0 = (uint8_t *)HeapAlloc(512000)) != NULL);
  memset(p0, 0, 512000);
  assert((p1 = (uint8_t *)HeapAlloc(511000)) != NULL);
  memset(p1, 0, 511000);
  assert((p2 = (uint8_t *)HeapAlloc(26000)) != NULL);
  memset(p2, 0, 26000);
  HeapDone(&pendingBlk);
  assert(pendingBlk == 3);

  HeapInit(memPool, 2097152);
  assert((p0 = (uint8_t *)HeapAlloc(1000000)) != NULL);
  memset(p0, 0, 1000000);
  assert((p1 = (uint8_t *)HeapAlloc(250000)) != NULL);
  memset(p1, 0, 250000);
  assert((p2 = (uint8_t *)HeapAlloc(250000)) != NULL);
  memset(p2, 0, 250000);
  assert((p3 = (uint8_t *)HeapAlloc(250000)) != NULL);
  memset(p3, 0, 250000);
  assert((p4 = (uint8_t *)HeapAlloc(50000)) != NULL);
  memset(p4, 0, 50000);
  assert(HeapFree(p2));
  assert(HeapFree(p4));
  assert(HeapFree(p3));
  assert(HeapFree(p1));
  assert((p1 = (uint8_t *)HeapAlloc(500000)) != NULL);
  memset(p1, 0, 500000);
  assert(HeapFree(p0));
  assert(HeapFree(p1));
  HeapDone(&pendingBlk);
  assert(pendingBlk == 0);

  HeapInit(memPool, 2359296);
  assert((p0 = (uint8_t *)HeapAlloc(1000000)) != NULL);
  memset(p0, 0, 1000000);
  assert((p1 = (uint8_t *)HeapAlloc(500000)) != NULL);
  memset(p1, 0, 500000);
  assert((p2 = (uint8_t *)HeapAlloc(500000)) != NULL);
  memset(p2, 0, 500000);
  assert((p3 = (uint8_t *)HeapAlloc(500000)) == NULL);
  assert(HeapFree(p2));
  assert((p2 = (uint8_t *)HeapAlloc(300000)) != NULL);
  memset(p2, 0, 300000);
  assert(HeapFree(p0));
  assert(HeapFree(p1));
  HeapDone(&pendingBlk);
  assert(pendingBlk == 1);

  HeapInit(memPool, 2359296);
  assert((p0 = (uint8_t *)HeapAlloc(1000000)) != NULL);
  memset(p0, 0, 1000000);
  assert(!HeapFree(p0 + 1000));
  HeapDone(&pendingBlk);
  assert(pendingBlk == 1);

  vector<uint8_t *> ptrs;

  cout << "Start:" << (void *)memPool << endl;
  cout << "END:" << (void *)(memPool + 2359296) << endl;
  while (true) {
    int choice = rand() % 5;
    switch (choice) {
    case 0: {
      //    cout << "GOOD ALLOC" << endl;
      int size = rand() % 2359296;
      ptrs.push_back((uint8_t *)HeapAlloc(size));
      break;
    }
    case 1: {
      //      cout << "BAD ALLOC" << endl;
      int size2 = (rand() % 4359296) - 1359296;
      HeapAlloc(size2);
      break;
    }
    case 2: {
      if (ptrs.empty())
        continue;
      //   cout << "GOOD FREE" << endl;
      int choice = rand() % ptrs.size();
      HeapFree(ptrs[choice]);
      ptrs.erase(ptrs.begin() + choice);
      break;
    }
    case 3: {
      //  cout << "BAD FREE" << endl;
      int choice1 = (rand() % 4359296) - 1359296;
      HeapFree(memPool - choice1);
      break;
    }
    }
  }

  return 0;
}
#endif /* __PROGTEST__ */

#ifndef __PROGTEST__
#include <cassert>
#include <cmath>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <cstring>
using namespace std;
#endif            /* __PROGTEST__ */
#include <chrono> // std::chrono::seconds
#include <iostream>
#include <thread> // std::this_thread::sleep_for

// algorithm will use Best Fit strategy

struct MemoryBlock {
  int size;
  bool free;
  MemoryBlock *next;
};

MemoryBlock *memory = nullptr;
int memorySize = 0;

// Make one block of memory from memPool
void HeapInit(void *memPool, int memSize) {
  cout << "[Init]_______________________________" << endl;
  memory = (MemoryBlock *)memPool;
  memorySize = memSize;
  memory->free = true;
  memory->size = memSize - sizeof(MemoryBlock);
  memory->next = nullptr;
  cout << "[Init] Addr: " << memory << endl;
  cout << "[Init] Free: " << memory->free << endl;
  cout << "[Init] Size: " << memory->size << endl;
  cout << "[Init] Next: " << memory->next << endl << endl;
}
void *HeapAlloc(int size) {
  cout << "[Alloc]_______________________________" << endl;
  MemoryBlock *actualBlock = memory;
  MemoryBlock * prevBlock = nullptr;
  MemoryBlock * bestBlock = nullptr;
  MemoryBlock * prevBestBlock = nullptr;

  int bestSize = memorySize;

  int requiredSize = size + sizeof(MemoryBlock);
  cout << "[Alloc] Mem: " << memory << endl;
  cout << "[Alloc] Req: " << requiredSize << endl;

  while (actualBlock) {
    // std::this_thread::sleep_for(std::chrono::seconds(5));
    if (!actualBlock->free) {
      actualBlock = actualBlock->next;
      continue;
    }
    if (actualBlock->size == size) {
      actualBlock->free = false;
      cout << "[Alloc] Fit: " << (void *)(actualBlock + 1) << endl;
      return (void *)(actualBlock + 1);
    } else if (requiredSize < actualBlock->size && actualBlock->size < bestSize) {
      prevBestBlock = prevBlock;
      bestBlock = actualBlock;
      bestSize = actualBlock->size;
    } 
    prevBlock = actualBlock;
    actualBlock = actualBlock->next;
  }
  if(bestBlock!=nullptr){
      MemoryBlock *prevBlock = bestBlock;
      actualBlock = (MemoryBlock *)((char *)prevBlock + requiredSize); // casted to char to get the pointer as 1byte pointer -> moving addres in bytes

      actualBlock->next = prevBlock->next;
      actualBlock->free = true;
      actualBlock->size = prevBlock->size - requiredSize;
      prevBlock->size = size;
      prevBlock->free = false;
      prevBlock->next = actualBlock;
      cout << "[Alloc] Best: " << (void *)(prevBlock + 1) << endl;
      return (void *)(prevBlock + 1);
  }

  cout << "[Alloc] Fail" << endl;
  return nullptr;
}

// tries to merge with next block
void MergeNextBlock(MemoryBlock *ptr) {
  if (ptr->next == nullptr || !ptr->next->free)
    return;
  ptr->size = ptr->size + ptr->next->size + sizeof(MemoryBlock);
  ptr->next = ptr->next->next;
}
// tries to merge with prev block
void MergePrevBlock(MemoryBlock *prevPtr, MemoryBlock *ptr) {
  if (!prevPtr->free)
    return;
  prevPtr->size = prevPtr->size + ptr->size + sizeof(MemoryBlock);
  prevPtr->next = ptr->next;
}

bool HeapFree(void *blk) {
  cout << "[Free]_______________________________" << endl;
  if (blk < (void *)memory || (void *)((char *)memory + memorySize) <= blk) {
    cout << "[Free] Invalid pointer " << endl;
    return false;
  }
  MemoryBlock *actualBlock = memory;
  MemoryBlock *prevBlock = nullptr;

  // actualBlock; // move pointer from data to "metadata" -> MemoryBlock
  cout << "[Free] To Free: " << blk << endl;

  while (actualBlock) {
    if ((void *)(actualBlock + 1) == blk) {
      if (actualBlock->free)
        return false;
      if (prevBlock == nullptr) { // freeing first block of memory
        actualBlock->free = true;
        MergeNextBlock(actualBlock);
        return true;
      } else {
        actualBlock->free = true;
        MergeNextBlock(actualBlock);
        MergePrevBlock(prevBlock, actualBlock);
        return true;
      }
    }
    actualBlock = actualBlock->next;
  }
  return false;
}

void HeapDone(int *pendingBlk) {
  cout << "[Done]_______________________________" << endl;
  MemoryBlock *actualBlock = memory;
  int numberOfAllocated = 0;
  while (actualBlock) {
    if (!actualBlock->free)
      ++numberOfAllocated;
    actualBlock = actualBlock->next;
  }
  *pendingBlk = numberOfAllocated;
}

#ifndef __PROGTEST__
int main(void) {
  uint8_t *p0, *p1, *p2, *p3, *p4;
  int pendingBlk;
  static uint8_t memPool[3 * 1048576];

  HeapInit(memPool, 2097152);
  assert((p0 = (uint8_t *)HeapAlloc(512000)) != NULL);
  memset(p0, 0, 512000);
  assert((p1 = (uint8_t *)HeapAlloc(511000)) != NULL);
  memset(p1, 0, 511000);
  assert((p2 = (uint8_t *)HeapAlloc(26000)) != NULL);
  memset(p2, 0, 26000);
  HeapDone(&pendingBlk);
  assert(pendingBlk == 3);

  HeapInit(memPool, 2097152);
  assert((p0 = (uint8_t *)HeapAlloc(1000000)) != NULL);
  cout << "p0: " << (void *)p0 << endl;
  memset(p0, 0, 1000000);
  assert((p1 = (uint8_t *)HeapAlloc(250000)) != NULL);
  cout << "p1: " << (void *)p1 << endl;
  memset(p1, 0, 250000);
  assert((p2 = (uint8_t *)HeapAlloc(250000)) != NULL);
  cout << "p2: " << (void *)p2 << endl;
  memset(p2, 0, 250000);
  assert((p3 = (uint8_t *)HeapAlloc(250000)) != NULL);
  cout << "p3: " << (void *)p3 << endl;
  memset(p3, 0, 250000);
  assert((p4 = (uint8_t *)HeapAlloc(50000)) != NULL);
  cout << "p4: " << (void *)p4 << endl << endl;
  memset(p4, 0, 50000);

  cout << "p0: " << (void *)p0 << endl;
  cout << "p1: " << (void *)p1 << endl;
  cout << "p2: " << (void *)p2 << endl;
  cout << "p3: " << (void *)p3 << endl;
  cout << "p4: " << (void *)p4 << endl;

  assert(HeapFree(p2));
  assert(HeapFree(p4));
  assert(HeapFree(p3));
  assert(HeapFree(p1));
  assert((p1 = (uint8_t *)HeapAlloc(500000)) != NULL);
  memset(p1, 0, 500000);
  assert(HeapFree(p0));
  assert(HeapFree(p1));
  HeapDone(&pendingBlk);
  assert(pendingBlk == 0);

  HeapInit(memPool, 2359296);
  assert((p0 = (uint8_t *)HeapAlloc(1000000)) != NULL);
  memset(p0, 0, 1000000);
  assert((p1 = (uint8_t *)HeapAlloc(500000)) != NULL);
  memset(p1, 0, 500000);
  assert((p2 = (uint8_t *)HeapAlloc(500000)) != NULL);
  memset(p2, 0, 500000);
  assert((p3 = (uint8_t *)HeapAlloc(500000)) == NULL);
  assert(HeapFree(p2));
  assert((p2 = (uint8_t *)HeapAlloc(300000)) != NULL);
  memset(p2, 0, 300000);
  assert(HeapFree(p0));
  assert(HeapFree(p1));
  HeapDone(&pendingBlk);
  assert(pendingBlk == 1);

  HeapInit(memPool, 2359296);
  assert((p0 = (uint8_t *)HeapAlloc(1000000)) != NULL);
  memset(p0, 0, 1000000);
  assert(!HeapFree(p0 + 1000));
  HeapDone(&pendingBlk);
  assert(pendingBlk == 1);

  return 0;
}
#endif /* __PROGTEST__ */

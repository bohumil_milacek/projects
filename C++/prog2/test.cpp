#ifndef __PROGTEST__
#include <cassert>
#include <cmath>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <cstring>

#include <iostream>
#include <vector>
using namespace std;
#endif /* __PROGTEST__ */
#define MAX_LEVELS 100
#define CONTROL 0xC8A09735

struct Segment;
struct List;

Segment *MergeBuddy(Segment *ptr);
void HeapInit(void *memPool, int memSize);
bool HeapFree(void *blk);
void HeapDone(int *pendingBlk);
inline int IndexInLevel(Segment *ptr);
inline Segment *GetBuddy(Segment *ptr);
inline short SegmentLevel(int size);
inline void *DataPointer(Segment *segment);

struct Segment {
  Segment *next;
  bool free;
  unsigned char level;
  unsigned control;

  int Size() const { return 1 << level; }
};

struct List {
  Segment *head;

  void Add(Segment *seg) {
    if (!seg)
      return;
    seg->next = head;
    head = seg;
  }

  void Remove(Segment *seg) {
    if (!seg)
      return;

    Segment *tmp = head;

    if (seg == tmp) {
      head = seg->next;
      return;
    }

    while (tmp) {
      if (tmp->next == seg)
        tmp->next = seg->next;
      tmp = tmp->next;
    }
  }

  Segment *Pop() {
    Segment *toReturn = head;
    this->Remove(toReturn);
    return toReturn;
  }

  void Clear() { head = nullptr; }

  bool Empty() const { return head == nullptr; }
};

int memorySize;
void *memoryStart;

int allocated;
List freeSegments[MAX_LEVELS];

void HeapInit(void *memPool, int memSize) {
  memoryStart = memPool;
  memorySize = memSize;
  allocated = 0;
  for (int i = 0; i < MAX_LEVELS; i++)
    freeSegments[i].Clear();

  int level;
  while ((level = log2(memSize)) >= 0) {
    Segment *mem = (Segment *)memPool;
    mem->level = level;
    mem->free = true;
    mem->control = CONTROL;
    freeSegments[mem->level].Add(mem);
    memSize = memSize - mem->Size();
    memPool = (void *)GetBuddy(mem);
  }
}

Segment *Split(Segment *seg) {
  freeSegments[seg->level].Remove(seg);

  seg->free = true;
  seg->level = seg->level - 1;
  seg->control = CONTROL;

  Segment *buddy = (Segment *)(((char *)seg) + seg->Size());
  buddy->free = true;
  buddy->level = seg->level;
  buddy->control = CONTROL;
  freeSegments[buddy->level].Add(buddy);

  return seg;
}

void *HeapAlloc(int size) {
  int requestedLevel = SegmentLevel(size);
  int level = requestedLevel;

  while (freeSegments[level].Empty())
    if (++level == MAX_LEVELS)
      return nullptr;

  Segment *tmp = freeSegments[level].Pop();

  while (level > requestedLevel) {
    tmp = Split(tmp);
    level = tmp->level;
  }

  tmp->free = false;
  tmp->control = CONTROL;

  ++allocated;
  return DataPointer(tmp);
}

Segment *MergeBuddy(Segment *seg) {
  Segment *buddy;
  buddy = GetBuddy(seg);

  if (!buddy->free || buddy->level != seg->level)
    return nullptr;

  if (seg > buddy) {
    Segment *x = seg;
    seg = buddy;
    buddy = x;
  }

  freeSegments[seg->level].Remove(seg);
  freeSegments[buddy->level].Remove(buddy);

  seg->level = seg->level + 1;
  seg->free = true;
  seg->control = CONTROL;

  freeSegments[seg->level].Add(seg);
  return seg;
}

bool HeapFree(void *blk) {
  Segment *tmp = ((Segment *)blk) - 1;

  if (tmp->control != CONTROL)
    return false;

  tmp->free = true;
  freeSegments[tmp->level].Add(tmp);

  while (tmp && tmp->level != MAX_LEVELS - 1)
    tmp = MergeBuddy(tmp);

  allocated--;
  return true;
}

void HeapDone(int *pendingBlk) { *pendingBlk = allocated; }

/// HELPER FUNCTIONS
/// ____________________________________________________________________________
inline short SegmentLevel(int size) {
  return ceil(log2(size + sizeof(Segment)));
}
inline void *DataPointer(Segment *segment) { return (void *)(segment + 1); }
inline int IndexInLevel(Segment *ptr) {
  int size = ptr->Size();
  return (((char *)ptr - (char *)memoryStart) / size) % 2;
}
inline Segment *GetBuddy(Segment *ptr) {
  int indexInLevel = IndexInLevel(ptr);
  Segment *buddy = nullptr;
  if (indexInLevel == 1)
    buddy = (Segment *)((char *)ptr - ptr->Size());
  else
    buddy = (Segment *)((char *)ptr + ptr->Size());
  return buddy;
}
/// HELPER FUNCTIONS
/// ____________________________________________________________________________

#ifndef __PROGTEST__
int main(void) {
  uint8_t *p0, *p1, *p2, *p3, *p4;
  int pendingBlk;
  static uint8_t memPool[3 * 1048576];

  HeapInit(memPool, 2097152);
  assert((p0 = (uint8_t *)HeapAlloc(512000)) != NULL);

  memset(p0, 0, 512000);
  assert((p1 = (uint8_t *)HeapAlloc(511000)) != NULL);
  memset(p1, 0, 511000);
  assert((p2 = (uint8_t *)HeapAlloc(26000)) != NULL);
  memset(p2, 0, 26000);
  HeapDone(&pendingBlk);
  assert(pendingBlk == 3);
  HeapInit(memPool, 2097152);
  assert((p0 = (uint8_t *)HeapAlloc(1000000)) != NULL);
  memset(p0, 0, 1000000);
  assert((p1 = (uint8_t *)HeapAlloc(250000)) != NULL);
  memset(p1, 0, 250000);
  assert((p2 = (uint8_t *)HeapAlloc(250000)) != NULL);
  memset(p2, 0, 250000);
  assert((p3 = (uint8_t *)HeapAlloc(250000)) != NULL);
  memset(p3, 0, 250000);
  assert((p4 = (uint8_t *)HeapAlloc(50000)) != NULL);
  memset(p4, 0, 50000);
  cout << hex << (void *)p2 << endl;
  assert(HeapFree(p2));
  assert(HeapFree(p4));
  assert(HeapFree(p3));
  assert(HeapFree(p1));
  assert((p1 = (uint8_t *)HeapAlloc(500000)) != NULL);
  memset(p1, 0, 500000);
  assert(HeapFree(p0));
  assert(HeapFree(p1));
  HeapDone(&pendingBlk);
  assert(pendingBlk == 0);

  HeapInit(memPool, 2359296);
  assert((p0 = (uint8_t *)HeapAlloc(1000000)) != NULL);
  memset(p0, 0, 1000000);
  assert((p1 = (uint8_t *)HeapAlloc(500000)) != NULL);
  memset(p1, 0, 500000);
  assert((p2 = (uint8_t *)HeapAlloc(500000)) != NULL);
  memset(p2, 0, 500000);
  assert((p3 = (uint8_t *)HeapAlloc(500000)) == NULL);
  assert(HeapFree(p2));
  assert((p2 = (uint8_t *)HeapAlloc(300000)) != NULL);
  memset(p2, 0, 300000);
  assert(HeapFree(p0));
  assert(HeapFree(p1));
  HeapDone(&pendingBlk);
  assert(pendingBlk == 1);

  HeapInit(memPool, 2359296);
  assert((p0 = (uint8_t *)HeapAlloc(1000000)) != NULL);
  memset(p0, 0, 1000000);
  assert(!HeapFree(p0 + 1000));
  HeapDone(&pendingBlk);
  assert(pendingBlk == 1);

  return 0;
}
#endif /* __PROGTEST__ */

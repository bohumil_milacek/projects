#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <cstring>
#include <cassert>
#include <cmath>
#include <cctype>
#include <climits>

#include <cstdint>
#include <unistd.h>

#include <bitset>
#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <list>
#include <algorithm>
#include <functional>
#include <memory>
//#include "Picture.hpp"

using namespace std;

const uint16_t ENDIAN_LITTLE = 0x4949;
const uint16_t ENDIAN_BIG = 0x4d4d;

class Chanel
{
public:
  uint16_t value;

  uint16_t getValue()
  {
    return value;
  }
  void setValue(uint16_t valueToSet)
  {
    value = valueToSet;
  }
};

class Pixel
{
public:
  Chanel *chanels;
};

class Picture
{
public:
  uint16_t imageWidth;
  uint16_t imageHeight;
  uint16_t chanels;
  uint16_t bitsPerChanel;
  uint16_t endianBytes;
  uint16_t chanelInfo;
  uint16_t chanelInfoOriginal;

  const string srcFile;
  Pixel **pixelArray;
  ifstream input;

  Picture(string srcFile) : srcFile(srcFile)
  {
    cout << srcFile << endl;
    input.open(srcFile, ios::binary | ios::in);
  }

  bool readHeaderFile()
  {

    if (input.fail())
    {
      cout << "Input file fail" << endl;
      return false;
    }

    input.read((char *)&endianBytes, sizeof(endianBytes));
    input.read((char *)&imageWidth, sizeof(imageWidth));
    input.read((char *)&imageHeight, sizeof(imageHeight));
    input.read((char *)&chanelInfo, sizeof(chanelInfo));

    switch (endianBytes)
    {
    case ENDIAN_BIG:
      cout << "BIG_ENDIAN" << endl;
      cout << "*****SWAPPING BYTES*****" << endl;
      byteSwap(imageWidth);
      byteSwap(imageHeight);
      byteSwap(chanelInfo);
      break;
    case ENDIAN_LITTLE:
      cout << "LITTLE_ENDIAN" << endl;
      break;
    default:
      cout << "WRONG ENDIAN" << endl;
      return false;
    }

    chanelInfoOriginal = chanelInfo;

    if (imageHeight == 0 || imageWidth == 0)
      return false;

    // 00 - 1
    // 01 - no
    // 10 - 3
    // 11 - 4
    uint16_t tmp = chanelInfo << 14;
    chanels = tmp >> 14;
    bitset<16> x(tmp);
    bitset<16> z(chanels);
    bitset<16> y(chanelInfo);
    /*cout << "CHANEL INFO:\t" << y << endl;
    cout << "TMP:\t\t" << x << endl;
    cout << "CHANELS:\t" << z << endl;
*/
    chanels++;
    if (chanels == 2)
      return false;

    bitsPerChanel = chanelInfo >> 2;
    // uint16_t restOfBits = chanelInfo >> 13;
    switch (bitsPerChanel)
    {
    case 0:
      bitsPerChanel = 1;
      break;
    case 3:
      bitsPerChanel = 8;
      break;
    case 4:
      bitsPerChanel = 16;
      break;
    default:
      return false;
    }

    cout << "Image width:" << imageWidth << endl;
    cout << "Image height:" << imageHeight << endl;
    cout << "Image chanels:" << chanels << endl;
    cout << "Bits per chanel:" << bitsPerChanel << endl; // cout << "Rest of bits:" << restOfBits << endl;

    pixelArray = new Pixel *[imageHeight];
    for (int i = 0; i < imageHeight; ++i)
      pixelArray[i] = new Pixel[imageWidth];
    return true;
  }

  bool readPixels()
  {
    cout << "BITS PER CHANEL" << bitsPerChanel;
    if (bitsPerChanel == 1)
    {
      cout << "Reading bit pixels" << endl;
      return readBitPixels();
    }
    uint16_t tmp = 0;
    string tmpString;

    int bytesToRead = (bitsPerChanel != 1 ? (bitsPerChanel / 8) : 1);
    cout << "Bytes to read: " << bytesToRead << endl;

    for (size_t i = 0; i < imageHeight; i++)
    {
      for (size_t j = 0; j < imageWidth; j++)
      {

        tmp = 0;

        Pixel p;

        p.chanels = new Chanel[chanels];

        for (size_t k = 0; k < chanels; k++)
        {
          if (input.eof())
          {
            cout << "Less bytes than expected: " << endl;
            return false;
          }

          else
          {
            input.read((char *)&tmp, bytesToRead);
            if (endianBytes == ENDIAN_BIG && bytesToRead == 2)
            {
              byteSwap(tmp);
            }
            p.chanels[k].setValue(tmp);
          }
          //  cout << p.chanels[k].getValue() << " ";

          /*  tmpString = int_to_hex(tmp);
          cout << tmpString << " ";
          p.chanels[k].setValue(tmpString);*/
          if (input.fail())
          {
            cout << "Input file fail while reading" << endl;
            return false;
          }
        }
        pixelArray[i][j] = p;

        /**/
      }
    }

    input.read((char *)&tmp, 1);
    cout << "End OF file char:[" << tmp << "]" << endl;
    if (!input.eof())
    {
      cout << "Too many bytes" << endl;
      return false;
    }

    cout << endl
         << "Reading pixels done:[" << srcFile << "]" << endl;

    cout << endl;
    print();
    input.close();
    return true;
  }

private:
  bool readBitPixels()
  {
    int actualPixelRow = 0;
    int actualPixelColumn = 0;

    uint16_t tmp = 0;
    string tmpString;

    int bytesToRead = 1;
    while (!input.eof())
    {
      input.read((char *)&tmp, bytesToRead);
      cout << "[" << tmp << "]";

      uint16_t tmpBit0 = 0x01 & tmp;
      uint16_t tmpBit1 = 0x02 & tmp;
      uint16_t tmpBit2 = 0x04 & tmp;
      uint16_t tmpBit3 = 0x08 & tmp;
      uint16_t tmpBit4 = 0x10 & tmp;
      uint16_t tmpBit5 = 0x20 & tmp;
      uint16_t tmpBit6 = 0x40 & tmp;
      uint16_t tmpBit7 = 0x80 & tmp;

      cout << tmpBit0 << " ";
      cout << tmpBit1 << " ";
      cout << tmpBit2 << " ";
      cout << tmpBit3 << " ";
      cout << tmpBit4 << " ";
      cout << tmpBit5 << " ";
      cout << tmpBit6 << " ";
      cout << tmpBit7 << " ";

      cout << endl;
    }

    return true;
  }

public:
  void flipHorizontally()
  {
    cout << "Swapping vertically" << endl;
    for (size_t i = 0; i < imageHeight; i++)
    {
      for (size_t j = 0; j < imageWidth / 2; j++)
      {
        swapPixels(pixelArray[i][j], pixelArray[i][imageWidth - 1 - j]);
      }
    }
  }

  void flipVertically()
  {
    cout << "Swapping horizontally" << endl;

    for (size_t i = 0; i < imageHeight / 2; i++)
    {
      for (size_t j = 0; j < imageWidth; j++)
      {
        swapPixels(pixelArray[i][j], pixelArray[imageHeight - 1 - i][j]);
      }
    }
  }

  void swapPixels(Pixel &r, Pixel &s)
  {
    Pixel tmp = r;
    r = s;
    s = tmp;
  }

  void print()
  {
    cout << "printing array" << endl;
    for (size_t i = 0; i < imageHeight; i++)
    {
      for (size_t j = 0; j < imageWidth; j++)
      {
        for (size_t k = 0; k < chanels; k++)
        {
          cout << pixelArray[i][j].chanels[k].getValue() << " ";
        }
      }
      cout << endl;
    }
    cout << endl;
  }

  bool writeToFile(const char *dstFile)
  {
    ofstream output;
    output.open(dstFile, ios::binary | ios::out);
    if (output.fail())
    {
      cout << "Output file fail" << endl;
      return false;
    }
    cout << "Writing to " << dstFile << endl;

    uint16_t tmpWidth = imageWidth;
    uint16_t tmpHeight = imageHeight;
    uint16_t tmpChanelInfo = chanelInfo;
    switch (endianBytes)
    {
    case ENDIAN_BIG:
      cout << ".........................SWAPING BAAAACK.........................." << endl;
      byteSwap(tmpWidth);
      byteSwap(tmpHeight);
      byteSwap(tmpChanelInfo);
      break;
    }

    output.write((char *)&endianBytes, 2);
    output.write((char *)&tmpWidth, 2);
    output.write((char *)&tmpHeight, 2);
    output.write((char *)&tmpChanelInfo, 2);

    int bytesToWrite = (bitsPerChanel != 1 ? (bitsPerChanel / 8) : 1);

    for (size_t i = 0; i < imageHeight; i++)
    {
      for (size_t j = 0; j < imageWidth; j++)
      {
        for (size_t k = 0; k < chanels; k++)
        {
          uint16_t valueToWrite = pixelArray[i][j].chanels[k].getValue();
          if (endianBytes == ENDIAN_BIG && bytesToWrite == 2)
          {
            byteSwap(valueToWrite);
          }
          output.write((char *)&valueToWrite, bytesToWrite);
          if (output.fail())
          {
            cout << "Output file while writing fail" << endl;
            return false;
          }
        }
      }
    }
    output.close();
    return true;
  }

public:
  void byteSwap(uint16_t &byteSwap)
  {
    uint16_t firstByte = byteSwap >> 8;
    uint16_t secondByte = byteSwap << 8;
    byteSwap = (firstByte + secondByte);
  }
};

bool flipImage(const char *srcFileName,
               const char *dstFileName,
               bool flipHorizontal,
               bool flipVertical)
{
  Picture pic(srcFileName);

  if (!pic.readHeaderFile())
    return false;
  if (!pic.readPixels())
    return false;

  if (flipHorizontal)
    pic.flipHorizontally();
  if (flipVertical)
    pic.flipVertically();

  pic.print();

  if (!pic.writeToFile(dstFileName))
    return false;
  return true;
}
// todo

bool identicalFiles(const char *fileName1,
                    const char *fileName2)
{
  ifstream input1;
  input1.open(fileName1, ios::binary | ios::in);
  ifstream input2;
  input2.open(fileName2, ios::binary | ios::in);

  char c1;
  char c2;

  while (!input1.eof() || !input2.eof())
  {
    input1.read(&c1, 1);
    input2.read(&c2, 1);
    if (c1 != c2)
    {
      cout << "Nerovnají se!" << endl;
      cout << "c1:" << c1 << " c2:" << c2 << endl;
      return false;
    }
  }
  if (!(input1.eof() && input2.eof()))
  {
    cout << "Nerovnají se!" << endl;
    return false;
  }
  return true;
}

int main(void)
{
  /*Picture p("input_00.img");
  uint16_t swapNum = 0xC1A1;
  cout << "Before swap:" << hex << swapNum << endl;
  p.byteSwap(swapNum);
  cout << "After swap:" << hex << swapNum << endl;
*/
  //flipImage("input_test.img", "output_test.img", true, true);
  assert(flipImage("input_00.img", "asd.img", true, false) && identicalFiles("output_00.img", "ref_00.img"));

  assert(flipImage("input_00.img", "output_00.img", true, false) && identicalFiles("output_00.img", "ref_00.img"));
  assert(flipImage("input_01.img", "output_01.img", false, true) && identicalFiles("output_01.img", "ref_01.img"));

  assert(flipImage("input_02.img", "output_02.img", true, true) && identicalFiles("output_02.img", "ref_02.img"));

  assert(flipImage("input_03.img", "output_03.img", false, false) && identicalFiles("output_03.img", "ref_03.img"));

  assert(flipImage("input_04.img", "output_04.img", true, false) && identicalFiles("output_04.img", "ref_04.img"));

  assert(flipImage("input_05.img", "output_05.img", true, true) && identicalFiles("output_05.img", "ref_05.img"));

  assert(flipImage("input_06.img", "output_06.img", false, true) && identicalFiles("output_06.img", "ref_06.img"));

  assert(flipImage("input_07.img", "output_07.img", true, false) && identicalFiles("output_07.img", "ref_07.img"));

  assert(flipImage("input_08.img", "output_08.img", true, true) && identicalFiles("output_08.img", "ref_08.img"));

  assert(!flipImage("input_09.img", "output_09.img", true, false));

  // extra inputs (optional & bonus tests)
  assert(flipImage("extra_input_00.img", "extra_out_00.img", true, false) && identicalFiles("extra_out_00.img", "extra_ref_00.img"));
  assert(flipImage("extra_input_01.img", "extra_out_01.img", false, true) && identicalFiles("extra_out_01.img", "extra_ref_01.img"));
  assert(flipImage("extra_input_02.img", "extra_out_02.img", true, false) && identicalFiles("extra_out_02.img", "extra_ref_02.img"));
  assert(flipImage("extra_input_03.img", "extra_out_03.img", false, true) && identicalFiles("extra_out_03.img", "extra_ref_03.img"));
  assert(flipImage("extra_input_04.img", "extra_out_04.img", true, false) && identicalFiles("extra_out_04.img", "extra_ref_04.img"));
  assert(flipImage("extra_input_05.img", "extra_out_05.img", false, true) && identicalFiles("extra_out_05.img", "extra_ref_05.img"));
  assert(flipImage("extra_input_06.img", "extra_out_06.img", true, false) && identicalFiles("extra_out_06.img", "extra_ref_06.img"));
  assert(flipImage("extra_input_07.img", "extra_out_07.img", false, true) && identicalFiles("extra_out_07.img", "extra_ref_07.img"));
  assert(flipImage("extra_input_08.img", "extra_out_08.img", true, false) && identicalFiles("extra_out_08.img", "extra_ref_08.img"));
  assert(flipImage("extra_input_09.img", "extra_out_09.img", false, true) && identicalFiles("extra_out_09.img", "extra_ref_09.img"));
  assert(flipImage("extra_input_10.img", "extra_out_10.img", true, false) && identicalFiles("extra_out_10.img", "extra_ref_10.img"));
  assert(flipImage("extra_input_11.img", "extra_out_11.img", false, true) && identicalFiles("extra_out_11.img", "extra_ref_11.img"));
  return 0;
}

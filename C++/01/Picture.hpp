#pragma once
#include "Endianity.hpp"
class Picture
{
    Picture::bool readHeaderFile(const char *srcFileName)
    {

        ifstream input;
        input.open(srcFileName, ios::binary | ios::in);

        uint16_t endianBytes;
        input.read((char *)&endianBytes, sizeof(endianBytes));
        cout << hex << endianBytes << endl;

        switch (endianBytes)
        {
        case 18761:
            endianity = LITTLE_ENDIAN;
            cout << "LITTLE_ENDIAN" << endl;
            break;
        case 19789:
            endianity = BIG_ENDIAN;
            cout << "LITTLE_ENDIAN" << endl;
            break;
        }
    }
};

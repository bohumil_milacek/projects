#ifndef __PROGTEST__

#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <cstring>
#include <cassert>
#include <cmath>
#include <cctype>
#include <climits>

#include <cstdint>
#include <unistd.h>

#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <list>
#include <algorithm>
#include <functional>
#include <memory>
//#include "Picture.hpp"

using namespace std;

const uint16_t ENDIAN_LITTLE = 0x4949;
const uint16_t ENDIAN_BIG = 0x4d4d;

#endif /* __PROGTEST__ */

class Chanel
{
public:
  uint16_t value;

  uint16_t getValue()
  {
    return value;
  }
  void setValue(uint16_t valueToSet)
  {
    value = valueToSet;
  }
};

class Pixel
{
public:
  Chanel *chanels;
};

class Picture
{
public:
  uint16_t imageWidth;
  uint16_t imageHeight;
  uint16_t chanels;
  uint16_t bitsPerChanel;
  uint16_t endianBytes;
  uint16_t chanelInfo;
  uint16_t chanelInfoOriginal;

  const string srcFile;
  Pixel **pixelArray;
  ifstream input;

  Picture(string srcFile) : srcFile(srcFile)
  {
    cout << srcFile << endl;
    input.open(srcFile, ios::binary | ios::in);
  }

  bool readHeaderFile()
  {

    if (input.fail())
    {
      cout << "Input file fail" << endl;
      return false;
    }

    input.read((char *)&endianBytes, sizeof(endianBytes));
    input.read((char *)&imageWidth, sizeof(imageWidth));
    input.read((char *)&imageHeight, sizeof(imageHeight));
    input.read((char *)&chanelInfo, sizeof(chanelInfo));

    switch (endianBytes)
    {
    case ENDIAN_BIG:
      cout << "BIG_ENDIAN" << endl;
      cout << "*****SWAPPING BYTES*****" << endl;
      byteSwap(imageWidth);
      byteSwap(imageHeight);
      byteSwap(chanelInfo);
      break;
    case ENDIAN_LITTLE:
      cout << "LITTLE_ENDIAN" << endl;
      break;
    default:
      cout << "WRONG_ENDIAN" << endl;
      return false;
    }

    chanelInfoOriginal = chanelInfo;

    if (imageHeight == 0 || imageWidth == 0)
      return false;

    // 00 - 1
    // 01 - no
    // 10 - 3
    // 11 - 4
    uint16_t tmp = chanelInfo << 14;
    chanels = tmp >> 14;
    /*bitset<16> x(tmp);
    bitset<16> z(chanels);
    bitset<16> y(chanelInfo);*/
    /*cout << "CHANEL INFO:\t" << y << endl;
    cout << "TMP:\t\t" << x << endl;
    cout << "CHANELS:\t" << z << endl;
*/
    chanels++;
    if (chanels == 2)
      return false;

    bitsPerChanel = chanelInfo >> 2;
    // uint16_t restOfBits = chanelInfo >> 13;
    switch (bitsPerChanel)
    {
    case 0:
      bitsPerChanel = 1;
      break;
    case 3:
      bitsPerChanel = 8;
      break;
    case 4:
      bitsPerChanel = 16;
      break;
    default:
      return false;
    }

    cout << "Image width:" << imageWidth << endl;
    cout << "Image height:" << imageHeight << endl;
    cout << "Image chanels:" << chanels << endl;
    cout << "Bits per chanel:" << bitsPerChanel << endl; // cout << "Rest of bits:" << restOfBits << endl;

    pixelArray = new Pixel *[imageHeight];
    for (int i = 0; i < imageHeight; ++i)
      pixelArray[i] = new Pixel[imageWidth];
    return true;
  }

  bool readPixels()
  {
    uint16_t tmp = 0;
    string tmpString;

    int bytesToRead = (bitsPerChanel != 1 ? (bitsPerChanel / 8) : 1);
    cout << "Bytes to read: " << bytesToRead << endl;

    for (size_t i = 0; i < imageHeight; i++)
    {
      for (size_t j = 0; j < imageWidth; j++)
      {

        tmp = 0;

        Pixel p;

        p.chanels = new Chanel[chanels];

        for (size_t k = 0; k < chanels; k++)
        {
          if (input.eof())
          {
            cout << "Less bytes than expected: " << endl;
            return false;
          }

          input.read((char *)&tmp, bytesToRead);
          if (endianBytes == ENDIAN_BIG && bytesToRead == 2)
          {
            byteSwap(tmp);
          }
          p.chanels[k].setValue(tmp);
          //  cout << p.chanels[k].getValue() << " ";

          /*  tmpString = int_to_hex(tmp);
          cout << tmpString << " ";
          p.chanels[k].setValue(tmpString);*/
          if (input.fail())
          {
            cout << "Input file fail while reading" << endl;
            return false;
          }
        }
        pixelArray[i][j] = p;

        /**/
      }
    }

    input.read((char *)&tmp, 1);
    cout << "End OF file char:[" << tmp << "]" << endl;
    if (!input.eof())
    {
      cout << "Too many bytes" << endl;
      return false;
    }

    cout << endl
         << "Reading pixels done:[" << srcFile << "]" << endl;

    cout << endl;
    print();
    input.close();
    return true;
  }

public:
  void flipHorizontally()
  {
    cout << "Swapping vertically" << endl;
    for (size_t i = 0; i < imageHeight; i++)
    {
      for (size_t j = 0; j < imageWidth / 2; j++)
      {
        swapPixels(pixelArray[i][j], pixelArray[i][imageWidth - 1 - j]);
      }
    }
  }

  void flipVertically()
  {
    cout << "Swapping horizontally" << endl;

    for (size_t i = 0; i < imageHeight / 2; i++)
    {
      for (size_t j = 0; j < imageWidth; j++)
      {
        swapPixels(pixelArray[i][j], pixelArray[imageHeight - 1 - i][j]);
      }
    }
  }

  void swapPixels(Pixel &r, Pixel &s)
  {
    Pixel tmp = r;
    r = s;
    s = tmp;
  }

  void print()
  {
    cout << "printing array" << endl;
    for (size_t i = 0; i < imageHeight; i++)
    {
      for (size_t j = 0; j < imageWidth; j++)
      {
        for (size_t k = 0; k < chanels; k++)
        {
          cout << pixelArray[i][j].chanels[k].getValue() << " ";
        }
      }
      cout << endl;
    }
    cout << endl;
  }

  bool writeToFile(const char *dstFile)
  {
    ofstream output;
    output.open(dstFile, ios::binary | ios::out);
    if (output.fail())
    {
      cout << "Output file fail" << endl;
      return false;
    }
    cout << "Writing to " << dstFile << endl;

    uint16_t tmpWidth = imageWidth;
    uint16_t tmpHeight = imageHeight;
    uint16_t tmpChanelInfo = chanelInfo;
    switch (endianBytes)
    {
    case ENDIAN_BIG:
      cout << ".........................SWAPING BAAAACK.........................." << endl;
      byteSwap(tmpWidth);
      byteSwap(tmpHeight);
      byteSwap(tmpChanelInfo);
      break;
    }

    output.write((char *)&endianBytes, 2);
    output.write((char *)&tmpWidth, 2);
    output.write((char *)&tmpHeight, 2);
    output.write((char *)&tmpChanelInfo, 2);

    if (output.fail())
    {
      cout << "Cant write to file" << endl;
      return false;
    }

    int bytesToWrite = (bitsPerChanel != 1 ? (bitsPerChanel / 8) : 1);

    for (size_t i = 0; i < imageHeight; i++)
    {
      for (size_t j = 0; j < imageWidth; j++)
      {
        for (size_t k = 0; k < chanels; k++)
        {
          uint16_t valueToWrite = pixelArray[i][j].chanels[k].getValue();
          if (endianBytes == ENDIAN_BIG && bytesToWrite == 2)
          {
            byteSwap(valueToWrite);
          }
          output.write((char *)&valueToWrite, bytesToWrite);
          if (output.fail())
          {
            cout << "Output file while writing fail" << endl;
            return false;
          }
        }
      }
    }
    output.close();
    return true;
  }

public:
  void byteSwap(uint16_t &byteSwap)
  {
    uint16_t firstByte = byteSwap >> 8;
    uint16_t secondByte = byteSwap << 8;
    byteSwap = (firstByte + secondByte);
  }
};

bool flipImage(const char *srcFileName,
               const char *dstFileName,
               bool flipHorizontal,
               bool flipVertical)
{
  Picture pic(srcFileName);

  if (!pic.readHeaderFile())
    return false;
  if (!pic.readPixels())
    return false;

  if (flipHorizontal)
    pic.flipHorizontally();
  if (flipVertical)
    pic.flipVertically();

  pic.print();

  if (!pic.writeToFile(dstFileName))
    return false;
  return true;
}
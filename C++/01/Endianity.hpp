#pragma once

enum Endianity
{
    LITTLE_ENDIAN,
    BIG_ENDIAN
};
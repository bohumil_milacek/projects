.cseg		; nasledujici umistit do pameti programu (implicitni)
; definice pro nas typ procesoru
.include "m169def.inc"
; podprogramy pro praci s displejem
.org 0x1000
.include "print.inc"
.org	0	; zaciname od adresy 0 (implicitni)
		JMP start

.org	0x100
	menu_start:	.db "START",0/*;	"NEJAKY ZAJIMAVY TEXT VELKYMI PISMENY BEZ DIAKRITIKY",0	; retezec zakonceny nulou*/
	menu_score:	.db "SCORE",0/*;	"NEJAKY ZAJIMAVY TEXT VELKYMI PISMENY BEZ DIAKRITIKY",0	; retezec zakonceny nulou*/
	menu_speed:	.db "SPEED",0/*;	"NEJAKY ZAJIMAVY TEXT VELKYMI PISMENY BEZ DIAKRITIKY",0	; retezec zakonceny nulou*/
.DSEG
	adresa_menu: .BYTE 1
	rychlost:	 .BYTE 1
	max_score:	 .BYTE 1
.CSEG


	;...
start:
	;INICIALIZACE POZIC PISMEN
	LDI R20,0x00
	LDI R21,0x00
	LDI R22,0x00
	LDI R23,0x00
	LDI R24,0x00   
	
      ; Inicializace zasobniku
      ldi r16, 0xFF
      out SPL, r16
      ldi r16, 0x04
      out SPH, r16
      ; Inicializace displeje
      call init_disp
	  call joystick_init
	; inicializace menu
	LDI	R28, low(adresa_menu)	; dolni bajt bajtove adresy retezce (registr R30 = dolni bajt 16ti bitoveho registru Z)
	LDI	R29, high(adresa_menu)	; horni bajt bajtove adresy retezce (registr R31 = horni bajt 16ti bitoveho registru Z)
	LDI R16,0
	ST	Y,R16
	; inicializace nejvyssiho skore
	LDI	R28, low(max_score)	; dolni bajt bajtove adresy retezce (registr R30 = dolni bajt 16ti bitoveho registru Z)
	LDI	R29, high(max_score)	; horni bajt bajtove adresy retezce (registr R31 = horni bajt 16ti bitoveho registru Z)
	ST	Y,R16
	; inicializace rychlost
	LDI	R28, low(rychlost)	; dolni bajt bajtove adresy retezce (registr R30 = dolni bajt 16ti bitoveho registru Z)
	LDI	R29, high(rychlost)	; horni bajt bajtove adresy retezce (registr R31 = horni bajt 16ti bitoveho registru Z)
	LDI R16,1
	ST	Y,R16


; main loop
infinite_loop:
	CALL display_menu
	IN R16,PINE
	CALL Delay_1sec
	IN R17,PINE
	CP R16,R17
	BRNE infinite_loop
	; STS zaps�n� do portu
; menu ______
	;check joystick right
	SBIS PINE,3	
	JMP next_menu_item
	;check joystick left
	SBIS PINE,2
	JMP prev_menu_item

	; SELECTED MENU ITEM
	SBIS PINB,4
	JMP menu_selected/**/
; end menu 			

jmp infinite_loop

;MENU NEXT PREV________________________________________________________________________________________________________
next_menu_item:
	LDI	R28, low(adresa_menu)	; dolni bajt bajtove adresy retezce (registr R30 = dolni bajt 16ti bitoveho registru Z)
	LDI	R29, high(adresa_menu)	; horni bajt bajtove adresy retezce (registr R31 = horni bajt 16ti bitoveho registru Z)
	LD	R16, Y
	INC R16
	CPI R16,0x03
	BREQ go_menu_begining
	JMP continue1
	go_menu_begining:
	LDI R16,0x00
continue1:
	ST Y,R16
	;SBI PINE,3
	JMP infinite_loop

prev_menu_item:
	LDI	R28, low(adresa_menu)	; dolni bajt bajtove adresy retezce (registr R30 = dolni bajt 16ti bitoveho registru Z)
	LDI	R29, high(adresa_menu)	; horni bajt bajtove adresy retezce (registr R31 = horni bajt 16ti bitoveho registru Z)
	LD	R16, Y
	DEC R16
	CPI R16,0xFF
	BREQ go_menu_end
	JMP continue2
	go_menu_end:
	LDI R16,0x02
continue2:
	ST Y,R16
;	SBI PINE,2
	JMP infinite_loop	


load_menu_start:
	LDI	R30, low(2*menu_start)	; dolni bajt bajtove adresy retezce (registr R30 = dolni bajt 16ti bitoveho registru Z)
	LDI	R31, high(2*menu_start)	; horni bajt bajtove adresy retezce (registr R31 = horni bajt 16ti bitoveho registru Z)
	LPM	R20, Z+
	LPM	R21, Z+	
	LPM	R22, Z+	
	LPM	R23, Z+	
	LPM	R24, Z+	
	CALL display_text
RET

load_menu:

RET

load_menu_score:
	LDI	R30, low(2*menu_score)	; dolni bajt bajtove adresy retezce (registr R30 = dolni bajt 16ti bitoveho registru Z)
	LDI	R31, high(2*menu_score)	; horni bajt bajtove adresy retezce (registr R31 = horni bajt 16ti bitoveho registru Z)
	LPM	R20, Z+
	LPM	R21, Z+	
	LPM	R22, Z+	
	LPM	R23, Z+	
	LPM	R24, Z+	
	CALL display_text
RET

load_menu_speed:
	LDI	R30, low(2*menu_speed)	; dolni bajt bajtove adresy retezce (registr R30 = dolni bajt 16ti bitoveho registru Z)
	LDI	R31, high(2*menu_speed)	; horni bajt bajtove adresy retezce (registr R31 = horni bajt 16ti bitoveho registru Z)
	LPM	R20, Z+
	LPM	R21, Z+	
	LPM	R22, Z+	
	LPM	R23, Z+	
	LPM	R24, Z+	
	CALL display_text
RET


display_menu:
	LDI	R28, low(adresa_menu)	; dolni bajt bajtove adresy retezce (registr R30 = dolni bajt 16ti bitoveho registru Z)
	LDI	R29, high(adresa_menu)	; horni bajt bajtove adresy retezce (registr R31 = horni bajt 16ti bitoveho registru Z)
	LD	R16, Y

	CPI R16,0x00
	BREQ load_menu_start
	CPI R16,0x01
	BREQ load_menu_speed
	CPI R16,0x02
	BREQ load_menu_score
RET



display_text:
		;CHAR_1-------------------------------- 
		MOV R16,R20
		ldi R17, 2      ; pozice (zacinaji od 1)
		call show_char  ; zobraz znak
		;CHAR_2-------------------------------- 
		MOV R16,R21
		ldi R17, 3      ; pozice (zacinaji od 1)
		call show_char  ; zobraz znak
		;CHAR_3-------------------------------- 
		MOV R16,R22
		ldi R17, 4      ; pozice (zacinaji od 1)
		call show_char  ; zobraz znak
		;CHAR_4-------------------------------- 
		MOV R16,R23
		ldi R17, 5      ; pozice (zacinaji od 1)
		call show_char  ; zobraz znak
		;CHAR_5-------------------------------- 
		MOV R16,R24
		ldi R17, 6      ; pozice (zacinaji od 1)
		call show_char  ; zobraz znak
	RET

;MENU END________________________________________________________________________________________________________
show_score:
	LDI	R28, low(max_score)	; dolni bajt bajtove adresy retezce (registr R30 = dolni bajt 16ti bitoveho registru Z)
	LDI	R29, high(max_score)	; horni bajt bajtove adresy retezce (registr R31 = horni bajt 16ti bitoveho registru Z)
	LD	R16, Y	
	MOV R31,R16
	ANDI R16,0x0F
	LSR R31
	LSR R31
	LDI R30,48
	ADD R16,R30
	ADD R31,R30
		;CHAR_1-------------------------------- 
		ldi R17, 6      ; pozice (zacinaji od 1)
		call show_char  ; zobraz znak
		;CHAR_2-------------------------------- 
		MOV R16,R31
		ldi R17, 5      ; pozice (zacinaji od 1)
		call show_char  ; zobraz znak
		;CHAR_3-------------------------------- 
		LDI R16,':'
		ldi R17, 4      ; pozice (zacinaji od 1)
		call show_char  ; zobraz znak
		;CHAR_4-------------------------------- 
		LDI R16,'P'
		ldi R17, 3      ; pozice (zacinaji od 1)
		call show_char  ; zobraz znak
		;CHAR_5-------------------------------- 
		LDI R16,'T'
		ldi R17, 2      ; pozice (zacinaji od 1)
		call show_char  ; zobraz znak
JMP menu_selected_loop

/*SPEED__________________________________________________________________________________________*/
set_speed:
	CALL display_speed
	IN R16,PINB
	CALL Delay_1sec
	IN R17,PINB
	CP R16,R17
	BRNE set_speed
; menu ______
	;check joystick up
	SBIS PINB,6	
	JMP speed_up
	;check joystick down
	SBIS PINB,7
	JMP speed_down
	; SELECTED MENU ITEM
	SBIS PINB,4
	JMP go_back_to_menu
JMP set_speed

/******/
menu_selected:
	SBI PINB,4
	LDI	R28, low(adresa_menu)	; dolni bajt bajtove adresy retezce (registr R30 = dolni bajt 16ti bitoveho registru Z)
	LDI	R29, high(adresa_menu)	; horni bajt bajtove adresy retezce (registr R31 = horni bajt 16ti bitoveho registru Z)
	LD	R16, Y

	CPI R16,0x00
	BREQ start_game
	CPI R16,0x01
	BREQ set_speed
	CPI R16,0x02
	BREQ show_score
menu_selected_loop: 
	; SELECTED MENU ITEM
	SBIS PINB,4
	JMP go_back_to_menu
JMP menu_selected_loop
/******/

display_speed:
	LDI	R28, low(rychlost)	; dolni bajt bajtove adresy retezce (registr R30 = dolni bajt 16ti bitoveho registru Z)
	LDI	R29, high(rychlost)	; horni bajt bajtove adresy retezce (registr R31 = horni bajt 16ti bitoveho registru Z)
	LD	R16, Y	
	MOV R31,R16
	ANDI R16,0x0F
	LSR R31
	LSR R31
	LDI R30,48
	ADD R16,R30
	ADD R31,R30
		;CHAR_1-------------------------------- 
		ldi R17, 6      ; pozice (zacinaji od 1)
		call show_char  ; zobraz znak
		;CHAR_2-------------------------------- 
		MOV R16,R31
		ldi R17, 5      ; pozice (zacinaji od 1)
		call show_char  ; zobraz znak
		;CHAR_3-------------------------------- 
		LDI R16,':'
		ldi R17, 4      ; pozice (zacinaji od 1)
		call show_char  ; zobraz znak
		;CHAR_4-------------------------------- 
		LDI R16,'P'
		ldi R17, 3      ; pozice (zacinaji od 1)
		call show_char  ; zobraz znak
		;CHAR_5-------------------------------- 
		LDI R16,'S'
		ldi R17, 2      ; pozice (zacinaji od 1)
		call show_char  ; zobraz znak
RET


;BEGIN GAME LOOP________________________________________________________________________________________________________
start_game:
	LDI R21,3  ; BALL POSITION
	LDI R22,1; indicates where the ball should go

game_loop:
CALL Game_delay
CALL Display_game_screen
CALL Shift_ball

end_game: JMP game_loop

speed_down:
	LDI	R28, low(rychlost)	; dolni bajt bajtove adresy retezce (registr R30 = dolni bajt 16ti bitoveho registru Z)
	LDI	R29, high(rychlost)	; horni bajt bajtove adresy retezce (registr R31 = horni bajt 16ti bitoveho registru Z)
	LD  R16,Y
	DEC R16
	CPI R16,0
	BREQ go_speed_from_end
	JMP save_speed
go_speed_from_end:
	LDI R16,3
	JMP save_speed

speed_up:
	LDI	R28, low(rychlost)	; dolni bajt bajtove adresy retezce (registr R30 = dolni bajt 16ti bitoveho registru Z)
	LDI	R29, high(rychlost)	; horni bajt bajtove adresy retezce (registr R31 = horni bajt 16ti bitoveho registru Z)
	LD  R16,Y
	INC R16
	CPI R16,4
	BREQ go_speed_from_begining
	JMP save_speed
go_speed_from_begining:
	LDI R16,1
save_speed:
	ST  Y,R16
JMP set_speed
/*______________________________________________________________________________________________*/
go_back_to_menu:
	JMP infinite_loop

Display_game_screen:
	CALL clear_display
	;BALL______________________
		LDI R16,'X'
		MOV R17, R21      ; pozice (zacinaji od 1)
		call show_char  ; zobraz znak
RET

	;check joystick right
	SBIS PINE,3	
	JMP next_menu_item
	;check joystick left
	
	JMP prev_menu_item

Shift_ball:
	CPI R21,2
	BREQ check_right_joystick
	CPI R21,6
	BREQ check_left_joystick

	;check joystick right
	SBIS PINE,3	
	JMP game_end
	;check joystick left
	SBIS PINE,2
	JMP game_end
	JMP continue_shift_ball

check_left_joystick:
	SBIS PINE,2
	JMP go_ball_left
	JMP game_end


check_right_joystick:
	SBIS PINE,3
	JMP go_ball_right
	JMP game_end

go_ball_right:
	LDI R22,1
	JMP continue_shift_ball
go_ball_left:
	LDI R22,0

continue_shift_ball:
	CPI R22,1
	BREQ shift_ball_right
	CPI R22,0
	BREQ shift_ball_left
	JMP continue_shift_ball2
shift_ball_right:
	INC R21
	JMP continue_shift_ball2
shift_ball_left:
	DEC R21

continue_shift_ball2:


RET

;END GAME LOOP________________________________________________________________________________________________________
game_end:
	CALL display_looser
	CALL Delay_1sec
	CALL clear_display
	CALL Delay_1sec
JMP game_end




;DELAY________________________________________________________________________________________________________
; Delay 8 000 000 cycles
; 1s at 8.0 MHz

Delay_1sec:                 ; For CLK(CPU) = 1 MHz
    LDI     R18, 2       ; One clock cycle;
Delay1:
    LDI     R19, 255        ; One clock cycle
Delay2:
    LDI     R20, 255     ; One clock cycle
Delay3:
    DEC     R20            ; One clock cycle
    NOP                     ; One clock cycle
    BRNE    Delay3          ; Two clock cycles when jumping to Delay3, 1 clock when continuing to DEC

    DEC     R19            ; One clock cycle
    BRNE    Delay2          ; Two clock cycles when jumping to Delay2, 1 clock when continuing to DEC

    DEC     R18            ; One clock Cycle
    BRNE    Delay1          ; Two clock cycles when jumping to Delay1, 1 clock when continuing to RET
RET



Game_delay:
	LDI	R28, low(adresa_menu)	; dolni bajt bajtove adresy retezce (registr R30 = dolni bajt 16ti bitoveho registru Z)
	LDI	R29, high(adresa_menu)	; horni bajt bajtove adresy retezce (registr R31 = horni bajt 16ti bitoveho registru Z)
	LD	R16, Y

    LDI     R18, 3       ; One clock cycle;
	SUB 	R18,R16
Game_delay1:
    LDI     R19, 100        ; One clock cycle - 255
Game_delay2:
    LDI     R20, 255     ; One clock cycle
Game_delay3:
    DEC     R20            ; One clock cycle
    NOP                     ; One clock cycle
    BRNE    Game_delay3          ; Two clock cycles when jumping to Delay3, 1 clock when continuing to DEC

    DEC     R19            ; One clock cycle
    BRNE    Game_delay2          ; Two clock cycles when jumping to Delay2, 1 clock when continuing to DEC

    DEC     R18            ; One clock Cycle
    BRNE    Game_delay1          ; Two clock cycles when jumping to Delay1, 1 clock when continuing to RET
RET



joystick_init:
	in r19, DDRE
	andi r19, 0b11110011
	in r18, PORTE
	ori r18, 0b00001100
	out DDRE, r19
	out PORTE, r18
	ldi r18, 0b00000000
	sts DIDR1, r18
	in r19, DDRB
	andi r19, 0b00101111
	in r18, PORTB
	ori r18, 0b11010000
	out DDRB, r19
	out PORTB, r18
RET


clear_display:
		LDI R16,0
		
		;CHAR_2-------------------------------- 
		ldi R17, 5      ; pozice (zacinaji od 1)
		call show_char  ; zobraz znak
		;CHAR_3-------------------------------- 
		ldi R17, 4      ; pozice (zacinaji od 1)
		call show_char  ; zobraz znak
		;CHAR_4-------------------------------- 
		ldi R17, 3      ; pozice (zacinaji od 1)
		call show_char  ; zobraz znak
		;CHAR_5-------------------------------- 
		LDI R16, 'I'
		ldi R17, 2      ; pozice (zacinaji od 1)
		call show_char  ; zobraz znak
		;CHAR_1-------------------------------- 
		ldi R17, 6      ; pozice (zacinaji od 1)
		call show_char  ; zobraz znak
RET


display_looser:
		;CHAR_1-------------------------------- 
		LDI R16,'L'
		ldi R17, 2      ; pozice (zacinaji od 1)
		call show_char  ; zobraz znak
		;CHAR_2-------------------------------- 
		LDI R16,'O'
		ldi R17, 3      ; pozice (zacinaji od 1)
		call show_char  ; zobraz znak
		;CHAR_3-------------------------------- 
		LDI R16,'S'
		ldi R17, 4      ; pozice (zacinaji od 1)
		call show_char  ; zobraz znak
		;CHAR_4-------------------------------- 
		LDI R16,'E'
		ldi R17, 5      ; pozice (zacinaji od 1)
		call show_char  ; zobraz znak
		;CHAR_5-------------------------------- 
		LDI R16,'R'
		ldi R17, 6      ; pozice (zacinaji od 1)
		call show_char  ; zobraz znak
RET



#include <stdlib.h>
#include <openssl/evp.h>
#include <string.h>

int GetST(unsigned char *, unsigned char *);
void print(int, unsigned char *, unsigned char *);
void encrypt();
void decrypt(unsigned char *, unsigned char *, unsigned char *);
void hexToByte(unsigned char *, unsigned char *);
unsigned char byteVal(unsigned char);

int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        printf("Invalid number of arguments\n");
        exit(1);
    }
    if (strcmp(argv[1], "-e") == 0)
        encrypt();
    else if (strcmp(argv[1], "-d") == 0)
    {
        if (argc == 5)
            decrypt(argv[2], argv[3], argv[4]);
        else
        {
            printf("Invalid number of arguments\n");
            exit(1);
        }
    }
    else
        printf("Invalid argument %s\n", argv[1]);
    exit(0);
}

void encrypt()
{
    unsigned char
        ot1[1024] = "abcdefghijklmnopqrstuvwxyz0123", // open text
        ot2[1024] = "Igonnamakehimofferhecantrefuse"; // open text

    unsigned char st1[1024], st2[1024]; // sifrovany text
    int stLength1 = 0, stLength2 = 0;

    stLength1 = GetST(ot1, st1);
    stLength2 = GetST(ot2, st2);

    print(stLength1, ot1, st1);
    print(stLength2, ot2, st2);
}

void decrypt(unsigned char *ot1, unsigned char *hexSt1, unsigned char *hexSt2)
{
    int stLen1 = strlen(hexSt1) / 2;
    int stLen2 = strlen(hexSt2) / 2;
    int stLen = stLen1 < stLen2 ? stLen1 : stLen2;

    int otLen = strlen(ot1);

    unsigned char *ot2 = calloc(otLen, sizeof(unsigned char));
    unsigned char *st1 = calloc(stLen, sizeof(unsigned char));
    unsigned char *st2 = calloc(stLen, sizeof(unsigned char));

    hexToByte(hexSt1, st1);
    hexToByte(hexSt2, st2);

    for (int i = 0; i < stLen; ++i)
        ot2[i] = (st1[i] ^ st2[i]) ^ ot1[i];

    printf("\nDT: %s\n", ot2);
}


void hexToByte(unsigned char *hex, unsigned char *byte)
{
    int length = strlen(hex);
    size_t i = 0;
    if (length % 2 == 1)
    {
        unsigned char left = byteVal(hex[0]);
        byte[0] = left;
        size_t i = 2;
    }

    for (i; i < length; i += 2)
    {
        unsigned char left = byteVal(hex[i]) * 16;
        unsigned char right = byteVal(hex[i + 1]);
        byte[i / 2] = left + right;
    }
}

unsigned char byteVal(unsigned char hexChar)
{
    switch (hexChar)
    {
    case '0':
        return 0;
    case '1':
        return 1;
    case '2':
        return 2;
    case '3':
        return 3;
    case '4':
        return 4;
    case '5':
        return 5;
    case '6':
        return 6;
    case '7':
        return 7;
    case '8':
        return 8;
    case '9':
        return 9;
    case 'A':
    case 'a':
        return 10;
    case 'B':
    case 'b':
        return 11;
    case 'C':
    case 'c':
        return 12;
    case 'D':
    case 'd':
        return 13;
    case 'E':
    case 'e':
        return 14;
    case 'F':
    case 'f':
        return 15;
    default:
        break;
    }
}

void print(int stLength, unsigned char *ot, unsigned char *st)
{
    unsigned char a = 0x4b;
    /* Vypsani zasifrovaneho a rozsifrovaneho textu. */
    printf("------------------\n");
    printf("ST: %s\nDT: %s\n", st, ot);
    printf("\n");
    for (int i = 0; i < stLength; i++)
        printf("%02x", st[i]);
    printf("\n");
}

int GetST(unsigned char ot[1024], unsigned char st[1024])
{
    int res;
    unsigned char key[EVP_MAX_KEY_LENGTH] = "TheGodfather";  // klic pro sifrovani
    unsigned char iv[EVP_MAX_IV_LENGTH] = "inicial. vektor"; // inicializacni vektor
    const char cipherName[] = "RC4";
    const EVP_CIPHER *cipher;

    OpenSSL_add_all_ciphers();
    /* sifry i hashe by se nahraly pomoci OpenSSL_add_all_algorithms() */
    cipher = EVP_get_cipherbyname(cipherName);
    if (!cipher)
    {
        printf("Sifra %s neexistuje.\n", cipherName);
        exit(1);
    }

    int otLength = strlen((const char *)ot);
    int stLength = 0;
    int tmpLength = 0;

    EVP_CIPHER_CTX *ctx; // context structure
    ctx = EVP_CIPHER_CTX_new();
    if (ctx == NULL)
        exit(2);

    printf("OT: %s\n", ot);

    /* Sifrovani */
    res = EVP_EncryptInit_ex(ctx, cipher, NULL, key, iv); // context init - set cipher, key, init vector
    if (res != 1)
        exit(3);
    res = EVP_EncryptUpdate(ctx, st, &tmpLength, ot, otLength); // encryption of pt
    if (res != 1)
        exit(4);
    stLength += tmpLength;
    res = EVP_EncryptFinal_ex(ctx, st + stLength, &tmpLength); // get the remaining ct
    if (res != 1)
        exit(5);
    stLength += tmpLength;

    printf("Zasifrovano %d znaku.\n", stLength);

    /* Desifrovani */
    res = EVP_DecryptInit_ex(ctx, cipher, NULL, key, iv); // nastaveni kontextu pro desifrovani
    if (res != 1)
        exit(6);
    res = EVP_DecryptUpdate(ctx, ot, &tmpLength, st, stLength); // desifrovani st
    if (res != 1)
        exit(7);
    otLength += tmpLength;
    res = EVP_DecryptFinal_ex(ctx, ot + otLength, &tmpLength); // dokonceni (ziskani zbytku z kontextu)
    if (res != 1)
        exit(8);
    otLength += tmpLength;

    /* Clean up */
    EVP_CIPHER_CTX_free(ctx);
    return stLength;
}
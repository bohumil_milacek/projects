#include <memory.h>
#include <netdb.h>
#include <netinet/in.h>
#include <openssl/ssl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <unistd.h>

#define PORT 443
#define DOMAIN "fit.cvut.cz"
#define BUFF_SIZE 1024
#define HTTP_REQ "GET / HTTP/1.1\r\nHost: fit.cvut.cz\r\n\r\n"
void error(const char *msg, char code) {
  printf("%s", msg);
  exit(code);
}

void NetInit(int *tcp_socket) {

  struct hostent *server = gethostbyname(DOMAIN);
  struct sockaddr_in server_address;
  if (!server)
    error("NET Address error", 1);
  if ((*tcp_socket = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    error("NET Socket creation failed", 1);

  memset(&server_address, 0, sizeof(server_address)); // vymazání struktury
  server_address.sin_family = AF_INET;                // nastavení IPv4
  memcpy(&server_address.sin_addr.s_addr, server->h_addr_list[0],
         server->h_length);              // nakopírování samotné adresy
  server_address.sin_port = htons(PORT); // htons: převod do big-endian

  if (connect(*tcp_socket, (const struct sockaddr *)&server_address,
              sizeof(server_address)) < 0)
    error("NET Connection failed", 1);
}

void SSLCommunication(const int tcp_socket) {
  SSL_library_init();
  OpenSSL_add_all_algorithms();

  SSL_CTX *ctx = NULL;
  SSL *sslStruct = NULL;

  if (!(ctx = SSL_CTX_new(SSLv23_client_method())))
    error("SSL context failed", -1);
  SSL_CTX_set_options(ctx, SSL_OP_NO_SSLv2 | SSL_OP_NO_SSLv3 | SSL_OP_NO_TLSv1);

  if (SSL_CTX_set_default_verify_paths(ctx) == 0)
    error("SSL veirfy paths failed", -1);
  if (!(sslStruct = SSL_new(ctx)))
    error("SSL structure failed", -1);
  if (SSL_set_fd(sslStruct, tcp_socket) == 0)
    error("SSL set FD failed", -1);
  if (SSL_set_tlsext_host_name(sslStruct, DOMAIN) == 0)
    error("SSL host name failed", -1);
  // forbid cipher
  if (SSL_set_cipher_list(sslStruct, "DEFAULT:!ECDHE-RSA-AES256-GCM-SHA384") ==
      0)
    error("SSL cipher forbid failed", -1);
  if (SSL_connect(sslStruct) <= 0)
    error("SSL connect failed", -1);
  if (SSL_get_verify_result(sslStruct) != X509_V_OK)
    error("SSL veryfiing result failed", -1);
  if (SSL_write(sslStruct, HTTP_REQ, strlen(HTTP_REQ)) <= 0)
    error("SSL write failed", -1);

  const char *cipherForPriority;
  int priority = 0;
  while (!(cipherForPriority = SSL_get_cipher_list(sslStruct, priority++)))
    printf("[SSL] Priority: %d, Cipher: '%s'", priority, cipherForPriority);

  const SSL_CIPHER *cipher = SSL_get_current_cipher(sslStruct);
  if (!cipher)
    error("SSL cipher failed", -1);
  printf("[SSL] Cipher: '%s'\n", SSL_CIPHER_get_name(cipher));

  X509 *certificate = NULL;
  if (!(certificate = SSL_get_peer_certificate(sslStruct)))
    error("SSL certificate failed", -1);
  FILE *foutCertificate = fopen("certificate.pem", "wb");
  if (!foutCertificate)
    error("Certificate file error", -1);
  PEM_write_X509(foutCertificate, certificate);

  unsigned char buffer[BUFF_SIZE];
  short bytesRead = 0;
  FILE *fout = fopen("page.txt", "wb");
  if (!fout)
    error("Page file error", -1);

  while (true) {
    if ((bytesRead = SSL_read(sslStruct, buffer, BUFF_SIZE)) <= 0)
      error("SSL read failed", -1);
    else if (bytesRead < BUFF_SIZE)
      break;
    fwrite(buffer, sizeof(unsigned char), bytesRead, fout);
  }

  SSL_shutdown(sslStruct);
  close(tcp_socket);
  SSL_free(sslStruct);
  SSL_CTX_free(ctx);
  fclose(fout);
  fclose(foutCertificate);
}

int main(void) {

  int tcp_socket;

  NetInit(&tcp_socket);
  SSLCommunication(tcp_socket);

  return 0;
}
/// Author: Bohumil Milacek
#include <fstream>
#include <iostream>
#include <openssl/evp.h>
#include <openssl/pem.h>
#include <openssl/rand.h>
#include <string>

using namespace std;

void handleError(const string &msg, const int exitNum = 1) {
  cout << msg << endl;
  exit(exitNum);
}

EVP_PKEY *loadPublicKey(const string &filepath) {
  FILE *fp = fopen(filepath.c_str(), "rb");
  if (!fp)
    handleError("Error loading public key file");
  EVP_PKEY *pubkey = PEM_read_PUBKEY(fp, NULL, NULL, NULL);
  if (!pubkey)
    handleError("Error reading public key");
  return pubkey;
}

EVP_PKEY *loadPrivateKey(const string &filepath) {
  FILE *fp = fopen(filepath.c_str(), "rb");
  if (!fp)
    handleError("Error loading private key file");
  EVP_PKEY *privkey = PEM_read_PrivateKey(fp, NULL, NULL, NULL);
  if (!privkey)
    handleError("Error reading private key");
  return privkey;
}

const EVP_CIPHER *getCipherByName(string const &cipherName) {
  const EVP_CIPHER *cipher = EVP_get_cipherbyname(cipherName.c_str());
  if (!cipher)
    handleError("Cipher doesn't exist");
  return cipher;
}

void seal(const string &finPath, const string &foutPath,
          const string &pubKeyPath, const string &cipherName) {

  FILE *fin = fopen(finPath.c_str(), "rb");
  FILE *fout = fopen(foutPath.c_str(), "wb");
  if (!fin || !fout)
    handleError("Error seal opening files");

  EVP_CIPHER_CTX *ctx;
  EVP_PKEY *pub_key = loadPublicKey(pubKeyPath);
  const EVP_CIPHER *cipher = getCipherByName(cipherName);
  unsigned char *my_ek = (unsigned char *)malloc(EVP_PKEY_size(pub_key));
  int my_ekl;
  unsigned char iv[EVP_MAX_IV_LENGTH];

  if (!(ctx = EVP_CIPHER_CTX_new()))
    handleError("Seal context error");

  if (1 != EVP_SealInit(ctx, cipher, &my_ek, &my_ekl, iv, &pub_key, 1))
    handleError("Seal init error");

  fwrite(cipherName.c_str(), sizeof(unsigned char), cipherName.length(), fout);
  fwrite("\n", sizeof("\n"), 1, fout);
  fwrite(iv, sizeof(unsigned char), EVP_MAX_IV_LENGTH, fout);
  fwrite(&my_ekl, sizeof(int), 1, fout);
  fwrite(my_ek, sizeof(unsigned char), my_ekl, fout);

  unsigned char ot[100];
  unsigned char st[200];
  int stLen = 0;
  int otLen = 0;

  while (otLen = fread(ot, sizeof(unsigned char), 100, fin)) {
    if (1 != EVP_SealUpdate(ctx, st, &stLen, ot, otLen))
      handleError("Seal update error");
    fwrite(st, sizeof(unsigned char), stLen, fout);
  }

  if (1 != EVP_SealFinal(ctx, st, &stLen))
    handleError("Seal final error");
  fwrite(st, sizeof(unsigned char), stLen, fout);

  /* Clean up */
  EVP_CIPHER_CTX_free(ctx);
  free(my_ek);
}

void open(const string &finPath, const string &foutPath,
          const string &privKeyPath) {

  FILE *fin = fopen(finPath.c_str(), "rb");
  FILE *fout = fopen(foutPath.c_str(), "wb");

  if (!fin || !fout)
    handleError("Error seal opening files");

  unsigned char *my_ek;
  int my_ekl;
  unsigned char iv[EVP_MAX_IV_LENGTH];

  EVP_CIPHER_CTX *ctx;
  EVP_PKEY *priv_key = loadPrivateKey(privKeyPath);
  const EVP_CIPHER *cipher;

  char line[256];
  fgets(line, sizeof(line), fin);
  fgetc(fin);
  string cipherName(line);
  if (cipherName[cipherName.length() - 1] == '\n')
    cipherName[cipherName.length() - 1] = '\0';
  cipher = getCipherByName(cipherName);
  fread(iv, sizeof(unsigned char), EVP_MAX_IV_LENGTH, fin);
  fread(&my_ekl, sizeof(int), 1, fin);
  my_ek = (unsigned char *)malloc(my_ekl);
  fread(my_ek, sizeof(unsigned char), my_ekl, fin);

  if (!(ctx = EVP_CIPHER_CTX_new()))
    handleError("Error open context");

  if (1 != EVP_OpenInit(ctx, cipher, my_ek, my_ekl, iv, priv_key))
    handleError("Error open init");

  unsigned char ot[200];
  unsigned char st[100];
  int stLen = 0;
  int otLen = 0;
  while (stLen = fread(st, sizeof(unsigned char), 100, fin)) {
    if (1 != EVP_OpenUpdate(ctx, ot, &otLen, st, stLen))
      handleError("Error open update");
    fwrite(ot, sizeof(unsigned char), otLen, fout);
  }

  if (1 != EVP_OpenFinal(ctx, ot, &otLen))
    handleError("Error open final");
  fwrite(ot, sizeof(unsigned char), otLen, fout);

  EVP_CIPHER_CTX_free(ctx);
  free(my_ek);
}

int main(int argc, char *argv[]) {
  if (argc < 5)
    handleError("Invalid number of arguments", 1);

  string option = argv[1];
  string keyPath = argv[2];
  string finPath = argv[3];
  string foutPath = argv[4];

  if (RAND_load_file("/dev/random", 32) != 32)
    handleError("Cannot seed the random generator!");
  OpenSSL_add_all_ciphers();

  if (option.compare("-e") == 0) {
    if (argc < 6)
      handleError("Invalid number of arguments", 1);
    string type = argv[5];
    seal(finPath, foutPath, keyPath, type);
  } else if (option.compare("-d") == 0)
    open(finPath, foutPath, keyPath);
  else
    handleError("Invalid option -> expectin -e/-d", 1);
  exit(0);
}

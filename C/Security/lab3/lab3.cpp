/// Author: Bohumil Milacek
#include <iostream>
#include <openssl/evp.h>
#include <string>

using namespace std;

class BMP {
    unsigned char key[EVP_MAX_KEY_LENGTH]; 
    unsigned char iv[EVP_MAX_IV_LENGTH];

    string mod;
    string ext = ".bmp";

    string filename;

    unsigned char header[54];

    const EVP_CIPHER *cipher;

public:
    BMP(string inFilename, string mod)
            : key("BigMac"), iv("McNugget"), mod(mod) {
        filename = inFilename.substr(0, inFilename.size() - 4);
        if (mod.compare("ecb") == 0)
            cipher = EVP_des_ecb();
        else if (mod.compare("cbc") == 0)
            cipher = EVP_des_cbc();
        else {
            cout << "Incorrect cipher" << endl;
            exit(20);
        }
    }

    void encrypt() {
        FILE *fin = fopen((filename + ext).c_str(), "rb");
        unsigned char header[54];
        if (fread(header, sizeof(unsigned char), 54, fin) != 54 ||
            !isHeaderValid(header)) {
            cout << "BMP is not in valid format" << endl;
            exit(1);
        }
        FILE *fout = fopen((filename + "_" + mod + ext).c_str(), "wb");
        fwrite(header, sizeof(unsigned char), 54, fout);

        int res;
        OpenSSL_add_all_ciphers();
        EVP_CIPHER_CTX *ctx; 
        ctx = EVP_CIPHER_CTX_new();
        if (ctx == NULL)
            exit(1);
        res = EVP_EncryptInit_ex(ctx, cipher, NULL, key, iv); 
        if (res != 1)
            exit(1);

        int otLength = 0;
        int stLength = 0;
        int fullOtLength = 54;      // starting alrady with header counted in 
        unsigned int fileSize = *(unsigned int *)&header[2];
        unsigned char *ot = new unsigned char[100];
        unsigned char *st = new unsigned char[110];

        while (otLength = fread(ot, sizeof(unsigned char), 100, fin)) {
            // Sifrovani
            res = EVP_EncryptUpdate(ctx, st, &stLength, ot, otLength);
            if (res != 1)
                exit(1);
            fwrite(st, sizeof(unsigned char), stLength, fout);
            fullOtLength += otLength;
        }

        if (fullOtLength != fileSize){
            fclose(fout);
            cout << "File data not in correct format"<< endl;
            if( remove( (filename + "_" + mod + ext).c_str() ) != 0 )
                cout << "Error occured" << endl;
            exit(1);
        }

        res = EVP_EncryptFinal_ex(ctx, st, &stLength); 
        if (res != 1)
            exit(1);
        fwrite(st, sizeof(unsigned char), stLength, fout);

        fclose(fin);
        fclose(fout);
        EVP_CIPHER_CTX_free(ctx);
    }

    void decrypt() {
        FILE *fin = fopen((filename + ext).c_str(), "rb");
        unsigned char header[54];
        if (fread(header, sizeof(unsigned char), 54, fin) != 54 ||
            !isHeaderValid(header)) {
            cout << "BMP is not in valid format" << endl;
            exit(1);
        }
        FILE *fout = fopen((filename + "_dec" + ext).c_str(), "wb");
        fwrite(header, sizeof(unsigned char), 54, fout);

        int res;
        OpenSSL_add_all_ciphers();

        EVP_CIPHER_CTX *ctx; // context structure
        ctx = EVP_CIPHER_CTX_new();
        if (ctx == NULL)
            exit(2);
        res = EVP_DecryptInit_ex(ctx, cipher, NULL, key, iv); // nastaveni
        if (res != 1)
            exit(2);

        int otLength = 0;
        int stLength = 0;
        unsigned char *ot = new unsigned char[100];
        unsigned char *st = new unsigned char[110];
        while (stLength = fread(st, sizeof(unsigned char), 100, fin)) {
            res = EVP_DecryptUpdate(ctx, ot, &otLength, st, stLength); // desifrovani st
            if (res != 1)
                exit(2);
            fwrite(ot, sizeof(unsigned char), otLength, fout);
        }
        res = EVP_DecryptFinal_ex(ctx, ot, &otLength); // dokonceni
        if (res != 1)
            exit(2);
        fwrite(ot, sizeof(unsigned char), otLength, fout);

        fclose(fin);
        fclose(fout);
        EVP_CIPHER_CTX_free(ctx);

    }

private:
    bool isHeaderValid(unsigned char (&header)[54]) const {
      unsigned int fileSize = *(unsigned int *)&header[2];
      unsigned int dataStart = *(int *)&header[10];

        if (header[0] != 'B' || header[1] != 'M' || dataStart>=fileSize)
            return false;
        return true;
    }
};

int main(int argc, char *argv[]) {
    if (argc < 4) {
        cout << "Invalid number of arguments\n" << endl;
        exit(1);
    }
    BMP bmp(argv[3], argv[2]);

    if (string(argv[1]).compare("-e") == 0)
        bmp.encrypt();
    else if (string(argv[1]).compare("-d") == 0)
        bmp.decrypt();
    else {
        cout << "Invalid option -> expectin -e/-d";
        exit(3);
    }
    exit(0);
}

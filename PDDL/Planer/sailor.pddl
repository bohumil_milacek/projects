(define (domain sailor)
(:requirements :strips :typing :disjunctive-preconditions)
(:types 
    LOCATION SHIP GOODS ALCOHOLISM - object
    GOLDEN_SEED GOLDEN_COIN GOLDEN_BAR - object
    RING WOOD FLOWER ALCOHOL MAP PEARL COCAINE - object
    HAPPINESS DRUNKENESS ADDICTION - object
    TOUGHED BEATEN_BEAR CRIME_RECORD BAD_CONNECTION GOOD_CONNECTION SMUGLERS_CONNECTION BECOME_CAPITAN BECOME_PIRATE BEATEN_PIRATES OVERWHELMED_GIRL - object
    MARRIED_MAN ADMIRAL COCAINE_KING - object

    COCONUT LEATHER - GOODS
    WATER GROUND - LOCATION
    LIGHTBEAM SEA ISLAND HARBOUR - WATER
    HARBOUR PUB RIVER FOREST CITY ACADEMY - GROUND
    BOAT FREGATA CARAVEL - SHIP

)
(:predicates
    (path ?from ?to)
    (player-at ?where)
    (at ?what ?where)
    (own ?item)
    (has ?attr)
    (is ?what)
)

;BEGIN_MOVEMENT_______________________________________________________________________________________________
(:action MOVE_WATER
    :parameters (?loc1 - WATER ?loc2 - WATER ?ship - SHIP)
    :precondition
    (and
	    (player-at ?loc1)
	    (path ?loc1 ?loc2)
        (own ?ship)
    )
    :effect 
    (and 
        (not (player-at ?loc1))  
        (player-at ?loc2)
    )
)
(:action MOVE_GROUND
    :parameters (?loc1 - GROUND ?loc2 - GROUND)
    :precondition 
    (and
	    (player-at ?loc1)
	    (path ?loc1 ?loc2)
	)
    :effect 
    (and 
        (not (player-at ?loc1))
        (player-at ?loc2)
    )
)
;END_MOVEMENT_______________________________________________________________________________________________

;BEGIN_ALCOHOL_______________________________________________________________________________________________
(:action DRINK1
    :parameters (?alcohol - ALCOHOL ?happiness - HAPPINESS)
    :precondition (own ?alcohol)
    :effect 
    (and 
        (not (own ?alcohol))
        (has ?happiness)
    )
)
(:action DRINK2
    :parameters (?alcohol - ALCOHOL ?happiness - HAPPINESS ?drunkeness - DRUNKENESS)
    :precondition 
    (and
        (own ?alcohol)
        (has ?happiness)
    )
    :effect 
    (and 
        (not (own ?alcohol))
        (not (has ?happiness))
        (has ?drunkeness)
    )
)
(:action DRINK3
    :parameters (?alcohol - ALCOHOL ?drunkeness - DRUNKENESS ?addiction - ADDICTION)
    :precondition 
    (and
        (own ?alcohol)
        (has ?drunkeness)
    )
    :effect 
    (and 
        (not (own ?alcohol))
        (not (has ?drunkeness))
        (has ?addiction)
    )
)
;END_ALCOHOL_______________________________________________________________________________________________


;BEGIN_CRAFTING_______________________________________________________________________________________________
(:action MAKE_BOAT
    :parameters (?wood - WOOD ?boat - BOAT)
    :precondition 
    (and 
        (own ?wood)
        (not (own ?boat))
    )
    :effect 
    (and 
        (not(own ?wood))
        (own ?boat)
    )
)
(:action MAKE_FREGATA
    :parameters (?wood - WOOD ?boat - BOAT ?golden_seed - GOLDEN_SEED ?fregata - FREGATA)
    :precondition 
    (and
	    (own ?wood)
        (own ?boat)
        (own ?golden_seed)
	)
    :effect 
    (and 
        (not (own ?wood))
        (not (own ?boat))
        (not (own ?golden_seed))
        (own ?fregata)
    )
)
(:action MAKE_CARAVEL
    :parameters (?boat - BOAT ?wood - WOOD ?golden_coin - GOLDEN_COIN ?caravel - CARAVEL)
    :precondition 
    (and
	    (own ?wood)
        (own ?boat)
        (own ?golden_coin)
	)
    :effect 
    (and 
        (not (own ?wood))
        (not (own ?boat))
        (not (own ?golden_coin))
        (own ?caravel)
    )
)
(:action MAKE_RING
    :parameters (?golden_bar - GOLDEN_BAR ?pearl - PEARL ?ring - RING)
    :precondition 
    (and
	    (own ?golden_bar)
        (own ?pearl)
	)
    :effect 
    (and 
        (not (own ?golden_bar))
        (not (own ?pearl))
        (own ?ring)
    )
)
;END_CRAFTING_______________________________________________________________________________________________

;BEGIN_FOREST_ACTIONS_______________________________________________________________________________________________
(:action CHOP_WOOD_FOREST
    :parameters (?forest - FOREST ?wood - WOOD)
    :precondition (player-at ?forest)
    :effect (own ?wood)
)
(:action PICK_FLOWER
    :parameters (?forest - FOREST ?flower - FLOWER)
    :precondition (player-at ?forest)
    :effect (own ?flower)
)
(:action FIGHT_BEAR
    :parameters (?forest - FOREST ?leather - LEATHER ?toughed - TOUGHED ?beaten_bear - BEATEN_BEAR)
    :precondition (player-at ?forest)
    :effect
    (and
        (own ?leather)
        (has ?toughed)
        (has ?beaten_bear)    
    )
)
(:action MEET_MAGIC_GRANDPA
    :parameters (?forest - FOREST ?alcohol - ALCOHOL ?map - MAP ?bad_connection - BAD_CONNECTION)
    :precondition 
    (and 
        (player-at ?forest)
        (own ?alcohol)
        (at ?map ?forest)
    )
    :effect
    (and
        (not (own ?alcohol))
        (own ?map)
        (has ?bad_connection)
    )
)
;END_FOREST_ACTIONS_______________________________________________________________________________________________

;BEGIN_RIVER_ACTIONS_______________________________________________________________________________________________
(:action STEAL_BOAT
    :parameters (?river - RIVER ?boat - BOAT ?crime_record - CRIME_RECORD)
    :precondition (and
        (player-at ?river)
        (at ?boat ?river)
    )
    :effect 
    (and
        (own ?boat)
        (has ?crime_record)
    )
)
(:action PURIFY_GOLD
    :parameters (?river - RIVER ?golden_seed - GOLDEN_SEED)
    :precondition
    (and 
        (player-at  ?river)
        (not (own ?golden_seed))
    )
    :effect (own ?golden_seed)
)
(:action TAKE_BATH_RIVER
    :parameters (?river - RIVER ?drunk_mood - DRUNKENESS)
    :precondition
    (and 
        (player-at  ?river)
        (has ?drunk_mood)
    )
    :effect (not (has ?drunk_mood))
)
;END_RIVER_ACTIONS_______________________________________________________________________________________________

;BEGIN_HARBOUR_ACTIONS_______________________________________________________________________________________________
(:action WORK
    :parameters (?harbour - HARBOUR ?golden_seed - GOLDEN_SEED)
    :precondition(player-at ?harbour)
    :effect (own ?golden_seed)
)
(:action TRADE
    :parameters (?harbour - HARBOUR ?goods - GOODS ?golden_coin - GOLDEN_COIN)
    :precondition 
    (and
        (player-at ?harbour)
        (own ?goods)
    )
    :effect
    (and
        (not (own ?goods))
        (own ?golden_coin)
    )
)
(:action MEET_SMUGLERS
    :parameters (?harbour - HARBOUR ?bad_connection - BAD_CONNECTION ?golden_bar - GOLDEN_BAR ?smuglers_connection - SMUGLERS_CONNECTION)
    :precondition
    (and
        (player-at ?harbour)
        (own ?golden_bar)
        (has ?bad_connection)
    )
    :effect (has ?smuglers_connection)
)
;END_HARBOUR_ACTIONS_______________________________________________________________________________________________

;BEGIN_PUB_ACTIONS_______________________________________________________________________________________________
(:action BUY_ALCOHOL
    :parameters (?pub - PUB ?alcohol - ALCOHOL ?golden_seed - GOLDEN_SEED)
    :precondition
    (and
        (player-at ?pub)
        (own ?golden_seed)
    )
    :effect
    (and
        (not (own ?golden_seed))
        (own ?alcohol)
    )
)
(:action BUY_ALCOHOL_FOR_EVERYONE
    :parameters (?pub - PUB ?golden_coin - GOLDEN_COIN ?good_connection - GOOD_CONNECTION)
    :precondition
    (and
        (player-at ?pub)
        (own ?golden_coin)
    )
    :effect
    (and
        (not (own ?golden_coin))
        (has ?good_connection)
    )
)
(:action FIGHT_IN_PUB
    :parameters (?pub - PUB ?toughed - TOUGHED ?happiness - HAPPINESS)
    :precondition
    (and
        (player-at ?pub)
        (has ?happiness)
    )
    :effect (has ?toughed)
)
;END_PUB_ACTIONS_______________________________________________________________________________________________

;BEGIN_CITY_ACTIONS_______________________________________________________________________________________________
(:action SAVE
    :parameters (?city - CITY ?golden_seed - GOLDEN_SEED ?golden_coin - GOLDEN_COIN ?good_connection - GOOD_CONNECTION)
    :precondition
    (and
        (player-at ?city)
        (own ?golden_seed)
    )
    :effect
    (and
        (not (own ?golden_seed))
        (own ?golden_coin)
        (has ?good_connection)
    )
)
(:action INVEST
    :parameters (?city - CITY ?golden_coin - GOLDEN_COIN ?golden_bar - GOLDEN_BAR ?good_connection - GOOD_CONNECTION)
    :precondition
    (and
        (player-at ?city)
        (own ?golden_coin)
    )
    :effect
    (and
        (not (own ?golden_coin))
        (own ?golden_bar)
        (has ?good_connection)
    )
)
(:action STEAL
    :parameters (?city - CITY ?golden_coin - GOLDEN_COIN ?crime_record - CRIME_RECORD)
    :precondition 
    (and 
        (player-at ?city)
        (not (own ?golden_coin))
    )
    :effect
    (and
        (own ?golden_coin)
        (has ?crime_record)
    )
)
(:action REMOVE_CRIME_RECORD_BUYOUT
    :parameters (?city - CITY ?golden_seed - GOLDEN_SEED ?crime_record - CRIME_RECORD)
    :precondition 
    (and 
        (player-at ?city)
        (own ?golden_seed)
        (has ?crime_record)
    )
    :effect
    (and
        (not (own ?golden_seed))
        (not (has ?crime_record))
    )
)
(:action REMOVE_CRIME_RECORD_WORK
    :parameters (?city - CITY ?happiness - HAPPINESS ?crime_record - CRIME_RECORD)
    :precondition 
    (and 
        (player-at ?city)
        (has ?crime_record)
    )
    :effect
    (and
        (not (has ?crime_record))
        (has ?happiness)
    )
)
;END_CITY_ACTIONS_______________________________________________________________________________________________

;BEGIN_ACADEMY_ACTIONS_______________________________________________________________________________________________
(:action STUDY
    :parameters (?academy - ACADEMY ?golden_coin - GOLDEN_COIN ?crime_record - CRIME_RECORD ?become_capitan - BECOME_CAPITAN)
    :precondition
    (and 
        (player-at ?academy)
        (not (has ?crime_record))
        (own ?golden_coin)
    )
    :effect
    (and 
        (has ?become_capitan) 
        (not (own ?golden_coin))
    )
)
;END_ACADEMY_ACTIONS_______________________________________________________________________________________________

;BEGIN_SEA_ACTIONS_______________________________________________________________________________________________
(:action FIGHT_PIRATES_LOSE
    :parameters (?sea - SEA ?toughed - TOUGHED ?golden_bar - GOLDEN_BAR ?golden_coin - GOLDEN_COIN ?golden_seed - GOLDEN_SEED ?fregata - FREGATA ?caravel - CARAVEL)
    :precondition
    (and 
        (player-at ?sea)
        (not (has ?toughed))
    )
    :effect
    (and 
        (has ?toughed)
        (not (own ?golden_bar))
        (not (own ?golden_coin))
        (not (own ?golden_seed))
        (not (own ?fregata))
        (not (own ?caravel))
    )
)
(:action FIGHT_PIRATES_WIN
    :parameters (?sea - SEA ?golden_bar - GOLDEN_BAR ?golden_coin - GOLDEN_COIN ?golden_seed - GOLDEN_SEED ?toughed - TOUGHED ?caravel - CARAVEL ?fregata - FREGATA ?boat - BOAT ?beaten_pirates - BEATEN_PIRATES)
    :precondition
    (and 
        (player-at ?sea)
        (has ?toughed)
        (own ?caravel)
    )
    :effect
    (and 
        (own ?golden_bar)
        (own ?golden_coin)
        (own ?golden_seed)
        (own ?fregata)
        (own ?boat)
        (has ?beaten_pirates)    
    )
)
(:action JOIN_PIRATES
    :parameters (?sea - SEA ?bad_connection - BAD_CONNECTION ?become_pirate - BECOME_PIRATE ?happiness - HAPPINESS)
    :precondition
    (and 
        (player-at ?sea)
        (has ?bad_connection)
    )
    :effect
    (and 
        (has ?become_pirate)
        (has ?happiness)
    )
)
(:action TAKE_BATH_SEA
    :parameters (?sea - SEA  ?drunkeness - DRUNKENESS)
    :precondition (player-at ?sea)
    :effect (not (has ?drunkeness))
)
(:action DIVE_FOR_PEARL
    :parameters (?sea - SEA ?pearl - PEARL)
    :precondition (player-at ?sea)
    :effect (own ?pearl)
)
;END_SEA_ACTIONS_______________________________________________________________________________________________

;BEGIN_LIGHTBEAM_ACTIONS_______________________________________________________________________________________________
(:action OVERWHELM_GIRL
    :parameters (?lightbeam - LIGHTBEAM ?beaten_bear - BEATEN_BEAR ?become_capitan - BECOME_CAPITAN ?beaten_pirates - BEATEN_PIRATES ?overwhelmed_girl - OVERWHELMED_GIRL)
    :precondition
    (and 
        (player-at ?lightbeam)
        (or 
            (has ?beaten_bear)
            (has ?become_capitan)
            (has ?beaten_pirates)
        )
    )
    :effect (has ?overwhelmed_girl)
)
;END_LIGHTBEAM_ACTIONS_______________________________________________________________________________________________

;BEGIN_ISLAND_ACTIONS_______________________________________________________________________________________________
(:action COLLECT_COCONUTS
    :parameters (?island - ISLAND ?coconut - COCONUT)
    :precondition (player-at ?island)
    :effect (own ?coconut)
)
(:action CHOP_WOOD_ISLAND
    :parameters (?island - ISLAND ?wood - WOOD)
    :precondition (player-at ?island)
    :effect (own ?wood)
)
(:action FIND_COCAINE
    :parameters (?island - ISLAND ?map - MAP ?cocaine - COCAINE)
    :precondition
    (and 
        (player-at ?island)
        (own ?map)
    )
    :effect (own ?cocaine)
)
;END_ISLAND_ACTIONS_______________________________________________________________________________________________


;BEGIN_ENDINGS_______________________________________________________________________________________________
(:action MARRY_GIRL
    :parameters (?island - ISLAND ?overwhelmed_girl - OVERWHELMED_GIRL ?ring - RING ?flower - FLOWER ?married_man - MARRIED_MAN ?good_connection - GOOD_CONNECTION ?crime_record - CRIME_RECORD ?drunkeness - DRUNKENESS ?addiction - ADDICTION)
    :precondition
    (and 
        (player-at ?island)
        (has ?overwhelmed_girl)
        (own ?ring)
        (own ?flower)
        (has ?good_connection)
        (not (has ?crime_record))
        (not (has ?drunkeness))
        (not (has ?addiction))
    )
    :effect (is ?married_man)
)
(:action BECOME_ADMIRAL
    :parameters (?academy - ACADEMY ?become_capitan - BECOME_CAPITAN ?beaten_pirates - BEATEN_PIRATES ?h - HAPPINESS ?d - DRUNKENESS ?a - ADDICTION ?admiral - ADMIRAL)
    :precondition
    (and 
        (player-at ?academy)
        (has ?become_capitan)
        (has ?beaten_pirates)
        (not (has ?h))
        (not (has ?d))
        (not (has ?a))
    )
    :effect (is ?admiral)
)
(:action COCAINE_END
    :parameters (?cocaine - COCAINE ?addiction - ADDICTION ?fregata - FREGATA ?smuglers_connection - SMUGLERS_CONNECTION ?golden_bar - GOLDEN_BAR ?cocaine_king - COCAINE_KING)
    :precondition
    (and 
        (own ?cocaine)
        (has ?addiction)
        (own ?fregata)
        (has ?smuglers_connection)
        (own ?golden_bar)
    )
    :effect (is ?cocaine_king)
)
;END_ENDINGS_______________________________________________________________________________________________

)
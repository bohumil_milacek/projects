(define (problem sailor_get_maried)
   (:domain sailor)
    (:requirements :strips :typing :disjunctive-preconditions)
   (:objects

     lightbeam                - LIGHTBEAM
     sea                      - SEA
     island                   - ISLAND
     pub                      - PUB
     river                    - RIVER
     forest                   - FOREST
     city                     - CITY
     academy                  - ACADEMY
     harbour                  - HARBOUR

     boat                     - BOAT
     fregata                  - FREGATA
     caravel                  - CARAVEL

     map                      - MAP
     wood                     - WOOD
     flower                   - FLOWER
     alcohol                  - ALCOHOL
     pearl                    - PEARL
     leather                  - LEATHER
     coconut                  - COCONUT
     ring                     - RING
     cocaine                  - COCAINE

     golden_seed              - GOLDEN_SEED
     golden_coin              - GOLDEN_COIN
     golden_bar               - GOLDEN_BAR

     toughed                  - TOUGHED
     beaten_bear              - BEATEN_BEAR
     crime_record             - CRIME_RECORD
     bad_connection           - BAD_CONNECTION
     good_connection          - GOOD_CONNECTION
     smuglers_connection      - SMUGLERS_CONNECTION
     become_capitan           - BECOME_CAPITAN
     beaten_pirates           - BEATEN_PIRATES
     become_pirate            - BECOME_PIRATE
     overwhelmed_girl         - OVERWHELMED_GIRL

     happiness                - HAPPINESS
     drunkeness               - DRUNKENESS
     addiction                - ADDICTION

     cocaine_king             - COCAINE_KING
     admiral                  - ADMIRAL
     married_man              - MARRIED_MAN

  )
     (:init

          (player-at harbour)

          (at map forest)
          (at pearl sea)
          (at boat river)
          
          (path forest river)     (path river forest)
          (path river harbour)     (path harbour river)
          (path pub harbour)       (path harbour pub)
          (path harbour city)      (path city harbour)
          (path city academy)      (path academy city)
          (path lightbeam harbour) (path harbour lightbeam)
          (path harbour sea)       (path sea harbour)
          (path sea lightbeam)     (path lightbeam sea)
          (path sea island)        (path island sea)

          
     )
     (:goal (is married_man))
)
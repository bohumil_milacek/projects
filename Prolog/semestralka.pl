% getRowsCount(+SUDOKU , -ROW_COUNT) :-
% counts number of sudoku rows, returns false if sudoku doesn't contain all rows
getRowsCount([], 0).
getRowsCount([ROW|T], ROW_COUNT) :-
	is_list(ROW),
	getRowsCount(T, PREV_ROW_COUNT),
	ROW_COUNT is PREV_ROW_COUNT + 1.
%_______________________________________________________________________________

% getCollsCount(+ROW , -COLLS_COUNT) :-
% counts number of sudoku colls, returns false if row contains another sublists
getCollsCount([],0).
getCollsCount([H|T], COLLS_COUNT) :-
	not(is_list(H)),
	(integer(H);
	H == '-'),
	getCollsCount(T, PREV_COLLS_COUNT),
	COLLS_COUNT is PREV_COLLS_COUNT + 1.
%_______________________________________________________________________________

% checkSudoku(+SUDOKU) :-
% checks wether columns equal rows, returns the N of the sudoku
checkSudoku([ROW|T], N) :-
	getCollsCount(ROW, COLLS_COUNT),
	checkSudoku(T, COLLS_COUNT, 1),
	getSqrt(COLLS_COUNT, N).

checkSudoku([], COLLS_COUNT, COLLS_COUNT).

checkSudoku([ROW|T], COLLS_COUNT, ROW_COUNT) :-
	getCollsCount(ROW, ACTUAL_COLLS_COUNT),
	(ACTUAL_COLLS_COUNT == COLLS_COUNT),
	NEXT_ROW_COUNT is ROW_COUNT + 1,
    checkSudoku(T, COLLS_COUNT, NEXT_ROW_COUNT).




%_______________________________________________________________________________

% getSqrt(+NUM, -SQRT)
% checks if number has integer sqrt and returns sqrt
getSqrt(NUM, SQRT) :-
	sqrt(NUM, SQRT_FLOAT),
	floor(SQRT_FLOAT, SQRT),
	(SQRT_FLOAT-SQRT) =:= 0.


%_______________________________________________________________________________
%unresolvedPositions(+SUDOKU, -ROW, -COL):-
% returns column and row of an unresolved position
unresolvedPositions(SUDOKU, ROW, COL):-
  nth0(ROW, SUDOKU, SUDOKU_ROW),
  nth0(COL, SUDOKU_ROW, '-').


%_______________________________________________________________________________
% returns N-th row of Matrix or N-th column of Matrix
rowN([H|_],0,H):-!.
rowN([_|T],I,X) :-
    I1 is I-1,
    rowN(T,I1,X).

columnN([],_,[]).
columnN([H|T], I, [R|X]):-
    rowN(H, I, R),
	columnN(T,I,X).

%_______________________________________________________________________________
% replaces E-lement in matrix in specific ROW and COLUMN, returns RES-olution
matrixReplace(MATRIX, ROW, COL, E, RES) :-
	matrixReplace(MATRIX, ROW, COL, 0, E, RES).

matrixReplace([], _, _, _, _, []).
matrixReplace([H_ROW|MATRIX], ROW, COL, ACC, E, [H_ROW_RES|RES]) :-
	(ACC == ROW),
	ACC_NEXT is ACC + 1,
	rowReplace(H_ROW, COL, E, H_ROW_RES), !,
	matrixReplace(MATRIX, ROW, COL, ACC_NEXT, E, RES).

matrixReplace([H_ROW|MATRIX], ROW, COL, ACC, E, [H_ROW|RES]) :-
	ACC_NEXT is ACC + 1,
	matrixReplace(MATRIX, ROW, COL, ACC_NEXT, E, RES).

rowReplace([_|T], 0, X, [X|T]).
rowReplace([H|T], I, X, [H|R]):- I > -1, NI is I-1, rowReplace(T, NI, X, R), !.
rowReplace(L, _, _, L).

%___________________________________________________________________________________
solve(SUDOKU) :-
	checkSudoku(SUDOKU, N),
	solve(SUDOKU, N), !.

solve(SUDOKU, N) :-
	unresolvedPositions(SUDOKU, ROW, COL), !,
	getAvailableNumber(SUDOKU, N, ROW, COL, X),
	matrixReplace(SUDOKU,ROW, COL, X, RES),
	sleep(0.02), nl,
	print_matrix(RES),
	solve(RES, N).

solve(SUDOKU, _) :-
	not(unresolvedPositions(SUDOKU, _, _)),
	write("resolution found__________________________"), nl,
	print_matrix(SUDOKU).


getAvailableNumber(SUDOKU, N, ROW, COL, X) :-
	N_POW is N * N,
	rowN(SUDOKU, ROW, N_ROW),
    columnN(SUDOKU, COL, N_COL),
    between(1, N_POW, X),
	not(member(X,N_ROW)),
	not(member(X,N_COL)),
	getBlockPos(N, COL, ROW, COL_START, COL_END, ROW_START, ROW_END),
	getBlock(SUDOKU, ROW_START, ROW_END, COL_START, COL_END, BLOCK_NUMBERS),
	not(member(X,BLOCK_NUMBERS)).

print_matrix([]).
print_matrix([H|T]) :- write(H), nl, print_matrix(T).

getBlockPos(N, COL, ROW, COL_START, COL_END, ROW_START, ROW_END) :-
	floor(COL/N, COL_START_BLOCK),
	floor(ROW/N, ROW_START_BLOCK),
	COL_START is (N * COL_START_BLOCK),
	COL_END is (COL_START + N -1),
	ROW_START is (N * ROW_START_BLOCK),
    ROW_END is (ROW_START + N -1).


getElementsBetween(ROW, START, END, RES) :-
	getElementsBetween(ROW, START, END, 0, RES) , !.

getElementsBetween([], _, _, _,[]).
getElementsBetween([H|T], START, END, POS, [H|RES]) :-
	(START =< POS),
	(POS =< END),
	NEXT_POS is POS + 1,
	getElementsBetween(T, START, END, NEXT_POS, RES).

getElementsBetween([_|T], START, END, POS, RES) :-
	NEXT_POS is POS + 1,
	getElementsBetween(T, START, END, NEXT_POS, RES).

getBlock(SUDOKU, ROW_START, ROW_END, COL_START, COL_END, RES) :-
	getElementsBetween(SUDOKU, ROW_START, ROW_END, 0, LIST_OF_ROWS),
	concatColls(LIST_OF_ROWS,COL_START,COL_END, RES_COLLS),
	flatten(RES_COLLS, RES), !.

concatColls([],_,_, []).
concatColls([ROW|ROWS],COL_START,COL_END, [RES_H|RES_T]) :-
	concatColls(ROWS,COL_START,COL_END, RES_T),
	getElementsBetween(ROW,COL_START,COL_END, RES_H).











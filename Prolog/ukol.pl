%SECOND MIN
%--------------------------------------------------------------------
secondMin([X|REST],MINIMUM) :-
    secondMin(REST,X,MINIMUM).

secondMin([X|REST],MIN,MINIMUM) :-
    (X<MIN, secondMin(REST,X,MIN,MINIMUM));
    (X\=MIN, secondMin(REST,MIN,X,MINIMUM));
    secondMin(REST,MIN,MINIMUM).

secondMin([X|REST],MIN1,MIN2,MINIMUM) :-
    (X<MIN1, secondMin(REST,X,MIN1,MINIMUM));
    (X<MIN2, X\=MIN1, secondMin(REST,MIN1,X,MINIMUM));
    secondMin(REST,MIN1,MIN2,MINIMUM).

secondMin([],_,MIN2,MIN2) :- !.


%MATRIX ADD
%--------------------------------------------------------------------
matrixAdd([],[],[]).
matrixAdd([ROW1|ROWS1],[ROW2|ROWS2], [ROW_RESULT|RESULT]) :-
	rowAdd(ROW1,ROW2,ROW_RESULT),
	matrixAdd(ROWS1,ROWS2,RESULT).


rowAdd([],[],[]).
rowAdd([H1|T1],[H2|T2],[RES|RESULT]) :-
	RES is H1 + H2,
	rowAdd(T1,T2,RESULT).

%--------------------------------------------------------------------


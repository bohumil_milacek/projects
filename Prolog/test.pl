male(albert).
male(bob).
male(bill).
male(carl).
male(charlie).
male(dan).
male(edward).

female(alice).
female(betsy).
female(diana).


happy(albert).
happy(alice).
happy(bob).
happy(bill).
with_albert(alice).

runs(X) :- happy(X).

dances(X) :- happy(X), with_albert(X).

does_dance(X) :- dances(X), write('X Dances ').


does_alice_dance :- dances(alice), write('Alice dances when she is happy and with albert').


/*
swims(X) :- happy(X).
swims(X) :- near_water(X).
*/



parent(albert,bob).
parent(albert,betsy).
parent(albert,bill).

parent(alice,bob).
parent(alice,betsy).
parent(alice,bill).

parent(bob,carl).
parent(bob,charlie).

pair(X,Y) :- parent(X,bob) , male(X), parent(Y,bob), female(Y).

grandchild(X) :- parent(X,Y),
                 parent(Y,Z),
                 format("~w grandparent ~s grandchildren ~w ~n",[X,"has",Z]).

/*
brother(X,Y) :- parent(P, X), parent(P, Y), male(X), male(Y), X\=Y.
*/

brother(bob,bill).


uncle(CHILD) :- parent(PARENT,CHILD),
                brother(PARENT,UNCLE),
                format("~w has uncle ~w",[CHILD,UNCLE]).











































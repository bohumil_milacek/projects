male(albert).
male(bob).
male(bill).
male(carl).
male(charlie).
male(dan).
male(edward).

female(alice).
female(betsy).
female(diana).

brother(albert, bob).
brother(carl, charlie).
brother(edward, dan).
brother(dan, edward).
brother(betsy, dan).
brother(alice, dan).
brother(diana, bill).

sister(dan, alice).
sister(dan, betsy).

sibling(X) :- brother(X,Y) , format("~w",Y)
           ; sister(X,Z) , format("~w",Z).

what_grade(5) :- format("Go to kindergarden").

what_grade(6) :- format("Go to elementary").

what_grade(Other) :-
        Grade is 10,
        format("Go to ~w",Grade).




owns(albert,pet(cat,olive)).


customer(smith, tom, 12.20).
customer(jenny, hawkins, 128.30).


get_customer_bal(Fname,LName):-
    customer(Fname,LName,Bal),
    write(Fname),
    tab(1),
    format(" owes ~w",Bal).


vertical(
    line(
        point(X,Y1),
          point(X,Y2)
      )
  ).


horizontal(
    line(
        point(X1,Y),
          point(X2,Y)
      )
 ).
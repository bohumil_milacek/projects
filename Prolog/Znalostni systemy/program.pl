% muzi

muz(deda_ze_san).
muz(jaroslav_prokes).
muz(bohumil_milacek_deda).
muz(bohumil_milacek_otec).
muz(bohumil_milacek_ja).
muz(radek_letninsky).
muz(zdenek_konvalina).
muz(david_tichy).
muz(jakub_letninsky).
muz(tomas_konvalina).
muz(stepan_konvalina).
muz(tomas_tichy).


% zeny

zena(babicka_ze_san).
zena(iva_prokesova).
zena(jana_milackova).
zena(jaroslava_letninska).
zena(iva_milackova_mamka).
zena(iva_milackova_sestra).
zena(jitka_konvalinova).
zena(liba_ticha).
zena(sara_letninska).
zena(sarka_ticha).


% vztah manzelstvi

manzelka(babicka_ze_san, deda_ze_san).
manzelka(iva_prokesova, jaroslav_prokes).
manzelka(jana_milackova, bohumil_milacek_deda).
manzelka(jaroslava_letninska, radek_letninsky).
manzelka(iva_milackova_mamka, bohumil_milacek_otec).
manzelka(jitka_konvalinova, zdenek_konvalina).
manzelka(liba_ticha, david_tichy).



% vztah otcovstvi

otec(deda_ze_san,jaroslav_prokes).

otec(jaroslav_prokes,jitka_konvalinova).
otec(jaroslav_prokes,iva_milackova_mamka).
otec(jaroslav_prokes,jaroslava_letninska).

otec(radek_letninsky,jakub_letninsky).
otec(radek_letninsky,sara_letninska).

otec(zdenek_konvalina,tomas_konvalina).
otec(zdenek_konvalina,stepan_konvalina).

otec(bohumil_milacek_deda,bohumil_milacek_otec).
otec(bohumil_milacek_deda,liba_ticha).

otec(bohumil_milacek_otec,iva_milackova_sestra).
otec(bohumil_milacek_otec,bohumil_milacek_ja).

otec(david_tichy,sarka_ticha).
otec(david_tichy,tomas_tichy).


% vztah materstvi

matka(babicka_ze_san,jaroslav_prokes).

matka(iva_prokesova,jitka_konvalinova).
matka(iva_prokesova,iva_milackova_mamka).
matka(iva_prokesova,jaroslava_letninska).

matka(jaroslava_letninska,jakub_letninsky).
matka(jaroslava_letninska,sara_letninska).

matka(jitka_konvalinova,tomas_konvalina).
matka(jitka_konvalinova,stepan_konvalina).

matka(jana_milackova,bohumil_milacek_otec).
matka(jana_milackova,liba_ticha).

matka(iva_milackova_mamka,iva_milackova_sestra).
matka(iva_milackova_mamka,bohumil_milacek_ja).

matka(liba_ticha,sarka_ticha).
matka(liba_ticha,tomas_tichy).

% vztahy definovane na zaklade predchozich pomoci pravidel

bratr(X,Y) :-
   muz(X),otec(O,X),matka(M,X),otec(O,Y),matka(M,Y).

sestra(X,Y) :-
   zena(X),otec(O,X),matka(M,X),otec(O,Y),matka(M,Y).

deda(X,Y) :-
   muz(X),otec(X,Z),(otec(Z,Y);matka(Z,Y)).

baba(X,Y) :-
   zena(X),matka(X,Z),(otec(Z,Y);matka(Z,Y)).

vnuk(X,Y) :-
   muz(X),(deda(Y,X);baba(Y,X)).

vnucka(X,Y) :-
   zena(X),(deda(Y,X);baba(Y,X)).

rodic(X,Y) :- otec(X,Y);matka(X,Y).

praded(X,Y) :- otec(X,Z),(deda(Z,Y);baba(Z,Y)).

prababa(X,Y) :- matka(X,Z),(deda(Z,Y);baba(Z,Y)).		

/* Program vyuzivajici rekurzi - korektni, 2 varianty */

% zjistovani predku a potomku

predek1(X,Y) :- rodic(X,Y).
predek1(X,Y) :- rodic(Z,Y),predek1(X,Z).

predek2(X,Y) :- rodic(X,Y).
predek2(X,Y) :- rodic(X,Z),predek2(Z,Y).

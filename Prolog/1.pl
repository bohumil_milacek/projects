bigger(elephant, horse).
bigger(horse, donkey).
bigger(donkey, dog).
bigger(donkey, monkey).
bigger(strange_animal, strange_animal).
bigger(X, Y) :- bigger(X, Z), bigger(Z, Y).

muz(bart).
muz(homer).
muz(abraham).
muz(clancy).
muz(alfred).

zena(mona).
zena(marge).
zena(liza).
zena(meggie).
zena(selma).
zena(patty).
zena(jacqueline).

rodic(homer,bart).
rodic(homer,liza).
rodic(homer,meggie).
rodic(homer,alfred).
rodic(abraham,homer).
rodic(marge,bart).
rodic(marge,liza).
rodic(marge,meggie).
rodic(mona,homer).
rodic(jacqueline,marge).
rodic(jacqueline,patty).
rodic(jacqueline,selma).

manzel(homer,marge).
manzel(abraham,mona).
manzel(clancy,jacqueline).


matka(M,D) :- rodic(M,D),zena(M).
otec(O,D) :- rodic(O,D),muz(O).

sourozenci(X,Y) :- rodic(R1,X), rodic(R1,Y), rodic(R2,Y), R1\=R2, X\=Y,

teta(T,D) :- rodic(R,D), sourozenci(R,T), zena(T).

nenalezi(X,[]).
nenalezi(X,[X|R]) :- X\=Y, nenalezi(X,R).

%comment
delka([],0).
delka([X,S],D) :- delka(S,D1), D is D1+1.

cetnost(_, [], 0).
cetnost(P,[X|S],C) :- cetnost(P,S,C1), X2 == P, C1 is C1+1.
cetnost(P,[X|S],C) :- cetnost(P,S,C), X2 \= P.


vloz(P,S,[P|S]).

vloz_na_konec(P,[],[P]).
vloz_na_konec(P,[Y|S],[Y|S2]) :- vloz_na_konec(P,S,S2).

spoj([],S1,S1).
spoj(S1,[],S1).
spoj([X|S1],S2,[X|S]) :- spoj(S1,S2,S).



otoc(S1,S2) :- otoc(S1,S2,[]).
otoc([],A,A).
otoc([X|S1],S2,A) :- otoc(S1,S2,[X|A]).


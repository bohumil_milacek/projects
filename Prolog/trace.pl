warm_blooded(penguin).
warm_blooded(human).

produce_milk(penguin).
produce_milk(human).

have_fethers(penguin).
have_hair(human).

mamal(X) :-
    warm_blooded(X),
    produce_milk(X),
    have_hair(X).


say_hi :-
    read(X),
    format('Say hi to ~w',X).


count_to_20(20) :- write(20).

count_to_20(X) :-
    write(X),nl,
    Y is (X*X*X),
    count_to_20(Y).

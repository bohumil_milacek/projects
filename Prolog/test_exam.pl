split_list(List, Left, Right) :-
	split_list(List,List,Left,Right), !.
split_list(T, [],[], T).
split_list([H|T1], [_,_|T2], [H|Left], Right) :-
	split_list(T1, T2, Left, Right).
split_list([H|T1], [_|T2], [H|Left], Right) :-
	split_list(T1, T2, Left, Right).


merge_list([],[],[]).
merge_list([],Right,Right).
merge_list(Left,[],Left).

merge_list([H1|T1],[H2|T2],[H1|Res]) :-
	H1<H2,
	merge_list(T1,[H2|T2],Res).
merge_list([H1|T1],[H2|T2],[H2|Res]) :-
   	H2=<H1,
   	merge_list([H1|T1],T2,Res).

merge_sort([X],[X]).
merge_sort(List,Sorted) :-
	split_list(List,Left,Right),
	merge_sort(Left,SortedLeft),
	merge_sort(Right,SortedRight),
	merge_list(SortedLeft,SortedRight,Sorted).


max_list([Max],Max).
max_list([H|T],H) :-
	max_list(T,Max),
	H > Max.

max_list([H|T],Max) :-
	max_list(T,Max),
	H =< Max.

remove_first([], _, []).
remove_first([E|T], E, T).
remove_first([H|T], E, [H|Res]) :-
	remove_first(T, E, Res).

select_sort([], []).
select_sort(List, [Max|PrevSorted]) :-
	max_list(List,Max),
	remove_first(List,Max,RemovedList),
	select_sort(RemovedList, PrevSorted), !.


tree_add([],Val,[Val,[],[]]).
tree_add([Val,L,R],Val,[Val,L,R]).
tree_add([H,[],R],Val,[H,[Val,[],[]],R]) :-
	Val<H.
tree_add([H,L,[]],Val,[H,L,[Val,[],[]]]) :-
	H<Val.
tree_add([H,L,R],Val,[H,PrevNode,R]) :-
	Val<H,
	tree_add(L,Val,PrevNode).
tree_add([H,L,R],Val,[H,L,PrevNode]) :-
	H<Val,
	tree_add(R,Val,PrevNode).


tree_build(List,Res) :-
	tree_build(List,[],Res).

tree_build([],Acc,Acc).
tree_build([H|T],Acc,Res) :-
	tree_add(Acc,H,NewTree),
	tree_build(T,NewTree,Res).


list_reverse(List,Res) :-
	list_reverse(List,[],Res).

list_reverse([],Acc,Acc).
list_reverse([H|T],Acc,Res) :-
	list_reverse(T,[H|Acc],Res).


tree_sort(List,Res) :-
	tree_build(List,Tree),
	list_from_tree(Tree,Res), !.

list_from_tree([],[]).
list_from_tree([H,L,R],List) :-
	list_from_tree(L,LeftList),
	list_from_tree(R,RightList),
	append(LeftList,[H|RightList],List).


matrix_first_column([],[]).
matrix_first_column([[H|_]|Tm],[H|PrevColumn]) :-
	matrix_first_column(Tm,PrevColumn).



matrix_remove_first([], []).
matrix_remove_first([H|T], [ModifiedRow|PrevModified]) :-
	matrix_remove_first(T, PrevModified),
	row_remove_first(H, ModifiedRow).


row_remove_first([],[]).
row_remove_first([_|T],T).


matrix_transponse([], []).
matrix_transponse([[]|_], []).
matrix_transponse(Matrix, [FirstCol|Transponed]) :-
	matrix_first_column(Matrix,FirstCol),
	matrix_remove_first(Matrix,RemovedMatrix),
	matrix_transponse(RemovedMatrix, Transponed).






































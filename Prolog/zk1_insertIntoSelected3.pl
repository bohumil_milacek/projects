%fib(2, 1, [1,0]).
%fib(N, X, [F|T]) :-
%	N1 is N-1,
%	N2 is N-2,
%	fib(N1, X1, T),
%	fib(N2, X2, T),
%	X is X1 + X2.

%_______________________________________________
% contains(+Lst, +Elem)
contains([],_) :- false.
contains([X|_], X) :- !.
contains([_|T], X) :-
	contains(T,X).

%_______________________________________________
% not_contains(+Lst, +Elem)
not_contains([], _) :- true.
not_contains([H|T], X) :-
	H\=X,
	not_contains(T,X).

%_______________________________________________
% nth(+Lst, +Elem, -Res)
nth([H|_], 0, H).
nth([_|T], N, RES) :-
	N1 is N - 1,
	nth(T, N1, RES).

%_______________________________________________
%length(+Lst, -Res)
my_length([],0).
my_length([_|T], RES) :-
	length(T,RES1),
	RES is RES1+1.

%_______________________________________________
% lengthWithSublists(+Lst, -Res)
lengthWithSublists([],0).
lengthWithSublists([H|T], RES) :-
	is_list(H),
	lengthWithSublists(H,RES1),
	lengthWithSublists(T,RES2),
	RES is RES1 + RES2,
	!.

lengthWithSublists([_|T], RES) :-
	lengthWithSublists(T,RES1),
	RES is RES1 + 1.
%_______________________________________________

% append(+Lst, +Elem, -Res)
append([], E, [E]).

append([H|T], E, [H|R]) :-
	append(T, E, R).


%_______________________________________________
%prepend(+Lst, +Elem, -Res)
prepend(LIST, E, [E|LIST]).
%_______________________________________________

%delete_first(+Lst, +Elem, -Res)
delete_first([],_,[]).
delete_first([E|T], E, T) :- !.
delete_first([H|T], E, [H|RES1]) :-
	delete_first(T, E, RES1).

delete_last([], _, []).
delete_last(LIST, E, RES2) :-
	reverse(LIST, REVERSED),
	delete_first(REVERSED, E, RES1),
	reverse(RES1,RES2).

%reverse(+List,-Res).
reverse([],[]).
reverse([H|T], REVERS) :-
	reverse(T, RES1),
	append(RES1,H,REVERS).


delete_all([], _, []).
delete_all([E|T], E, RES) :-
	delete_all(T, E, RES).

delete_all([H|T], E, [H|RES]) :-
	delete_all(T, E, RES).

%delete_all([H|T], E, RES) :-


%_______________________________________________
%replace(+Lst, +S, +R, -Res)
replace([], _, _, []).
replace([S|T], S, R, [R|RES]) :-
	replace(T, S, R, RES).

replace([H|T], S, R, [H|RES]) :-
	replace(T, S, R, RES).


%_______________________________________________
%replace(+Lst, +S, +R, -Res)
replace([], _, _, []).
replace([S|T], S, R, [R|RES]) :-
	replace(T, S, R, RES).

replace([H|T], S, R, [H|RES]) :-
	replace(T, S, R, RES).

%_______________________________________________
%last(+Lst, -Res)
last([X],X).
last([_|T], RES) :-
	last(T,RES).

%_______________________________________________
%count(+E, +Lst, -Res)

count(_, [], 0).
count(E, [E|T], RES) :-
	count(E, T, RES1),
	RES is 1 + RES1.

count(E, [_|T], RES) :-
	count(E, T, RES).

%_______________________________________________
%countSub(+E, +Lst, -Res)

countSub(_, [], 0).
countSub(E, [E|T], RES) :-
	countSub(E, T, RES1),
	RES is 1 + RES1.

countSub(E, [H|T], RES) :-
	countSub(E, H, RES1),
	countSub(E, T, RES2),
	RES is RES1 + RES2.

countSub(E, [_|T], RES) :-
	countSub(E, T, RES1),
	RES is 1 + RES1.

countSub(E, [_|T], RES) :-
	countSub(E, T, RES).


%_______________________________________________
%flatten(+Lst, -Res)
flatten([],[]).
flatten([H|T],RES) :-
	is_list(H),
	flatten(H,RES1),
	flatten(T,RES2),
	append_list(RES1,RES2,RES).

flatten([H|T], [H|RES]) :-
	flatten(T,RES).
%_______________________________________________
append_list([],X,X).
append_list([H1|T1],L2,[H1|RES]) :-
	append_list(T1,L2,RES).



%_______________________________________________
zamen([],_,[]).
zamen([E|T], E, [EE|RES]) :-
	EE is E * E,
	zamen(T,E,RES).

zamen([H|T], E, [RES1|RES2]) :-
	is_list(H),
	zamen(H,E,RES1),
	zamen(T,E,RES2).

zamen([H|T], E, [H|RES]) :-
	zamen(T,E,RES).


%_______________________________________________

f(1,5).
f(2,4).
f(N,RES) :-
	N2 is N-2,
	N1 is N-1,
	f(N2, RES2),
	f(N1, RES1),
	RES is RES2 * RES2 - RES1.


fl(N, RES) :-
	fl_h(N,RES1),
	reverse(RES1,RES).

fl_h(0,[]).
fl_h(N,[X|RES]) :-
	N1 is N-1,
	fl_h(N1,RES),
	f(N,X).

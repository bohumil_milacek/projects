%
%
%my_delete([],_,[]).
%my_delete([Del|T],Del,Res) :- my_delete(T,Del,Res), !.
%my_delete([H|T],Del,[H|Res]) :- my_delete(T,Del,Res), !.
%
%
%my_counter([],[]).
%my_counter([H|T],[[H,HCnt]|Res]) :-
%    my_cnt([H|T],H,Cnt),
%    my_delete(T,H,NewT),
%    my_counter(NewT, Res).
%






%----------------------------------------------------------------------
% BST

% bst=[]
% bst=[val, bst-l, bst-r]

%bst_insert(+Bst, +Val, -BST).
bst_insert([],Val,[Val,[],[]]).
bst_insert([BV, BL, BR], Val, [BV, Res, BR]) :-
    Val < BV,
    bst_insert(BL,Val,Res), !.
bst_insert([BV, BL, BR], Val, [BV, Res, BR]) :-
    Val > BV,
    bst_insert(BL,Val,Res).


% bst_find(+BST, +Val)
bst_find([],_) :- fail.
bst_find([BV,_,BR],Val) :-
    Val < BV,
    bst_find(BR,Val), !.
bst_find([BV,BL,_],Val) :-
    Val > BV,
    bst_find(BL,Val), !.
bst_find([Val,_,_],Val).

%my_treesort(+Lst, -Sorted).
my_treesort(Lst,Sorted).
    bst_construct(Lst,BST),
    bst_inorder(Bst,Sorted).

%bst_construct(+Lst, -Bst)
bst_construct([],[]).
bst_construct([H|T], ...) :-
    bst_construct(T,BST) :-
    bst_insert(Bst, H, NBst).

%bst_inorder(+Bst, -Res).
bst_inorder([],[]).
bst_inorder([Val,BL,BR], Res) :-
    bst_inorder(BL,ResL),
    bst_inoirder(BR,ResR),
    bst_append(ResL,[Val,ResR],Res).


%my_append(+L1, +L2, -L)
my_append([],L2, L2).
my_append([H|T],L2,[H,Res]) :-
    my_append(T, L2, Res).



%bst_height(+Bst,-H)
bst_height([Val,BL,BR],H1) :-
    bst_height(BL,HL),
    bst_height(BR,HR),
    my_max(HL,HR,H),
    H is H + 1.

my_max(A,B,B) :- A =< B.
my_max(A,B,A) :- A > B.


%---------------------------------------------------------------------------------------------
%MATICE
% mat = [row1 ... rown] and row_i=[ ... ]

%mat_dim(+Mat,-Rows,-Cols).

mat_ex([[1,2,3],[4,5,6],[7,8,9],[10,11,12]]).

mat_dim([Row1|T],Rows,Cols) :-
	my_length([Row1|T],Rows),


my_length([],0).
my_length([_|T],Ln) :-
	my_length(T,Ln1),
	Ln is Ln1 + 1.

mat_nth_row(Mat, N, Row) :-
	my_nth_row(Mat, N, Row).

mat_nth_col([], _, []) :-
mat_nth_col([Row|T], N, [X|Col]) :-
	my_nth(Row, N, X),
	my_nth_col(T,N,Res).

my_nth([],_,_) :- fail.
my_nth([H|_],0,H) :- !.
my_nth([_|T], N, E) :- N1 is N-1, my_nth(T, N1, E).


%mat_diag(+Mat, -Diag).
mat_diag(Mat, Diag) :-
    mat_dim(Mat,Rows,_),
    mat_diag(Mat, 0, Rows, Cols, Diag).

mat_diag(_, UB, UB, _, []) :- !.
mat_diag(_, Cols, _, Cols, []) :- !.
mat_diag(Mat, I, UB, [E|Res]) :-
	my_nth_row(Mat, I, IthRow),
	my_nth(IthRow, I, E),
	I1 is I +1,
	mat_diag(Mat,I1,UB, Cols, Res).


%mat_add(Mat1,Mat2,Res) :-
list_add([],[],[]).
list_add([H1|Lst1], [H2|Lst2], [Res1,Res]) :-
	Res1 is H1+ H2,
	lst_add(Lst1,Lst2,Res).

mat_add([],[],[]).
mat_add([Row1|Mat1], [Row2|Mat2], [RowRes|Res]):-
	lst_add(Row1,Row2,RowRes),
	mat_add(Mat1,Mat2,Res).











%--------------------------------------------------------------------------------
mergesort([],[]) :-!.
mergesort(Lst, Sorted) :-
	halve(Lst, L1, L2),
	mergesort(L1,S1),
	mergesort(L2,S2),
	my_merge(S1,S2,Sorted), !.

my_merge(L1,[],L1).
my_merge([],L2,L2).
my_merge([H1|T1],[H2,T2],[H1|Res]) :-
	H1 =< H2,
	my_merge(T1, [H2|T2], Res), !.
my_merge([H1|T1],[H2,T2],[H2|Res]) :-
	H1 > H2,
	my_merge([H1|T1], T2, Res), !.


halve(Lst, L1, L2) :- halve1(Lst, L1, L2).
halve1([],[],[]).
halve1([H|T], [H|L1], L2) :- halve2(T,L1,L2).
halve2([],[],[]).
halve2([H|T], L1, [H|L2]) :- halve2(T,L1,L2).
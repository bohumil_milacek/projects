out([]).

out([A|TAIL]):-
    format(A),nl,
    out(TAIL).


append_str(STR1,STR2,STR3) :-
    name(STR1,Str1List),
    name(STR2,Str2List),
    append(Str1List,Str2List,Str3List),
    name(STR3,Str3List).